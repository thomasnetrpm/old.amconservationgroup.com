<?php 
/* 
Template Name: Test template 
*/

define("CSS", "hotbuys.css"); 

if(is_mobile()) {
	get_header('mobile-p-s');
} else {
	get_header('shop-amc');
}
global $amc_urls;
?>
<style type="text/css">
  #programs_services h2 {
    color: #000000;
    font-family: "open sans";
    font-size: 18px;
    font-weight: normal;
    margin: 20px 0 0 25px;
    width: 113%;
}
#programs_services h3 {

   color: #000000;
    font-family: "open sans";
    font-size: 13px;
    font-weight: bold;
    line-height: 24px;
    margin: 30px 0 0 21px;
    width: 90%;

  }
  #programs_services ul li {

  color: #3968B1;
    font-family: "open sans";
    font-size: 13px;
    font-weight: normal;
    line-height: 11px;
    list-style-type: disc !important;
    margin: 17px 0 -5px;
    width: 95%;

	}
	
.pl-btn.btn-primary{
    background: #f4911e;
	height: 42px;
	width:20%;
	}
</style>


<!-- Title and Breadcrumb  -->
      <div class="row headersession">      
        <div class="col-md-7"><h3 class="headerlabel">This Month's Sales<?php // the_title(); ?></h3></div>
        <div class="col-md-5 breadcrumbs inBread">
          <ol class="breadcrumb">
           <?php //breadcrumbs_fmg();  ?>
          </ol>
        </div>    
<?php if(is_mobile()) { ?>
	<div class="col-md-4 back-btn back-page-com"> <a onClick="history.go(-1)"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/back_btn.png">Back </a></div>
<?php } ?>
    </div>
     <!--/  Ends Title and Breadcrumb   -->

<?php
$pageAttachments = wpba_get_attachments();

if(!empty($pageAttachments) && is_array($pageAttachments)) {
?>
<div class="row"><div class="col-md-12 aboutbanner"><img src="<?php echo $pageAttachments[0]->guid; ?>"></div></div>
<?php
}
?>

<div class="row pns-content">      
          <div class="col-md-11 col-md-offset-1 pns-container">
            <div id="programs_services">
              <h1 class="pns-headertxt">Weatherization Products</h1>
              <div id="tps_content">
              <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
                <?php the_content(); ?>
              <?php endwhile; endif; ?>
            </div> 
            </div>


<a class="back_btn" href="<?php echo $amc_urls['hotbuys-url']; ?>"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/back_btn.png"><span class="back-txt">Back</span></a>

          

  
</div> 
      </div> 
<?php
   #Get Api Values 
   $Api_Url = $amc_urls['maxlite_Api_url'];
   # Get The Country Dropdown List #
   $Country_List = file_get_contents($Api_Url.'/utility-rebates/states');
   $Country_List = json_decode($Country_List);
  ?>
  		<form name="rebate_form">
  		<select class="form-field country_list" name="country_list">
		<option value=""> Choose state </option>
  		<?php if($Country_List) {
  			foreach ($Country_List as $key => $value) {
  			?>
  			<option value="<?php echo $value->Id;?>"><?php echo $value->StateName;?></option>
  			<?php 
  				}
  			} ?>
  		</select>


<script type="text/javascript">
var siteURL = '<?php echo $amc_urls['home']; ?>';
var siteAssetURL = '<?php echo get_template_directory_uri(); ?>/assets/';
</script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/mediaquery.js"></script>
<script src="http://www.ajax-cross-origin.com/js/jquery.ajax-cross-origin.min.js"></script>
<!-- <script type="text/javascript" src="jquery.xdomainrequest.min.js"></script> -->

<script type="text/javascript">

$(document).ready(function(){

    /*  var xdr = new XDomainRequest();
       xdr.onload = function() { alert("READY"); };
        xdr.open("GET", "http://developer.maxlite.com/api/utility-rebates/states/AK");
        xdr.send();
*/

    $.support.cors = true;

    $(".country_list").change(function()
     { 

         alert(1);
      
         var url= 'http://www.amconservationgroup.com/test-json';
         var apiurl= 'http://developer.maxlite.com/api/utility-rebates/states/AK/';
        
         alert("Going to External url");
         alert(apiurl);
         

           $.ajax({          
            url: apiurl,
            type: 'POST',
            contentType: 'text/plain',
            dataType: 'jsonp',
            success: function(fdata,status,xhr){
                alert(2);
                alert(fdata);
                console.log(fdata);
             },
             error: function (err, textStatus, errorThrown) {
                console.log("AJAX error in request: " + err.statusText);
                console.log(err);
                alert(textStatus);
                alert(errorThrown);
             } 
          });



    });
});
</script>
<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>