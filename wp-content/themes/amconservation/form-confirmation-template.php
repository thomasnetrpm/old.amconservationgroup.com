<?php 
/* 
Template Name: Form confirmation template 
*/ 
define("CSS", "terms.css");
if(is_mobile()) {
	get_header('mobile');
} else {
	get_header('form-amc');
}
?>

 <!-- Title and Breadcrumb  -->
      <div class="row headersession">      
        <div class="col-md-7"><h3 class="headerlabel"><?php the_title(); ?></h3></div>
        <div class="col-md-5 breadcrumbs ">
          <ol class="breadcrumb">
           <?php breadcrumbs_fmg();  ?>
          </ol>
        </div>    
    </div>
     <!--/  Ends Title and Breadcrumb   -->
    <div class=" row content">
     <div class="col-md-10 col-md-offset-1">

                       <!-- Dynamic  contents comes From the  AdminSide  -->   
           <?php if (have_posts()) : while (have_posts()) : the_post();?>
              <?php the_content(); ?>
              <?php endwhile; endif; ?>
                               
              </div>
            </div>
    </div> 

<script type="text/javascript">
var siteURL = '<?php echo $amc_urls['home']; ?>';
var siteAssetURL = '<?php echo get_template_directory_uri(); ?>/assets/';
</script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/mediaquery.js"></script>

<?php
if(is_mobile()) {
	get_footer('mobile-confirmation');
} else {
	get_footer('confirmation');
}
?>