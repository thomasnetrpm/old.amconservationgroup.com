<?php global $theme_shortname, $theme_options, $amc_urls; ?>
<?php $theme_options = get_option( $theme_shortname . '_options' ); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico">
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;
	wp_title( '|', true, 'right' );
	bloginfo( 'name' );
	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
	echo " | $site_description";
	?></title>

<!-- Bootstrap core CSS -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/flexislider.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/amc_style.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/rwd_styles.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/rwd_styles_mobiles.css" rel="stylesheet">
<!-- Custom styles for this template -->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/html5shiv.js">
  </script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/respond.min.js">
  </script>
  <![endif]-->

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-flexislider.js"></script>
<script src="http://jarallax.com/download/jarallax-0.2.js" type="text/javascript"></script>

<style type="text/css"></style>
</head>

<body>
<div class="container homepg" >
  <div class="row header">
    <div class="col-md-4 col-xs-12 logo"> <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png"></a> </div>
    <!--  <div class="col-md-2"></div> -->
    <div class="col-md-12 col-xs-12 dashboard">
    <?php
// Check for login user
if(userIsLoggedIn()) {
	$loggedInUser = getLoggedInUser();
?>
		<span class="username">Hi <?php echo $loggedInUser->firstname; ?>!</span>
		<span class="userlinks"><a href="<?php echo $amc_urls['my_account']; ?>">My Account </a>|</span>
		<span class="userlinks"><a href="<?php echo $amc_urls['quote_basket']; ?>">Quote basket (<?php echo getQuoteCount(); ?>)</a></span> 
<?php 
} ?>

    </div>
    <div class="col-md-8 col-xs-12">
      <div class="topnav-right">
	<a class="menu1" href="tel:<?php  echo $amc_urls["contact_number"]; ?>" > <span class="top-icon"></span> <span class="toplink1"><?php  echo $amc_urls["contact_number"]; ?></span> </a> 
	<a class="menu2" href="<?php echo $amc_urls['message_us_online']; ?>"> <span class="top-icon2"></span> <span class="toplink2">Message Us Online</span> </a> 
<?php
// Check for login user
if(userIsLoggedIn()) {
	$loggedInUser = getLoggedInUser();
?>
	<a class="menu3" href="<?php echo $amc_urls['logout']; ?>"> <span class="top-icon3"></span> <span class="toplink3">Log Out</span> </a> 
<?php
} else {
?>
	<a class="menu3" href="<?php echo $amc_urls['register_login']; ?>"> <span class="top-icon3"></span> <span class="toplink3">Register/Login</span> </a> 
<?php } ?>

	<a class="collapsableMenu"> <span class="top-icon4"></span> <span class="toplink4">Menu</span> </a>
          </div>
		  <!-- Search Menu @ The Top -->
		 
	<form action="<?php echo site_url(); ?>" method="get" name="headerSearchFrm">
		  <div class="top-search search js_handleSearch">
			<input type="hidden" name="post_type" value="product"  /> 
			<span class="search-arrow"></span>
		  </div>
		  <div class="search-panel">
			<input type="text" name="s" maxlength="100" class="search-form-field js_searchBox" value="<?php echo get_search_query(); ?>" placeholder="TYPE HERE TO SEARCH">
		  </div>
	  </form>
		
  <!-- Ends Search Menu @ The Top -->  
    </div>
  </div>
  <div class="row ">
       
    <div class="mainnav-home">
        <a href="<?php echo $amc_urls['programs_services'];?>" class="col-md-1 col-xs-1 prog-link">
          Programs & Services
        </a>
      <div class="col-md-10 col-xs-10 energymenu">
<?php 
if (function_exists('fmg_menu') && fmg_var('category-menu')) {
	fmg_menu('product_cat');
} else { 
	icore_nav_menu('primary-menu', 'nav sf');
}
?>
      </div>
    </div>
	</div>

   
<div class="row">
    <div class="col-md-12">
      <div class="slider" >
        <div class="flexslider" id="homeslider">
          <ul class="slides">
            <li>
            <div class="text_bg"><p>With our vast offering of over 500 innovative energy and water conservation products and kits, determine which are best for your program.</p> </div><img class="picture_a" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner1_mobile.png" alt="picture1"> </li>
            <li><div class="text_bg"><p>Learn how we will facilitate educational programs providing impactful behavior modification curriculum and delivering energy efficiency kits.</p></div> <img class="picture_a" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner2_mobile.png" alt="picture2"></li>
            <li><div class="text_bg"><p>Discover how comprehensive turnkey fulfillment programs will meet your program’s energy savings needs.</p> </div><img class="picture_a" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner3_mobile.png" alt="picture3"> </li>
            <li><div class="text_bg"><p>Allow us to supply your utility direct install programs with our expansive inventory offering and thorough reporting.</p> </div><img class="picture_a" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner4_mobile.png" alt="picture4"> </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="car-menu">
      <div class="menusession">
         <div class="leftarrow SlideOff"></div>
        <div class="menu-container">
<?php
wp_nav_menu(array(
	'theme_location' => 'middle-menu',
	'link_before' => '',
	'link_after' => '',
	'menu_class' => 'carmenus',
	'container' => false
));
?>
<script type="text/javascript">
jQuery('ul.carmenus li').addClass('menu');
jQuery('ul.carmenus li a').addClass('caro-menulink');
// jQuery('ul.carmenus li:last').removeClass().addClass('menulast');
</script>
        </div>
      </div>
    </div>  
  </div>

  <div class="row">
    <div class="am_conversion">
      <div class="am-container">
        <h2 class="am-header"> AM Conservation Group</h2>
        <span class="dot"></span>
        <p class="yourone-txt">Your one source for energy and water efficiency products, services, and programs of any size 
          with unmatched speed and flexibility. </p>
        <div class="bs-example twinbtn">
          <button type="button" class="btn btn-default home1" onclick="window.location='<?php echo $amc_urls['sc_url']; ?>';"> 
            <span class="amctwin-txt1">Home Owners Click Here </span> 
            <span class="arrowicon1"></span> </button>
            <!-- <span class="homeowner">You will be redirected to our Simply Conserve shopping site where our products are available for purchase.</span> -->
          <button type="button" class="btn btn-default professional" onclick="window.location='<?php echo $amc_urls['home_category_landing']; ?>';">
            <span class="amctwin-txt2">Professionals/Businesses Click Here</span> 
            <span class="arrowicon2"></span> </button>
        </div>
      </div>
    </div>
  </div>
<?php include('woocommerce/request-quote-modal.php'); ?>
<script type="text/javascript">
$("div .products").css("display","none");
$("div .services").css("display","block");
$(".bigclose2").on( "click", function() {
	$(this).parent().css("display","none");
});  
$(".rac-radio1 :radio").click(
	function() {  
		$("div .services").css("display","none");
		$("div .products").css("display","block");        
	}
);
$(".rac-radio2 :radio").click(
	function() {
		$("div .products").css("display","none");
		$("div .services").css("display","block");
});
  </script>
  
  <div class="row "> 
  <div class="col-md-12 parallexbg">
  
  <div class="px-phoneimgwrap">
      <div class="px-phoneimg"> <!--<img class="px-ImageBg" src="<?php echo get_template_directory_uri(); ?>/assets/images/pipefitting_bg.jpg" />-->
      <div class="slider" >
        <div class="flexslider" id="secondary-slider">
          <ul class="slides">
          
            <li>
            
            <div id="px-Program" class="px-BtmBox2">
          		<span>Programs &amp; Services</span>
          		
            <p>AM Conservation Group has years of proven expertise having managed all types and sizes of programs for utility companies, program managers and government agencies throughout the U.S. You can count on us to provide turnkey program solutions to ensure that your program and planning goals are met on time and on budget.</p>

            	<button class="btn btn-warning" onclick="window.location.href = '<?php echo $amc_urls['programs_services']; ?>';">Read More</button>

            </div>
          
        </li>
        <li>
           <div id="px-Program" class="px-BtmBox2"> 
         		<span>Testimonials</span>
<?php
$tResults = $wpdb->get_results('SELECT message AS content, NAME AS author,id,YEAR(created) AS Y  FROM wp_vm_testimonials WHERE (STATUS = "A" AND isinpool = "yes") ORDER BY created DESC LIMIT 1 ', ARRAY_A);
if(!empty($tResults) && is_array($tResults)) {
	foreach($tResults as $k => $tResultsItem) {
?>
<p><?php echo '"'.wp_trim_words($tResultsItem['content'], 20, ' ...').'" - '.$tResultsItem['author']; ?></p>
<?php
	}
} 
?>
            	<button class="btn btn-warning" onclick="window.location.href = '<?php echo $amc_urls['testimonial']; ?>';">Read More</button>

        	</div>
      
         
        
        </li>
            <li>
            <div id="px-Program" class="px-BtmBox2"> 
            	<span>Talk To Us</span> 
             	
            	<p>Our knowledgeable staff is ready to assist you with your product and program needs. Having served some of the largest utility programs in the country, our expertise is unmatched. Call our AM Conservation Group team today at 1. 888. 866. 1624 to learn how we can provide solutions to your specific needs.</p>

            	<button class="btn btn-warning" onclick="window.location.href = '<?php echo $amc_urls['contact_us']; ?>';">Read More</button>

            </div>
          
        </li> 
        
                  
          </ul>
        </div>
      </div>
    </div>
    </div>
    </div>
    </div>
<?php
if(is_mobile()) {
  get_footer('mobile');
} else {
  get_footer();
}
exit();
?>  
  <div class="row">
      <div class="col-md-12  horizontal-line-divider"></div>    
  </div> 
  
  <div class="row socialfooter">
    <div class="col-md-6 social-left">
      <div class="col-md-2">
        <div class="social-container"> <span> 
		<a href="<?php echo $amc_urls['pinterest']; ?>"><span class="p-icon"></span> </a>
		<a href="<?php echo $amc_urls['twitter']; ?>"><span class="t-icon"></span> </a>
		<a href="<?php echo $amc_urls['facebook']; ?>"><span class="f-icon"></span> </a>
		</span> </div>
      </div>
        <div class="row">
      <div class="col-md-12  horizontal-line-divider"></div>    
        </div> 
      <div class="col-md-4 contact-info">
        <div class="phoneno"><a class="phonelink" href="#" onclick="return false;"><span class="bottom-icon1"></span> <span class="toplink4"><?php echo $amc_urls['contact_number']; ?></span></a></div>
        <div class="catalog"><a class="cataloglink" href="<?php echo $amc_urls['request_a_catalog']; ?>"><span class="bottom-icon2"></span> <span class="toplink5">Request a Catalog</span></a></div>
      </div>
    </div>
    <div class="col-md-6 newsletter">
      <div class="form-group">
        <label for="inputEmail1" class="col-md-2 control-label">Sign Up for Newsletter</label>
        <span class="col-md-4">
        <input type="email" class="form-control-nl" id="inputEmail1" placeholder="Email ID">
        <a class="newsletter-btn" href="#"><span></span><span class="arrowbtn"></span></a> </span> </div>
    </div>
  </div>
  <div class="row footerbg">
    <div class="col-md-9">
      <div class="footermenu-container"><a href="#" class="collapsablefooterMenu"> <span class="top-icon4"></span> <span class="toplink4">Menu</span> </a></div>
<?php
wp_nav_menu(array(
	'theme_location' => 'footer-menu',
	'after'  => '',
	'container_class' => '',
	'container_id'    => '',
	'menu_class'      => 'footer-sub-menu1',
	'menu_id'         => ''
));
?>
<script type="text/javascript">
// jQuery('div.menu-primary_footer_menu-container')
jQuery('ul.footer-sub-menu1 li a').removeClass().addClass('submenu-btn');
jQuery('<li> <a class="footer-subBack-btn" href="#">Back</a> </li>').prependTo('ul.footer-sub-menu1');
<?php if(userIsLoggedIn()) { ?>
jQuery('ul.footer-sub-menu1').prepend('<li class="submenu-btn"><a href="<?php echo $amc_urls['image_gallery']; ?>">Image Gallery</a></li>');
<?php } ?>
</script>

    <div class="subnav_web_templates">Copyright <?php echo date("Y"); ?> AM Conservation Group</div>
  </div>
</div>

<script type="text/javascript">
      var signTpl='<input type="text" class="search-form-field" placeholder="TYPE HERE TO SEARCH">'
      $('.mainmenu-icon6').attr("data-content",signTpl)
      $('.mainmenu-icon6').popover({html : true,placement:"left"});
      $('.mainmenu-icon6').on('shown.bs.popover', function () {
          var search_xpos=$(".search").offset().left;
          var pop_wd=$(".popover").offset().left+$(".popover").width();
          var search_coeff=pop_wd-search_xpos;
          //alert("_xpos "+_xpos);
          // console.log(-(search_xpos-27)+" pop_wd "+search_coeff);
          $(".popover").css("left",-(search_xpos)+"px");
          $(".popover-content").css("width",search_xpos+"px");
          $(".arrow").css("left", $(".popover").width()+10+"px");
      })
    </script>
<script type="text/javascript">
// Handle the search click
$('.js_handleSearch').on('click', function() {
	if($('input[name=s].js_searchBox').val() != undefined && $('input[name=s].js_searchBox').val() != '' && ($('input[name=s].js_searchBox').val() != '<?php echo get_search_query(); ?>')) {
		$('form[name=headerSearchFrm]').submit();
	}
});

/*var collapsablefooterMenu = $(".footer-sub-menu1").width();*/
var flag = false;

var sHt = 0;
$(window).load(function () {
    
    var toolTp = 'You will be redirected to our Simply Conserve shopping site where our products are available for purchase.'
    $(".home").attr("data-title", toolTp)
    $(".home").tooltip({
        html: false,
        placement: "bottom"
    });

    $(".collapsableMenu").click(function () {
        $(".mainnav-home").toggle();
    });
    $(".energymenu .col-xs-2").click(function (e) {
        // e.stopPropagation();
        $(this).find("ul").css("left", "0%")
		return false;
    });
    $(".subBack-btn").click(function (e) {
        //console.log($($(this).parent().parent()).attr("class"))
        // e.stopPropagation();
        $($(this).parent().parent()).css("left", "100%");
		return false;
    });

    $(".collapsablefooterMenu").on('click', function () {
        $(".footer-sub-menu1").css("top", "0%");
		return false;
    });

    $(".footer-subBack-btn").on('click', function (e) {
        // e.stopPropagation();
        $(".footer-sub-menu1").css("top", "100%");
		return false;
    });


    $(".menu1").hover(function () {
        $(".top-icon").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat -3.567% 86.273%");
        $(".toplink1").css("color", "#f4911e");
    }, function () {
        $(".top-icon").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat -3.567% 33.273%");
        $(".toplink1").css("color", "#525151");
    });

    $(".menu2").hover(function () {
        $(".top-icon2").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 8.433% 86.273%");
        $(".toplink2").css("color", "#f4911e");
    }, function () {
        $(".top-icon2").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 8.433% 33.273%");
        $(".toplink2").css("color", "#525151");
    });

    $(".menu3").hover(function () {
        $(".top-icon3").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 18.433% 86.273%");
        $(".toplink3").css("color", "#f4911e");
    }, function () {
        $(".top-icon3").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 18.433% 33.273%");
        $(".toplink3").css("color", "#525151");
    });

    $(".search-btn").hover(function () {
        $(".mainmenu-icon6").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 77.433% 90.273%");
    }, function () {
        $(".mainmenu-icon6").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 99.433% 50.273%");
    });

    $(".phonelink").hover(function () {
        $(".bottom-icon1").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 42.433% 88.273%");
        $(".toplink4").css("color", "#f4911e");
    }, function () {
        $(".bottom-icon1").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 31.433% 35.273%");
        $(".toplink4").css("color", "#525151");
    });

    $(".home").hover(function () {

        $(".arrowicon1").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 62.273%");
        $(".btn-default.home").css("background", "#fff");
        $(".btn.btn-default.home:before").css("display", "block")
    }, function () {
        $(".arrowicon1").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 36.273%");
        $(".btn-default.home").css("background", "#525151");
        $(".btn.btn-default.home:before").css("display", "none")
    });

    $(".professional").hover(function () {
        $(".arrowicon2").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 62.273%");
        $(".professional").css("background", "#fff");
    }, function () {
        $(".arrowicon2").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 36.273%");
        $(".professional").css("background", "#525151");
    });

    $(".cataloglink").hover(function () {
        $(".bottom-icon2").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 31.38% 87.273%");
        $(".toplink5").css("color", "#f4911e");
    }, function () {
        $(".bottom-icon2").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 42.433% 34.273%");
        $(".toplink5").css("color", "#525151");
    });

    $(".newsletter-btn").hover(function () {
        $(".arrowbtn").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 92.273%");
    }, function () {
        $(".arrowbtn").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 35.273%");

    });
});




function Parallax() {
    var jarallax = new Jarallax();
    jarallax.addAnimation('#px-Program', [{
        progress: '0%',
        top: '600px'
    }, {
        progress: '100%',
        top: '-350px'
    }])
    jarallax.addAnimation('#px-Testimonial', [{
        progress: '0%',
        top: '1200px'
    }, {
        progress: '100%',
        top: '-350px'
    }])
    jarallax.addAnimation('#px-Talktous', [{
        progress: '0%',
        top: '1800px'
    }, {
        progress: '100%',
        top: '-350px'
    }])
    jarallax.addAnimation('.px-ImageBg', [{
        progress: '0%',
        top: '0px'
    }, {
        progress: '100%',
        top: '-350px'
    }])
}

function parallaxResizeFunction() {
    var mWwid = Math.round((($(window).innerWidth()) - 90) / 3);
    mWwid -= 10;
    $('.px-BtmBox').css('width', mWwid + 'px');
    $('#px-Program').css('left', '40px');
    $('#px-Testimonial').css('left', mWwid + 60 + 'px');
    $('#px-Talktous').css('left', (mWwid * 2) + 80 + 'px');
    Parallax();

}
$(window).resize(function () {
    //initParallax();
});

$(document).ready(function () {

    //initParallax();

    $(".menu-container").scrollLeft();
    $(".leftarrow").click(function () {

        $(this).toggleClass("SlideOff");
        $(this).toggleClass("SlideOnn");
        $(this).parent('.product-heading-container').toggleClass("SlideOnn");
        if ($(this).hasClass("SlideOnn") == true) {
            $(".menu-container").animate({
                scrollLeft: 1000
            }, 1000);;
            //alert("yes");
        } else {
            $(".menu-container").animate({
                scrollLeft: 0
            }, 1000);;
            //alert("no");
        }

    });
});

function alignCarouseltext() {
    var slideHt = $("ul.slides img").height() / 2;
    var textboxHt = $("ul.slides p").height() / 2;
    $("ul.slides p").css("top", (slideHt - textboxHt) + "px");
}

function initParallax() {
    var int = self.setTimeout(function () {
        alignCarouseltext();
        SliderHeight();
    }, 500);

    function SliderHeight() {
        var slHei = $('.flex-viewport img').height();
        $('.slider').css('height', slHei + 'px');
        parallaxResizeFunction();
    }
}

      </script>
</body>
</html>