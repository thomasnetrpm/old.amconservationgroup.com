<div class="wrap">
	<h2>Service</h2>

	<p><a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=service_mng&action=add" title="Add">Add</a></p>

<?php
if(isset($_SESSION['msg'])) {
	if($_SESSION['msg'] == 'deletesuccess') {
		echo '<p style="color:green;">Deleted successfully.</p>';
	}
	if($_SESSION['msg'] == 'activesuccess') {
		echo '<p style="color:green;">Made active successfully.</p>';
	}
	if($_SESSION['msg'] == 'inactivesuccess') {
		echo '<p style="color:green;">Made inactive successfully.</p>';
	}
	unset($_SESSION['msg']);
}
?>

<?php
global $wpdb;
// $wpdb->show_errors();
// $wpdb->print_error();

//Our class extends the Service_List_Table class, so we need to make sure that it's there
if(!class_exists('Service_List_Table')){
	require_once( TEMPLATEPATH . '/classes/serviceClass.php' );
}

$action = (isset($_GET['action'])) ? $_GET['action'] : '';

if($action == 'add') {
	// Handle the add screen
	$formErrorAdd = array();
	$formSuccessAdd = array();
	if(isset($_POST['serviceAddFrm_submit']) && $_POST['serviceAddFrm_submit'] == 'Add') {
		$serviceAddFrm_title = sanitize_text_field($_POST['serviceAddFrm_title']);
		$serviceAddFrm_is_active = sanitize_text_field($_POST['serviceAddFrm_is_active']);

		if(empty($serviceAddFrm_title)) {
			$formErrorAdd[] = 'Please enter the name.';
		}

		if(empty($formErrorAdd)) {
			$wpdb->insert('wp_services', array(
				'created' => current_time('mysql'), 
				'modified' => current_time('mysql'),
				'servicename' => $serviceAddFrm_title,
				'is_active' => ((isset($serviceAddFrm_is_active) && $serviceAddFrm_is_active == 'yes') ? 1 : 0)
			));
			$formSuccessAdd[] = 'Added successfully.';
		}
	}
?>
<h3>Add - Service</h3>
<form name="serviceAddFrm" action="" method="post">
<div>

<?php
if(!empty($formSuccessAdd)) {
	echo '<p style="color:green;">Status follows,</p>';
	echo '<ol>';
	foreach($formSuccessAdd as $formSuccessAddItem) {
		echo '<li>'.$formSuccessAddItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}

if(!empty($formErrorAdd)) {
	echo '<p style="color:red;">The following errors found,</p>';
	echo '<ol>';
	foreach($formErrorAdd as $formErrorAddItem) {
		echo '<li>'.$formErrorAddItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}
?>

<p><label for="serviceAddFrm_title">Name</label><br /> <input type="text" name="serviceAddFrm_title" value="" id="serviceAddFrm_title" /></p>
<p><label for="serviceAddFrm_is_active">Is Active?</label> <input type="checkbox" name="serviceAddFrm_is_active" value="yes" id="serviceAddFrm_is_active" /></p>
<p><input type="submit" name="serviceAddFrm_submit" value="Add" /></p>
</div>
</form>
<?php

} else if($action == 'edit') {
	// Handle the edit screen
	$serviceId = $_GET['id'];
	$serviceRow = $wpdb->get_row('SELECT * FROM wp_services WHERE id = "'.$serviceId.'"');

	$formErrorEdit = array();
	$formSuccessEdit = array();
	if(isset($_POST['serviceEditFrm_submit']) && $_POST['serviceEditFrm_submit'] == 'Edit') {
		$serviceEditFrm_title = sanitize_text_field($_POST['serviceEditFrm_title']);
		$serviceEditFrm_is_active = sanitize_text_field($_POST['serviceEditFrm_is_active']);

		if(empty($serviceEditFrm_title)) {
			$formErrorEdit[] = 'Please enter the name.';
		}

		if(empty($formErrorEdit)) {
			$wpdb->update('wp_services', array(
				'modified' => current_time('mysql'),
				'servicename' => $serviceEditFrm_title,
				'is_active' => ((isset($serviceEditFrm_is_active) && $serviceEditFrm_is_active == 'yes') ? 1 : 0)
			), array(
				'id' => $serviceId
			));
			$formSuccessEdit[] = 'Edit successfully.';
			$serviceRow = $wpdb->get_row('SELECT * FROM wp_services WHERE id = "'.$serviceId.'"');
		}
	}
?>
<h3>Edit - Service</h3>
<form name="serviceEditFrm" action="" method="post">
<div>

<?php
if(!empty($formSuccessEdit)) {
	echo '<p style="color:green;">Status follows,</p>';
	echo '<ol>';
	foreach($formSuccessEdit as $formSuccessEditItem) {
		echo '<li>'.$formSuccessEditItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}

if(!empty($formErrorEdit)) {
	echo '<p style="color:red;">The following errors found,</p>';
	echo '<ol>';
	foreach($formErrorEdit as $formErrorEditItem) {
		echo '<li>'.$formErrorEditItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}
?>

<p><label for="serviceEditFrm_title">Name</label><br /> <input type="text" name="serviceEditFrm_title" value="<?php echo $serviceRow->servicename; ?>" id="serviceEditFrm_title" /></p>
<p><label for="serviceEditFrm_is_active">Is Active?</label> <input type="checkbox" name="serviceEditFrm_is_active" value="yes" id="serviceEditFrm_is_active" <?php echo (($serviceRow->is_active == 1) ? 'checked="checked"' : ''); ?> /></p>
<p><input type="submit" name="serviceEditFrm_submit" value="Edit" /></p>
</div>
</form>
<?php
} else if($action == 'delete') {
	// Handle the delete action
	$serviceId = $_GET['id'];
	$wpdb->delete('wp_services', array(
		'id' => $serviceId
	));

	$_SESSION['msg'] = 'deletesuccess';
	wp_safe_redirect(site_url().'/wp-admin/admin.php?page=service_mng'); exit;
} else if($action == 'makeactive') {
	// Handle the active action
	$serviceId = $_GET['id'];
	$wpdb->update('wp_services', array(
		'is_active' => 1
	), array(
		'id' => $serviceId
	));

	$_SESSION['msg'] = 'activesuccess';
	wp_safe_redirect(site_url().'/wp-admin/admin.php?page=service_mng'); exit;
} else if($action == 'makeinactive') {
	// Handle the inactive action
	$serviceId = $_GET['id'];
	$wpdb->update('wp_services', array(
		'is_active' => 0
	), array(
		'id' => $serviceId
	));

	$_SESSION['msg'] = 'inactivesuccess';
	wp_safe_redirect(site_url().'/wp-admin/admin.php?page=service_mng'); exit;
} else {
	//Prepare Table of elements
	$service_list_table = new Service_List_Table();
	$service_list_table->prepare_items();
	//Table of elements
	$service_list_table->display();
}
?>

</div>