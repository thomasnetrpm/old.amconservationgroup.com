<div class="wrap">
	<h2>Suggest a Site</h2>

	<p><a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=sas_mng&action=add" title="Add">Add</a></p>

	<p><select name="sasCategory">
		<option value="">Filter by category</option>
<?php
global $sas_categories, $sasCatId;
$sasCatId = $_GET['sasCatId'];
foreach($sas_categories as $sas_c_key => $sas_category) {
	$sasSelStr = ($sasCatId == $sas_c_key) ? 'selected' : '';
	echo '<option value="'.$sas_c_key.'" '.$sasSelStr.'>'.$sas_category.'</option>';
}
?>
	</select></p>
<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('select[name=sasCategory]').change(function() {
		var sasCatId = jQuery(this).val();
		if(sasCatId == '') {
			return false;
		}
		window.location = '<?php echo site_url(); ?>/wp-admin/admin.php?page=sas_mng&sasCatId='+sasCatId;
	});
});
</script>

<?php
if(isset($_SESSION['msg'])) {
	if($_SESSION['msg'] == 'deletesuccess') {
		echo '<p style="color:green;">Deleted successfully.</p>';
	}
	if($_SESSION['msg'] == 'activesuccess') {
		echo '<p style="color:green;">Made active successfully.</p>';
	}
	if($_SESSION['msg'] == 'inactivesuccess') {
		echo '<p style="color:green;">Made inactive successfully.</p>';
	}
	unset($_SESSION['msg']);
}
?>

<?php
global $wpdb;
// $wpdb->show_errors();
// $wpdb->print_error();

//Our class extends the SAS_List_Table class, so we need to make sure that it's there
if(!class_exists('SAS_List_Table')){
	require_once( TEMPLATEPATH . '/classes/sasClass.php' );
}

$action = (isset($_GET['action'])) ? $_GET['action'] : '';

if($action == 'add') {
	// Handle the add screen
	$formErrorAdd = array();
	$formSuccessAdd = array();
	if(isset($_POST['sasAddFrm_submit']) && $_POST['sasAddFrm_submit'] == 'Add') {
		$sasAddFrm_title = sanitize_text_field($_POST['sasAddFrm_title']);
		$sasAddFrm_url = sanitize_text_field($_POST['sasAddFrm_url']);
		$sasAddFrm_sas_category_id = sanitize_text_field($_POST['sasAddFrm_sas_category_id']);
		$sasAddFrm_is_active = sanitize_text_field($_POST['sasAddFrm_is_active']);

		if(empty($sasAddFrm_title)) {
			$formErrorAdd[] = 'Please enter the name.';
		}
		if(empty($sasAddFrm_url)) {
			$formErrorAdd[] = 'Please enter the URL.';
		}

		if(empty($formErrorAdd)) {
			$wpdb->insert('wp_suggestsite', array(
				'created' => current_time('mysql'), 
				'modified' => current_time('mysql'),
				'title' => $sasAddFrm_title,
				'url' => $sasAddFrm_url,
				'sas_category_id' => $sasAddFrm_sas_category_id,
				'is_active' => ((isset($sasAddFrm_is_active) && $sasAddFrm_is_active == 'yes') ? 1 : 0)
			));
			$formSuccessAdd[] = 'Added successfully.';
		}
	}
?>
<h3>Add - Suggest a site</h3>
<form name="sasAddFrm" action="" method="post">
<div>

<?php
if(!empty($formSuccessAdd)) {
	echo '<p style="color:green;">Status follows,</p>';
	echo '<ol>';
	foreach($formSuccessAdd as $formSuccessAddItem) {
		echo '<li>'.$formSuccessAddItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}

if(!empty($formErrorAdd)) {
	echo '<p style="color:red;">The following errors found,</p>';
	echo '<ol>';
	foreach($formErrorAdd as $formErrorAddItem) {
		echo '<li>'.$formErrorAddItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}
?>

<p><label for="sasAddFrm_title">Name</label><br /> <input type="text" name="sasAddFrm_title" value="" id="sasAddFrm_title" /></p>
<p><label for="sasAddFrm_url">URL</label><br /> <input type="text" name="sasAddFrm_url" value="" id="sasAddFrm_url" /></p>

<p><label for="sasAddFrm_sas_category_id">Category</label><br />
<select name="sasAddFrm_sas_category_id" id="sasAddFrm_sas_category_id">
<?php
global $sas_categories;
foreach($sas_categories as $sas_c_key => $sas_category) {
	echo '<option value="'.$sas_c_key.'">'.$sas_category.'</option>';
}
?>
</select>
</p>

<p><label for="sasAddFrm_is_active">Is Active?</label> <input type="checkbox" name="sasAddFrm_is_active" value="yes" id="sasAddFrm_is_active" /></p>
<p><input type="submit" name="sasAddFrm_submit" value="Add" /></p>

</div>
</form>
<?php

} else if($action == 'edit') {
	// Handle the edit screen
	$sasId = $_GET['id'];
	$sasRow = $wpdb->get_row('SELECT * FROM wp_suggestsite WHERE id = "'.$sasId.'"');

	$formErrorEdit = array();
	$formSuccessEdit = array();
	if(isset($_POST['sasEditFrm_submit']) && $_POST['sasEditFrm_submit'] == 'Edit') {
		$sasEditFrm_title = sanitize_text_field($_POST['sasEditFrm_title']);
		$sasEditFrm_url = sanitize_text_field($_POST['sasEditFrm_url']);
		$sasEditFrm_sas_category_id = sanitize_text_field($_POST['sasEditFrm_sas_category_id']);
		$sasEditFrm_is_active = sanitize_text_field($_POST['sasEditFrm_is_active']);

		if(empty($sasEditFrm_title)) {
			$formErrorEdit[] = 'Please enter the name.';
		}
		if(empty($sasEditFrm_url)) {
			$formErrorEdit[] = 'Please enter the URL.';
		}

		if(empty($formErrorEdit)) {
			$wpdb->update('wp_suggestsite', array(
				'modified' => current_time('mysql'),
				'title' => $sasEditFrm_title,
				'url' => $sasEditFrm_url,
				'sas_category_id' => $sasEditFrm_sas_category_id,
				'is_active' => ((isset($sasEditFrm_is_active) && $sasEditFrm_is_active == 'yes') ? 1 : 0)
			), array(
				'id' => $sasId
			));
			$formSuccessEdit[] = 'Edit successfully.';
			$sasRow = $wpdb->get_row('SELECT * FROM wp_suggestsite WHERE id = "'.$sasId.'"');
		}
	}
?>
<h3>Edit - Suggest a site</h3>
<form name="sasEditFrm" action="" method="post">
<div>

<?php
if(!empty($formSuccessEdit)) {
	echo '<p style="color:green;">Status follows,</p>';
	echo '<ol>';
	foreach($formSuccessEdit as $formSuccessEditItem) {
		echo '<li>'.$formSuccessEditItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}

if(!empty($formErrorEdit)) {
	echo '<p style="color:red;">The following errors found,</p>';
	echo '<ol>';
	foreach($formErrorEdit as $formErrorEditItem) {
		echo '<li>'.$formErrorEditItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}
?>

<p><label for="sasEditFrm_title">Name</label><br /> <input type="text" name="sasEditFrm_title" value="<?php echo $sasRow->title; ?>" id="sasEditFrm_title" /></p>
<p><label for="sasEditFrm_url">URL</label><br /> <input type="text" name="sasEditFrm_url" value="<?php echo $sasRow->url; ?>" id="sasEditFrm_url" /></p>

<p><label for="sasEditFrm_sas_category_id">Category</label><br />
<select name="sasEditFrm_sas_category_id" id="sasEditFrm_sas_category_id">
<?php
global $sas_categories;
foreach($sas_categories as $sas_c_key => $sas_category) {
	$sasSelStr = ($sasRow->sas_category_id == $sas_c_key) ? 'selected' : '';
	echo '<option value="'.$sas_c_key.'" '.$sasSelStr.'>'.$sas_category.'</option>';
}
?>
</select>
</p>

<p><label for="sasEditFrm_is_active">Is Active?</label> <input type="checkbox" name="sasEditFrm_is_active" value="yes" id="sasEditFrm_is_active" <?php echo (($sasRow->is_active == 1) ? 'checked="checked"' : ''); ?> /></p>
<p><input type="submit" name="sasEditFrm_submit" value="Edit" /></p>
</div>
</form>
<?php
} else if($action == 'delete') {
	// Handle the delete action
	$sasId = $_GET['id'];
	$wpdb->delete('wp_suggestsite', array(
		'id' => $sasId
	));

	$_SESSION['msg'] = 'deletesuccess';
	wp_safe_redirect(site_url().'/wp-admin/admin.php?page=sas_mng'); exit;
} else if($action == 'makeactive') {
	// Handle the active action
	$sasId = $_GET['id'];
	$wpdb->update('wp_suggestsite', array(
		'is_active' => 1
	), array(
		'id' => $sasId
	));

	$_SESSION['msg'] = 'activesuccess';
	wp_safe_redirect(site_url().'/wp-admin/admin.php?page=sas_mng'); exit;
} else if($action == 'makeinactive') {
	// Handle the inactive action
	$sasId = $_GET['id'];
	$wpdb->update('wp_suggestsite', array(
		'is_active' => 0
	), array(
		'id' => $sasId
	));

	$_SESSION['msg'] = 'inactivesuccess';
	wp_safe_redirect(site_url().'/wp-admin/admin.php?page=sas_mng'); exit;
} else {
	//Prepare Table of elements
	$SAS_List_Table = new SAS_List_Table();
	$SAS_List_Table->prepare_items();
	//Table of elements
	$SAS_List_Table->display();
}
?>

</div>