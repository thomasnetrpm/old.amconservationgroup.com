<?php

/**
 * iCore custom helper functions
 * 
 *
 * @since	iCore 2.0
 */


// Get theme options
global $theme_options, $theme_shortname;
$theme_options = get_option( $theme_shortname . '_options' ); 

// Get Category ID by it's name
function get_catId($cat_name)
{
	global $wpdb;
	$cat_name_id = $wpdb->get_var("SELECT term_id FROM $wpdb->terms WHERE name = '".$cat_name."'");
	return $cat_name_id;
}

// Get Page ID by it's name
function get_pageId($page_name)
{
	global $wpdb;
	$page_name_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_title = '".$page_name."' AND post_type = 'page'");
	return $page_name_id;
}

// Get Page Name by it's ID
function get_pagename($page_id)
{
	global $wpdb;
	$page_name = $wpdb->get_var("SELECT post_title FROM $wpdb->posts WHERE ID = '".$page_id."' AND post_type = 'page'");
	return $page_name;
}

// Get Category Name by it's ID
function get_categname($cat_id)
{
	global $wpdb;
	$cat_name = $wpdb->get_var("SELECT name FROM $wpdb->terms WHERE term_id = '".$cat_id."'");
	return $cat_name;
}


// Truncate Post Content
function truncate_post($limit, $html, $break=".", $pad="...") {

	$string = get_the_content();
	$string = apply_filters('the_content', $string);

if($html == 0) {	
	$string = preg_replace('@<script[^>]*?>.*?</script>@si', '', $string);
	$string = preg_replace('@<style[^>]*?>.*?</style>@si', '', $string);
	$tags = "<p>,<ol>,<ul>,<li>,<h1>,<h2>,<h3>,<h3>,<a>,<href>,<span>";
	$string = strip_tags($string); } 
		else 
	{	
		$string = preg_replace('@<script[^>]*?>.*?</script>@si', '', $string);
		$string = preg_replace('@<style[^>]*?>.*?</style>@si', '', $string);
		$tags = "";
		$string = strip_tags($string, $tags); }
	if(strlen($string) <= $limit) { echo $string;  }
	if(strlen($string) >= $limit) {  
		$string = substr($string, 0, $limit);
	   	   echo $string . $pad; 
	}
}


// Get custom field value
function icore_get_meta ($key, $echo = FALSE) {

    global $post;
    $custom_field = get_post_meta($post->ID, $key, true);
    return $custom_field;
}

// Get values of multiple custom fields
function icore_get_multimeta ($key) {

    foreach($key as $value) {
        global $post;
        
        if($value == 'Align') {
            
            $custom_field = get_post_meta($post->ID, $value, true);
            if($custom_field == '') $result[$value] = 'c';
            if($custom_field == 'center') $result[$value] = 'c';
            if($custom_field == 'top') $result[$value] = 't';
            if($custom_field == 'bottom') $result[$value] = 'b';
            if($custom_field == 'left') $result[$value] = 'l';
            if($custom_field == 'right') $result[$value] = 'r';           
        }
        else 
        {
        $custom_field = get_post_meta($post->ID, $value, true);
        $result[$value] =  $custom_field; }
    }
    return $result;
}

add_filter( 'woocommerce_page_title', 'woo_shop_page_title');

function woo_shop_page_title( $page_title ) {
	if( 'Hot Buys' == $page_title) {
		return "This Month's Sales";
	} else {
		return $page_title;
	}
}

// Get current location
function icore_get_location() {

	$location = 'index';
	
	if ( is_front_page() ) {
		// Front Page
		$location = 'front-page';
	} else if ( is_date() ) {
		// Date Archive Index
		$location = 'date';
	} else if ( is_author() ) {
		// Author Archive Index
		$location = 'author';
	} else if ( is_category() ) {
		// Category Archive Index
		$location = 'category';
	} else if ( is_tag() ) {
		// Tag Archive Index
		$location = 'tag';
	} else if ( is_tax() ) {
		// Taxonomy Archive Index
		$location = 'taxonomy';
	} else if ( is_archive() ) {
		// Archive Index
		$location = 'archive';
	} else if ( is_search() ) {
		// Search Results Page
		$location = 'search';
	} else if ( is_404() ) {
		// Error 404 Page
		$location = '404';
	} else if ( is_attachment() ) {
		// Attachment Page
		$location = 'attachment';
	} else if ( is_single() ) {
		// Single Blog Post
		$location = 'single';
	} else if ( is_page() ) {
		// Static Page
		$location = 'page';
	} else if ( is_home() ) {
		// Blog Posts Index
		$location = 'home';
	}
	
	return $location;
}



function thumb($width, $height, $align) {
    $meta = icore_get_multimeta(array('Thumbnail','Height', 'Width', 'Video', 'Align'));   
}


// Truncate Post Title
function icore_cut_title($amount) {
    
    $thetitle = get_the_title();
    $getlength = mb_strlen($thetitle, 'UTF-8');
    echo mb_substr($thetitle, 0, $amount, 'UTF-8');
    if ($getlength > $amount) echo "...";
}

// Truncate Title
function icore_title($amount='') { 
    
    if($amount !=='') { 
    icore_cut_title($amount); } 
    else 
    { the_title(); }
}

function icore_nav_menu($location = 'primary-menu', $menuClass = 'nav sf', $description = 'desc_off')
   { 
	
       $args = array(
									  'orderby' => 'name',
									  'order' => 'ASC',
									  'taxonomy'  => 'product_cat'
									  );
                        $categories = get_categories($args);
                        $menu = wp_nav_menu(array('theme_location' => $location, 'container' => '', 'fallback_cb' => '', 'menu_class' => $menuClass, 'echo' => false ));
						echo $menu; 
   }
// Custom menu Function Create for The Category Display  //    
     function fmg_menu($type="category") 
	   {
	   global $amc_urls;
		if ($type == 'product_cat' || $type == 'category') {

            $item_menu = array("energy-efficient-lighting","energy-saving-products","water-conservation","weatherization-products","conservation-kits");

         // get The Parent Prodcuct Category  // 	    
         		
            $catTerms = get_terms($type, array('hide_empty' => 0,'orderby'=> 'none', 'parent'=>0,'hierarchical'  => true));
            $i=1;
            $j=1;
			foreach($catTerms as $key=> $catTerm) :
             if($catTerm->term_id < 3800) {		
if(is_mobile()) {
				// Custom code for Menu  //
             if($i == 1) { $menu_tag= ""; } else { $menu_tag= $i;}

			echo '<div class="col-md-2 col-xs-2 mainmenu'.$j.' select">';
		
			 echo '<a href="'.esc_url(get_term_link( $catTerm->slug, $type)).'">
                   <div class="mainmenu-icon'.$menu_tag.'"></div>
                   <div class="mainmenulink">'.$catTerm->name;
			  echo '</div></a>';
			  
			  echo  ' <ul class="sub-menu'.$j.'">';
              	echo '<li><a class="midmenu-Back-btn" href="#">Back</a></li>';
               $catSubTerms = get_terms($type, array('hide_empty' => 0, 'parent'=>$catTerm->term_id,'hierarchical'  => true));
                  foreach($catSubTerms as $catTermSub) :
                  //print_r($catTermSub);
                  	        ##echo '<li><a class="submenu-btn" href="#">'.$catTermSub->name.'</a></li>';
			              echo '<li><a class="submenu-btn" href="'.esc_url(get_term_link( $catTermSub->slug, $type)).'">'.$catTermSub->name.'</a></li>';
			       endforeach;     

              echo '</ul>';
			  echo "</div>";
} else {
		// Custom code for Menu  //
		 // if($i == 1) { $menu_tag= ""; } else { $menu_tag= $i;}

	  echo '<li class="top-li-'.$i.'"><a href="'.esc_url(get_term_link( $catTerm->slug, $type)).'" class="top-menu'.$i.'"><span>'.$catTerm->name.'</span></a>';
	  echo  '<ul>';
	  
	   $catSubTerms = get_terms($type, array('hide_empty' => 0, 'parent'=>$catTerm->term_id,'hierarchical'  => true));
		  foreach($catSubTerms as $catTermSub) :
		  //print_r($catTermSub);
					##echo '<li><a class="submenu-btn" href="#">'.$catTermSub->name.'</a></li>';
				if($catTermSub->term_id == 3078){
				 echo '<li><a class="submenu-btn" href="'.$amc_urls['hotbuys-energy-efficient-lighting'].'">'.$catTermSub->name.'</a></li>';
				} else if($catTermSub->term_id == 3079) {
				echo '<li><a class="submenu-btn" href="'.$amc_urls['hotbuys-energy-saving-devices-products'].'">'.$catTermSub->name.'</a></li>';	
				} else if($catTermSub->term_id == 3080) {
				  echo '<li><a class="submenu-btn" href="'.$amc_urls['hotbuys-water-conversation-products'].'">'.$catTermSub->name.'</a></li>';
				}else if($catTermSub->term_id == 3081) {
				echo '<li><a class="submenu-btn" href="'.$amc_urls['hotbuys-weatherization-products'].'">'.$catTermSub->name.'</a></li>';
				} else {		
				  echo '<li><a class="submenu-btn" href="'.esc_url(get_term_link( $catTermSub->slug, $type)).'">'.$catTermSub->name.'</a></li>';
				}
		   endforeach;     

	  echo '</ul>';
	  echo "</li>";
}
			  $i++;
			  $j++;
			}
			endforeach;
		 }
	   }
  
// Print Nav Menu
  /*function icore_nav_menu($location = 'primary-menu', $menuClass = 'nav sf', $description = 'desc_off' ) {   
    global $theme_shortname;
	
    if (function_exists('wp_nav_menu') && $description == 'desc_off') { 
        $menu = wp_nav_menu(array('theme_location' => $location, 'container' => '', 'fallback_cb' => '', 'menu_class' => $menuClass, 'echo' => false ));
    } else { 
		if (function_exists('wp_nav_menu') && $description == 'desc_on') { 
                    $menu = wp_nav_menu(array('theme_location' => $location, 'container' => '', 'fallback_cb' => '', 'menu_class' => $menuClass, 'echo' => false, 'walker' => new icore_menudesc() ));
                            } }

                if($menu != '') { 
                    echo $menu; 
				} else { ?>
                    <ul class="<?php echo $menuClass; ?>">
                        <li>
                    
                    <?php echo '<a href="' . home_url() . '/wp-admin/nav-menus.php">Create menu</a>'; ?>
                     </li>
                     </ul>
                     <?php
                } 
} 
*/

// Print search bar
function icore_search_bar() {  
    global $theme_options;
    if ( isset($theme_options['search']) && $theme_options['search'] == '1' ) { ?>
    	<div id="searchbar">
            <?php get_search_form(); ?>
        </div>
<?php 
    } 
}

// Print logo
function icore_logo() {
	global $theme_options;
	    
    if( isset( $theme_options['logo'] ) && $logo = $theme_options['logo']) { 

        $logo = "<img class='site-title' src=".$logo." alt='' />";
        $logo = "<h1 id='site-title'><a href='".home_url('/')."'>".$logo."</a></h1>";
        
    } else { 

        $logo = get_bloginfo('name'); 
        $logo = "<h1 id='site-title'><a href='".home_url('/')."'>".$logo."</a></h1>";
            
    }
 
   return $logo; 
}


// Print Custom CSS
add_action( 'wp_head', 'icore_custom_css' );

function icore_custom_css() {
	global $theme_options;
	 
	if( isset( $theme_options['custom_css'] ) && $theme_options['custom_css'] <> '') {
		echo '<style type="text/css">';
		echo sanitize_text_field( $theme_options['custom_css'] );
		echo '</style>';
	 }
}



// Print Custom Primary Color
add_action( 'wp_footer', 'icore_primary_color' );

function icore_primary_color() {
	global $theme_options;
	 
	if( isset( $theme_options['primary_color'] ) && $theme_options['primary_color'] <> '') {
		echo '<style type="text/css">';
		echo '.primary-color {background-color:'.$theme_options['primary_color'].' }';
		echo '</style>';
	 }
}


// Print Custom Header/Footer Link Color
add_action( 'wp_footer', 'icore_header_link_color' );

function icore_header_link_color() {
	global $theme_options;
	 
	if( isset( $theme_options['header_link_color'] ) && $theme_options['header_link_color'] <> '') {
		echo '<style type="text/css">';
		echo 'h1.logo a, ul.top a, #footer-widgets h4.widget-title {color:'.$theme_options['header_link_color'].' }';
		echo '</style>';
	 }
}

// Print Custom Header/Footer Text Color
add_action( 'wp_footer', 'icore_header_text_color' );

function icore_header_text_color() {
	global $theme_options;
	 
	if( isset( $theme_options['header_text_color'] ) && $theme_options['header_text_color'] <> '') {
		echo '<style type="text/css">';
		echo '#tagline, span.menu-desc, #footer-widgets {color:'.$theme_options['header_text_color'].' }';
		echo '</style>';
	 }
}


// Print Custom Secondary Color
add_action( 'wp_footer', 'icore_secondary_color');

function icore_secondary_color() {
	global $theme_options;
	 
	if( isset( $theme_options['secondary_color'] ) && $theme_options['secondary_color'] <> '') {
		echo '<style type="text/css">';
		echo '
		.woocommerce-pagination .page-numbers li a, .woocommerce-pagination .page-numbers li span.current  { background-color:'.$theme_options['secondary_color'].' }
		.widget_price_filter .ui-slider .ui-slider-handle { background-color:'.$theme_options['secondary_color'].' }
		.secondary-color { background-color:'.$theme_options['secondary_color'].' }
		.blurb a.readmore, a.comment-reply-link, #respond input[type="submit"] /* input[type="button"] */, .widget_price_filter button, .shop_table input[type="submit"], input[type="submit"], .nav-next a, .nav-previous a, .pagination-prev a, .pagination-next a, span.onsale { background: '.$theme_options['secondary_color'].'}';
		echo '</style>';
	 }
}

// Print Custom Navigation link colors
add_action( 'wp_footer', 'icore_nav_color' );

function icore_nav_color() {
	global $theme_options;
	
	if( isset( $theme_options['nav_bg_color'] ) && $theme_options['nav_bg_color'] <> '') {
		echo '<style type="text/css">';
		echo 'ul.nav li {background-color:'.$theme_options['nav_bg_color'].' }';
		echo '</style>';
	 }
	
	if( isset( $theme_options['nav_color'] ) && $theme_options['nav_color'] <> '') {
		echo '<style type="text/css">';
		echo 'ul.nav li a, #cart-menu-inner a.menu-link {color:'.$theme_options['nav_color'].' }';
		echo '</style>';
	 }
	
	if( isset( $theme_options['nav_hover_color'] ) && $theme_options['nav_hover_color'] <> '') {
		echo '<style type="text/css">';
		echo 'ul.nav li a:hover, #cart-menu-inner a.menu-link:hover {color:'.$theme_options['nav_hover_color'].' }';
		echo '</style>';
	 }
	
	if( isset( $theme_options['nav_active_color'] ) && $theme_options['nav_active_color'] <> '') {
		echo '<style type="text/css">';
		echo 'ul.nav li.current-menu-item a {color:'.$theme_options['nav_active_color'].' }';
		echo '</style>';
	 }
	
}


// Print Google Analytics Code
add_action( 'wp_head', 'icore_ga_code' );

function icore_ga_code() {
	global $theme_options;
	
	if( isset( $theme_options['google_analytics'] ) && $theme_options['google_analytics'] <> '' ) echo $theme_options['google_analytics'];	
}

    


// Print Favicon
add_action( 'wp_head', 'icore_favicon' );

function icore_favicon() {
	global $theme_options;
	
	if ( isset( $theme_options['favicon'] ) && '' != $theme_options['favicon'] ) : ?>
	<link rel="shortcut icon" href="<?php echo esc_url( $theme_options['favicon'] ); ?>" />
<?php endif; 
}

// Add term page
function cat_product_dec_taxonomy_add_new_meta_field() {
	// this will add the custom meta field to the add new term page
	?>
	<div class="form-field">
	<label for="term_meta[cat_product_dec_meta]"><?php _e( 'Product Description', 'product_cat' ); ?></label>
		<textarea rows="10" cols="60" name="term_meta[cat_product_dec_meta]" id="term_meta[cat_product_dec_meta]" class="at-textarea large-text">
		</textarea>	
	<label for="term_meta[cat_product_dec_meta]"><?php _e( 'Button name for Product', 'btn_name' ); ?></label>
		<input type="text" name="term_meta[prod_btn_name]" size="30" id="term_meta[prod_btn_name]">
	<label for="term_meta[cat_product_dec_meta]"><?php _e( 'Product URL', 'prod_url' ); ?></label>
		<input type="text" name="term_meta[prod_url]" size="60" id="term_meta[prod_url]">
	<label for="term_meta[cat_product_dec_meta]"><?php _e( 'Button1 name for Category', 'btn_name_category1' ); ?></label>
		<input type="text" name="term_meta[btn_name_category1]" size="30" id="term_meta[btn_name_category1]">	
	<label for="term_meta[cat_product_dec_meta]"><?php _e( 'Button2 name for Category', 'btn_name_category2' ); ?></label>
		<input type="text" name="term_meta[btn_name_category2]" size="30" id="term_meta[btn_name_category2]">	
	<label for="term_meta[cat_product_dec_meta]"><?php _e( 'Category URL1', 'url1' ); ?></label>
		<input type="text" name="term_meta[cat_product_url1]" size="60" id="term_meta[cat_product_url1]">
	<label for="term_meta[cat_product_dec_meta]"><?php _e( 'Category URL2', 'url2' ); ?></label>
		<input type="text" name="term_meta[cat_product_url2]" size="60" id="term_meta[cat_product_url2]">
	</div>
<?php
}
add_action( 'product_cat_add_form_fields', 'cat_product_dec_taxonomy_add_new_meta_field', 12, 2 );


// Edit term page
function cat_product_dec_taxonomy_edit_meta_field($term) {
 
	// put the term ID into a variable
	$t_id = $term->term_id;
 
	// retrieve the existing value(s) for this meta field. This returns an array
	$term_meta = get_option( "taxonomy_$t_id" ); ?>
	<div class="form-fields">
	<div scope="row" valign="top"><label for="term_meta[custom_term_meta]"><?php _e( 'Product Description', 'product_cat' ); ?></label></div>
		<div>
		<textarea rows="10" cols="60" name="term_meta[cat_product_dec_meta]" id="term_meta[cat_product_dec_meta]" class="at-textarea large-text"><?php echo esc_attr( $term_meta['cat_product_dec_meta'] ) ? esc_attr( $term_meta['cat_product_dec_meta'] ) : ''; ?></textarea>			
		</div>
	<div scope="row" valign="top"><label for="term_meta[custom_term_meta]"><?php _e( 'Button name for product', 'btn_name' ); ?></label></div>
		<div>
		<input type="text" name="term_meta[btn_name]" size="60" id="term_meta[btn_name]" value="<?php echo esc_attr( $term_meta['btn_name'] ) ? esc_attr( $term_meta['btn_name'] ) : ''; ?>">			
		</div>
	<div scope="row" valign="top"><label for="term_meta[custom_term_meta]"><?php _e( 'Product URL', 'prod_url' ); ?></label></div>
		<div>
		<input type="text" name="term_meta[prod_url]" size="60" id="term_meta[prod_url]" value="<?php echo esc_attr( $term_meta['prod_url'] ) ? esc_attr( $term_meta['prod_url'] ) : ''; ?>">			
		</div>
	<div scope="row" valign="top"><label for="term_meta[custom_term_meta]"><?php _e( 'Button1 name for Category', 'btn_name_category1' ); ?></label></div>
		<div>
		<input type="text" name="term_meta[btn_name_category1]" size="60" id="term_meta[btn_name_category1]" value="<?php echo esc_attr( $term_meta['btn_name_category1'] ) ? esc_attr( $term_meta['btn_name_category1'] ) : ''; ?>">			
		</div>
	<div scope="row" valign="top"><label for="term_meta[custom_term_meta]"><?php _e( 'Button2 name for Category', 'btn_name_category2' ); ?></label></div>
		<div>
		<input type="text" name="term_meta[btn_name_category2]" size="60" id="term_meta[btn_name_category2]" value="<?php echo esc_attr( $term_meta['btn_name_category2'] ) ? esc_attr( $term_meta['btn_name_category2'] ) : ''; ?>">			
		</div>
	<div scope="row" valign="top"><label for="term_meta[custom_term_meta]"><?php _e( 'Category URL1', 'url1' ); ?></label></div>
		<div>
		<input type="text" name="term_meta[cat_product_url1]" size="60" id="term_meta[cat_product_url1]" value="<?php echo esc_attr( $term_meta['cat_product_url1'] ) ? esc_attr( $term_meta['cat_product_url1'] ) : ''; ?>">			
		</div>
	<div scope="row" valign="top"><label for="term_meta[custom_term_meta]"><?php _e( 'Category URL2', 'url2' ); ?></label></div>
		<div>
		<input type="text" name="term_meta[cat_product_url2]" size="60" id="term_meta[cat_product_url2]" value="<?php echo esc_attr( $term_meta['cat_product_url2'] ) ? esc_attr( $term_meta['cat_product_url2'] ) : ''; ?>">			
		</div>	
	</div>
<?php
}
add_action( 'product_cat_edit_form_fields', 'cat_product_dec_taxonomy_edit_meta_field', 12, 2 );

// Save extra taxonomy fields callback function.
function save_taxonomy_custom_meta( $term_id ) {
	if ( isset( $_POST['term_meta'] ) ) {
		$t_id = $term_id;
		$term_meta = get_option( "taxonomy_$t_id" );
		$cat_keys = array_keys( $_POST['term_meta'] );
		foreach ( $cat_keys as $key ) {
			if ( isset ( $_POST['term_meta'][$key] ) ) {
				$term_meta[$key] = $_POST['term_meta'][$key];
			}
		}
		// Save the option array.
		update_option( "taxonomy_$t_id", $term_meta );
	}
}  
add_action( 'edited_product_cat', 'save_taxonomy_custom_meta', 12, 2 );  
add_action( 'create_product_cat', 'save_taxonomy_custom_meta', 12, 2 );
?>