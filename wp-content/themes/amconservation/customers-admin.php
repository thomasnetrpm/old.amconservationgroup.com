<div class="wrap">
	<h2>Customers</h2>

<?php
/*
	<p><a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=customer_mng&action=add" title="Add">Add</a></p>
*/
?>

<?php
if(isset($_SESSION['msg'])) {
	if($_SESSION['msg'] == 'deletesuccess') {
		echo '<p style="color:green;">Deleted successfully.</p>';
	}
	if($_SESSION['msg'] == 'activesuccess') {
		echo '<p style="color:green;">Made active successfully.</p>';
	}
	if($_SESSION['msg'] == 'inactivesuccess') {
		echo '<p style="color:green;">Made inactive successfully.</p>';
	}
	unset($_SESSION['msg']);
}
?>

<?php
global $wpdb;
// $wpdb->show_errors();
// $wpdb->print_error();

//Our class extends the MUO_List_Table class, so we need to make sure that it's there
if(!class_exists('customers_List_Table')){
	require_once( TEMPLATEPATH . '/classes/customersClass.php' );
}

$action = (isset($_GET['action'])) ? $_GET['action'] : '';

if($action == 'add') {
	// Handle the add screen
	$formErrorAdd = array();
	$formSuccessAdd = array();
	if(isset($_POST['acctrepAddFrm_submit']) && $_POST['acctrepAddFrm_submit'] == 'Add') {
		$acctrepAddFrm_title = sanitize_text_field($_POST['acctrepAddFrm_title']);
		$acctrepAddFrm_is_active = sanitize_text_field($_POST['acctrepAddFrm_is_active']);

		if(empty($acctrepAddFrm_title)) {
			$formErrorAdd[] = 'Please enter the name.';
		}

		if(empty($formErrorAdd)) {
			$wpdb->insert('wp_customers', array(
				'created' => current_time('mysql'), 
				'modified' => current_time('mysql'),
				'acctrepname' => $acctrepAddFrm_title,
				'is_active' => ((isset($acctrepAddFrm_is_active) && $acctrepAddFrm_is_active == 'yes') ? 1 : 0)
			));
			$formSuccessAdd[] = 'Added successfully.';
		}
	}
?>
<h3>Add - Customer</h3>
<form name="acctrepAddFrm" action="" method="post">
<div>

<?php
if(!empty($formSuccessAdd)) {
	echo '<p style="color:green;">Status follows,</p>';
	echo '<ol>';
	foreach($formSuccessAdd as $formSuccessAddItem) {
		echo '<li>'.$formSuccessAddItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}

if(!empty($formErrorAdd)) {
	echo '<p style="color:red;">The following errors found,</p>';
	echo '<ol>';
	foreach($formErrorAdd as $formErrorAddItem) {
		echo '<li>'.$formErrorAddItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}
?>

<p><label for="acctrepAddFrm_title">Name</label><br /> <input type="text" name="acctrepAddFrm_title" value="" id="acctrepAddFrm_title" /></p>
<p><label for="acctrepAddFrm_is_active">Is Active?</label> <input type="checkbox" name="acctrepAddFrm_is_active" value="yes" id="acctrepAddFrm_is_active" /></p>
<p><input type="submit" name="acctrepAddFrm_submit" value="Add" /></p>
</div>
</form>
<?php

} else if($action == 'edit') {
	// Handle the edit screen
	$customerId = $_GET['id'];
	$acctrepRow = $wpdb->get_row('SELECT * FROM wp_customers WHERE id = "'.$customerId.'"');

	$formErrorEdit = array();
	$formSuccessEdit = array();
	if(isset($_POST['acctrepEditFrm_submit']) && $_POST['acctrepEditFrm_submit'] == 'Edit') {
		$acctrepEditFrm_title = sanitize_text_field($_POST['acctrepEditFrm_title']);
		$acctrepEditFrm_is_active = sanitize_text_field($_POST['acctrepEditFrm_is_active']);

		if(empty($acctrepEditFrm_title)) {
			$formErrorEdit[] = 'Please enter the name.';
		}

		if(empty($formErrorEdit)) {
			$wpdb->update('wp_customers', array(
				'modified' => current_time('mysql'),
				'acctrepname' => $acctrepEditFrm_title,
				'is_active' => ((isset($acctrepEditFrm_is_active) && $acctrepEditFrm_is_active == 'yes') ? 1 : 0)
			), array(
				'id' => $customerId
			));
			$formSuccessEdit[] = 'Edit successfully.';
			$acctrepRow = $wpdb->get_row('SELECT * FROM wp_customers WHERE id = "'.$customerId.'"');
		}
	}
?>
<h3>Edit - Customer</h3>
<form name="acctrepEditFrm" action="" method="post">
<div>

<?php
if(!empty($formSuccessEdit)) {
	echo '<p style="color:green;">Status follows,</p>';
	echo '<ol>';
	foreach($formSuccessEdit as $formSuccessEditItem) {
		echo '<li>'.$formSuccessEditItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}

if(!empty($formErrorEdit)) {
	echo '<p style="color:red;">The following errors found,</p>';
	echo '<ol>';
	foreach($formErrorEdit as $formErrorEditItem) {
		echo '<li>'.$formErrorEditItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}
?>

<p><label for="customerEditFrm_title">Name</label><br /> <input type="text" name="customerEditFrm_title" value="<?php echo $acctrepRow->acctrepname; ?>" id="acctrepEditFrm_title" /></p>
<p><label for="acctrepEditFrm_is_active">Is Active?</label> <input type="checkbox" name="acctrepEditFrm_is_active" value="yes" id="acctrepEditFrm_is_active" <?php echo (($acctrepRow->is_active == 1) ? 'checked="checked"' : ''); ?> /></p>
<p><input type="submit" name="acctrepEditFrm_submit" value="Edit" /></p>
</div>
</form>
<?php
} else if($action == 'delete') {
	// Handle the delete action
	$customerId = $_GET['id'];
	$wpdb->delete('wp_customers', array(
		'id' => $customerId
	));

	$_SESSION['msg'] = 'deletesuccess';
	wp_safe_redirect(site_url().'/wp-admin/admin.php?page=customer_mng'); exit;
} else if($action == 'makeactive') {
	// Handle the active action
	$customerId = $_GET['id'];
	$wpdb->update('wp_customers', array(
		'is_active' => 1
	), array(
		'id' => $customerId
	));

	$_SESSION['msg'] = 'activesuccess';
	wp_safe_redirect(site_url().'/wp-admin/admin.php?page=customer_mng'); exit;
} else if($action == 'makeinactive') {
	// Handle the inactive action
	$customerId = $_GET['id'];
	$wpdb->update('wp_customers', array(
		'is_active' => 0
	), array(
		'id' => $customerId
	));

	$_SESSION['msg'] = 'inactivesuccess';
	wp_safe_redirect(site_url().'/wp-admin/admin.php?page=customer_mng'); exit;
} else {
	//Prepare Table of elements
	$list_table = new customers_List_Table();
	$list_table->prepare_items();
	//Table of elements
	$list_table->display();
}
?>

</div>