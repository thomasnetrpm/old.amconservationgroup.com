<?php
/* 
Template Name: Reset password template 
*/

global $wpdb, $amc_urls, $show_msg, $success_msg, $error_msg, $success_msg2, $error_msg2;

$uEmail = sanitize_text_field($_GET['email']);
$uHashkey = sanitize_text_field($_GET['hashkey']);

if(isset($uEmail) && !empty($uEmail) && isset($uHashkey) && !empty($uHashkey)) {
	$result = $wpdb->get_row('SELECT * FROM wp_customers WHERE email = "'.$uEmail.'" AND olduserkey = "'.$uHashkey.'"');
	if(!empty($result)) {

// Starting of the template
define("CSS", "reset_password.css");
redirectLoggedInUser();

$show_msg = 'none';
$show_msg2 = 'none';

// Handle the form submission
$form_errors2 = array();
$login_errors = array();
// $v = wp_check_password('1234567x', '$P$BZSZoCFqYk5UFs9ZQ1Z6Z3WanejfQJ0');

if(isset($_POST['userRstPwdForm_submit']) && ($_POST['userRstPwdForm_submit'] == 'Submit') && isset($_POST['name_userRstPwdForm']) && wp_verify_nonce($_POST['name_userRstPwdForm'],'action_userRstPwdForm')) {
	// echo '<pre>'; print_r($_POST); echo '</pre>';

	// verify the email/hash again
	$uEmail = sanitize_text_field($_POST['userRstPwdForm_uemail']);
	$uHashkey = sanitize_text_field($_POST['userRstPwdForm_uhashkey']);
	$result = $wpdb->get_row('SELECT * FROM wp_customers WHERE email = "'.$uEmail.'" AND olduserkey = "'.$uHashkey.'"');
	if(empty($result)) {
		$form_errors2['userRstPwdForm_auth'] = 'Invalid request';
	}

	if(sanitize_text_field($_POST['userRstPwdForm_newpassword']) == '') {
		$form_errors2['userRstPwdForm_newpassword'] = 'Please enter password';
	}

	if(sanitize_text_field($_POST['userRstPwdForm_cnewpassword']) == '') {
		$form_errors2['userRstPwdForm_cnewpassword'] = 'Please enter confirm password';
	}

	if(sanitize_text_field($_POST['userRstPwdForm_newpassword']) != sanitize_text_field($_POST['userRstPwdForm_cnewpassword'])) {
		$form_errors2['userRstPwdForm_newpassword'] = 'Your password, confirm password doesn\'t match.';
	}

	if(empty($form_errors2)) {
		// If everything is Ok, then update the password
		$newPwd = sanitize_text_field($_POST['userRstPwdForm_newpassword']);
		$hashNewPwd = wp_hash_password($newPwd);

		$result = $wpdb->update('wp_customers', array(
			'password' => $hashNewPwd,
			'olduserkey' => ''
		), array(
			'email' => $uEmail,
			'olduserkey' => $uHashkey
		));

		$show_msg2 = 'block';
		$success_msg2 = 'Your password has been set. Please click <a href="'.$amc_urls['register_login'].'?olduser=yes"><strong>here</strong></a> to login.';
	}
}

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}
?>
<div class="row bgfill ">
    <div class=" topspacer">
      <div class="message-form-container">
        <div class="content">
          <div class="col-md-4 col-md-offset-4">
            <div class="form-group">
            
<?php
$alertClass = 'alert-warning';
if(!empty($error_msg2)) {
	$alertClass = 'alert-danger';
}

if($show_msg2 != 'none') {
?>

<div class="js_result alert <?php echo $alertClass; ?> fade in">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<?php
if(!empty($success_msg2)) {
	echo $success_msg2;
} else if(!empty($error_msg2)) {
	echo $error_msg2;
} else if(!empty($form_errors2)) {
	echo implode('<br />', $form_errors2);
}
?>
</div>

<?php
}
?>

<form id="form1" role="form" method="post" action="" name="userRstPwdForm" parsley-validate novalidate>
<?php wp_nonce_field('action_userRstPwdForm', 'name_userRstPwdForm'); ?>

<input type="password" name="userRstPwdForm_newpassword" id="password" class="form-control" placeholder="New Password" required parsley-error-message="Please enter your password"><br>

<input type="password" name ="userRstPwdForm_cnewpassword" id="confirm-pwd" class="form-control" placeholder="Confirm New Password"  required parsley-error-message="Please confirm your password" parsley-equalto="#password"><br>

<input type="hidden" name="userRstPwdForm_uemail" value="<?php echo $uEmail; ?>" />
<input type="hidden" name="userRstPwdForm_uhashkey" value="<?php echo $uHashkey; ?>" />

<div class="submit-container"><div class="offset-top-1"><div><input type="submit" value="Submit" class="btn fp-submit-btn " name="userRstPwdForm_submit" /></div></div></div>
</form>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
// Ending of the template
	} else {
		wp_safe_redirect(get_bloginfo('url'));
	}
} else {
	wp_safe_redirect(get_bloginfo('url'));
}
exit();