/* _____ FORUM SCRIPTS ______________________________________________________
QWIK7


File Layout
-----------
[1] ACCORDION ACTION
[2] VOTE OPTION BUTTON ACTION

____________________________________________________________________________ */


/* _____ [1] ACCORDION ACTION ________________________________________________________ */
$('.accordion-group').on('shown.bs.collapse', function () {
   // $(this).find(".accordion-heading").css('background-position-x',' #0143a1 url("assets/images/up-arrow.png")no-repeat 98% 50%')
     $(this).find(".accordion-heading").css({'background-image' : "url('assets/images/up-arrow.png')", 'background-repeat': 'no-repeat','background-color': '#0143a1','background-position-x':'98%', 'background-position-y':'50%'})
});
$('.accordion-group').on('hidden.bs.collapse', function () {
    $(this).find(".accordion-heading").css({'background-image' : "url('assets/images/down-arrow.png')", 'background-repeat': 'no-repeat','background-color': '#1b6fe3','background-position-x':'98%', 'background-position-y':'50%'})
    //$(this).find(".accordion-heading").css('background',' #1b6fe3 url("assets/images/down-arrow.png")no-repeat 98% 50%')
});

/* _____ [2] VOTE OPTION BUTTON ACTION ________________________________________________________ */
var voteState=null;
$(".vote-good, .vote-fair, .vote-bad").on("click", function() {
  resetVote($(this), voteState);
  voteState=$(this);
  var self="#"+$(voteState).parent().attr("id");
  $(this).parent().data("id", $(voteState).parent().attr("id"));
  $(this).parent().data("voteState", $(voteState).attr("data-vote"));
  $(this).parent().find(voteState).toggleClass($(voteState).attr("data-vote"));
});

function resetVote(parentObj, obj){
  if(obj!==null){
    var pobj="#"+$(parentObj).parent().attr("id");
    var v1="#"+$(pobj).data("id");
    var v2="."+$(pobj).data("voteState");
    $(v1+" > "+v2).toggleClass($(pobj).data("voteState"));
  }
}