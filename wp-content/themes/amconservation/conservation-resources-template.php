<?php 
/* 
Template Name: Conservation-Resources  template 
*/

if(is_mobile()) {
	include('conservation-resources-mobile-template.php');
	exit();
}

define("CSS", "conservationresources.css");

global $wpdb, $amc_urls;

// Handle the form submission
$form_errors= array();
$ins_errors = array();
$sucess_msg = array();

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	if(isset($_POST['name_suggestSiteFrm']) && wp_verify_nonce($_POST['name_suggestSiteFrm'],'action_suggestSiteFrm')) {
		 //echo '<pre>'; print_r($_POST); echo '</pre>'; exit();

		if(sanitize_text_field($_POST['suggestSiteFrm_URL']) == '') {
			$form_errors['status'] = 'error';
			$form_errors['msg'] = 'Please enter website address';
			echo json_encode($form_errors);
		}

		if(empty($form_errors)) {
			$ins_ID = $wpdb->insert('wp_suggestsite', array(
				'created' => current_time('mysql'), 
				'modified' => current_time('mysql'),
				'url' => clean_url(sanitize_text_field($_POST['suggestSiteFrm_URL']))
			));

			if($ins_ID) {
				$sucess_msg['status'] = 'success';
				$sucess_msg['msg'] = 'The website address has been submitted successfully.';
				echo json_encode($sucess_msg);
			} else {
				$ins_errors[] = 'Unable to add URL. Try, after some time.';
				$ins_errors['status'] = 'error';
				$ins_errors['msg'] = 'Unable to add URL. Try, after some time.';
				echo json_encode($ins_errors);
			}
			// Mail suggested site information to admin
			$suggestSiteMsg = 'A user has suggested a site for Conservation Resources'."<br /><br />";
			$suggestSiteMsg .= 'Site : '.sanitize_text_field($_POST['suggestSiteFrm_URL'])."<br />";
			//$suggestSiteMsg .= 'Regards'."<br />";
			//$suggestSiteMsg .= 'AM Conservation Group Team';
			
			$mailHeaders = 'From: '.$amc_urls['mail_from_name'].' <'.get_bloginfo('admin_email').'>' . "\r\n";

			add_filter( 'wp_mail_content_type', 'set_html_content_type' );
			$mailStatus = wp_mail(
				get_option('suggest_a_site_email_field'), 
				'Suggestion for a site.', 
				$suggestSiteMsg, 
				$mailHeaders);
			remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

		}
	}
	exit();
}

get_header(); ?>
<div class="row">      
        <div class="col-md-12 aboutbanner"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/conservation_resources_banner.jpg"></div>  
    </div>

 <!-- Middle Mennu  -->
        <div class="row">
             <div class="aboutmenu-container">
                 <?php require_once('middle-menu.php'); ?>
             </div>
    </div>
     <!--/  Ends Middle Mennu  -->

<!-- Dynamic  contents comes From the  AdminSide  -->   
<div class="row cr ipadp-cont">
<div class="ipad-back1"></div>
<div class="ipad-back2">Back</div>
	<div class="col-md-5 col-lg-4 col-md-offset-1 ipadp-c1">
   
		<div class="cr-container">
         
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
		</div>
	</div>
	<div class="col-lg-2 col-md-1 hidden-xs hidden-sm  cr-middledivider"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/cr-divider1.png"></div>
	<div class="col-md-5 ipadp-c2">
<?php
/*
// The Query
$the_query = new WP_Query('page_id=3307');

// The Loop
if ( $the_query->have_posts() ) {
	while ( $the_query->have_posts() ) {
		$the_query->the_post();
		echo get_the_content();
	}
}
wp_reset_postdata();
*/
?>
<?php
global $sas_categories;
foreach($sas_categories as $sas_c_key => $sas_category) {
	$sasRecs = $wpdb->get_results('SELECT * FROM wp_suggestsite WHERE sas_category_id = '.$sas_c_key.' AND is_active = 1 ORDER BY menu_order ASC');
	// Check for General category
	if($sas_c_key == 1) {
		if(!empty($sasRecs)) {
?>
<h3 class="cr-headertxt3">Conservation Fact Links</h3>
<ul class="cr-ulist">
<?php foreach($sasRecs as $sasRec) { ?>
	<li><a href="<?php echo $sasRec->url; ?>" target="_blank"><?php echo $sasRec->title; ?></a></li>
<?php } ?>
</ul>
<?php
		}
	} else {
		if(!empty($sasRecs)) {
?>
<h3 class="cr-headertxt4"><?php echo $sas_category; ?></h3>
<ul class="cr-ulist2">
<?php foreach($sasRecs as $sasRec) { ?>
	<li><a href="<?php echo $sasRec->url; ?>" target="_blank"><?php echo $sasRec->title; ?></a></li>
<?php } ?>
</ul>
<?php
		}
	}
}
?>
<h3 class="suggest-txt"><a href="#" data-toggle="modal" data-target=".suggestasite">Suggest A Site</a></h3>
	</div>
</div>




<!-- Request a Quote popup page -->
<div class="modal fade suggestasite in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
  <div class="modal-content-rac">
	<div class="modal-header-rac">
	  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	</div>
	<div class="modal-body">

<form role="form" name="suggestSiteFrm" action="" method="post" parsley-validate novalidate>
<?php wp_nonce_field('action_suggestSiteFrm', 'name_suggestSiteFrm'); ?>
<div class="form-group-rac">
	<h3 class="form-header-txt">Suggest A Site</h3>

<br />
<div class="js_result_sas alert fade in" style="display:none;">
	<button type="button" class="close js_close_sas" aria-hidden="true">×</button>
	<p></p>
</div>

	<p style="margin: 0px; padding: 0px; color: green; font-size: 10px; display:none;" class="js_suc_msg"></p>
	<p style="margin: 0px; padding: 0px; color: red; font-size: 10px; display:none;" class="js_err_msg">Please enter an URL</p>
	<input type="text" name="suggestSiteFrm_URL" parsley-error-message="Please enter a valid website address" parsley-type="url" parsley-trigger="change" required="" placeholder="Please enter website address*" class="rac-form-field parsley-validated" maxlength="150"  />

	<div class="mandatory-txt">*Mandatory Fields</div>
	
	<input type="submit" name="suggestSiteFrm_submit" class="submit-btn btn-primary" value="SUBMIT" />                      
</div>
</form>
	</div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
$('form[name=suggestSiteFrm]').on('submit', function(e) {
	 e.preventDefault();
	if($('form[name=suggestSiteFrm]').parsley('isValid')) {
		$.ajax({
			url: '<?php echo $amc_urls['conservation-resources']; ?>',
			method: 'post',
			data: $('form[name=suggestSiteFrm]').serialize(),
			success: function(d) {
				var o = $.parseJSON(d);
				if(o.status == 'error') {
					// $('p.js_err_msg').text(o.msg).show().fadeOut(2000);
					$('.js_result_sas').removeClass('alert-warning').addClass('alert-danger').find('p').text(o.msg);
					$('.js_result_sas').show();
				} else if(o.status == 'success') {
					// $('p.js_suc_msg').text(o.msg).show().fadeOut(2000);
					$('.js_result_sas').removeClass('alert-danger').addClass('alert-warning').find('p').text(o.msg);
					$('.js_result_sas').show();
					$('input[name=suggestSiteFrm_URL]').val('');
					
				}
			}
		});
		
	}
	
	return true;
});

$('button.js_close_sas').click(function() {
	$('.js_result_sas').hide();
});

$(".ipad-back1").click(function(){
	//$( ".cont_sliderbx" ).animate({marginLeft: '-98%'}, 500)
	$(".ipadp-c1").hide(500);
	$(".ipadp-c2").show(500);
	$('.ipad-back1').hide();
	$('.ipad-back2').show();
	//$( ".cont_sliderbx" ).animate({scrollLeft: 500}, 800);         
});
$(".ipad-back2").click(function(){
	//$( ".cont_sliderbx" ).animate({marginLeft: '0'}, 500); 
	$(".ipadp-c2").hide(500);   
	$(".ipadp-c1").show(500);
	$('.ipad-back1').show();
	$('.ipad-back2').hide();
	//( ".cr_data1" ).show();    
});

</script>

<?php   get_footer(); ?>    