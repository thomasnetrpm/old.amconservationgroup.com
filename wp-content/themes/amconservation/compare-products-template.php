<?php 
/* 
Template Name: Compare products template 
*/ 

define("CSS", "compare.css");

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}
?>

<?php global $post, $woocommerce, $product; $FilterSet; ?>

<div class="row compare"> 
	<div class="col-md-12 compare-container total_col_width">
	<div class="row">
<?php if(isset($_POST) && isset($_POST['prodCmpArray'])) { ?>

<?php foreach($_POST['prodCmpArray'] as $productId) { ?>
<?php $product = get_product($productId); 
		$xTerms = get_the_terms($product->post->ID,'product_cat');
		sort($xTerms);
		$Fslug = $xTerms[1]->slug;
		$AttributeSet = $FilterSet[$Fslug];
 ?>

		<div class="col-md-3 col-xs-3 col-sm-3 comparesession col_width">

<span class="compare-bigclose"><a href="#" onclick="$(this).parent().parent().hide(); return false;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/compare-closebtn.png"></a></span> 

<?php 
 //print_r($product);
echo $product->get_image('medium',array("class"=>'product-content2', 'alt' => $product->get_title()));
 ?>
<p class="compare-product-name"><?php echo $product->get_title(); ?></p>

<button type="button" class="compare-btn btn-primary" data-target=".requestaquote" data-toggle="modal">Request a Quote</button>
<span class="jsPostId" style="display:none;"><?php echo $productId; ?></span>

<div class="compare-description">
	<p class="itemnum">Item Number: </p>
	<p class="productname"><?php echo strtoupper($product->get_sku()); ?></p>

	<p class="compare-hd">Description:</p>
	<div id="product_content">
	<?php echo $product->post->post_content; ?>
    </div>
    <p class="priceunits"> </p>
	<?php
	$Colval = array();
	$AttVal = array();
	$TableAttributes = woocommerce_get_product_terms($product->post->ID, 'pa_table-attributes', 'all');
	if(is_array($TableAttributes) && count($TableAttributes) > 0)
		 {
		 	$AttrSet = $TableAttributes;
		 }
		 else
		 {
			//$AttrSet = array();
		 }
       if(is_array($AttrSet)) {
		 foreach($AttrSet as $key=>$value)
		 {
		 	$Colval[$value->slug] = array("label" =>$value->name,"slug"=>$value->slug);
		 }
		}
		// Creating the attribute combination array // 
		if(is_array($AttributeSet)) {
		foreach($AttributeSet as $value)
		{
			$the_tax = get_taxonomy('pa_'.$value);
			//print_r($the_tax);
			//echo $value;
	 		$tmp_Colval[$value] = array("label" =>$the_tax->labels->name,"slug"=>$value);
		}
		$Colval = array_merge($Colval, $tmp_Colval);
		}
		//print_r($Colval);

		  //$xTerms = get_the_terms($product->post->ID, 'product_cat');
		  // echo '<pre>'; print_r($xTerms); echo '</pre>';

	$prodAttributes = $product->get_attributes();
	foreach($prodAttributes as $prodAttributesItem) {
		$attribute_value = get_post_meta($productId,'attribute_'.$prodAttributesItem['name']);
		$v = ($product->variation_id)?$attribute_value[0] : $product->get_attribute($prodAttributesItem['name']);
		$AttVal[$prodAttributesItem['name']]=$v;

	}
	if(is_array($Colval)) {
	 foreach($Colval as $Pval) {
	 	$pakey = "pa_".$Pval["slug"];
	 	$term = get_term_by('slug',$AttVal[$pakey], $pakey);
	 	$v=(empty($term->name)) ? 'N/A' : $term->name;
	 	echo '<p class="compare-proname">'.$Pval["label"].'</p>';
	 	echo '<p class="qtynum2">'.$v.'</p>';

	 }
	}
	?>
     </div>
   </div>                                   

   
   <?php } ?>
<?php } else { ?>
<p>Please select some product(s) to compare. Click <a href="#" onclick="history.back();" title="">here</a> to go back.</p>

<?php } ?>
</div>
	</div>
</div>

<?php include('woocommerce/request-quote-modal.php'); ?>
<script>
// RWD set width for compare container start 
$(document).ready(function() {
   if ($(window).width() < 769) {
   var col = $('.col_width').outerWidth();
   $('.col_width').css('width',col);
   //console.log('res' + col);
   
   var col_len = $('.col_width').length;
   var col_plus = $('.col_width').outerWidth();
   var col_tot = col_plus * col_len;
   $('.total_col_width').css('width',col_tot);
   //console.log('res' + col_plus);
	}
});
// RWD set width for compare container End

$(".product-content2").removeAttr("height");
$(".product-content2").removeAttr("width");

 $('#product_content ul li:empty').remove();
alignProductlistHeightCompare();
/* _____alignProductlistHeight____ */
$(window).resize(function () {
	//alignProductlistHeightCompare();
	setHeight('.compare-description');
	// /alert('asda');
});
function alignProductlistHeightCompare(){
	//$(".product-content2").css("height", "80%");
}
/* _____alignProductlistHeight____ */
$(".radio-group2 .rac-radio1 :radio").click(function() {
	$("div .services").css("display","none");
	$("div .products").css("display","block");
});

$(".radio-group2 .rac-radio2 :radio").click(function() {
	$("div .products").css("display","none");
	$("div .services").css("display","block");
});

 $(function() {
 		 setHeight('.compare-description');
		 setHeight('.compare-product-name');
		 //var cmpHt=$(".social-left").offset().top-$(".compare-description").offset().top;
		 //$(".compare-description").css("height",cmpHt+"px");
});
 function setHeight(pxBtmBox) {
    var maxHeight = 0;
    //Get all the element with class = col
    column = $(pxBtmBox);
    column.css('height', 'auto');
    //Loop all the column
    column.each(function () {
        //Store the highest value
        if ($(this).height() > maxHeight) {
            maxHeight = $(this).height();
           
        }
    });
    //Set the height
    column.height(maxHeight);
}

$('<a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="back_btn"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/back_btn.png"><span class="back-txt">Back</span></a>').appendTo('h3.headerlabel');
</script>


<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>    