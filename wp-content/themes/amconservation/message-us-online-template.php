<?php 
/* 
Template Name: Message us online
*/ 
define("CSS", "message_us.css");

global $post, $wpdb, $amc_urls, $show_msg, $success_msg, $error_msg;
$show_msg = 'none';
// $wpdb->show_errors();

// Get the how did you find us options
$hfuOptions = '';
$hfuResults = $wpdb->get_results('SELECT title FROM wp_howfindus WHERE is_active = 1');
foreach($hfuResults as $hfuResultsItem) {
	$hfuOptions .= '<option value="'.$hfuResultsItem->title.'">'.$hfuResultsItem->title.'</option>';
}

// Check for login user
if(userIsLoggedIn()) {
	$loggedInUser = getLoggedInUser();
	$uName = $loggedInUser->firstname.' '.$loggedInUser->lastname;
	$uCompany = $loggedInUser->companyname;
	$uEmail = $loggedInUser->email;
}

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header('shop-amc');
}
?>
 
<!-- Title and Breadcrumb  -->
      <div class="row headersession">      
        <div class="col-md-7"><h3 class="headerlabel"><?php the_title(); ?></h3></div>
        <div class="col-md-5 breadcrumbs ">
          <ol class="breadcrumb">
           <?php breadcrumbs_fmg();  ?>
          </ol>
        </div>    
    </div>
     <!--/  Ends Title and Breadcrumb   -->
    
       <div class="row bgfill ">
           
           <div class=" topspacer">
           
                    <div class="col-md-12 message-form-container"> 
                            <div class="row content">  
                           <div class="col-md-4 col-md-offset-4"> 

                           <div class="form-group"> 

<?php
	// Handle the form submission
	$form_errors = array();
	if(isset($_POST['subm_message_us_online']) && ($_POST['subm_message_us_online'] == 'SUBMIT') && isset($_POST['name_frm_message_us_online']) && wp_verify_nonce($_POST['name_frm_message_us_online'],'action_frm_message_us_online')) {
		// echo '<pre>'; print_r($_POST); echo '</pre>';

		if(sanitize_text_field($_POST['user_name']) == '') {
			$form_errors['user_name'] = 'Please enter name';
		}
		if(sanitize_text_field($_POST['company_name']) == '') {
			$form_errors['company_name'] = 'Please enter company name';
		}
		if(sanitize_text_field($_POST['email_address']) == '') {
			$form_errors['email_address'] = 'Please enter email address';
		} else {
			if(!is_email(sanitize_text_field($_POST['email_address']))) {
				$form_errors['email_address'] = 'Please enter valid email address';
			}
		}

		if(sanitize_text_field($_POST['phone_number']) == '') {
			$form_errors['phone_number'] = 'Please enter phone number';
		}
		if(sanitize_text_field($_POST['message']) == '') {
			$form_errors['message'] = 'Please enter message';
		}

		if(empty($form_errors)) {
			$ins_ID = $wpdb->insert( 
				'wp_message_us_online', 
				array( 
					'created' => current_time('mysql'), 
					'modified' => current_time('mysql'),
					'user_name' => sanitize_text_field($_POST['user_name']),
					'company_name' => sanitize_text_field($_POST['company_name']),
					'email_address' => sanitize_text_field($_POST['email_address']),
					'phone_number' => sanitize_text_field($_POST['phone_number']),
					'how_find' => sanitize_text_field($_POST['how_find']),
					'message' => sanitize_text_field($_POST['message']),
					'pref_contact' => sanitize_text_field($_POST['pref_contact']),
				) 
			);
			
			$form_id = "message_us_online";
			include('hubspot-form-api.php');

			if($ins_ID) {
				// echo '<p>Success added data.</p>';
				$show_msg = 'block';
				$success_msg = 'Thank you for contacting us. We will get back to you soon.'; // 'Successfully added.';
				
				// Mail message us information to admin
			
				$messageUsMsg = 'Name : '.sanitize_text_field($_POST['user_name'])."<br />";
				$messageUsMsg .= 'Company Name : '.sanitize_text_field($_POST['company_name'])."<br />";
				$messageUsMsg .= 'Email Address : '.sanitize_text_field($_POST['email_address'])."<br />";
				$messageUsMsg .= 'Phone Number : '.sanitize_text_field($_POST['phone_number'])."<br />";
				$messageUsMsg .= 'How did you find us? : '.sanitize_text_field($_POST['how_find'])."<br />";
				$messageUsMsg .= 'Reason for contacting : '.sanitize_text_field($_POST['message'])."<br />";
				$messageUsMsg .= 'Preferred contact method : '.sanitize_text_field($_POST['pref_contact'])."<br />";
				//$messageUsMsg .= 'Regards'."<br />";
				//$messageUsMsg .= 'AM Conservation Group Team';

				$mailHeaders = 'From: '.$amc_urls['mail_from_name'].' <'.get_bloginfo('admin_email').'>' . "\r\n";

				add_filter( 'wp_mail_content_type', 'set_html_content_type' );
				$mailStatus = wp_mail(
					get_option('message_us_email_field'), 
					'Request for Help on AMConservation', 
					stripslashes($messageUsMsg), 
					$mailHeaders);
				remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
				unset($_POST);
				?>
<!-- Google Code for New Contact Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ /
var google_conversion_id = 1071850939;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "dUDmCMvhqVkQu8uM_wM";
var google_remarketing_only = false;
/ ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1071850939/?label=dUDmCMvhqVkQu8uM_wM&guid=ON&script=0"/>
</div>
</noscript>					
				<?php
			}
		} else {
			/*
			echo '<p>Some errors found.</p>';
			echo '<ol>';
			foreach($form_errors as $form_errors_item) {
				echo '<li>'.$form_errors_item.'</li>';
			}
			echo '</ol>';
			*/

			$show_msg = 'block';
			$error_msg = 'Errors found. Try, again.';
		}
	}
?>

<script charset="utf-8" src="//js.hsforms.net/forms/current.js"></script>
<script>
  hbspt.forms.create({ 
    portalId: '448562',
    formId: '9125bed1-b11b-4950-bf3b-38fd6b774c1a'
  });
</script>
                           
    </div> 
    </div>
    </div>
    </div>
    </div>
	</div>

<div class="row bgfill ">
  <div class="space-above-footer"> </div>
</div>

<?php include('woocommerce/request-quote-modal.php'); ?>


<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>