<?php   

/***************************************************************************
    Built by UFO Themes. http://www.ufothemes.com
    Copyright (C) 2012 UFO Themes    
***************************************************************************/ 

global $theme_shortname; 
global $theme_options;
global $FilterSet;
global $ParentCat ;
global $amc_urls;
$amc_urls = array(
	'home' => get_bloginfo('url'),
	'message_us_online' => get_bloginfo('url').'/message-us-online/',
	'register_login' => get_bloginfo('url').'/register/',
	'forgot_password' => get_bloginfo('url').'/forgot-your-password/',
	'logout' => get_bloginfo('url').'/logout/',
	'my_account' => get_bloginfo('url').'/my-account/',
	'quote_basket' => get_bloginfo('url').'/quote-basket/',
	'image_gallery' => get_bloginfo('url').'/image-gallery/',
	'request_a_quote' => get_bloginfo('url').'/request-a-quote/',
	'home_category_landing' => get_bloginfo('url').'/category-landing-page/',
	'reset_password' => get_bloginfo('url').'/reset-password/',
	'forgot_reset_password' => get_bloginfo('url').'/forgot-reset-password/',
	'nl_subscription' => get_bloginfo('url').'/newsletter-subscription/',

	'pinterest' => 'http://www.pinterest.com/amconservation/',
	'twitter' => 'https://twitter.com/AmConservation',
	'facebook' => 'https://www.facebook.com/pages/AM-Conservation-Group-Inc/116618728380579',
	'request_a_catalog' => get_bloginfo('url').'/request-a-catalog/',
	'testimonial' => get_bloginfo('url').'/testimonials/',
	'about_us' => get_bloginfo('url').'/about-us/',
	'contact_us' => get_bloginfo('url').'/contact-us/',
	'conservation-resources' => get_bloginfo('url').'/conservation-resources/',
	'programs_services' => get_bloginfo('url').'/programs-services/',
	'news' => get_bloginfo('url').'/news/',

	'energy_efficiency_education' => get_bloginfo('url').'/programs-services/energy-efficiency-education/',
	'commercial_and_industrial_fulfillment' => get_bloginfo('url').'/programs-services/commercial-and-industrial-fulfillment/',
	'efficiency_program_management' => get_bloginfo('url').'/programs-services/efficiency-program-management/',
	'direct_mail_fulfillment' => get_bloginfo('url').'/programs-services/direct-mail-fulfillment/',
	'call_center_services' => get_bloginfo('url').'/programs-services/call-center-services/',
	'direct_install_fulfillment' => get_bloginfo('url').'/programs-services/direct-install-fulfillment/',
	'weatherization_contractor_services' => get_bloginfo('url').'/programs-services/weatherization-contractor-services/',
	'turnkey_program_solutions' => get_bloginfo('url').'/programs-services/turnkey-program-solutions/',
	'database_management' => get_bloginfo('url').'/programs-services/database-management/',
	'contact_number'      => "1.888.866.1624",

	'catalog_link' => get_bloginfo('url').'/wp-content/uploads/Catalog_for_print_2013_REVISED_9-3.zip',
	'creditapplication_link' => get_bloginfo('url').'/wp-content/uploads/credit_application.pdf',
	'casestudies_link' => get_bloginfo('url').'/wp-content/uploads/ecobeco case study.pdf',

	'sc_url' => 'http://sc.uat002.fmgdevelopment.com/site/',

	'mail_from_name' => 'AM Conservation Group',
	'mail_regards' => 'AM Conservation Group Team'
);

global $sas_categories;
$sas_categories = array(
	'1' => 'General',
	'2' => 'Educational Resources',
	'3' => 'Government Sites',
	'4' => 'Organizations',
	'5' => 'Home Performance Specialists',
	'6' => 'Magazines',
	'7' => 'Environmental Advocacy Organizations',
	'8' => 'Energy Conservation',
	'9' => 'Water Conservation',
	'10' => 'Weatherization'
);

function sas_sort_save() {
	global $wpdb;

	$order = explode(',', $_POST['order']);
	$counter = 1;
	foreach ($order as $item_id) {
		$wpdb->update('wp_suggestsite', array( 'menu_order' => $counter ), array( 'id' => str_replace('record_', '', $item_id)) );
		$counter++;
	}
	die(1);
}
add_action('wp_ajax_sas_sort_save', 'sas_sort_save');
add_action('wp_ajax_nopriv_sas_sort_save', 'sas_sort_save');

// Overwriting the config values from DB
global $wpdb;
$amcConfResults = $wpdb->get_results('SELECT * FROM wp_amcconfig');
foreach($amcConfResults as $amcConfItem) {
	$amc_urls[$amcConfItem->confslug] = $amcConfItem->confvalue;
}

$ParentCat = array(1866,1862,1864,1865,1863);
$theme_shortname = 'bonanza';
$theme_options = get_option( $theme_shortname . '_options' );

$template_dir = get_template_directory();

// Load iCore Framework and Theme Options
require_once ( $template_dir . '/icore/icore-init.php' );

function register_my_menus() {
register_nav_menus(
array(
'footer-menu' => __( 'Footer Menu' ),'middle-menu' => __('Middle Menu')
)
);
}
add_action( 'init', 'register_my_menus' );

// Disable updates for core, plugins, themes
add_filter( 'pre_site_transient_update_core', create_function( '$a', "return null;" ) );
remove_action( 'load-update-core.php', 'wp_update_themes' );
add_filter( 'pre_site_transient_update_themes', create_function( '$a', "return null;" ) );
remove_action( 'load-update-core.php', 'wp_update_plugins' );
add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );

// Register sessions
function register_session() {
	if( !session_id())
		session_start();
}
add_action('init','register_session');

function userIsLoggedIn() {
	if(isset($_SESSION['user_logged_in']) && ((INT)$_SESSION['user_logged_in'] != 0)) {
		return true;
	}
	return false;
}

function getLoggedInUser() {
	if(isset($_SESSION['user_logged_in']) && ($_SESSION['user_logged_in'] != '')) {
		global $wpdb;
		$userLoggedInArr = $wpdb->get_row('SELECT * FROM wp_customers WHERE id = "'.sanitize_text_field($_SESSION['user_logged_in']).'"');
		return $userLoggedInArr;
	}
	return false;
}

function redirectUnLoggedUser() {
	global $amc_urls;

	if(!userIsLoggedIn()) {
		unset($_SESSION['user_logged_in']);
		unset($_SESSION['user_logged_in_data']);
		wp_safe_redirect($amc_urls['register_login']);
		exit;
	}
}

function redirectLoggedInUser() {
	global $amc_urls;

	if(userIsLoggedIn()) {
		wp_safe_redirect($amc_urls['home']);
		exit;
	}
}

function isNewsletterSubscribed($userId = null) {
	if((INT)$userId >= 0) {
		global $wpdb;

		$loggedInUser = getLoggedInUser();
		$result = $wpdb->get_row("SELECT * FROM wp_newsletter WHERE email = '".$loggedInUser->email."'");
		if(!empty($result)) {
			return true;
		}
	}
	return false;
}

function getQuoteCount() {
	global $wpdb;
	$loggedInUser = getLoggedInUser();
	$count = $wpdb->get_var("SELECT COUNT(*) FROM wp_requestquote WHERE email = '".$loggedInUser->email."'");
	return $count;
}

function set_html_content_type() {
	return 'text/html';
}

function getCurURL() {
	$curURL = (isset($_SERVER['HTTPS'])) ? 'https://' : 'http://';
	return $curURL . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}

function emailExists($email = '') {
	global $wpdb;
	$status = true;

	$result = $wpdb->get_row("SELECT * FROM wp_customers WHERE email = '".$email."'");
	if(is_null($result) || empty($result)) {
		$status = false;
	}
	return $status;
}

// Adding extra menus
add_menu_page('AMC - Message us online', 'Message us online', 'manage_options', 'message_us_online', 'message_us_online', '', 100);

function message_us_online() {
	require_once(TEMPLATEPATH.'/message-us-online-admin.php');
}

add_menu_page('AMC - Contact us', 'Contact us', 'manage_options', 'contact_us_mng', 'contact_us_mng', '', 101);

function contact_us_mng() {
	require_once(TEMPLATEPATH.'/contact-us-admin.php');
}

add_menu_page('AMC - How did you find us?', 'How did you find us?', 'manage_options', 'how_find_us_mng', 'how_find_us_mng', '', 102);

function how_find_us_mng() {
	require_once(TEMPLATEPATH.'/how-find-us-admin.php');
}

add_menu_page('AMC - Account representatives', 'Account representatives', 'manage_options', 'acctrep_mng', 'acctrep_mng', '', 103);

function acctrep_mng() {
	require_once(TEMPLATEPATH.'/acctrep-admin.php');
}

add_menu_page('AMC - Services', 'Services', 'manage_options', 'service_mng', 'service_mng', '', 104);

function service_mng() {
	require_once(TEMPLATEPATH.'/service-admin.php');
}

add_menu_page('AMC - Suggest a Site', 'Suggest a Site', 'manage_options', 'sas_mng', 'sas_mng', '', 105);

function sas_mng() {
	require_once(TEMPLATEPATH.'/sas-admin.php');
}

add_menu_page('AMC - Request a catalog', 'Request a catalog', 'manage_options', 'reqcatalog_mng', 'reqcatalog_mng', '', 106);

function reqcatalog_mng() {
	require_once(TEMPLATEPATH.'/req-catalog-admin.php');
}

add_menu_page('AMC - Request a quote', 'Request a quote', 'manage_options', 'reqquote_mng', 'reqquote_mng', '', 107);

function reqquote_mng() {
	require_once(TEMPLATEPATH.'/req-quote-admin.php');
}

add_menu_page('AMC - Customers', 'Customers', 'manage_options', 'customer_mng', 'customer_mng', '', 108);

function customer_mng() {
	require_once(TEMPLATEPATH.'/customers-admin.php');
}

add_menu_page('AMC - Configuration', 'Configuration', 'manage_options', 'amcconfig_mng', 'amcconfig_mng', '', 109);

function amcconfig_mng() {
	require_once(TEMPLATEPATH.'/amcconfig-admin.php');
}

/*** adds the Custom area for attribute filter ****/ 

    if (function_exists('register_sidebar')) {  
         register_sidebar(array(  
          'name' => 'AMC Filter',  
          'id'   => 'amc-fiter',  
          'description'   => 'AMC Filter area',  
          'before_widget' => '<div id="one" class="two">',  
          'after_widget'  => '</div>',  
          'before_title'  => '',  
          'after_title'   => ''  
         ));  
        }  
 // ** get The real Name From The  pa_ for attributes */ 
function attribute_name($pa_name)
 {
  
   return ucwords(( str_replace('_',' ',str_replace( 'pa_', '', $pa_name))) );

 }


/*** Load Theme Related Files ***/

// Load Theme Setup files
require_once ( $template_dir . '/includes/icore/theme-functions.php' );
// Load Theme Sidebars
require_once ( $template_dir . '/includes/icore/theme-sidebars.php' );
// Load Theme Filters
require_once ( $template_dir . '/includes/icore/theme-filters.php' );
// Load Theme Javascript
require_once ( $template_dir . '/includes/icore/theme-scripts.php' );
// Load Theme Setup files
require_once ( $template_dir . '/includes/icore/theme-taxonomies.php' );
// Load Theme Widgets
require_once ( $template_dir . '/includes/icore/theme-widgets.php' );
// Load Theme Setup files
require_once ( $template_dir . '/includes/icore/theme-customizer.php' );
// Load Google fonts
require_once ( $template_dir . '/includes/icore/fonts.php' );
// Load Theme Shortcodes
require_once ( $template_dir . '/ufo-shortcodes/shortcodes.php' );

require_once ( $template_dir . '/breadcrumbs.php' );


/*** Load WooCommerce Related Files ***/

//Load WooCommerce Config
if ( class_exists('woocommerce') ) {
	require_once ( $template_dir . '/includes/woocommerce/woocommerce-config.php');
}





// Add product categories to the "Product" breadcrumb in WooCommerce.
 
// Get breadcrumbs on product pages that read: Home > Shop > Product category > Product Name
add_filter( 'woo_breadcrumbs_trail', 'woo_custom_breadcrumbs_trail_add_product_categories', 20 );
 
function woo_custom_breadcrumbs_trail_add_product_categories ( $trail ) {
  if ( ( get_post_type() == 'product' ) && is_singular() ) {
    global $post;
    
    $taxonomy = 'product_cat';
    
    $terms = get_the_terms( $post->ID, $taxonomy );
    $links = array();
 
    if ( $terms && ! is_wp_error( $terms ) ) {
    $count = 0;
      foreach ( $terms as $c ) {
        $count++;
        if ( $count > 1 ) { continue; }
        $parents = woo_get_term_parents( $c->term_id, $taxonomy, true, ', ', $c->name, array() );
 
        if ( $parents != '' && ! is_wp_error( $parents ) ) {
          $parents_arr = explode( ', ', $parents );
          
          foreach ( $parents_arr as $p ) {
            if ( $p != '' ) { $links[] = $p; }
          }
        }
      }
      
      // Add the trail back on to the end.
      // $links[] = $trail['trail_end'];
      $trail_end = get_the_title($post->ID);
 
      // Add the new links, and the original trail's end, back into the trail.
      array_splice( $trail, 2, count( $trail ) - 1, $links );
      
      $trail['trail_end'] = $trail_end;
    }
  }
 
  return $trail;
} // End woo_custom_breadcrumbs_trail_add_product_categories()
 
/**
 * Retrieve term parents with separator.
 *
 * @param int $id Term ID.
 * @param string $taxonomy.
 * @param bool $link Optional, default is false. Whether to format with link.
 * @param string $separator Optional, default is '/'. How to separate terms.
 * @param bool $nicename Optional, default is false. Whether to use nice name for display.
 * @param array $visited Optional. Already linked to terms to prevent duplicates.
 * @return string
 */
 
if ( ! function_exists( 'woo_get_term_parents' ) ) {
function woo_get_term_parents( $id, $taxonomy, $link = false, $separator = '/', $nicename = false, $visited = array() ) {
  $chain = '';
  $parent = &get_term( $id, $taxonomy );
  if ( is_wp_error( $parent ) )
    return $parent;
 
  if ( $nicename ) {
    $name = $parent->slug;
  } else {
    $name = $parent->name;
  }
 
  if ( $parent->parent && ( $parent->parent != $parent->term_id ) && !in_array( $parent->parent, $visited ) ) {
    $visited[] = $parent->parent;
    $chain .= woo_get_term_parents( $parent->parent, $taxonomy, $link, $separator, $nicename, $visited );
  }
 
  if ( $link ) {
    $chain .= '<a href="' . get_term_link( $parent, $taxonomy ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $parent->name ) ) . '">'.$parent->name.'</a>' . $separator;
  } else {
    $chain .= $name.$separator;
  }
  return $chain;
} // End woo_get_term_parents()
}

###### -- User Registration adding Custom Fields to It ###########
/**
 * Add cutom field to registration form
 */

/*
add_action('register_form','show_first_name_field');
add_action('register_post','check_fields',10,3);
add_action('user_register', 'register_extra_fields');

function show_first_name_field()
{
?>
  <p>
  <label>Twitter<br/>
  <input id="twitter" type="text" tabindex="30" size="25" value="<?php echo $_POST['twitter']; ?>" name="twitter" />
  </label>
  </p>
<?php
}

function check_fields ( $login, $email, $errors )
{
  global $twitter;
  if ( $_POST['twitter'] == '' )
  {
    $errors->add( 'empty_realname', "<strong>ERROR</strong>: Please Enter your twitter handle" );
  }
  else
  {
    $twitter = $_POST['twitter'];
  }
}

function register_extra_fields ( $user_id, $password = "", $meta = array() )
{
  update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
}
*/

/*
$x = '["pa_adapter_type","pa_base_type","pa_bulb_type","pa_color","pa_color_temp","pa_decorative_model","pa_design","pa_diameter_inches","pa_dome_type","pa_energy_dollars_saved","pa_energy_star","pa_energy_start_dropdown","pa_feet_per_case","pa_finish_color","pa_flood_type","pa_gallons_per_minute","pa_gasket_type","pa_key","pa_lamp_type","pa_length","pa_list_items2","pa_list_title","pa_lumens","pa_manufacturer","pa_max_length","pa_mol_inches","pa_nightlight_option","pa_opening","pa_overall_length","pa_package_quantity","pa_packaging","pa_par_size_watt","pa_parsize","pa_personalizable","pa_power_factor","pa_price_per","pa_r_value","pa_rated_watts","pa_recommended_usage","pa_reflector_diameter","pa_size","pa_std_incand_equivalent","pa_threads","pa_top","pa_type_of_flow","pa_uniqueid","pa_units_per_case1","pa_usage","pa_water_sense","pa_width","pa_zoomable"]';

// echo '<pre>'; print_r(json_decode($x)); exit();

foreach(json_decode($x) as $k => $v) {
	// register_taxonomy($v, 'product', array());
}
*/

function isPSActiveClass() {
	$class = '';
	global $post;
	if($post->post_name == 'programs-services') {
		$class = 'active';
	}

	$postParentIdAry = get_post_ancestors($post->ID);
	if(!empty($postParentIdAry)) {
		$postParentObj = get_post($postParentIdAry[0]);
		if($postParentObj->post_name == 'programs-services') {
			$class = 'active';
		}
	}

	return $class;
}

// Global Declaration of the Filter Attributes //
$FilterSet = array(
	             "category-filter"  =>array(
	             	"compact-fluorescent-lighting",
	             	"light-fixtures",
	             	"cold-cathode",
	             	"led-lighting",
	             	"faucet-aerators-faucet-adaptors",
	             	"showerheads",
	             	"caulks-and-sealants",
	             	"weatherstrip",
	             	"hardware"
	             	),
				"search" =>array(
							// "rated-watts",
							// "std-incand-equivalent",
							// "lumens"
							),
						"compact-fluorescent-lighting" =>array(
							"rated-watts",
							"std-incand-equivalent",
							"lumens"
							),
						"light-fixtures"=>array(
							"rated-watts",
							"lumens",
							"finish-color"
							),
						"cold-cathode" => array(
							"rated-watts",
							"color"),
						"led-lighting" => array(
							"rated-watts",
							"lumens",
							"bulb-type",
							"design",
							"color"),
						"window-insulation"=>array(
							"size"),
						"air-conditioner-covers"=>array(
							"size","color"),
						"occupancy-sensors" =>array(
							"color"),
						"pipe-insulation" =>array(
							"size"
							//"r-value"
							),
						"faucet-aerators-faucet-adaptors"=>array(
							"gallons-per-minute",
							"adapter-type",
							"recommended-usage"
							),
						"outdoor-water-savers" =>array(
							"color",
							"decorative-model"),
						"showerheads" =>array(
							"color",
							"size",
							"gallons-per-minute",
							"adapter-type"),
						"caulks-and-sealants"=>array(
							"color"),
						"door-bottoms-and-thresholds" =>array(
							"size"),
						"door-sweeps" => array(
							"color","size"),
						"switch-outlet-sealers"=> array("gasket-type"),
						"weatherstrip"=>array(
							"color",
							"size"),
						"duct-mastic" =>array("size"),
						"tapes" =>array(
							"size"),
						"hardware"=>array(
							"size",
							"key"),
						"screws"=>array(
							"color"),
						"wall-patch-and-repair"=>array(
							"size",
							"opening",
							"package-quantity")
					
			);

// ////////////////////////
// ////////////////////////
// ////////////////////////
// ////////////////////////
// ////////////////////////
// Range attribute handling
include('range-functions.php');

//Home page image carousel admin interface
add_action( 'init', 'create_post_type' );
function create_post_type() {
 register_post_type( 'image_carousel',
  array(
   'labels' => array(
    'name' => __( 'Carousel' ),
    'singular_name' => __( 'Carousel' )
   ),
	  'taxonomies' => array('category'), 
	  'supports' => array('page-attributes','title', 'editor', 'thumbnail'),
  'public' => true,
  'has_archive' => true,
  )
 );
}

// Function to add custom fields in admin interface
function register_fields()
{
    register_setting('general', 'message_us_email_field', 'esc_attr');
    add_settings_field('message_us_email_field', '<label for="message_us_email_field">'.__('E-mail Address for Message Us Online Notifications' , 'message_us_email_field' ).'</label>' , 'print_message_us_email_field', 'general');
	
	register_setting('general', 'suggest_a_site_email_field', 'esc_attr');
    add_settings_field('suggest_a_site_email_field', '<label for="suggest_a_site_email_field">'.__('E-mail Address for Suggest A Site Notifications' , 'suggest_a_site_email_field' ).'</label>' , 'print_suggest_a_site_email_field', 'general');
	
	register_setting('general', 'contact_us_email_field', 'esc_attr');
    add_settings_field('contact_us_email_field', '<label for="contact_us_email_field">'.__('E-mail Address for Contact Us Notifications' , 'contact_us_email_field' ).'</label>' , 'print_contact_us_email_field', 'general');

	register_setting('general', 'req_a_quote_email_field', 'esc_attr');
    add_settings_field('req_a_quote_email_field', '<label for="req_a_quote_email_field">'.__('E-mail Address for Request a quote Notifications' , 'req_a_quote_email_field' ).'</label>' , 'print_req_a_quote_email_field', 'general');
}

function print_message_us_email_field()
{
    $value = get_option( 'message_us_email_field', '' );
    echo '<input type="text" id="message_us_email_field" name="message_us_email_field" class="regular-text ltr" value="' . $value . '" /><p class="description">This address is used for admin purposes, like Message Us Online Notifications.</p>';
}

function print_suggest_a_site_email_field()
{
    $value = get_option( 'suggest_a_site_email_field', '' );
    echo '<input type="text" id="suggest_a_site_email_field" name="suggest_a_site_email_field" class="regular-text ltr" value="' . $value . '" /><p class="description">This address is used for admin purposes, like Suggest A Site Notifications.</p>';
}

function print_contact_us_email_field()
{
    $value = get_option( 'contact_us_email_field', '' );
    echo '<input type="text" id="contact_us_email_field" name="contact_us_email_field" class="regular-text ltr" value="' . $value . '" /><p class="description">This address is used for admin purposes, like Contact Us Notifications.</p>';
}

function print_req_a_quote_email_field()
{
    $value = get_option( 'req_a_quote_email_field', '' );
    echo '<input type="text" id="req_a_quote_email_field" name="req_a_quote_email_field" class="regular-text ltr" value="' . $value . '" /><p class="description">This address is used for admin purposes, like Request a quote Notifications.</p>';
}

add_filter('admin_init', 'register_fields');