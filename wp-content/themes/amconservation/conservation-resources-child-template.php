<?php 
/* 
Template Name: Conservation Resources child template 
*/

define("CSS", "tps-programsnservices.css"); 

if(is_mobile()) {
	get_header('mobile-p-s');
} else {
	get_header('shop-amc');
}
global $amc_urls;
?>
<style type="text/css">
  #programs_services h2 {
    color: #000000;
    font-family: "open sans";
    font-size: 18px;
    font-weight: normal;
    margin: 20px 0 0 25px;
    width: 113%;
}
#programs_services h3 {

   color: #000000;
    font-family: "open sans";
    font-size: 13px;
    font-weight: bold;
    line-height: 24px;
    margin: 30px 0 0 21px;
    width: 90%;

  }
  #programs_services ul li {

  color: #3968B1;
    font-family: "open sans";
    font-size: 13px;
    font-weight: normal;
    line-height: 11px;
    list-style-type: disc !important;
    margin: 17px 0 -5px;
    width: 95%;

  }
</style>


<!-- Title and Breadcrumb  -->
      <div class="row headersession">      
        <div class="col-md-7"><h3 class="headerlabel">Conservation Resources<?php // the_title(); ?></h3></div>
        <div class="col-md-5 breadcrumbs inBread">
          <ol class="breadcrumb">
           <?php breadcrumbs_fmg();  ?>
          </ol>
        </div>    
<?php if(is_mobile()) { ?>
	<div class="col-md-4 back-btn back-page-com"> <a onClick="history.go(-1)"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/back_btn.png">Back </a></div>
<?php } ?>
    </div>
     <!--/  Ends Title and Breadcrumb   -->

<?php
$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
$url; 	
//$pageAttachments = wpba_get_attachments();
if($url !='') {
//if(!empty($pageAttachments) && is_array($pageAttachments)) {
?>
<div class="row"><div class="col-md-12 aboutbanner"><img src="<?php echo $url; //echo $pageAttachments[0]->guid; ?>"></div></div>
<?php
}
?>

<div class="row pns-content">      
          <div class="col-md-11 col-md-offset-1 pns-container">
            <div id="programs_services">
              <h1 class="pns-headertxt"><?php the_title(); ?></h1>
              <div id="tps_content">
              <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
              <?php endwhile; endif; ?>
            </div> 
            </div>
           
         <p class="contactdetail-txt">For more information on our <?php the_title(); ?>, please consult with an account representative by calling <b><?php echo $amc_urls['contact_number']; ?></b> or submitting a <span class="contact-blue"> <a href="<?php echo $amc_urls['contact_us'];?>"> “Contact Us”</a></span> form.</p> 

<a class="back_btn" href="<?php echo $amc_urls['conservation-resources']; ?>"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/back_btn.png"><span class="back-txt">Back</span></a>

          

  
</div> 
      </div> 


<script type="text/javascript">
var siteURL = '<?php echo $amc_urls['home']; ?>';
var siteAssetURL = '<?php echo get_template_directory_uri(); ?>/assets/';
</script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/mediaquery.js"></script>
<script type="text/javascript">
   
  // var html = $("#tps_content").html();
  // console.log(html);


</script>
<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>