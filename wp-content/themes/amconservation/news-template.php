<?php 
/* 
Template Name: News Template  
*/ 
define("CSS", "news_amc_style.css");

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}

// To get the latest news
$mainContentNewsItems = new WP_Query(array(
        'post_type' => 'news',
        'news-category' => 'maincontent',
        'posts_per_page' => 1,
));
//echo "<pre>"; print_r($mainContentNewsItems->posts[0]->post_content);
?>
<div class="row">      
        <div class="col-md-12 aboutbanner"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/news_banner.jpg"></div>  
    </div>

 <!-- Middle Mennu  -->
         <div class="row menu_pos">
			<div class="leftarrow-inner SlideOff"></div>
             <div class="aboutmenu-container">
               <?php require_once('middle-menu.php'); ?>
             </div>
    </div>
    <!--    content area start      -->
 </div>
 <div class="container">
       <div class="row">
          <!--left content -->
<!-- Dynamic  contents comes From the  AdminSide  -->   

<!--ight content -->

          <!--left content -->
        <div class="col-md-7">
          <!--Trademarks-->
        <div class="row"> <div class="col-md-11 col-md-offset-1 cs-top-spacer-1">
         <h2 class="headerlabel1">Press Releases </h2> 
         <h3>Welcome to the AM Conservation Group Press Center</h3>            
         </div><p>&nbsp;</p> </div>
  		<!--Trademarks -->
     
     <!--content below heading -->
 <!--<div class="row"> <div class=" col-md-10 col-md-offset-1 ">
         <p class="cs-content-1 page-spacing ">
         <span class="cs-content-heading2"> Hello AM Conservation Group Acquires Energy and Water Conservation 
      Products Division from Niagara Conservation
      </span></p>    
      <p class="cs-content-2 page-spacing"> <i>Leading Water and Energy Conservation Companies Unite to Best Serve Utility, Government Agency Markets</p>     
         </i></div> </div>
          <div class="row"> <div class=" col-md-10 col-md-offset-1 ">
         <p class="cs-content-2 page-spacing"><strong>CHARLESTON, S.C. (Jan. 7, 2014)</strong> – AM Conservation Group (AMCG) announced today that it has 
acquired the energy and water conservation products division from its longtime former competitor 
Niagara Conservation, Inc., uniting forces to provide the broadest and most competitive product line in 
the industry. Niagara Conservation, Inc. will continue to operate their high-efficiency toilet business 
division.</p>

<p class="cs-content-2 page-spacing">“We are a long standing industry provider of energy and water conservation products and services and 
believe there are many synergies to be gained by uniting our two great companies,” said Todd Recknagel, 
CEO of AM Conservation Group. “For over three decades, Niagara has been known as a strong water 
conservation company and we are excited to gain access to offering their exceptional water and energy 
conservation product line, enabling us to market the best products from both companies.”</p>

<p class="cs-content-2 page-spacing">“With so many innovative conservation programs in action, we felt it was necessary to create an online resource that recognizes those making an impact in preserving energy for future generations,” said Kristi Mailloux, Chief Marketing Officer of AM Conservation Group, Inc. “The online news forum will highlight states, municipalities, businesses and individuals that go above and beyond with conservation practices, programs and policies. We hope to inspire and encourage others to follow their lead.” </p>

<p class="cs-content-2 page-spacing">According to Recknagel, Niagara decided to sell off a portion of its business after meeting with AMCG’s 
ownership. He said the Niagara Conservation owners felt it made “perfect sense” to combine forces in 
serving the utility company and governmental agency markets to become the undisputed industry leader. </p>

<p class="cs-content-2 page-spacing">The combination of the two companies allows customers to choose the best product option selection 
from both brands, as well as give the newly formed entity the future ability to ship both AMCG and 
Niagara Conservation branded products from the west and east coasts, allowing more efficient and faster 
shipping.</p>  

<p class="cs-content-2 page-spacing">AM Conservation Group started in 1989 and quickly became known as a full service source for high 
quality, competitively priced water and energy saving products. Over the years, AMCG has supplied 
products and services for some of the largest and most comprehensive conservation programs in U.S. 
history.</p>

<p class="cs-content-2 page-spacing">Originally founded in New Jersey in 1977, the now Fort Worth, Texas based Niagara Conservation has 
earned a solid reputation for delivering water and energy conservation products that maximize efficiency 
while still maintaining optimal performance. The Niagara Conservation owners will continue to build the 
high-efficiency, toilet-related business and focus their energies on growing that market. </p>

<p class="cs-content-2 page-spacing">As part of the acquisition, AM Conservation Group will bring many key Niagara Conservation employees 
to its team and has already taken steps to integrate them into their company and culture.</p>

<p class="cs-content-2 page-spacing">“We felt there were many strong synergies to be gained by providing AM Conservation Group with the 
rights to produce and distribute Niagara branded products,” said Niagara Conservation Founder and 
President William Cutler. “Customers will benefit tremendously as the newly expanded AM Conservation 
Group team will now be the premier provider for energy and water efficiency products, services, and 
programs.” </p>

 <p class="cs-content-2 page-spacing"><span class="cs-content-heading3">About AM Conservation Group, Inc. </span><br></p>
<p class="cs-content-2 page-spacing">Established in 1989, AM Conservation Group, Inc. is the leading expert in energy and water efficiency solutions designed to provide maximum value for a client’s investment. Since its inception, the company has facilitated some of the largest and most effective energy preservation programs in U.S. history with utility companies, program managers and government agencies.  AM Conservation’s underlying mission is to increase the number of energy and water conservation programs on a nation-wide scale.  Founded on principles of customer service, AM Conservation provides its clients with personalized conservation services or programs tailored to meet their individual needs. To support the numerous programs and services upheld by AM Conservation, the company specializes in the development, manufacturing and distribution of over 700 professional-grade products for the conservation industry, ranging from energy efficient LED light bulbs to custom-designed, water saving showerheads. For more information, visit <a href="http://www.amconservationgroup.com.">http://www.amconservationgroup.com. </a></p>

 <p class="cs-content-2 page-spacing"><span class="cs-content-heading3">About Niagara Conversation. </span><br></p>
<p class="cs-content-2 page-spacing">With a history of more than 35 years of quality and innovation, Niagara Conservation has earned a 
reputation as a premier manufacturer of high-efficiency water and energy conservation products. 
Niagara is a leading developer of conservation solutions for plumbing professionals, utility companies, 
government leaders, energy management officials and environmentally conscious consumers. Founder 
and President William Cutler established Niagara in 1977. Headquartered in Fort Worth, Texas, the 
company has satellite offices around the world.</p>

<p class="cs-content-2 page-spacing"><strong>Media Contact:</strong> Natalee Walker, Fishman Public Relations (847) 945-1300 or <a href="mailto:nwalker@fishmanpr.com">nwalker@fishmanpr.com</a></p>
          
         </div> </div>-->
          <!-- Dynamically  loads latest news  -->  
        <div class="row">
            <div class=" col-md-10 col-md-offset-1 ">
                    <p class="cs-content-1 page-spacing">
                    <span class="cs-content-heading2"><?php  echo $mainContentNewsItems->posts[0]->post_title; ?></span>

                    </p>
            </div>
        </div>
        <?php  echo $mainContentNewsItems->posts[0]->post_content; ?>
          
      
          <!--<div class="row"> <div class=" col-md-4  col-md-offset-1  div-line2 "> </div></div>  

            <div class="row"> <div class=" col-md-10 col-md-offset-1 ">
              <p class="cs-content-1 page-spacing"><span class="cs-content-heading2">
             New Online Think Tank Explores Best Practices in Energy and Water Conservationt </span><br>
          <a href="<?php echo get_bloginfo('url').'/amcnews/new-online-think-tank-explores-best-practices-in-energy-and-water-conservation/'; ?>"><span style="color:#e08a12; text-decoration:underline ">Read More</span></a></p>            
                   </div> </div>          
   <div class="row"> <div class=" col-md-4  col-md-offset-1  div-line2 "> </div></div>  
    <div class="row"> <div class=" col-md-10 col-md-offset-1 ">
    <p class="cs-content-1 page-spacing"><span class="cs-content-heading2">
   The ECO Center™ Offers Retail Consumers a One-Stop Station to Save Time, Money, 
Energy, Water -- and the Environment </span><br><a href="<?php echo get_bloginfo('url').'/amcnews/the-eco-center-offers-retail-consumers-a-one-stop-station-to-save-time-money-energy-water-and-the-environment/'; ?>"><span style="color:#e08a12; text-decoration:underline ">Read More</span></a></p>            
         </div> </div>           

 <div class="row"> <div class=" col-md-4  col-md-offset-1  div-line2 "> </div></div>  
  <div class="row"> <div class=" col-md-10 col-md-offset-1 ">
    <p class="cs-content-1 page-spacing"><span class="cs-content-heading2">
   Dish Squeegee™ Named 2011 Housewares Design Awards Finalist </span><br><a href="<?php echo get_bloginfo('url').'/amcnews/dish-squeegee-named-2011-housewares-design-awards-finalist/'; ?>"><span style="color:#e08a12; text-decoration:underline ">Read More</span></a></p>            
         </div> </div> 
  
          <div class="row"> <div class=" col-md-4  col-md-offset-1  div-line2 "> </div></div> --> 
         
         <!--content below heading -->
         <?php
         global $post;
         $curNewsId = $mainContentNewsItems->posts[0]->ID;
         //echo "<pre>"; print_r($curNewsId);
         $remainingNewsItems = new WP_Query(array(
	 'post_type' => 'news',
	 'news-category' => 'maincontent',
	 'post__not_in' => array($curNewsId),
         ));
         foreach($remainingNewsItems->posts as $remainingNewsItems) {
?>
<div class="row">
	<div class=" col-md-10 col-md-offset-1 ">
		<p class="cs-content-1 page-spacing"><span class="cs-content-heading2"><?php echo $remainingNewsItems->post_title; ?></span><br><a href="<?php echo get_permalink($remainingNewsItems->ID); ?>"><span style="color:#e08a12; text-decoration:underline ">Read More</span></a></p>
	</div>
</div>
<div class="row "><div class=" col-md-4  col-md-offset-1  div-line2 "></div></div> 
<?php
}
?>
  
             
   
   <div class="cs-top-spacer-1 hidden-xs hidden-sm"></div>
   

</div>
<!--left content -->

<!--mid content -->

<!--<div class="col-md-1 divider hidden-xs hidden-sm"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/divider.png"></div>-->



<div class="col-md-1 new-mid-div hidden-xs hidden-sm"> 

<div class="news-top-spacer2"></div> 

<div class="row"><div class=" col-md-2 col-md-offset-1 ">
<div class="col-md-2 divider hidden-xs hidden-sm"><img class="line-divider1" src="<?php echo get_template_directory_uri(); ?>/assets/images/news_bg1.png"></div>
</div></div>


<div class="row"> <div class=" col-md-2 col-md-offset-1 ">
  
         <div class="col-md-2 divider hidden-xs hidden-sm"><img class="line-divider2" src="<?php echo get_template_directory_uri(); ?>/assets/images/news_bg2.png"></div>
         </div> </div>
         
  <div class="row"><div class=" col-md-2 col-md-offset-1 ">
<div class="col-md-2 divider hidden-xs hidden-sm "><img class="line-divider3" src="<?php echo get_template_directory_uri(); ?>/assets/images/news_bg3.png"></div>
</div></div>       
         
         </div>
<!--mid content -->

<!--right content -->
<div class="col-md-4 col-lg-3">

<div class="row hidden-xs hidden-sm "> <div class=" col-md-2 "> </div></div>
<div class=""> <div class=""> <div class="news-top-spacer hidden-xs hidden-sm "></div>
<div class=""> <div class="">
 <h3 class="headerlabel2">News Articles and Videos </h3> 

 <?php
$newsVideos = get_posts(array( 
  'post_type' => 'news_video',
  'post_status' => 'publish'
));

foreach($newsVideos as $newsVideo) {

$newsVideoAttachments = wpba_get_attachments($newsVideo);
if(!empty($newsVideoAttachments) && is_array($newsVideoAttachments)) {

// Get the video/image attachment
foreach($newsVideoAttachments as $newsVideoAttachment) {
  if(strpos($newsVideoAttachment->post_mime_type, 'video') !== false) {
    $videoAttachment = $newsVideoAttachment;
  }
  if(strpos($newsVideoAttachment->post_mime_type, 'image') !== false) {
    $imageAttachment = $newsVideoAttachment;
  }
}
?>
<div id="video-container">
  <p><?php echo $newsVideo->post_title;?></p>
  <!--<video width="100%" height="100%" src="<?php echo $videoAttachment->guid; ?>" type="<?php echo $videoAttachment->post_mime_type; ?>" id="player1" poster="<?php echo $imageAttachment->guid; ?>" controls="controls" preload="none"></video>-->
       <div class="video-container"> <video  width="640" height="360" style="width: 100%; height: 100%;" src="<?php echo  $videoAttachment->guid; ?>" type="<?php echo $videoType; ?>"
                                    id="player1" poster="<?php echo $imageAttachment->guid;?>"
                                    controls="controls" preload="none">
                                
                <object type="application/x-shockwave-flash" data="<?php echo get_template_directory_uri(); ?>/assets/js/flashmediaelement.swf">
                <param name="movie" value="<?php echo get_template_directory_uri(); ?>/assets/js/flashmediaelement.swf" />
                <param name="flashvars" value="controls=true&amp;file=<?php echo  $videoAttachment->guid; ?>" />
                <img src="<?php echo $imageAttachment->guid;?>" alt="No video playback capabilities" />
              </object>
              </video>
              </div>
</div>
<p></p>
<?php
}

?>  
<?php } ?>

<?php
	$page_id = 5538;
	$page_data = get_page( $page_id ); 
	echo apply_filters('the_content', $page_data->post_content); 
	
?>
       <!--  <p class="cs-content-2 page-spacing bt-line"><span class="cs-content-heading3">  <a href="http://www.energymanagertoday.com/energy-efficiency-taught-to-kids-nationwide-094893/" target="_blank">Energy Efficiency Taught to Kids Nationwide</a> 
</span><br>August 23, 2013<br> <br>
Energy Manager Today interviews Todd Recknagel, CEO of AM Conservation Group, Inc. </p>  
         
         </div>
       
         </div>
         
         

  
  

         <p class="cs-content-2 page-spacing bt-line"><span class="cs-content-heading3">  <a href="http://www.amconservationgroup.com/media/othermedia/E_Source_-_Profiling_Residential_Smart_Strips.pdf " target="_blank">E Source - Profiling Residential Smart Strips: <br> Is Your Program Smart?</a> </span><br> 
     July 25, 2013 : <br> <br> E Source, Alexandra Behringer - Associate Research Director  
 </p>            
      
         
         
         
     
         <p class="cs-content-2 page-spacing bt-line"><span class="cs-content-heading3">     <a href="http://www.amconservationgroup.com/media/specsheets/USNews.pdf" target="_blank">Why Your Energy Company Wants You to Use Less Energy</a></span><br> 
    April 2, 2013 : <br> <br> U.S. News &amp; World Report interviews Todd Recknagel, CEO of AM Conservation Group, Inc.

 </p>            
        
            
       
       
       
    
         <p class="cs-content-2 page-spacing bt-line"><span class="cs-content-heading3">    <a title="Wall Street Journal Feb 2013" href="http://www.amconservationgroup.com/media/specsheets/2013_02_12_WallStreetJournal.pdf" target="_blank">Businesses Weigh Response If New Climate Rules Come</a>
</span><br>February 12, 2013: <br> <br>
The Wall Street Journal interviews Todd Recknagel, CEO of AM Conservation Group, Inc.</p>            
        
         
         


         <p class="cs-content-2 page-spacing bt-line"><span class="cs-content-heading3">   <a href="http://www.foxbusiness.com/personal-finance/2012/10/17/how-to-reduce-your-utility-bills-this-winter/" target="_blank">How to Reduce Your Utility Bills This Winter</a></span><br> 
     October 17, 2012:   <br> <br> FOXBusiness interviews Todd Recknagel, CEO, on ways to lower utility bills during the winter. 
 </p>            
        
         
         
         
         
     
         <p class="cs-content-2 page-spacing bt-line"><span class="cs-content-heading3">     <a href="http://www.homechannelnews.com/article/am-conservation-group-rolls-out-simply-conserve-line" target="_blank">AM Conservation Group rolls out Simply Conserve line</a></span><br> 
    September 11, 2012: <br> <br>Home Channel News introduces Simply Conserve brand.

 </p>            
        
            
            
         
  
         <p class="cs-content-2 page-spacing bt-line"><span class="cs-content-heading3">   <a href="http://www.realenergywriters.com/ee-podcast/2012/08/16/ee-is-hot-when-temperatures-are-high/" target="_blank">EE is Hot when Temperatures are High</a>
</span><br>August 16, 2012: <br> <br>
Lisa Cohn of RealEnergyWriters.com interviews Todd Recknagel, CEO of AM Conservation Group.</p>            
        








         <p class="cs-content-2 page-spacing bt-line"><span class="cs-content-heading3">  <a href="http://www.workingmother.com/family-time/daddy-board-saying-thanks-dad" target="_blank">Daddy on Board: Saying Thanks to Dad</a></span><br> 
     March 4, 2011:  <br> <br> Working Mother recommends the Dish Squeegee  as a great chore helper for dads and kids. 
 </p>            
         
         
       
         
      
         <p class="cs-content-2 page-spacing bt-line"><span class="cs-content-heading3"> <a href="http://giftandhometoday.blogspot.com/2011/02/eco-friendly-kitchen-gadget-promotes.html" target="_blank">Eco-friendly Kitchen Gadget Promotes Water Savings</a></span><br> 
   February 10, 2011:  <br> <br>"Use the squeegee to push leftovers into a garbage pail or disposal (or better yet, compost nonmeat waster)." - Gift &amp; Home Today

 </p>            
         
         
         
         
         
     
         
         
         
      
         <p class="cs-content-2 page-spacing bt-line"><span class="cs-content-heading3"> <a href="http://blog.nj.com/hg_impact/print.html?entry=/2010/12/dish_squeegee_saves_water.html" target="_blank">Dish Squeegee Saves Water</a></span><br> 
   December 27, 2010:  <br> <br>

 </p>            
        
         
          
         
      
         <p class="cs-content-2 page-spacing bt-line"><span class="cs-content-heading3"> <a href="http://www.gourmetretailer.com/top-story-housewares_design_awards_finalists_announced-9655.html" target="_blank">2011 Housewares Design Awards Finalists Named</a></span><br> 
  
November 29, 2010: <br> <br>Dish Squeegee™ named as Finalist 

 </p>            
         
         
          
         
      
         <p class="cs-content-2 page-spacing bt-line"><span class="cs-content-heading3"> <a href="http://www.moneypit.com/print/6607" target="_blank">Apartment and House Insulation Tips - Heat It Up!</a></span><br> 
 November 22, 2010:  <br> <br>Money Pit recommends multiple AM Conservation Group products for insulating and saving around the home. 

 </p>            
         
         
          
         
      
         <p class="cs-content-2 page-spacing bt-line"><span class="cs-content-heading3"> <a href="http://www.realsimple.com/work-life/money/saving/have-on-winterizing-home-00000000045337/index.html" target="_blank">How to Save on Winterizing Your Home</a></span><br> 
   November 2010: <br> <br>Real Simple magazine recommends the Furnce Filter Whistle to alert you when your filter becomes clogged. 

 </p>            
        
         
          
         
     
         <p class="cs-content-2 page-spacing bt-line"><span class="cs-content-heading3"> <a href="http://www.homechannelnews.com/article/iha-design-event-shows-innovation" target="_blank">IHA Design Event Shows Off Innovation</a></span><br> 
  
October 25, 2010:  <br> <br>Home Channel News highlights the Plug Guard® and Switch Plate Energy Save (F/K/A Universal Wall Plate Thermometer). 

 </p>            
         
         
          
         
      
         <p class="cs-content-2 page-spacing bt-line"><span class="cs-content-heading3"> <a href="http://www.allyou.com/budget-home/organizing-cleaning/cheap-kitchen-gadgets-tools/dishwasher-aid" target="_blank">20 Kitchen Gadgets Under $20</a></span><br> 
   September 24, 2010: <br> <br>Home Channel News highlights the Plug Guard® and Switch Plate Energy Save (F/K/A Universal Wall Plate Thermometer). 

 </p>            
         
         
        
         
         
         
      
         <p class="cs-content-2 page-spacing bt-line"><span class="cs-content-heading3"> <a href="http://www.housewares.org/show/exhibit/dir/files/780043965_PRESS%20RELEASE-%202010%20Design%20Defined%20Honorees.pdf" target="_blank">26 Products Named 2010 Design Defined Honoree At International Home + Housewares Show</a></span><br> 
April 14, 2010 <br> <br>

 </p>            
         
         
         
           
      

         <p class="cs-content-2 page-spacing bt-line"><span class="cs-content-heading3"> <a href="http://inventorspotforum.com/viewtopic.php?f=20&amp;t=3867" target="_blank"> 4 inventions to Market in 1 Year - by 1 Inventor</a></span><br> 

January 19, 2010  <br> <br>
 </p>           -->


 
         


           
                   </div></div>

      </div>           
      


 </div>
<!--ight content -->
        
        </div>

<script type="text/javascript">
var siteURL = '<?php echo $amc_urls['home']; ?>';
var siteAssetURL = '<?php echo get_template_directory_uri(); ?>/assets/';
</script>  
      <script>
        
// make an array for the mediaelement players
//mediaElementPlayers = new Array();
/*
$('audio,video').mediaelementplayer(
{
    
    success: function (mediaElement, domObject) {
          // add this mediaelement to the mediaElementPlayers array
          mediaElementPlayers.push(mediaElement);
          // bind the play event to the pauseAllPlayers function
          mediaElement.addEventListener('play', function(e) {
              pauseAllPlayers(e.target); }, false); }
});
*/


// iterate through the mediaElementPlayers array, pause all players except the one that triggered the event.
/* function pauseAllPlayers(currentPlayer){
for(i=0; i<mediaElementPlayers.length; i++){
    if(mediaElementPlayers[i] != currentPlayer){
        mediaElementPlayers[i].pause();
    }
}
} */
$('video,audio').mediaelementplayer(/* Options */);
  </script>  
  
    
   <!-- / content area Ends      -->
<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>