<?php global $theme_shortname, $theme_options, $amc_urls; ?>
<?php $theme_options = get_option( $theme_shortname . '_options' ); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico">
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;
	wp_title( '|', true, 'right' );
	bloginfo( 'name' );
	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
	echo " | $site_description";
	?></title>

<!-- Bootstrap core CSS -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/flexislider.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/<?php echo CSS; ?>" rel="stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/rwd_styles.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/rwd_styles_mobiles.css" rel="stylesheet">
<script type="text/javascript">
var baseURL = '<?php echo get_template_directory_uri(); ?>';
</script>
<!-- Custom styles for this template -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/html5shiv.js">
  </script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/respond.min.js">
  </script>
  <![endif]-->

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/parsley.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-flexislider.js"></script>
<script src="http://jarallax.com/download/jarallax-0.2.js" type="text/javascript"></script>

<style type="text/css"></style>
<!-- Google Tag Manager --> 
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WQJWJ5" 
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> 
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': 
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], 
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); 
		})(window,document,'script','dataLayer','GTM-WQJWJ5');</script> 
		<!-- End Google Tag Manager -->
		<!-- Start of Async HubSpot Analytics Code -->
  <script type="text/javascript">
    (function(d,s,i,r) {
      if (d.getElementById(i)){return;}
      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/448562.js';
      e.parentNode.insertBefore(n, e);
    })(document,"script","hs-analytics",300000);
  </script>
<!-- End of Async HubSpot Analytics Code -->
</head>

<body>
<div class="container homepg" >
  <div class="row header">
    <div class="col-md-4 col-xs-12 logo"> <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png"></a> </div>
    <!--  <div class="col-md-2"></div> -->
    <div class="col-md-8 col-xs-12">
      <div class="topnav-right">
	<a class="menu1" href="#" onclick="return false;"> <span class="top-icon"></span> <span class="toplink1"><?php  echo $amc_urls["contact_number"]; ?></span> </a> 
	<a class="menu2" href="<?php echo $amc_urls['message_us_online']; ?>"> <span class="top-icon2"></span> <span class="toplink2">Message Us Online</span> </a> 
<?php
// Check for login user
if(userIsLoggedIn()) {
	$loggedInUser = getLoggedInUser();
?>
	<a class="menu3" href="<?php echo $amc_urls['logout']; ?>"> <span class="top-icon3"></span> <span class="toplink3">Log Out</span> </a> 
<?php
} else {
?>
	<a class="menu3" href="<?php echo $amc_urls['register_login']; ?>"> <span class="top-icon3"></span> <span class="toplink3">Register/Login</span> </a> 
<?php } ?>

	<a class="collapsableMenu"> <span class="top-icon4"></span> <span class="toplink4">Menu</span> </a>
          </div>
		  <!-- Search Menu @ The Top -->
		 
	<form action="<?php echo site_url(); ?>" method="get" name="headerSearchFrm">
		  <div class="top-search search js_handleSearch">
			<input type="hidden" name="post_type" value="product"  /> 
			<span class="search-arrow"></span>
		  </div>
		  <div class="search-panel">
			<input type="text" name="s" maxlength="100" class="search-form-field js_searchBox" value="<?php echo get_search_query(); ?>" placeholder="TYPE HERE TO SEARCH">
		  </div>
	  </form>
		
  <!-- Ends Search Menu @ The Top -->  
    </div>
  </div>
  <div class="row ">
       
    <div class="mainnav-home">
        <a href="<?php echo $amc_urls['programs_services'];?>" class="col-md-1 col-xs-1 prog-link">
          Programs & Services
        </a>
      <div class="col-md-10 col-xs-10 energymenu">
<?php 
if (function_exists('fmg_menu') && fmg_var('category-menu')) {
	fmg_menu('product_cat');
} else { 
	icore_nav_menu('primary-menu', 'nav sf');
}
?>
      </div>
    </div>
<!-- Page title and Breadcrumbs @ The Top -->  
</div>    
<div class="row headersession head-com">
	<div class="col-md-7"><h3 class="headerlabel">Programs & Services</h3></div>
	<div class="col-md-4 back-btn back-page-com"> <a onClick="history.go(-1)"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/back_btn.png">Back </a></div>
</div>
<!-- Ends Page title and Breadcrumbs @ The Top -->