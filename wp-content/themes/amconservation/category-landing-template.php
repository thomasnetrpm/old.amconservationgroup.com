<?php 
/* 
Template Name: Case Studies  template 
*/ 

define("CSS", "case_studies_amc_style.css");

global $amc_urls;

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}
?>
  <?php 

   function k99_image_link_void( $content ) {
      $content =
        preg_replace(
            array('{<a(.*?)(wp-att|wp-content\/uploads)[^>]*><img}','{ wp-image-[0-9]*" /></a>}'), "", $content
        );
    return $content;
}



?>
<div class="row">      
        <div class="col-md-12 aboutbanner"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/case_studies_pic1.jpg"></div>  
    </div>

 <!-- Middle Mennu  -->
        <div class="row menu_pos">
        <div class="leftarrow-inner SlideOff"></div>
             <div class="aboutmenu-container">
                 <?php require_once('middle-menu.php'); ?>
             </div>
    </div>
     <!--/  Ends Middle Mennu  -->


    <div class="row careerscontent">
        <div class="col-md-12">
         
        <div class="row"> <div class="col-md-offset-1 cs-top-spacer-1" >
              
         </div> </div>

     
     <div class="cs-content-align1">

        <div class="row"> <div class=" col-md-12  " >
        
<!-- Dynamic  contents comes From the  AdminSide  -->   
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<?php the_content(); ?>
<?php endwhile; endif; ?>

       <!--  <div ><div class=" cs-top-spacer-1"> <p class="cs-content-heading2 ">  Downloads</p> </div></div>-->
<?php
/*
       <div  > <div class=""> <div class="cs-drop-shadow" >  <div class=" col-md-1 col-md-offset-1">
	   
	   <a target="_blank" href="<?php echo $amc_urls['casestudies_link']; ?>">
	   <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pdf-icon_33.png" width="19" height="21" />
	   </a>
	   
	   </div>  <div class=" col-md-offset-2 fileformat">  <a target="_blank" href="<?php echo $amc_urls['casestudies_link']; ?>">Case Studies</a></div>
 </div> </div></div>
*/
?>
<!--
<?php
$caseStudyPDFS = wpba_get_attachments();
if(!empty($caseStudyPDFS)) {
	foreach($caseStudyPDFS as $caseStudyPDF) {
?>
<div>
	<div class="">
		<div class="cs-drop-shadow" >
			<div class=" col-md-1 col-md-offset-1">
				<a target="_blank" href="<?php echo $caseStudyPDF->guid; ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pdf-icon_33.png" width="19" height="21" /></a>
			</div>
			<div class=" col-md-offset-2 fileformat">
				<a target="_blank" href="<?php echo $caseStudyPDF->guid; ?>"><?php echo apply_filters( 'the_title' , $caseStudyPDF->post_title ); ?></a>
			</div>
		</div>
	</div>
</div><br />
<?php
	}
}
?>-->
         
         </div> </div>
         

  </div>
         

  

   
   <div class="cs-top-spacer-1"></div>
   

</div>
        </div>    
    <?php /*
                $attachments = get_children( array('post_parent' => get_the_ID(), 'post_type' => 'attachment', 'post_mime_type' => 'application/pdf') );
                    if ( $attachments ) { 
                    foreach($attachments as $key => $pdf) { 
                     $attached_pdf= wp_get_attachment_metadata ( $pdf->ID);
                     $upload_dir = wp_upload_dir();
                     $link =  $upload_dir['baseurl'] . "/" . $attached_image['file']; 
                      } }
                     */
                     ?>



<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>