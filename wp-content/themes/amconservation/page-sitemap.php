<?php 
/* 
Template Name: Sitemap
*/

define("CSS", "sitemap.css"); 

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}
?>
<?php 
global $theme_shortname;
$location = icore_get_location();   
$meta = icore_get_multimeta(array('Subheader'));
?>

<div class="row bgfill">
	<div class="topspacer">
		<div class="content">
<ul>
<?php
// Add pages you'd like to exclude in the exclude here
$WpPages = get_pages(array(
	'exclude' => '4,5,6,7,8,9,10,11,12,13,14,98,337,332,302,443,453,3978,3975,3407,461,4059',
	'title_li' => '',
));

$sitemapArray = array();
foreach($WpPages as $linkItem3) {
	$sitemapArray[] = $linkItem3->post_title;
    $Link_array[$linkItem3->post_title] = get_permalink($linkItem3->ID);
}
?>
</ul>

<ul>
<?php
// Display the categories of products
$prodCat = get_terms('product_cat', array(
	'hide_empty' => 0,
	'parent' => 0,
	'hierarchical'  => true,
	'orderby' => 'id',
	'order' => 'asc'
));



foreach($prodCat as $linkItem) {
    //$sitemapArray[] = '<li><a href="'.esc_url(get_term_link($linkItem->slug, 'product_cat')).'" title="">'.$linkItem->name.'</a></li>';
    $sitemapArray[] = $linkItem->name;
    $Link_array[$linkItem->name] = get_term_link($linkItem->slug, 'product_cat');
    
    $prodSubCat = get_terms('product_cat', array(
		'hide_empty' => 0,
		'parent' => $linkItem->term_id,
		'hierarchical'  => true,
		'orderby' => 'id',
		'order' => 'asc'
	));
        foreach($prodSubCat as $linkItem2) {
            
            //$sitemapArray[] = '<li><a href="'.esc_url(get_term_link($linkItem2->slug, 'product_cat')).'" title="">'.$linkItem2->name.'</a></li>';
            $sitemapArray[] = $linkItem2->name;
            $Link_array[$linkItem2->name] = get_term_link($linkItem2->slug, 'product_cat');
            
		
	}
}

$sitemapArray = array_unique($sitemapArray);
$lastChar = '';
sort($sitemapArray, SORT_STRING | SORT_FLAG_CASE); 
echo '<div class="content-byAlphabet-text thin">';
foreach($sitemapArray as $key=>$val) { 
  $char = trim(strip_tags($val[0])); //first char

  if ($char !== $lastChar) {
    if ($lastChar !== '')
      echo '<br>';

    echo '<span class="sitemap-alp">'.strtoupper($char).'</span>'; //print A / B / C etc
    $lastChar = $char;
  }
 echo '<a href="'.$Link_array[$val].'">'.$val.'</a>';
}
?>
    </div>
</ul>
                
		</div>
	</div>
</div>

<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>