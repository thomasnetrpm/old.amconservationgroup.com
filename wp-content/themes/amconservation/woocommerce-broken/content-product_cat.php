<?php
/**
 * The template for displaying product category thumbnails within loops.
 * Overrided 
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $woocommerce_loop,$woocommerce,$wpdb;

//print_r($category);

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
// Increase loop count
?>

<?php 

if($category->parent == 3077){
	include "hotbuys.php";	
} else {

        /*  $Reorder_Check =  $wpdb->get_results("SELECT COUNT(id) as C FROM wp_reorder_post_rel WHERE category_id ='".$category->term_id."' ");
          if($Reorder_Check[0]->C == 0) {

          $args = array(
                      'orderby' => 'menu_order', 
                      'order' => 'asc', 
                      'post_type' => 'product',
                      'posts_per_page' => 1,
                      'product_cat' => $category->slug,
                      'hierarchical' => true); 

        } else {
          $args = array(
                      'post_type' => 'product',
                      'posts_per_page' => 1,
                      'orderby' => 'id', 
                      'order' => 'asc',
                      'product_cat' => $category->slug,
                      'hierarchical' => true); 
          
            }
        $loop = new WP_Query( $args );
        unset($Reorder_Check);
    */
        //echo $loop->post->ID.'<br/>' ;
?>
 <div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
     <div class="image-container">          
        <div class="gallery1">
        <?php
          $Reorder_Check =  $wpdb->get_results("SELECT COUNT(id) as C FROM wp_reorder_post_rel WHERE category_id ='".$category->term_id."' ");
          if($Reorder_Check[0]->C == 0) {
             $args = array(
                      'orderby' => 'menu_order', 
                      'order' => 'asc', 
                      'post_type' => 'product',
                      'posts_per_page' => 1,
                      'product_cat' => $category->slug,
                      'hierarchical' => true); 
           $loop = new WP_Query( $args );  
          // NO item in Reorder Table //
          if ( $loop->have_posts() ) :
            woocommerce_product_loop_start();
            while ( $loop->have_posts() ) : $loop->the_post();
          echo '<a href="'.get_term_link( $category->slug, 'product_cat' ).'">'.get_the_post_thumbnail($loop->post->ID, 'shop_catalog',array('class'  => "gallery-image", 'alt' => $category->name)).'<span class="view-category">VIEW PRODUCTS</span></a>';

            endwhile;
            woocommerce_product_loop_end();
            else:
          echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="100px" height="100px" class="gallery-without-image" />'; 
            endif;
          }
          else
          {
           $Reoder_Data = $wpdb->get_results("SELECT   wp_posts.ID 
            FROM wp_posts  INNER JOIN wp_term_relationships
             ON (wp_posts.ID = wp_term_relationships.object_id) 
             INNER JOIN wp_postmeta ON (wp_posts.ID = wp_postmeta.post_id) 
             INNER JOIN wp_reorder_post_rel ON wp_posts.ID = wp_reorder_post_rel.post_id 
             and incl = 1   WHERE 1=1  AND ( wp_term_relationships.term_taxonomy_id 
              IN (".$category->term_taxonomy_id.") ) AND wp_posts.post_type = 'product' 
                  AND (wp_posts.post_status = 'publish' 
                  OR wp_posts.post_status = 'expired'
                  OR wp_posts.post_status = 'private') AND ( (wp_postmeta.meta_key = '_visibility' 
                  AND CAST(wp_postmeta.meta_value AS CHAR) 
                  IN ('visible','catalog')) ) AND 
                  wp_reorder_post_rel.category_id = '".$category->term_id."' 
                  GROUP BY wp_posts.ID 
                  ORDER BY wp_reorder_post_rel.id ASC LIMIT 0, 1");
           if($Reoder_Data){
           foreach($Reoder_Data as $loop){
            echo '<a href="'.get_term_link( $category->slug, 'product_cat' ).'">'.get_the_post_thumbnail($loop->ID, 'shop_catalog',array('class'  => "gallery-image", 'alt' => $category->name)).'<span class="view-category">VIEW PRODUCTS</span></a>';
           }
           }
           else
           {
              echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="100px" height="100px" class="gallery-without-image" />'; 
           }

          }
           
        ?>
       </div>
         

      <?php  
       /*
       if($loop->post->ID) {
       if (has_post_thumbnail( $loop->post->ID )) 
        echo '<a href="'.get_term_link( $category->slug, 'product_cat' ).'">'.get_the_post_thumbnail($loop->post->ID, 'shop_catalog',array('class'  => "gallery-image")).'<span class="view-category">VIEW PRODUCT</span></a>'; 
       else 
        echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="100px" height="100px" class="gallery-image" />';
        }
        else
        {
        echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="100px" height="100px" class="gallery-without-image" />';
        }

        */
         ?>
<div class="desc"><div class="gallery-content-heading"><?php echo $category->name ?></div><div class="gallery-content">
 <?php  echo $category->description; ?>
</div></div> 
</div> 


</div> 


      
<?php 
 } 
 $woocommerce_loop['loop']++;
?>

  
 
