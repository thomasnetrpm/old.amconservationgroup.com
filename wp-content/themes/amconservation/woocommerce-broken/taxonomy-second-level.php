<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
if ( ! defined( 'ABSPATH' ) ) exit;  // Exit if accessed directly

/*
// Commented out as per suggestion from Jabir
if(is_mobile()) {
	include('taxonomy-second-level-mobile.php');
	exit();
}
*/

global $product_cat;

global $term;
$metafieldArray = get_option('taxonomy_'. $term->term_id);
$metafieldoutput = $metafieldArray['cat_product_dec_meta'];


// Handle the load more
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
// Handle the session setting of Compare product IDs
if(isset($_POST['prodCat']) && isset($_POST['prodId']) && isset($_POST['action'])) {
	if($_POST['action'] == 'add') {
		$_SESSION[$_POST['prodCat']][] = $_POST['prodId'];
	} else if($_POST['action'] == 'remove') {
		$cmpProdAryRKey = array_search($_POST['prodId'], $_SESSION[$_POST['prodCat']]);
		unset($_SESSION[$_POST['prodCat']][$cmpProdAryRKey]);
	}
	exit();
}

?>
<?php if ( have_posts() ) : ?>

<?php while ( have_posts() ) : the_post(); ?>
<?php woocommerce_get_template_part( 'content', 'product' ); ?>
<?php endwhile; // end of the loop. ?>

<div style="display:none;" class="js_pagiLinks"><?php do_action( 'woocommerce_after_shop_loop' ); ?></div>

<?php endif; ?>
<?php
	exit();
}

define("CSS", "productlisting.css");

$cmpProdsStr = '';
$cmpProdIdsCnt = 0;

if(isset($_SESSION[$product_cat])) {
	$cmpProdIdsAry = $_SESSION[$product_cat];
	$cmpProdIdsCnt = count($cmpProdIdsAry);
	foreach($cmpProdIdsAry as $cmpProdIdStr) {
		$cmpProdId = explode('_', $cmpProdIdStr);
		$cmpProdId = $cmpProdId[0];

		$prodObj = new WC_Product_Simple($cmpProdId);
		$cmpProdImgStr = $prodObj->get_image(array(67, 65));
		if(empty($cmpProdImgStr)) {
			$cmpProdImgStr = '<img src="'.get_template_directory_uri().'/assets/images/no_image.png" alt="" witdh="67" height="65" />';
		}

		$cmpProdsStr .= '<div class="col-md-3 col-lg-2  col-sm-3 col-xs-6 graybg" id="js_click_compare_content_'.$cmpProdIdStr.'">
	<span class="smallclose"><a class="js_remove_cp_con_prdt" href="#"><img src="'.get_template_directory_uri().'/assets/images/close-small.png"></a></span>
	<div class="graybg-compare">
	<div class="product-img">'.$cmpProdImgStr.'</div>
	<div class="product-codeid"><span class="model">SKU</span><br><span class="codenum">'.strtoupper($prodObj->get_sku()).'</span></div>
	<input type="hidden" value="'.$cmpProdId.'" name="prodCmpArray[]"></div></div>';
	}
}

// Form the NO Product text
$noProdTxt = 'No products found which match your selection.';
if($product_cat == 'emergency-kits') {
	$noProdTxt = 'Products are coming soon.';
}

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header('shop-amc');
}
 ?>

 <div class="row headersession">
        <div class="col-md-6"><h3 class="headerlabel">
		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
	    <?php woocommerce_page_title(); ?>
		 <?php endif; ?>
		 </h3></div>

<div class="col-md-6 bcs inBread">
          <ol class="breadcrumb">
            <?php breadcrumbs_fmg();  ?>
          </ol>
        </div>
<?php if(is_mobile()) { ?>
	<div class="col-md-4 back-btn back-page-com"> <a onClick="history.go(-1)"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/back_btn.png">Back </a></div>
<?php } ?>
    </div> 

 <div class="row content-session categog-second">
        <div class="col-md-3 space">
          <div class="headerrow">Narrow By</div>
          <div class="bodyrow">
<?php
	if (function_exists('dynamic_sidebar')):
		dynamic_sidebar('AMC Filter');
	endif;
?>
          </div>
        </div>

<!-- compare box -->
<div class="col-md-9">
	<div class="row">
		<div class="col-md-12 topspace">
<form id="prodCompareBox" method="post" action="<?php echo get_site_url().'/compare-products/' ?>">
          <div class="product-compare-container">
               <span class="bigclose"><a href="#" class="js_close_compareBox"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/close-big.png"></a></span>
<?php echo $cmpProdsStr; ?>
<?php if($cmpProdIdsCnt <=3 ) { ?>
			
               <div class="col-md-4 col-lg-2 col-xs-12 col-sm-4 bluebg">
					<div class="bluebg-inner">
                    <div class="product-img"> <img height="65" height="65" src="<?php echo get_template_directory_uri(); ?>/assets/images/add-icon.png"></div>
                    <div class="product-codeid"><span class="model">Add<br>Product</span></div>
					</div>
               </div>
<?php } ?>
               <div class="col-md-8 col-lg-6  col-sm-8 col-xs-12">
			   <input type="submit" class="js_cmpBtn compare-btn btn-primary col-lg-2 col-md-2" value="COMPARE" disabled>
			   <?php if($metafieldArray['btn_name'] != '') { ?>
			   <a href="<?php echo !empty($metafieldArray['prod_url']) ? $metafieldArray['prod_url'] : "#"; ?>" class="compare-btn  col-lg-2 col-md-2 col-xs-12 sample_btn" style="text-align:center;padding-top:10px"> <?php echo $metafieldArray['btn_name'];?> </a>
			   <?php } ?>
			   </div>
			   <div class="col-lg-12 product_cont">
			  <?php 
			 
				echo $metafieldoutput;
?>
</div>
          </div>
</form>

<script type="text/javascript">
$('.js_close_compareBox').click(function() {
	$('div.topspace').hide();
	return false;
});
</script>

        </div>

		<div class="col-md-12 space">
          <div class="headerrow2 js_pagiStr">Items 1-<?php echo get_option('posts_per_page'); ?></div>

          <div class="row">
		<?php if ( have_posts() ) : ?>
			<?php woocommerce_product_loop_start(); ?>
				<?php //woocommerce_product_subcategories(); ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php woocommerce_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>
			<?php woocommerce_product_loop_end(); ?>
			<div style="display:none;" class="js_pagiLinks">
			<?php
				do_action( 'woocommerce_after_shop_loop' );
			?>
			</div>
		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
			<?php woocommerce_get_template( 'loop/no-products-found.php' ); ?>
		<?php endif; ?>
		</div>
		 <div class="col-md-12 loadmore js_handleLoadMore" style="cursor:pointer;">
        <div class="loadingmore-row">Load more products</div>
                  </div>
 </div>
 </div>
 </div>
<script type="text/javascript">
// 2837 - Remove the narrow by filter
if($('div.bodyrow div#accordion').length == 0) {
    $('div.bodyrow').parent().hide();
    $('div.bodyrow').parent().next().removeClass('col-md-9').addClass('col-md-12 css_noNarrow');
}
</script>
 </div>

<style type="text/css">
.small-productcode_thumb { height: 142px; width: 156px; }
</style>

<?php include('request-quote-modal.php'); ?>

<script type="text/javascript">
$(document).ready(function() {
<?php
if(isset($_SESSION[$product_cat])) {
	$cmpProdIdsAry = $_SESSION[$product_cat];
	foreach($cmpProdIdsAry as $cmpProdIdStr) {
?>
$('input[type=checkbox][value=<?php echo $cmpProdIdStr; ?>].js_click_compare').attr('checked', true);
<?php
	}
}
?>
});
</script>

<script>
$(".radio-group2 .rac-radio1 :radio").click(function() {
	$("div.requestaquote div .services").css("display","none");
	$("div.requestaquote div .products").css("display","block");
});

$(".radio-group2 .rac-radio2 :radio").click(function() {
	$("div.requestaquote div .products").css("display","none");
	$("div.requestaquote div .services").css("display","block");
});
</script>

<script type="text/javascript">
/* Handle the empty variation records occuring during filtering */
var varProds =  $('ul.products div.col-md-9');
var varProdsLength;
for(i=0; i<varProds.length; i++) {
	varProdsLength = $(varProds[i]).find('table.table-prddisplay tbody tr td').length;
	if(varProdsLength == 0) {
		$(varProds[i]).prev().remove();
		$(varProds[i]).remove();
	}
}

var postLimit = '<?php echo get_option('posts_per_page'); ?>';
$('.js_pagiStr').text('Items 1-' + ($('ul.products div.pl-productname').length));

// Show the empty product message on filtering
if($('ul.products div.pl-productname').length == 0 ) {
	$('.js_pagiStr').text('<?php echo $noProdTxt; ?>');
	$('.js_handleLoadMore').hide();
}

// Hide the load more if 1st page has less that 'postLimit' products
if($('ul.products div.pl-productname').length < postLimit) {
	$('.js_handleLoadMore').hide();
}

$('.js_handleLoadMore').click(function() {
	$(this).find('div.loadingmore-row').text('Loading ...');
	var nextElmt = $('.js_pagiLinks ul li span.current').parent().next().find('a');

	if($('ul.products .js_pagiLinks').length != 0) {
		nextElmt = $('ul.products .js_pagiLinks:last ul li span.current').parent().next().find('a');
	}

	if(nextElmt.length > 0) {
		var nexturl = nextElmt.attr('href');
		var nextpage = nextElmt.text();
		$.ajax({
			url: nexturl,
			success: function(d) {
				$('.js_handleLoadMore').find('div.loadingmore-row').text('Load more products');

				$('.js_pagiLinks').html($(d).find('.js_pagiLinks').html());
				$(d).appendTo($('ul.products'));
				// $('.js_pagiStr').text('page 1-'+(nextpage*postLimit));
				$('.js_pagiStr').text('Items 1-' + ($('ul.products div.pl-productname').length));

				// Hide the load more for last page
				if($('ul.products').find('div.js_pagiLinks:last').find('ul li:last span').hasClass('current')) {
					$('.js_handleLoadMore').hide();
				}
				resizeTable();
			}
		});
	}
	return false;
});
</script>
<script type="text/javascript">
$(document).on( 'click', 'input.js_click_compare', function() {
	$('div.topspace').show();
	var _this = $(this);
	var canAdd = true;

	if(($('div.product-compare-container .graybg').length >= 4) && ($(this).is(':checked')) ) {
		return false;
	}

	if($('div.product-compare-container .graybg').length >= 3) {
		hideAddProdBox();
	} else {
		showAddProdBox();
	}

	if($('div.product-compare-container .graybg').length > 3) {
		canAdd = false;
	}

	if(canAdd) {
		if(_this.is(':checked')) {
			$('div.product-compare-container span.bigclose').after($('div#js_click_compare_content_container_'+_this.val()).html());

			// Add the compare prod in the session
			$.ajax({
				url: '<?php echo getCurURL(); ?>',
				method: 'post',
				data: { prodCat: '<?php echo $product_cat; ?>', prodId: _this.val(), action: 'add' }
			});
		} else {
			$('div.product-compare-container').find('div#js_click_compare_content_'+_this.val()).remove();
			// Remove the compare prod in the session
			$.ajax({
				url: '<?php echo getCurURL(); ?>',
				method: 'post',
				data: { prodCat: '<?php echo $product_cat; ?>', prodId: _this.val(), action: 'remove' }
			});
		}
	} else {
		if(_this.is(':checked')) { } else {
			$('div.product-compare-container').find('div#js_click_compare_content_'+_this.val()).remove();
			// Remove the compare prod in the session
			$.ajax({
				url: '<?php echo getCurURL(); ?>',
				method: 'post',
				data: { prodCat: '<?php echo $product_cat; ?>', prodId: _this.val(), action: 'remove' }
			});
		}
	}

	enDisCmpBtn();
});

// Remove product item from compare box
$(document).on( 'click', 'a.js_remove_cp_con_prdt', function(e) {
	e.preventDefault();

	if($('div.product-compare-container .graybg').length >= 3) {
		hideAddProdBox();
	} else {
		showAddProdBox();
	}

	// Uncheck the related compare checkbox
	$('input[type=checkbox][value='+$(this).parent().parent().attr('id').replace('js_click_compare_content_', '')+']').attr('checked', false);

	$(this).parent().parent().remove();
	// Remove the compare prod in the session
	$.ajax({
		url: '<?php echo getCurURL(); ?>',
		method: 'post',
		data: { prodCat: '<?php echo $product_cat; ?>', prodId: $(this).parent().parent().attr('id').replace('js_click_compare_content_', ''), action: 'remove' }
	});

	enDisCmpBtn();
});

function enDisCmpBtn() {
	if($('div.product-compare-container .graybg').length >= 2) {
		$('input.js_cmpBtn').attr('disabled', false);
	} else {
		$('input.js_cmpBtn').attr('disabled', true);
	}
}
enDisCmpBtn();

function showAddProdBox() {
	$('div.product-compare-container .bluebg').show();
}

function hideAddProdBox() {
	$('div.product-compare-container .bluebg').hide();
}
			  $('.panel-default').on('shown.bs.collapse', function () { 
                  $(this).find(".panel-title").css({'background-image' : "url('<?php echo get_template_directory_uri(); ?>/assets/images/accordion-uparrow.png')", 'background-repeat': 'no-repeat','background-position-x':'97%', 'background-position-y':'50%'})
              });
              $('.panel-default').on('hidden.bs.collapse', function () {
                  $(this).find(".panel-title").css({'background-image' : "url('<?php echo get_template_directory_uri(); ?>/assets/images/accordion-downarrow.png')", 'background-repeat': 'no-repeat','background-position-x':'97%', 'background-position-y':'50%'})                  
              });
</script>
<script>
       $(document).ready(function(){
                $('.panel-default').on('shown.bs.collapse', function () {
                  $(this).find(".panel-title").css({'background-image' : "url('<?php echo get_template_directory_uri(); ?>/assets/images/accordion-uparrow.png')", 'background-repeat': 'no-repeat','background-position-x':'97%', 'background-position-y':'50%'})
              });
              $('.panel-default').on('hidden.bs.collapse', function () {
                // console.log(thisObj);
                  $(this).find(".panel-title").css({'background-image' : "url('<?php echo get_template_directory_uri(); ?>/assets/images/accordion-downarrow.png')", 'background-repeat': 'no-repeat','background-position-x':'97%', 'background-position-y':'50%'})                  
              });
              $('.product-content-container:nth-child(2n+1)').addClass('odd');
			  $('.product-content-container:nth-child(2n+2)').addClass('even');
			  
			  /* to make arrow and head in the same height */
			 
			  
			  $(".table-responsive").scrollLeft();
            $(".leftarrow").click(function () {
                $(this).toggleClass("active");
                
                if ($(this).hasClass("active") == true) {
                    $(this).next(".table-responsive").animate({ scrollLeft: 2250 }, 1000);;
                    //alert("yes");
                }
                else {
                    $(this).next(".table-responsive").animate({ scrollLeft: 0 }, 1000);;
                    //alert("no");
                }

            });
			  

});
$(window).load(function(){
 setTimeout('arrowheight()',1000);
			 
});

$(window).resize(function(){
arrowheight();
});
function arrowheight(){
 $('.prd-listing').each(function(){
				var theadH = $(this).find('table th').outerHeight();
				$(this).find('.leftarrow').css('height', theadH + 'px')
			  })
}

</script>

<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer('shop');
}
?>