<?php // Show the simple product - Product with NO variations ?>

<?php
global $product, $woocommerce_loop, $woocommerce,$post,$FilterSet;
$prodDetails = $product;
$Attr_Keys = array();
 $term       = get_queried_object();
 $FilterKey = $term->slug;
 $LeftColumnValue = $FilterSet[$FilterKey];
			$newPostId = $post->ID;
	        $prodDetails = get_product($newPostId);
            $prodAttributes = $prodDetails->get_attributes();

               $i = 0;
              if(is_array($prodAttributes))  { 
              foreach($prodAttributes as $prodAttributesItem) {
            
                $attribute_values = false;
                 if(!in_array($prodAttributesItem['name'], $remove_items))
                 {
                        $attribute_key    =  $prodAttributesItem['name'];
                        $attribute_values =  array_shift(woocommerce_get_product_terms($newPostId, $prodAttributesItem['name'], 'names')); 
                       if($attribute_values) {
                       	$Attr_Keys[]  	  = $attribute_key;
                        $attribute_list[$attribute_key] = $attribute_values ;
                        
                      } 
                   $i++;
                 }
                 
              }
              } 
              //print_r($attribute_list);
             // if(count($Attr_Keys) > 0 ) {
             $Balance = 4 - count($Attr_Keys);
             if($Balance!= 0)
               {
                for($i=count($Attr_Keys);$i<4;$i++)
                {

                  array_push($Attr_Keys,"n/a");
                }

               }  
            //   }        
?>

<div class="col-xs-12 col-sm-12">
                    <div class="leftarrow SlideOff"></div>
						<div class="table-responsive">
<table class="table table-bordered rwdtable">
	<thead>
		<tr>
			<th>Product Options</th>
<?php
//if(!is_array($S_LeftColumnValue)) { $S_LeftColumnValue =  $Attr_Keys; }
$sim_ctr = 2;
foreach($S_LeftColumnValue as $key=>$prodAttributesItem) {
	if($sim_ctr < 5) {

		$filter_name = ($prodAttributesItem =='n/a')?" ":$woocommerce->attribute_label('pa_'.$prodAttributesItem);
		echo '<th>'.$filter_name.'</th>';
		++$sim_ctr;
	}
}
?>
  <th></th>
		</tr>
	</thead>

	<tbody>

<?php
$sim_ctr3 = '';
$sim_ctr4 = 1;
?>
	<tr>
		<td>
		<?php // echo $prodDetails->get_image(array(87, 74), array('class' => 'small-productcode')) ?>
<?php
	$prodDetImage = $prodDetails->get_image(array(87, 74), array('class' => 'small-productcode'));
	if(empty($prodDetImage)) {
		echo '<img class="small-productcode" src="'.get_template_directory_uri().'/assets/images/no_image.png" alt="" witdh="87" height="74" />';
		$prodDetCmpImg = '<img src="'.get_template_directory_uri().'/assets/images/no_image.png" alt="" witdh="67" height="65" />';
	} else {
		echo $prodDetImage;
		$prodDetCmpImg = $prodDetails->get_image(array(67, 65));
	}
?>
			<div class="compare-container">
				<div class="checkbox addtocompare"><label><input type="checkbox" value="<?php echo $prodDetails->id.'_'.$sim_ctr4; ?>" class="js_click_compare"><span class="addlabel">compare</span></label></div>
			</div>
		</td>

<!-- compare box product content -->
<div style="display:none;" id="js_click_compare_content_container_<?php echo $prodDetails->id.'_'.$sim_ctr4; ?>">
	<div id="js_click_compare_content_<?php echo $prodDetails->id.'_'.$sim_ctr4; ?>" class="col-md-3 graybg">
		<span class="smallclose"><a href="#" class="js_remove_cp_con_prdt"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/close-small.png"></a></span>
		<div class="product-img"><?php echo $prodDetCmpImg; ?></div>
		<div class="product-codeid"><span class="model">SKU</span><br><span class="codenum"><?php echo $prodDetails->sku; ?></span></div>
		<input type="hidden" name="prodCmpArray[]" value="<?php echo $prodDetails->id; ?>" />
	</div>
</div>

<?php
if(count($S_LeftColumnValue) == 0){
			echo '<th></th>';
		}

	$attrValue = array();
 	foreach($Attr_Keys as $attributesItem) {
		$attrValue[$attributesItem] = $attribute_list[$attributesItem];	
	}

	//print_r($attrValue);

	$sim_ctr2 = 2;
		foreach($S_LeftColumnValue as $key=>$attributesItem) {
		if($sim_ctr2 < 5) {
			$key = 'pa_'.$attributesItem;
			//echo $key;
			echo '<td>'.$attrValue[$key].'</td>'; // class="product-content'.$sim_ctr2.'"
			++$sim_ctr2;
		}
	}
?>
		<td> 
			<a data-toggle="modal" data-target=".requestaquote"><button type="button" class="pl-btn btn-primary">Request a Quote</button></a>
			<span class="jsPostId" style="display:none;"><?php echo $product->id; ?></span>

			<a href="<?php the_permalink($product->id); ?>"><button type="button" class="pl-btn btn-primary">View Product Details</button></a>
		</td>
	</tr>
<?php
	if(empty($sim_ctr3)) {
		$sim_ctr3 = 2;
	} else {
		++$sim_ctr3;
	}
	++$sim_ctr4;
?> 
</tbody>
</table>
</div>
</div>