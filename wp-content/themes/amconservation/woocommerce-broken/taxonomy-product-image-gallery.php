<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $theme_options, $amc_urls; 
 ?>
<div class="col-md-9 ">
 <div  id= "one" class="gallery-1 galy">
        <div class="row bgfill content">

          <div>     
            <div class="   gallery-offset">
              <div class="row">
                <div class="col-md-12">
        <?php 
        $CategorySlug      = empty($_GET['view'])?'':$_GET['view'];
        $args = array( 'post_type' => 'product', 'product_cat' => $CategorySlug, 'orderby' => 'name' );
        $loop = new WP_Query( $args );
        //print_r($loop);
         while ( $loop->have_posts() ) : $loop->the_post(); 

        ?>
                  <div class="col-md-4 col-lg-3 col-sm-4 col-xs-6 down-img-cont">
                    <div class="image-container"style="margin-bottom:30px;">
                      <div class="gallery1 ">
                      <a href="<?php echo the_permalink($loop->post->ID);?>" target="__blank">
                        <div><?php  if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog',array('class'  => "gallery-image-product",)); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="100px" height="100px" class="gallery-image" />'; ?> <span class="view-category">VIEW PRODUCT</span></div>
                        </a> </div>
                      <div class="desc">
                        <div class="gallery-content-heading"><?php echo $loop->post->post_title; ?></div>
                        <?php
                        $post_thumbnail_id = get_post_thumbnail_id($loop->post->ID);
                        $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
                        ?>
                        <div class="download-btn">
<?php
/*
						<a href="<?php echo $post_thumbnail_url;?>" download="<?php echo basename($post_thumbnail_url); ?>">Download Image</a>
*/
?>
						<a target="_blank" href="<?php echo site_url().'/image-download/?image_id='.$post_thumbnail_id; ?>">Download Image</a>
						</div>
                      </div>
                    </div>
                  </div>
          <?php 
          endwhile;
          wp_reset_query();  
           ?>
               </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<script type="text/javascript">
  /*$(".gallery-image-product").mouseenter(function(e) {
        $( "<div class='category-mouseover-btn'><p class='view-category'>VIEW PRODUCT</p></div>" ).prependTo( $(this).parent().parent());     
                     $(this).animate({ opacity: 0.3},200);     

        }).mouseleave(function() {
        $(this).animate({ opacity: 1},200); 
        $(this).parent().parent().find("div.category-mouseover-btn").remove();
      })*/
</script>

<script type="text/javascript">
  $('.view-category').hide();
	  $('.gallery1').find('img').attr('width','auto');
	  $('.gallery1').find('img').attr('height','auto')
	 $('.gallery1').mouseenter(function(e) {

			     $(this).find('.view-category').show();
			  	           $(this).find('img').animate({ opacity: 0.3},200); 	   

})
.mouseleave(function() {
$(this).find('img').animate({ opacity: 1},200); 
	 $(this).find('.view-category').hide();
});

$(window).load(function(){
rwdImage();
 setHeight('.desc');
})
$(window).resize(function(){
rwdImage();
setHeight('.desc');
})
function rwdImage(){
var currwidth = $('.gallery1').outerWidth();
$('.gallery1').css('height',currwidth + 'px');
$()
}

 function setHeight(pxBtmBox) {
    var maxHeight = 0;
    //Get all the element with class = col
    column = $(pxBtmBox);
    column.css('height', 'auto');
    //Loop all the column
    column.each(function () {
        //Store the highest value
        if ($(this).height() > maxHeight) {
            maxHeight = $(this).height();
           
        }
    });
    //Set the height
    column.height(maxHeight);
}
</script>
 