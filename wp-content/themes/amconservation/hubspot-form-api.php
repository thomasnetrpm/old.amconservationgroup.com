  <?php
	//error_reporting(E_ALL);
 // echo phpinfo();
    //Process a new form submission in HubSpot in order to create a new Contact.
	
	if($form_id == "contact_us") {
		$for_hubspot = "/contact-us/";
		$page_name = "Contact Us";
		$hub_form_id = "96ebc406-0d14-4ef5-a804-4137d9e1c34f";
	} else if($form_id == "message_us_online") {
		$for_hubspot = "/message-us-online/";
		$page_name = "Message Us Online";
		$hub_form_id = "9125bed1-b11b-4950-bf3b-38fd6b774c1a";
	} else if($form_id == "request_a_catalog") {
		$for_hubspot = "/request-a-catalog/";
		$page_name = "Request a Catalog";
		$hub_form_id = "ae7eae5a-84f3-4942-8f53-61164a65a730";
	}

	$endpoint = 'https://forms.hubspot.com/uploads/form/v2/448562/'.$hub_form_id;
    $hubspotutk = $_COOKIE['hubspotutk'];  //grab the cookie from the visitors browser.
    $ip_addr = $_SERVER['REMOTE_ADDR'];  //IP address too.
    $hs_context = array(
            'hutk' => $hubspotutk,
            'ipAddress' => $ip_addr,
            'pageUrl' => get_site_url().$for_hubspot,
            'pageName' => $page_name,
        );
    $hs_context_json = json_encode($hs_context);
	if($form_id == "contact_us") {
		//Need to populate these varilables with values from the form.
		$uname=sanitize_text_field($_POST['contUsFrm_name']);
		$email=sanitize_text_field($_POST['contUsFrm_email']);
		$phonenumber=sanitize_text_field($_POST['contUsFrm_phone']).' X'.sanitize_text_field($_POST['contUsFrm_ext']);
		$company=sanitize_text_field($_POST['contUsFrm_company']);
		$howhelp=sanitize_text_field($_POST['contUsFrm_howhelp']);
		$prefcont = sanitize_text_field($_POST['contUsFrm_prefcont']);
		$acctrepname= sanitize_text_field($_POST['contUsFrm_acctrepname']);

		$str_post = "firstname=" . urlencode($uname)
				. "&email=" . urlencode($email)
				. "&phone=" . urlencode($phonenumber)
				. "&company=" . urlencode($company)
				. "&howhelp=" . urlencode($howhelp)
				. "&prefcont=" . urlencode($prefcont)
				. "&acctrepname=" . urlencode($acctrepname);  //Leave this one be :)

		 //replace the values in this URL with your portal ID and your form GUID
	
    } else if($form_id == "request_a_catalog"){
		$emailCat = (sanitize_text_field($_POST['reqCatForm_emailcat']) == 'yes') ? 1 : 0;
		//Need to populate these varilables with values from the form.
		$firstname=sanitize_text_field($_POST['reqCatForm_firstname']);
		$lastname=sanitize_text_field($_POST['reqCatForm_lastname']);
		$company=sanitize_text_field($_POST['reqCatForm_company']);
		$email=sanitize_text_field($_POST['reqCatForm_email']);
		$address=sanitize_text_field($_POST['reqCatForm_address']);
		$comments = sanitize_text_field($_POST['reqCatForm_comments']);
		$is_catalog_emailed=$emailCat;
					
		$str_post = "firstname=" . urlencode($firstname)
				. "&lastname=" . urlencode($lastname)
				. "&company=" . urlencode($company)
				. "&email=" . urlencode($email)
				. "&address=" . urlencode($address)
				. "&comments=" . urlencode($comments)
				. "&is_catalog_emailed=" . urlencode($is_catalog_emailed);  //Leave this one be :)

		 //replace the values in this URL with your portal ID and your form GUID   
	} else if($form_id == "message_us_online") {
	
		$user_name = sanitize_text_field($_POST['user_name']);
		$company_name = sanitize_text_field($_POST['company_name']);
		$email_address = sanitize_text_field($_POST['email_address']);
		$phone_number = sanitize_text_field($_POST['phone_number']);
		$how_find = sanitize_text_field($_POST['how_find']);
		$message = sanitize_text_field($_POST['message']);
		$pref_contact = sanitize_text_field($_POST['pref_contact']);
	
		$str_post = "user_name=" . urlencode($user_name)
				. "&company_name=" . urlencode($company_name)
				. "&email_address=" . urlencode($email_address)
				. "&phone_number=" . urlencode($phone_number)
				. "&how_find=" . urlencode($how_find)
				. "&message=" . urlencode($message)
				. "&pref_contact=" . urlencode($pref_contact);  //Leave this one be :)
	
	}
    $ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $str_post);
    curl_setopt($ch, CURLOPT_URL, $endpoint);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);  //Log the response from HubSpot as needed.
	$info = curl_getinfo($ch);
	/*echo "<pre>";
	print_r($info);
    curl_close($ch);
	echo "-------------"."<br>";
    echo ">>>".$response;
	echo "-------------"."<br>";
	print_r($str_post);
	echo "-------------"."<br>";
	print_r($_POST);
	echo "-------------"."<br>";
	echo  $hs_context_json;
	exit;*/
	//header("Location:http://awesomecloud.com/contact/success/");

    ?>