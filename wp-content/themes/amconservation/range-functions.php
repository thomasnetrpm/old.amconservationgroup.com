<?php

//Lumens handling
function woocommerce_lumens_filter_init() {
	add_filter('loop_shop_post_in', 'woocommerce_lumens_filter');
}
add_action( 'init', 'woocommerce_lumens_filter_init' );

function woocommerce_lumens_filter($filtered_posts) {
    global $wpdb;

    if(isset($_GET['filter_lumens'])) {
		$singleFilter = true;
		$filter_lumens =  $_GET['filter_lumens'];
		$filter_lumens_aryX = explode(',', $filter_lumens);

		$termSlugStr = '( ';

		if(count($filter_lumens_aryX) > 1) {
			$singleFilter = false;
			foreach($filter_lumens_aryX as $filter_lumens_ary_item) {
				$filter_lumens_ary[] = explode('-', $filter_lumens_ary_item);
			}
		} else {
			$filter_lumens_ary = explode('-', $filter_lumens_aryX[0]);
		}

		if($singleFilter) {
			$termSlugStr .= 't.slug BETWEEN '.$filter_lumens_ary[0].' AND '.$filter_lumens_ary[1];
		} else {
			$ctr1 = 1;
			foreach($filter_lumens_ary as $filter_lumens_aryItem) {
				$termSlugStr .= '(t.slug BETWEEN '.$filter_lumens_aryItem[0].' AND '.$filter_lumens_aryItem[1].') ';

				if($ctr1 < count($filter_lumens_ary)) {
					$termSlugStr .= ' OR ';
				}
				$ctr1++;
			}
		}
		$termSlugStr .= ' )';

        $matched_products = array();

		/*
        $min 	= floatval($filter_lumens_ary[0]);
        $max 	= floatval($filter_lumens_ary[1]);

		$minT = get_term_by('slug', $min, 'pa_lumens');
		$maxT = get_term_by('slug', $max, 'pa_lumens');

		$min = $minT->slug;
		$max = $maxT->slug;
		*/

		$qry = "SELECT DISTINCT wp_posts.ID, wp_posts.post_parent, wp_posts.post_type, post_title, t.* FROM wp_posts 

			INNER JOIN wp_term_relationships tr ON (wp_posts.ID = tr.object_id) 

			INNER JOIN wp_term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)

			INNER JOIN wp_terms t ON (tt.term_id = t.term_id) 

			WHERE 1=1 AND wp_posts.post_type IN ( 'product', 'product_variation' ) 

			AND ".$termSlugStr."

			AND tt.`taxonomy` = 'pa_lumens' 

			AND wp_posts.post_status = 'publish'";

		$matched_products_query = $wpdb->get_results( $qry, OBJECT_K ); 

		if ( $matched_products_query ) {
            foreach ( $matched_products_query as $product ) {
                if ( $product->post_type == 'product' )
                    $matched_products[] = $product->ID;
                if ( $product->post_parent > 0 && ! in_array( $product->post_parent, $matched_products ) )
                    $matched_products[] = $product->post_parent;
            }
        }

        // Filter the id's
        if ( sizeof( $filtered_posts ) == 0) {
            $filtered_posts = $matched_products;
            $filtered_posts[] = 0;
        } else {
            $filtered_posts = array_intersect( $filtered_posts, $matched_products );
            $filtered_posts[] = 0;
        }

    }

    return (array) $filtered_posts;
}

//Rated watts handling
function woocommerce_rated_watts_filter_init() {
	add_filter('loop_shop_post_in', 'woocommerce_rated_watts_filter');
}
add_action( 'init', 'woocommerce_rated_watts_filter_init' );

function woocommerce_rated_watts_filter($filtered_posts) {
    global $wpdb;

    if(isset($_GET['filter_rated-watts'])) {
		$singleFilter = true;
		$filter_rated_watts =  $_GET['filter_rated-watts'];
		$filter_rated_watts_aryX = explode(',', $filter_rated_watts);

		$termSlugStr = '( ';

		if(count($filter_rated_watts_aryX) > 1) {
			$singleFilter = false;
			foreach($filter_rated_watts_aryX as $filter_rated_watts_ary_item) {
				$filter_rated_watts_ary[] = explode('-', $filter_rated_watts_ary_item);
			}
		} else {
			$filter_rated_watts_ary = explode('-', $filter_rated_watts_aryX[0]);
		}

		if($singleFilter) {
			$termSlugStr .= 't.slug BETWEEN '.$filter_rated_watts_ary[0].' AND '.$filter_rated_watts_ary[1];
		} else {
			$ctr1 = 1;
			foreach($filter_rated_watts_ary as $filter_rated_watts_aryItem) {
				$termSlugStr .= '(t.slug BETWEEN '.$filter_rated_watts_aryItem[0].' AND '.$filter_rated_watts_aryItem[1].') ';

				if($ctr1 < count($filter_rated_watts_ary)) {
					$termSlugStr .= ' OR ';
				}
				$ctr1++;
			}
		}
		$termSlugStr .= ' )';

        $matched_products = array();
		/*
        $min 	= floatval($filter_rated_watts_ary[0]);
        $max 	= floatval($filter_rated_watts_ary[1]);

		$minT = get_term_by('slug', $min, 'pa_rated-watts');
		$maxT = get_term_by('slug', $max, 'pa_rated-watts');

		$min = $minT->slug;
		$max = $maxT->slug;
		*/

		$qry = "SELECT DISTINCT wp_posts.ID, wp_posts.post_parent, wp_posts.post_type, post_title, t.* FROM wp_posts 

			INNER JOIN wp_term_relationships tr ON (wp_posts.ID = tr.object_id) 

			INNER JOIN wp_term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)

			INNER JOIN wp_terms t ON (tt.term_id = t.term_id) 

			WHERE 1=1 AND wp_posts.post_type IN ( 'product', 'product_variation' ) 

			AND ".$termSlugStr."

			AND tt.`taxonomy` = 'pa_rated-watts' 

			AND wp_posts.post_status = 'publish'";

		$matched_products_query = $wpdb->get_results( $qry, OBJECT_K ); 

		if ( $matched_products_query ) {
            foreach ( $matched_products_query as $product ) {
                if ( $product->post_type == 'product' )
                    $matched_products[] = $product->ID;
                if ( $product->post_parent > 0 && ! in_array( $product->post_parent, $matched_products ) )
                    $matched_products[] = $product->post_parent;
            }
        }

        // Filter the id's
        if ( sizeof( $filtered_posts ) == 0) {
            $filtered_posts = $matched_products;
            $filtered_posts[] = 0;
        } else {
            $filtered_posts = array_intersect( $filtered_posts, $matched_products );
            $filtered_posts[] = 0;
        }

    }

    return (array) $filtered_posts;
}

// Overall length handling

function woocommerce_overall_length_filter_init() {
	add_filter('loop_shop_post_in', 'woocommerce_overall_length_filter');
}
add_action( 'init', 'woocommerce_overall_length_filter_init' );

function woocommerce_overall_length_filter($filtered_posts) {
    global $wpdb;

    if(isset($_GET['filter_overall-length'])) {
		$singleFilter = true;
		$filter_overall_length =  $_GET['filter_overall-length'];
		$filter_overall_length_aryX = explode(',', $filter_overall_length);

		$termSlugStr = '( ';

		if(count($filter_overall_length_aryX) > 1) {
			$singleFilter = false;
			foreach($filter_overall_length_aryX as $filter_overall_length_ary_item) {
				$filter_overall_length_ary[] = explode('-', $filter_overall_length_ary_item);
			}
		} else {
			$filter_overall_length_ary = explode('-', $filter_overall_length_aryX[0]);
		}

		if($singleFilter) {
			$termSlugStr .= 't.slug BETWEEN '.$filter_overall_length_ary[0].' AND '.$filter_overall_length_ary[1];
		} else {
			$ctr1 = 1;
			foreach($filter_overall_length_ary as $filter_overall_length_aryItem) {
				$termSlugStr .= '(t.slug BETWEEN '.$filter_overall_length_aryItem[0].' AND '.$filter_overall_length_aryItem[1].') ';

				if($ctr1 < count($filter_overall_length_ary)) {
					$termSlugStr .= ' OR ';
				}
				$ctr1++;
			}
		}
		$termSlugStr .= ' )';

        $matched_products = array();

		/*
        $min 	= floatval($filter_overall_length_ary[0]);
        $max 	= floatval($filter_overall_length_ary[1]);

		$minT = get_term_by('slug', $min, 'pa_overall-length');
		$maxT = get_term_by('slug', $max, 'pa_overall-length');

		$min = $minT->slug;
		$max = $maxT->slug;
		*/

		$qry = "SELECT DISTINCT wp_posts.ID, wp_posts.post_parent, wp_posts.post_type, post_title, t.* FROM wp_posts 

			INNER JOIN wp_term_relationships tr ON (wp_posts.ID = tr.object_id) 

			INNER JOIN wp_term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)

			INNER JOIN wp_terms t ON (tt.term_id = t.term_id) 

			WHERE 1=1 AND wp_posts.post_type IN ( 'product', 'product_variation' ) 

			AND ".$termSlugStr."

			AND tt.`taxonomy` = 'pa_overall-length' 

			AND wp_posts.post_status = 'publish'";

		$matched_products_query = $wpdb->get_results( $qry, OBJECT_K ); 

		if ( $matched_products_query ) {
            foreach ( $matched_products_query as $product ) {
                if ( $product->post_type == 'product' )
                    $matched_products[] = $product->ID;
                if ( $product->post_parent > 0 && ! in_array( $product->post_parent, $matched_products ) )
                    $matched_products[] = $product->post_parent;
            }
        }

        // Filter the id's
        if ( sizeof( $filtered_posts ) == 0) {
            $filtered_posts = $matched_products;
            $filtered_posts[] = 0;
        } else {
            $filtered_posts = array_intersect( $filtered_posts, $matched_products );
            $filtered_posts[] = 0;
        }

    }

    return (array) $filtered_posts;
}

// R-Value handling

function woocommerce_r_value_filter_init() {
	add_filter('loop_shop_post_in', 'woocommerce_r_value_filter');
}
add_action( 'init', 'woocommerce_r_value_filter_init' );

function woocommerce_r_value_filter($filtered_posts) {
    global $wpdb;

    if(isset($_GET['filter_r-value'])) {
		$singleFilter = true;
		$filter_r_value =  $_GET['filter_r-value'];
		$filter_r_value_aryX = explode(',', $filter_r_value);

		$termSlugStr = '( ';

		if(count($filter_r_value_aryX) > 1) {
			$singleFilter = false;
			foreach($filter_r_value_aryX as $filter_r_value_ary_item) {
				$filter_r_value_ary[] = explode('-', $filter_r_value_ary_item);
			}
		} else {
			$filter_r_value_ary = explode('-', $filter_r_value_aryX[0]);
		}

		if($singleFilter) {
			$termSlugStr .= 't.slug BETWEEN '.$filter_r_value_ary[0].' AND '.$filter_r_value_ary[1];
		} else {
			$ctr1 = 1;
			foreach($filter_r_value_ary as $filter_r_value_aryItem) {
				$termSlugStr .= '(t.slug BETWEEN '.$filter_r_value_aryItem[0].' AND '.$filter_r_value_aryItem[1].') ';

				if($ctr1 < count($filter_r_value_ary)) {
					$termSlugStr .= ' OR ';
				}
				$ctr1++;
			}
		}
		$termSlugStr .= ' )';

        $matched_products = array();

		/*
        $min 	= floatval($filter_r_value_ary[0]);
        $max 	= floatval($filter_r_value_ary[1]);

		$minT = get_term_by('slug', $min, 'pa_r-value');
		$maxT = get_term_by('slug', $max, 'pa_r-value');

		$min = $minT->slug;
		$max = $maxT->slug;
		*/

		$qry = "SELECT DISTINCT wp_posts.ID, wp_posts.post_parent, wp_posts.post_type, post_title, t.* FROM wp_posts 

			INNER JOIN wp_term_relationships tr ON (wp_posts.ID = tr.object_id) 

			INNER JOIN wp_term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)

			INNER JOIN wp_terms t ON (tt.term_id = t.term_id) 

			WHERE 1=1 AND wp_posts.post_type IN ( 'product', 'product_variation' ) 

			AND ".$termSlugStr."

			AND tt.`taxonomy` = 'pa_r-value' 

			AND wp_posts.post_status = 'publish'";

		$matched_products_query = $wpdb->get_results( $qry, OBJECT_K ); 

		if ( $matched_products_query ) {
            foreach ( $matched_products_query as $product ) {
                if ( $product->post_type == 'product' )
                    $matched_products[] = $product->ID;
                if ( $product->post_parent > 0 && ! in_array( $product->post_parent, $matched_products ) )
                    $matched_products[] = $product->post_parent;
            }
        }

        // Filter the id's
        if ( sizeof( $filtered_posts ) == 0) {
            $filtered_posts = $matched_products;
            $filtered_posts[] = 0;
        } else {
            $filtered_posts = array_intersect( $filtered_posts, $matched_products );
            $filtered_posts[] = 0;
        }

    }

    return (array) $filtered_posts;
}

// Input Wattage handling

function woocommerce_input_wattage_filter_init() {
	add_filter('loop_shop_post_in', 'woocommerce_input_wattage_filter');
}
add_action( 'init', 'woocommerce_input_wattage_filter_init' );

function woocommerce_input_wattage_filter($filtered_posts) {
    global $wpdb;

    if(isset($_GET['filter_input-wattage'])) {
		$singleFilter = true;
		$filter_input_wattage =  $_GET['filter_input-wattage'];
		$filter_input_wattage_aryX = explode(',', $filter_input_wattage);

		$termSlugStr = '( ';

		if(count($filter_input_wattage_aryX) > 1) {
			$singleFilter = false;
			foreach($filter_input_wattage_aryX as $filter_input_wattage_ary_item) {
				$filter_input_wattage_ary[] = explode('-', $filter_input_wattage_ary_item);
			}
		} else {
			$filter_input_wattage_ary = explode('-', $filter_input_wattage_aryX[0]);
		}

		if($singleFilter) {
			$termSlugStr .= 't.slug BETWEEN '.$filter_input_wattage_ary[0].' AND '.$filter_input_wattage_ary[1];
		} else {
			$ctr1 = 1;
			foreach($filter_input_wattage_ary as $filter_input_wattage_aryItem) {
				$termSlugStr .= '(t.slug BETWEEN '.$filter_input_wattage_aryItem[0].' AND '.$filter_input_wattage_aryItem[1].') ';

				if($ctr1 < count($filter_input_wattage_ary)) {
					$termSlugStr .= ' OR ';
				}
				$ctr1++;
			}
		}
		$termSlugStr .= ' )';

        $matched_products = array();

		/*
        $min 	= floatval($filter_r_value_ary[0]);
        $max 	= floatval($filter_r_value_ary[1]);

		$minT = get_term_by('slug', $min, 'pa_r-value');
		$maxT = get_term_by('slug', $max, 'pa_r-value');

		$min = $minT->slug;
		$max = $maxT->slug;
		*/

		$qry = "SELECT DISTINCT wp_posts.ID, wp_posts.post_parent, wp_posts.post_type, post_title, t.* FROM wp_posts 

			INNER JOIN wp_term_relationships tr ON (wp_posts.ID = tr.object_id) 

			INNER JOIN wp_term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)

			INNER JOIN wp_terms t ON (tt.term_id = t.term_id) 

			WHERE 1=1 AND wp_posts.post_type IN ( 'product', 'product_variation' ) 

			AND ".$termSlugStr."

			AND tt.`taxonomy` = 'pa_input-wattage' 

			AND wp_posts.post_status = 'publish'";

		$matched_products_query = $wpdb->get_results( $qry, OBJECT_K ); 

		if ( $matched_products_query ) {
            foreach ( $matched_products_query as $product ) {
                if ( $product->post_type == 'product' )
                    $matched_products[] = $product->ID;
                if ( $product->post_parent > 0 && ! in_array( $product->post_parent, $matched_products ) )
                    $matched_products[] = $product->post_parent;
            }
        }

        // Filter the id's
        if ( sizeof( $filtered_posts ) == 0) {
            $filtered_posts = $matched_products;
            $filtered_posts[] = 0;
        } else {
            $filtered_posts = array_intersect( $filtered_posts, $matched_products );
            $filtered_posts[] = 0;
        }

    }
    return (array) $filtered_posts;
}

//Lumens handling
function woocommerce_delivered_lumens_filter_init() {
	add_filter('loop_shop_post_in', 'woocommerce_delivered_lumens_filter');
}
add_action( 'init', 'woocommerce_delivered_lumens_filter_init' );

function woocommerce_delivered_lumens_filter($filtered_posts) {
    global $wpdb;

    if(isset($_GET['filter_delivered-lumens'])) {
		$singleFilter = true;
		$filter_delivered_lumens =  $_GET['filter_delivered-lumens'];
		$filter_delivered_lumens_aryX = explode(',', $filter_delivered_lumens);

		$termSlugStr = '( ';

		if(count($filter_delivered_lumens_aryX) > 1) {
			$singleFilter = false;
			foreach($filter_delivered_lumens_aryX as $filter_delivered_lumens_ary_item) {
				$filter_delivered_lumens_ary[] = explode('-', $filter_delivered_lumens_ary_item);
			}
		} else {
			$filter_delivered_lumens_ary = explode('-', $filter_delivered_lumens_aryX[0]);
		}

		if($singleFilter) {
			$termSlugStr .= 't.slug BETWEEN '.$filter_delivered_lumens_ary[0].' AND '.$filter_delivered_lumens_ary[1];
		} else {
			$ctr1 = 1;
			foreach($filter_delivered_lumens_ary as $filter_delivered_lumens_aryItem) {
				$termSlugStr .= '(t.slug BETWEEN '.$filter_delivered_lumens_aryItem[0].' AND '.$filter_delivered_lumens_aryItem[1].') ';

				if($ctr1 < count($filter_delivered_lumens_ary)) {
					$termSlugStr .= ' OR ';
				}
				$ctr1++;
			}
		}
		$termSlugStr .= ' )';

        $matched_products = array();

		/*
        $min 	= floatval($filter_lumens_ary[0]);
        $max 	= floatval($filter_lumens_ary[1]);

		$minT = get_term_by('slug', $min, 'pa_lumens');
		$maxT = get_term_by('slug', $max, 'pa_lumens');

		$min = $minT->slug;
		$max = $maxT->slug;
		*/

		$qry = "SELECT DISTINCT wp_posts.ID, wp_posts.post_parent, wp_posts.post_type, post_title, t.* FROM wp_posts 

			INNER JOIN wp_term_relationships tr ON (wp_posts.ID = tr.object_id) 

			INNER JOIN wp_term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)

			INNER JOIN wp_terms t ON (tt.term_id = t.term_id) 

			WHERE 1=1 AND wp_posts.post_type IN ( 'product', 'product_variation' ) 

			AND ".$termSlugStr."

			AND tt.`taxonomy` = 'pa_delivered-lumens' 

			AND wp_posts.post_status = 'publish'";

		$matched_products_query = $wpdb->get_results( $qry, OBJECT_K ); 

		if ( $matched_products_query ) {
            foreach ( $matched_products_query as $product ) {
                if ( $product->post_type == 'product' )
                    $matched_products[] = $product->ID;
                if ( $product->post_parent > 0 && ! in_array( $product->post_parent, $matched_products ) )
                    $matched_products[] = $product->post_parent;
            }
        }

        // Filter the id's
        if ( sizeof( $filtered_posts ) == 0) {
            $filtered_posts = $matched_products;
            $filtered_posts[] = 0;
        } else {
            $filtered_posts = array_intersect( $filtered_posts, $matched_products );
            $filtered_posts[] = 0;
        }

    }

    return (array) $filtered_posts;
}
