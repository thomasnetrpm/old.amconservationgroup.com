<div class="wrap">
	<h2>Message us Online</h2>

<?php
global $wpdb;
// $wpdb->show_errors();
// $wpdb->print_error();

//Our class extends the MUO_List_Table class, so we need to make sure that it's there
if(!class_exists('MUO_List_Table')){
	require_once( TEMPLATEPATH . '/classes/muoClass.php' );
}

//Prepare Table of elements
$muo_list_table = new MUO_List_Table();
$muo_list_table->prepare_items();
//Table of elements
$muo_list_table->display();
?>

</div>