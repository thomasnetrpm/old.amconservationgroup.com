<?php 
/* 
Template Name: HomeLatest 
*/ ?>

<?php
if(is_mobile()) {
	include('page-home-updated-mobile.php');
	exit();
}
?>
<?php global $amc_urls, $wpdb; ?>

<?php   get_header('home'); ?>
<div class="row">
 <div class="">
      <div class="slider" >
        <div class="flexslider">
          <ul class="slides">
			<?php
			// get The Slider Images Dynamically  From The  Admin //+
			//new WP_Query(array('reorder_term_id' => $term_id));
			$mainImagesSlider = get_posts(array(
			'post_type'  => 'image_carousel',
			'post_status'  => 'publish',
			'posts_per_page'=> -1,
			//'hierarchical'  => true,
			'orderby' => 'menu_order',
			'order' => 'ASC'			
			));
				if($mainImagesSlider) {
					foreach($mainImagesSlider as $Image) {
					//print_r($Image);
					$images = get_posts(array(
					'post_parent' => $Image->ID,
					'post_type' => 'attachment',
					'numberposts' => -1,
					'post_mime_type' => 'image'));
						foreach( $images as $image ) {
						$attachmenturl=wp_get_attachment_url($image->ID);
						$attachmentimage=wp_get_attachment_image_src( $image->ID, full );
						//echo "<pre>"; print_r($image);echo "</pre>";
						$attachment_link = get_post_meta( $Image->ID, 'image_carousel_url',true );
			?>
			<li>
			  <a href="<?php echo $attachment_link; ?>">
			  <div class="text_bg"><p><?php echo $Image->post_content;?></p></div>
			  <img class="picture_a" src="<?php echo $attachmentimage[0]; ?>" alt=""> 
			  </a>
			</li>
            <?php 
						}
					}
				}
			?>
              </ul>
            </div>
          </div>
        </div>
   </div>
   
  <div class="row">
   
   <div class="car-menu">
      <div class="menusession">
	<div class="position">	
	<div class="leftarrow SlideOff"></div>
        <div class="menu-container">
		
		<div class="small_menu">
		
<?php
wp_nav_menu(array(
	'theme_location' => 'middle-menu',
	'link_before' => '',
	'link_after' => '',
	'menu_class' => 'carmenus',
	'container' => false
));
?>
</div>
</div>
<script type="text/javascript">
jQuery('ul.carmenus li').addClass('menu');
jQuery('ul.carmenus li a').addClass('caro-menulink');
// jQuery('ul.carmenus li:last').removeClass().addClass('menulast');
jQuery('ul.carmenus li a[title=Blog]').attr('target', '_blank');
</script>
                  
                 </div>				 
               </div>
          </div>
      </div>
   
      <!-- Button Links -->
  <div class="row">
    <div class="am_conversion">
      <div class="am-container">
        <h2 class="am-header"> AM Conservation Group</h2>
        <span class="dot"></span>
        <p class="yourone-txt">Your one source for energy and water efficiency products, services, and programs of any size 
          with unmatched speed and flexibility. </p>
        <div class="bs-example twinbtn">
          <button type="button" class="btn btn-default home" onclick="window.location='<?php echo $amc_urls['sc_url']; ?>';"> 
            <span class="amctwin-txt1">Home Owners Click Here </span> 
            <span class="arrowicon1"></span> </button>
            <!-- <span class="homeowner">You will be redirected to our Simply Conserve shopping site where our products are available for purchase.</span> -->
          <button type="button" class="btn btn-default professional" onclick="window.location='<?php echo $amc_urls['home_category_landing']; ?>';"> 
            <span class="amctwin-txt2">Professionals/Businesses Click Here</span> 
            <span class="arrowicon2"></span> </button>
			<a  data-toggle="modal" data-target=".requestaquote"><button type="button" class="btn btn-default requestquote">To Request A Sales Quote Click Here<span class="arrowicon3"></span></button></a>    			
        </div>
      </div>
    </div>
  </div>
 
 <div class="row"> 
    <div class="px-phoneimgwrap">
      <div class="px-phoneimg"> <img class="px-ImageBg" src="<?php echo get_template_directory_uri(); ?>/assets/images/pipefitting_bg.jpg" />
        <div id="px-Program" class="px-BtmBox"><span>Programs & Services</span>

          <div class="px-BtmBox-hotspot">
            <div></div>
            <button class="btn btn-warning" onclick="window.location.href = '<?php echo $amc_urls['programs_services']; ?>';">Read More</button>
          </div>

          <p>AM Conservation Group has years of proven expertise having managed all types and sizes of programs for utility companies, program managers and government agencies throughout the U.S. You can count on us to provide turnkey program solutions to ensure that your program and planning goals are met on time and on budget.</p>
		  <button class="btn btn-warning btn-touch" onclick="window.location.href = '<?php echo $amc_urls['programs_services']; ?>';">Read More</button>
			<span class="border-pic"></span>
		</div>
        <div id="px-Testimonial" class="px-BtmBox"><span>Testimonials</span>

          <!--<div class="px-BtmBox-hotspot">
            <div></div>
            <button class="btn btn-warning" onclick="window.location.href = '<?php echo $amc_urls['testimonial']; ?>';">Read More</button>
          </div>-->
           <div class="contentslider">
                            <ul class="slides">
                            <?php
                            $tResults = $wpdb->get_results('SELECT message AS content, NAME AS author,id,YEAR(created) AS Y  FROM wp_vm_testimonials WHERE (STATUS = "A" AND isinpool = "yes") ORDER BY created DESC LIMIT 8 ', ARRAY_A);
                            if(!empty($tResults) && is_array($tResults)) {
                              foreach($tResults as $k => $tResultsItem) {
                              ?>
                                <li>
                                    <div class="slide-content">
                                        <p> <a href="<?php echo $amc_urls['testimonial']; ?>?y=<?php echo $tResultsItem["Y"]; ?>"> <?php echo wp_trim_words($tResultsItem['content'], 20, ' ...'); // echo esc_html(substr($tResultsItem['content'],0,300)); ?></a></p>
										                    <div class="slide-author"><strong><?php echo $tResultsItem['author'];?></strong></div>
                                        <!--<div class="btn-container"><a href="<?php echo $amc_urls['testimonial']; ?>?y=<?php echo $tResultsItem["Y"]; ?>" class="btn btn-warning"> Read More </a> </div>-->
                                    </div>
                                </li>
                                <?php
                                 }
                                } 
                              ?>
                            </ul>
                        </div>
		<span class="border-pic"></span>
        </div>
        <div id="px-Talktous" class="px-BtmBox"><span>Talk To Us</span>

          <div class="px-BtmBox-hotspot">
            <div></div>
            <button class="btn btn-warning" onclick="window.location.href = '<?php echo $amc_urls['contact_us']; ?>';">Read More</button>
          </div>

          <p>Our knowledgeable staff is ready to assist you with your product and program needs. Having served some of the largest utility programs in the country, our expertise is unmatched. Call our AM Conservation Group team today at 1. 888. 866.1624 to learn how we can provide solutions to your specific needs.</p>
		  <button class="btn btn-warning btn-touch " onclick="window.location.href = '<?php echo $amc_urls['contact_us']; ?>';">Read More</button>
			<span class="border-pic"></span>
		</div>
      </div>
    </div>
  </div>
 <div class="modified_form"><?php require_once('woocommerce/request-quote-modal.php'); ?></div> 

<?php   get_footer('home'); ?>

