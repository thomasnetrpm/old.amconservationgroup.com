<?php 
/* 
Template Name: FAQ  template 
*/ 

if(is_mobile()) {
	include('faq-mobile-template.php');
	exit();
}

define("CSS", "faq.css");

global $amc_urls; 

?>
<?php   get_header(); ?>

<div class="row">      
       <div class="col-md-12 faqbanner">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/faq.png">
        </div>  
    </div>

 <!-- Middle Mennu  -->
        <div class="row">
             <div class="aboutmenu-container">
                 <?php require_once('middle-menu.php'); ?>
             </div>
    </div>
     <!--/  Ends Middle Mennu  -->
 <div class="row">
             <div class="faqcontent-container">
                <div clas="col-md-10 col-md--offset-2">
                  <p class="faq-caption">Below is a list of the questions most frequently asked by our customers. <br />If you do not find the answer to your question below please contact us at <?php echo $amc_urls['contact_number']; ?>.
                  </p>                                
                </div>
             </div>
    </div>    
    <div class="row">
          <div class="faqcontent-container">
                <div clas="col-md-3">
                  <div class="faq_left">
   <?php
   // PHP code for Getting the Faq  Categories // 
      $args = array(
                    'orderby' => 'name',
                    'order' => 'ASC',
                    'taxonomy'  => 'faq-topic',
                    'hide_empty'               => 0,
                    'hierarchical'             => 1
                    );
      $faq_categories = get_categories($args);
      $class ="orangebox";
        foreach($faq_categories as $faq) {
         $Faq_Set_array[] = array("slug"=>$faq->slug);
        ?>
       <div class="<?php echo $class; ?>" id="<?php echo $faq->slug; ?>" style="cursor:pointer;"><?php echo $faq->name; ?></div>
       <?php 
       $class = "bluebox";
        }
        ?>
                  </div>  
                </div> 
                <div class="col-md-8">
                  <div class="faq_right">

                  <?php 
                         
                          global $post;
                           $display ="";
                          foreach($Faq_Set_array as $faq_slug) { 
                            ?>
                  <div id="faq-<?php echo $faq_slug['slug'];?>" style="<?php echo $display; ?>">          
                            <?php
                          $args = array( 'orderby' => 'menu_order', 'order' => 'asc', 'post_type' => 'question','posts_per_page' => -1,'faq-topic' =>''.$faq_slug["slug"].'');
                           $myposts = get_posts( $args );  
                           foreach( $myposts as $post ) {   setup_postdata($post);

                    ?>
                       
                         <p class="question"><strong>Q: <?php the_title(); ?> </strong></p>
                         <p class="ans">  A: <?php echo ($post->post_content); ?>  </p>
                        
                    <?php 
                    }
                    echo '</div>';
                   $display = "display:none;"; 
                                            } ?>    
     
     
                  </div> 
                </div> 
          </div>       
    </div>  

     <script type="text/javascript">
        $(".faq_left > *").on( "click", function() {
          $(".faq_left > *").attr("class","bluebox");
          $( this ).attr("class","orangebox");
          $(".faq_right > *").hide();
          // $($(".faq_right > *")[$( this ).index()]).show();
          //console.log($(this).attr("id"));
          $("#faq-"+$(this).attr("id")).show();
          return false;
        });
        </script> 
           
     



<?php   get_footer(); ?>    