<?php

define("CSS", "news_amc_style.css");

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}
?>

<?php 
global $theme_shortname;  
$meta = icore_get_multimeta(array('Subheader'));
$location = icore_get_location();    
?>
<?php if(is_mobile()) { ?>
<div class="row headersession">      
	<div class="col-md-7"><h3 class="headerlabel">News<?php // the_title(); ?></h3></div>
	<div class="col-md-5 breadcrumbs inBread">
	  <ol class="breadcrumb">
	   <?php breadcrumbs_fmg();  ?>
	  </ol>
	</div>    

<div class="col-md-4 back-btn back-page-com"> <a onClick="history.go(-1)"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/back_btn.png">Back </a></div>
<?php } ?>
</div>
<div class="container">
<div class="row">      
        <div class="col-md-12 aboutbanner"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/News.jpg"></div>  
    </div>

 <!-- Middle Mennu  -->
        <div class="row menu_pos">
			<div class="leftarrow-inner SlideOff"></div>
             <div class="aboutmenu-container">
               <?php require_once('middle-menu.php'); ?>
             </div>
    </div>
    <!--    content area start      -->


       <div class="row">

          <!--left content -->
<div class="col-md-7">
  <!--Trademarks-->
        <div class="row"> <div class="col-md-10 col-md-offset-1 cs-top-spacer-1">
         <h3 class="headerlabel1headerlabel1">Press Release </h3> 
         <p class="cs-content-heading2 page-spacing ">
           <?php the_title(); ?>
           
         </p>            
         </div> </div>
      <!--Trademarks -->

<?php if (have_posts()) : while (have_posts()) : the_post();?>
<?php the_content(); ?>
<?php endwhile; endif; ?>

<?php
global $post;
$curNewsId = $post->ID;
$mainContentNewsItems = new WP_Query(array(
	'post_type' => 'news',
	'news-category' => 'maincontent',
	'post__not_in' => array($curNewsId),
));

foreach($mainContentNewsItems->posts as $mainContentNewsItem) {
?>
<div class="row">
	<div class=" col-md-10 col-md-offset-1 ">
		<p class="cs-content-1 page-spacing"><span class="cs-content-heading2"><?php echo $mainContentNewsItem->post_title; ?></span><br><a href="<?php echo get_permalink($mainContentNewsItem->ID); ?>"><span style="color:#e08a12; text-decoration:underline ">Read More</span></a></p>
	</div>
</div>
<div class="row"><div class=" col-md-4  col-md-offset-1  div-line2 "></div></div> 
<?php
}
?>

</div>
<!--left content -->

<!--mid content -->
<div class="col-md-1  new-mid-div hidden-xs hidden-sm"> 

<div class="news-top-spacer2"></div> 

<div class="row"><div class=" col-md-2 col-md-offset-1 ">
<div class="col-md-2 divider hidden-xs hidden-sm "><img class="line-divider1" src="<?php echo get_template_directory_uri(); ?>/assets/images/news_bg1.png"></div>
</div></div>


<div class="row"> <div class=" col-md-2 col-md-offset-1 ">
  
         <div class="col-md-2 divider hidden-xs hidden-sm "><img class="line-divider2" src="<?php echo get_template_directory_uri(); ?>/assets/images/news_bg2.png"></div>
         </div> </div>
         
  <div class="row"><div class=" col-md-2 col-md-offset-1 ">
<div class="col-md-2 divider row hidden-xs hidden-sm"><img class="line-divider3" src="<?php echo get_template_directory_uri(); ?>/assets/images/news_bg3.png"></div>
</div></div>       
         
         </div>
<!--mid content -->

<!--right content -->
<div class="col-md-4">

<div class="row hidden-xs hidden-sm "> <div class=" col-md-2 "> </div></div>
<div class="row "> <div class=" col-md-10"> <div class="news-top-spacer hidden-xs hidden-sm "></div>
<div class="row"> <div class=" col-md-10 ">
  <h3 class="headerlabel2">News Articles </h3>     
 <p class="cs-content-2 page-spacing"><span class="cs-content-heading3">  <a href="http://www.energymanagertoday.com/energy-efficiency-taught-to-kids-nationwide-094893/" target="_blank">Energy Efficiency Taught to Kids Nationwide</a> 
</span><br>August 23, 2013<br> <br>
Energy Manager Today interviews Todd Recknagel, CEO of AM Conservation Group, Inc. </p>  
         
         </div>
       
         </div>
         
         
<div class="row"> <div class=" col-md-10  div-line "> </div></div>
  
  
<div class="row"> <div class=" col-md-10  ">
         <p class="cs-content-2 page-spacing"><span class="cs-content-heading3"> E Source - Profiling Residential Smart Strips: <br> Is Your Program Smart?</a> </span><br> 
     July 25, 2013 : <br> <br> E Source, Alexandra Behringer - Associate Research Director  
 </p>            
         </div> </div>
         
         <div class="row"> <div class=" col-md-10  div-line "> </div></div>
         
      <div class="row"> <div class=" col-md-10  ">
         <p class="cs-content-2 page-spacing "><span class="cs-content-heading3">Why Your Energy Company Wants You to Use Less Energy</a></span><br> 
    April 2, 2013 : <br> <br> U.S. News &amp; World Report interviews Todd Recknagel, CEO of AM Conservation Group, Inc.

 </p>            
         </div> </div>
            
       <div class="row"> <div class=" col-md-10  div-line "> </div></div>
       
       
      <div class="row"> <div class=" col-md-10 ">
         <p class="cs-content-2 page-spacing"><span class="cs-content-heading3">Businesses Weigh Response If New Climate Rules Come</a>
</span><br>February 12, 2013: <br> <br>
The Wall Street Journal interviews Todd Recknagel, CEO of AM Conservation Group, Inc.</p>            
         </div> </div>
         
         <div class="row"> <div class=" col-md-10  div-line "> </div></div>

<div class="row"> <div class=" col-md-10  ">
         <p class="cs-content-2 page-spacing"><span class="cs-content-heading3">   <a href="http://www.foxbusiness.com/personal-finance/2012/10/17/how-to-reduce-your-utility-bills-this-winter/" target="_blank">How to Reduce Your Utility Bills This Winter</a></span><br> 
     October 17, 2012:   <br> <br> FOXBusiness interviews Todd Recknagel, CEO, on ways to lower utility bills during the winter. 
 </p>            
         </div> </div>
         
         <div class="row"> <div class=" col-md-10  div-line "> </div></div>
         
         
      <div class="row"> <div class=" col-md-10  ">
         <p class="cs-content-2 page-spacing "><span class="cs-content-heading3">     <a href="http://www.homechannelnews.com/article/am-conservation-group-rolls-out-simply-conserve-line" target="_blank">AM Conservation Group rolls out Simply Conserve line</a></span><br> 
    September 11, 2012: <br> <br>Home Channel News introduces Simply Conserve brand.

 </p>            
         </div> </div>
            
            <div class="row"> <div class=" col-md-10  div-line "> </div></div>
         
  <div class="row"> <div class=" col-md-10  ">
         <p class="cs-content-2 page-spacing"><span class="cs-content-heading3">   <a href="http://www.realenergywriters.com/ee-podcast/2012/08/16/ee-is-hot-when-temperatures-are-high/" target="_blank">EE is Hot when Temperatures are High</a>
</span><br>August 16, 2012: <br> <br>
Lisa Cohn of RealEnergyWriters.com interviews Todd Recknagel, CEO of AM Conservation Group.</p>            
         </div> </div>

<div class="row"> <div class=" col-md-10  div-line "> </div></div>





<div class="row"> <div class=" col-md-10  ">
         <p class="cs-content-2 page-spacing "><span class="cs-content-heading3">  <a href="http://www.workingmother.com/family-time/daddy-board-saying-thanks-dad" target="_blank">Daddy on Board: Saying Thanks to Dad</a></span><br> 
     March 4, 2011:  <br> <br> Working Mother recommends the Dish Squeegee  as a great chore helper for dads and kids. 
 </p>            
         </div> </div>
         
       <div class="row"> <div class=" col-md-10  div-line "> </div></div>
         
      <div class="row"> <div class=" col-md-10  ">
         <p class="cs-content-2 page-spacing"><span class="cs-content-heading3"> <a href="http://giftandhometoday.blogspot.com/2011/02/eco-friendly-kitchen-gadget-promotes.html" target="_blank">Eco-friendly Kitchen Gadget Promotes Water Savings</a></span><br> 
   February 10, 2011:  <br> <br>"Use the squeegee to push leftovers into a garbage pail or disposal (or better yet, compost nonmeat waster)." - Gift &amp; Home Today

 </p>            
         </div> </div>
         
         
         
         
     
         
          <div class="row"> <div class=" col-md-10  div-line"> </div></div>
         
      <div class="row"> <div class=" col-md-10  ">
         <p class="cs-content-2 page-spacing"><span class="cs-content-heading3"> <a href="http://blog.nj.com/hg_impact/print.html?entry=/2010/12/dish_squeegee_saves_water.html" target="_blank">Dish Squeegee Saves Water</a></span><br> 
   December 27, 2010:  <br> <br>

 </p>            
         </div> </div>
         
          <div class="row"> <div class=" col-md-10  div-line "> </div></div>
         
      <div class="row"> <div class=" col-md-10  ">
         <p class="cs-content-2 page-spacing"><span class="cs-content-heading3"> <a href="http://www.gourmetretailer.com/top-story-housewares_design_awards_finalists_announced-9655.html" target="_blank">2011 Housewares Design Awards Finalists Named</a></span><br> 
  
November 29, 2010: <br> <br>Dish Squeegee™ named as Finalist 

 </p>            
         </div> </div>
         
          <div class="row"> <div class=" col-md-10  div-line "> </div></div>
         
      <div class="row"> <div class=" col-md-10  ">
         <p class="cs-content-2 page-spacing"><span class="cs-content-heading3"> <a href="http://www.moneypit.com/print/6607" target="_blank">Apartment and House Insulation Tips - Heat It Up!</a></span><br> 
 November 22, 2010:  <br> <br>Money Pit recommends multiple AM Conservation Group products for insulating and saving around the home. 

 </p>            
         </div> </div>
         
          <div class="row"> <div class=" col-md-10  div-line "> </div></div>
         
      <div class="row"> <div class=" col-md-10  ">
         <p class="cs-content-2 page-spacing"><span class="cs-content-heading3"> <a href="http://www.realsimple.com/work-life/money/saving/have-on-winterizing-home-00000000045337/index.html" target="_blank">How to Save on Winterizing Your Home</a></span><br> 
   November 2010: <br> <br>Real Simple magazine recommends the Furnce Filter Whistle to alert you when your filter becomes clogged. 

 </p>            
         </div> </div>
         
          <div class="row"> <div class=" col-md-10  div-line "> </div></div>
         
      <div class="row"> <div class=" col-md-10  ">
         <p class="cs-content-2 page-spacing"><span class="cs-content-heading3"> <a href="http://www.homechannelnews.com/article/iha-design-event-shows-innovation" target="_blank">IHA Design Event Shows Off Innovation</a></span><br> 
  
October 25, 2010:  <br> <br>Home Channel News highlights the Plug Guard® and Switch Plate Energy Save (F/K/A Universal Wall Plate Thermometer). 

 </p>            
         </div> </div>
         
          <div class="row"> <div class=" col-md-10  div-line "> </div></div>
         
      <div class="row"> <div class=" col-md-10  ">
         <p class="cs-content-2 page-spacing"><span class="cs-content-heading3"> <a href="http://www.allyou.com/budget-home/organizing-cleaning/cheap-kitchen-gadgets-tools/dishwasher-aid" target="_blank">20 Kitchen Gadgets Under $20</a></span><br> 
   September 24, 2010: <br> <br>Home Channel News highlights the Plug Guard® and Switch Plate Energy Save (F/K/A Universal Wall Plate Thermometer). 

 </p>            
         </div> </div>

         
      <div class="row"> <div class=" col-md-10  ">
         <p class="cs-content-2 page-spacing"><span class="cs-content-heading3"> <a href="http://www.housewares.org/show/exhibit/dir/files/780043965_PRESS%20RELEASE-%202010%20Design%20Defined%20Honorees.pdf" target="_blank">26 Products Named 2010 Design Defined Honoree At International Home + Housewares Show</a></span><br> 
April 14, 2010 <br> <br>

 </p>            
         </div> </div>
         
         
           <div class="row"> <div class=" col-md-10  div-line "> </div></div>


        
           <div class="row"> <div class=" col-md-10  ">
         <p class="cs-content-2 page-spacing"><span class="cs-content-heading3"> International Home &amp; Housewares Show Favorite Green Products</a></span><br> 

March 2010 : <br> <br>26 Video produced by local Chicago news station, CLTV, highlights the Switch Plate Energy Saver (F/K/A Universal Wall Plate Thermometer).  
 </p>            
         </div> </div> 
         
          <div class="row"> <div class=" col-md-10  div-line "> </div></div>
          
      
<div class="row"> <div class=" col-md-10  ">
         <p class="cs-content-2 page-spacing"><span class="cs-content-heading3"> <a href="http://inventorspotforum.com/viewtopic.php?f=20&amp;t=3867" target="_blank"> 4 inventions to Market in 1 Year - by 1 Inventor</a></span><br> 

January 19, 2010  <br> <br>
 </p>            
         </div> </div>


           <div class="row"> <div class=" col-md-10  div-line "> </div></div>
                   </div></div>

                 
      


 </div>
<!--ight content -->
        
</div>

</div>
<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>