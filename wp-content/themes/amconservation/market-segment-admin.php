<div class="wrap">
	<h2>Market segments</h2>

	<p><a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=market_segment&action=add" title="Add">Add</a></p>

<?php
if(isset($_SESSION['msg'])) {
	if($_SESSION['msg'] == 'deletesuccess') {
		echo '<p style="color:green;">Deleted successfully.</p>';
	}
	if($_SESSION['msg'] == 'activesuccess') {
		echo '<p style="color:green;">Made active successfully.</p>';
	}
	if($_SESSION['msg'] == 'inactivesuccess') {
		echo '<p style="color:green;">Made inactive successfully.</p>';
	}
	unset($_SESSION['msg']);
}
?>

<?php
global $wpdb;
// $wpdb->show_errors();
// $wpdb->print_error();

//Our class extends the Acctrep_List_Table class, so we need to make sure that it's there
if(!class_exists('Acctrep_List_Table')){
	require_once( TEMPLATEPATH . '/classes/marketSegmentClass.php' );
}

$action = (isset($_GET['action'])) ? $_GET['action'] : '';

if($action == 'add') {
	// Handle the add screen
	$formErrorAdd = array();
	$formSuccessAdd = array();
	if(isset($_POST['marketsegmentAddFrm_submit']) && $_POST['marketsegmentAddFrm_submit'] == 'Add') {
		$marketsegmentAddFrm_title = sanitize_text_field($_POST['marketsegmentAddFrm_title']);
		$marketsegmentAddFrm_is_active = sanitize_text_field($_POST['marketsegmentAddFrm_is_active']);

		if(empty($marketsegmentAddFrm_title)) {
			$formErrorAdd[] = 'Please enter the name.';
		}

		if(empty($formErrorAdd)) {
			$wpdb->insert('wp_marketsegments', array(
				'created' => current_time('mysql'), 
				'modified' => current_time('mysql'),
				'marketsegment' => $marketsegmentAddFrm_title,
				'is_active' => ((isset($marketsegmentAddFrm_is_active) && $marketsegmentAddFrm_is_active == 'yes') ? 1 : 0)
			));
			$formSuccessAdd[] = 'Added successfully.';
		}
	}
?>
<h3>Add - Market Segment</h3>
<form name="marketsegmentAddFrm" action="" method="post">
<div>

<?php
if(!empty($formSuccessAdd)) {
	echo '<p style="color:green;">Status follows,</p>';
	echo '<ol>';
	foreach($formSuccessAdd as $formSuccessAddItem) {
		echo '<li>'.$formSuccessAddItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}

if(!empty($formErrorAdd)) {
	echo '<p style="color:red;">The following errors found,</p>';
	echo '<ol>';
	foreach($formErrorAdd as $formErrorAddItem) {
		echo '<li>'.$formErrorAddItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}
?>

<p><label for="marketsegmentAddFrm_title">Market Segment</label><br /> <input type="text" name="marketsegmentAddFrm_title" value="" id="marketsegmentAddFrm_title" /></p>
<p><label for="marketsegmentAddFrm_is_active">Is Active?</label> <input type="checkbox" name="marketsegmentAddFrm_is_active" value="yes" id="marketsegmentAddFrm_is_active" /></p>
<p><input type="submit" name="marketsegmentAddFrm_submit" value="Add" /></p>
</div>
</form>
<?php

} else if($action == 'edit') {
	// Handle the edit screen
	$marketsegmentId = $_GET['id'];
	$marketsegmentRow = $wpdb->get_row('SELECT * FROM wp_marketsegments WHERE id = "'.$marketsegmentId.'"');

	$formErrorEdit = array();
	$formSuccessEdit = array();
	if(isset($_POST['marketsegmentEditFrm_submit']) && $_POST['marketsegmentEditFrm_submit'] == 'Edit') {
		$marketsegmentEditFrm_title = sanitize_text_field($_POST['marketsegmentEditFrm_title']);
		$marketsegmentEditFrm_is_active = sanitize_text_field($_POST['marketsegmentEditFrm_is_active']);

		if(empty($marketsegmentEditFrm_title)) {
			$formErrorEdit[] = 'Please enter the name.';
		}

		if(empty($formErrorEdit)) {
			$wpdb->update('wp_marketsegments', array(
				'modified' => current_time('mysql'),
				'marketsegment' => $marketsegmentEditFrm_title,
				'is_active' => ((isset($marketsegmentEditFrm_is_active) && $marketsegmentEditFrm_is_active == 'yes') ? 1 : 0)
			), array(
				'id' => $marketsegmentId
			));
			$formSuccessEdit[] = 'Edit successfully.';
			$marketsegmentRow = $wpdb->get_row('SELECT * FROM wp_marketsegments WHERE id = "'.$marketsegmentId.'"');
		}
	}
?>
<h3>Edit - Market Segment</h3>
<form name="marketsegmentEditFrm" action="" method="post">
<div>

<?php
if(!empty($formSuccessEdit)) {
	echo '<p style="color:green;">Status follows,</p>';
	echo '<ol>';
	foreach($formSuccessEdit as $formSuccessEditItem) {
		echo '<li>'.$formSuccessEditItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}

if(!empty($formErrorEdit)) {
	echo '<p style="color:red;">The following errors found,</p>';
	echo '<ol>';
	foreach($formErrorEdit as $formErrorEditItem) {
		echo '<li>'.$formErrorEditItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}
?>

<p><label for="marketsegmentEditFrm_title">Market Segment</label><br /> <input type="text" name="marketsegmentEditFrm_title" value="<?php echo $marketsegmentRow->marketsegment; ?>" id="marketsegmentEditFrm_title" /></p>
<p><label for="marketsegmentEditFrm_is_active">Is Active?</label> <input type="checkbox" name="marketsegmentEditFrm_is_active" value="yes" id="marketsegmentEditFrm_is_active" <?php echo (($marketsegmentRow->is_active == 1) ? 'checked="checked"' : ''); ?> /></p>
<p><input type="submit" name="marketsegmentEditFrm_submit" value="Edit" /></p>
</div>
</form>
<?php
} else if($action == 'delete') {
	// Handle the delete action
	$marketsegmentId = $_GET['id'];
	$wpdb->delete('wp_marketsegments', array(
		'id' => $marketsegmentId
	));

	$_SESSION['msg'] = 'deletesuccess';
	wp_safe_redirect(site_url().'/wp-admin/admin.php?page=market_segment'); exit;
} else if($action == 'makeactive') {
	// Handle the active action
	$marketsegmentId = $_GET['id'];
	$wpdb->update('wp_marketsegments', array(
		'is_active' => 1
	), array(
		'id' => $marketsegmentId
	));

	$_SESSION['msg'] = 'activesuccess';
	wp_safe_redirect(site_url().'/wp-admin/admin.php?page=market_segment'); exit;
} else if($action == 'makeinactive') {
	// Handle the inactive action
	$marketsegmentId = $_GET['id'];
	$wpdb->update('wp_marketsegments', array(
		'is_active' => 0
	), array(
		'id' => $marketsegmentId
	));

	$_SESSION['msg'] = 'inactivesuccess';
	wp_safe_redirect(site_url().'/wp-admin/admin.php?page=market_segment'); exit;
} else {
	//Prepare Table of elements
	$marketsegment_list_table = new Acctrep_List_Table();
	$marketsegment_list_table->prepare_items();
	//Table of elements
	$marketsegment_list_table->display();
}
?>

</div>