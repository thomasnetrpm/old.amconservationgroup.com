<?php 
//ob_start();
//session_start();
/* 
Template Name: Contactus
*/

define("CSS", "contact.css"); 

global $wpdb, $amc_urls, $show_msg, $success_msg, $error_msg;
$show_msg = 'none';

// Check for login user
if(userIsLoggedIn()) {
	$loggedInUser = getLoggedInUser();
	$uName = $loggedInUser->firstname.' '.$loggedInUser->lastname;
	$uCompany = $loggedInUser->companyname;
	$uEmail = $loggedInUser->email;
}

$acctReps = $wpdb->get_results("SELECT acctrepname FROM wp_acctreps WHERE is_active = 1 Order by acctrepname ASC");

foreach($acctReps as $acctRepsItem) {
	$acctRepOpts .= '<option value="'.$acctRepsItem->acctrepname.'">'.$acctRepsItem->acctrepname.'</option>';
}

// Handle the form submission
$form_errors = array();
$is_contact_added = 0;

if(isset($_POST['contUsFrm_submit']) && ($_POST['contUsFrm_submit'] == 'SUBMIT') && isset($_POST['name_contUsFrm']) && wp_verify_nonce($_POST['name_contUsFrm'],'action_contUsFrm')) {

			if(sanitize_text_field($_POST['contUsFrm_name']) == '') {
				$form_errors['contUsFrm_name'] = 'Please enter name';
			}
			if(sanitize_text_field($_POST['contUsFrm_email']) == '') {
				$form_errors['contUsFrm_email'] = 'Please enter email address';
			} else {
				if(!is_email(sanitize_text_field($_POST['contUsFrm_email']))) {
					$form_errors['contUsFrm_email'] = 'Please enter valid email address';
				}
			}
			if(sanitize_text_field($_POST['contUsFrm_phone']) == '') {
				$form_errors['contUsFrm_phone'] = 'Please enter a valid phone number';
			}
			if(sanitize_text_field($_POST['contUsFrm_ext']) == '') {
				// $form_errors['contUsFrm_ext'] = 'Please enter extension';
			}
			if(sanitize_text_field($_POST['contUsFrm_company']) == '') {
				$form_errors['contUsFrm_company'] = 'Please enter company name';
			}
			if(sanitize_text_field($_POST['contUsFrm_howhelp']) == '') {
				$form_errors['contUsFrm_howhelp'] = 'Please enter how to help';
			}

			if(empty($form_errors)) {
				// Mail contact us information to admin
					
				$contactUsMsg = 'Name : '.sanitize_text_field($_POST['contUsFrm_name'])."<br />";
				$contactUsMsg .= 'Email : '.sanitize_text_field($_POST['contUsFrm_email'])."<br />";
				$contactUsMsg .= 'Telephone : '.sanitize_text_field($_POST['contUsFrm_phone']).' X'.sanitize_text_field($_POST['contUsFrm_ext'])."<br />";
				$contactUsMsg .= 'Company : '.sanitize_text_field($_POST['contUsFrm_company'])."<br />";
				$contactUsMsg .= 'Please let us know how we can assist you? : '.sanitize_text_field($_POST['contUsFrm_howhelp'])."<br />";
				$contactUsMsg .= 'Preferred contact method : '.sanitize_text_field($_POST['contUsFrm_prefcont'])."<br />";
				$contactUsMsg .= 'Name of the account representative you worked with : '.sanitize_text_field($_POST['contUsFrm_acctrepname'])."<br />";
				
				$mailHeaders = 'From: '.$amc_urls['mail_from_name'].' <'.get_bloginfo('admin_email').'>' . "\r\n";

				add_filter( 'wp_mail_content_type', 'set_html_content_type' );
				$mailStatus = wp_mail(
					get_option('contact_us_email_field'), 
					'Request for Help on AMConservation', 
					stripslashes($contactUsMsg), 
					$mailHeaders);
				remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
						
				$ins_ID = $wpdb->insert( 
					'wp_contactus', 
					array( 
						'created' => current_time('mysql'), 
						'modified' => current_time('mysql'),
						'username' => sanitize_text_field($_POST['contUsFrm_name']),
						'email' => sanitize_text_field($_POST['contUsFrm_email']),
						'phone' => sanitize_text_field($_POST['contUsFrm_phone']).' X'.sanitize_text_field($_POST['contUsFrm_ext']),
						'company' => sanitize_text_field($_POST['contUsFrm_company']),
						'howcanhelp' => sanitize_text_field($_POST['contUsFrm_howhelp']),
						'prefcontact' => sanitize_text_field($_POST['contUsFrm_prefcont']),
						'acctrepname' => sanitize_text_field($_POST['contUsFrm_acctrepname'])
					) 
				);

				if($ins_ID) {
					$is_contact_added = 1;

		// Handle the SageCRM
		$dataSageCRM = array("name" => sanitize_text_field($_POST['contUsFrm_name']),
			"company" => sanitize_text_field($_POST['contUsFrm_company']),
			"email" => sanitize_text_field($_POST['contUsFrm_email']),
			"telephone" => sanitize_text_field($_POST['contUsFrm_phone']),
			"extension" => sanitize_text_field($_POST['contUsFrm_ext']),
			"comment"   => sanitize_text_field($_POST['contUsFrm_howhelp']),
			"preferred_contact" => sanitize_text_field($_POST['contUsFrm_prefcont']),
			"account_rep_name" => sanitize_text_field($_POST['contUsFrm_acctrepname']),
		);
		$jsonSageCRM = json_encode($dataSageCRM);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'http://98.124.78.27/CrmWebServices/AMCGWebsiteToCrmService.svc/AddContactUsSubmission');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonSageCRM);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($jsonSageCRM))                                                                       
		);
		
		
		$result = curl_exec($ch);
		$form_id = 'contact_us';
		include('hubspot-form-api.php');

					unset($_POST);
				}
				?>
<!-- Google Code for New Contact Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ /
var google_conversion_id = 1071850939;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "dUDmCMvhqVkQu8uM_wM";
var google_remarketing_only = false;
/ ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1071850939/?label=dUDmCMvhqVkQu8uM_wM&guid=ON&script=0"/>
</div>
</noscript>					
				<?php
			}
	
}

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}
?>
 
<div class="row"> 
     
        <div class="col-md-12 contactbanner aboutbanner"><!--<img src="<?php echo get_template_directory_uri(); ?>/assets/images/contactmap.jpg" class="contactbanner-img" >-->
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3349.324264520434!2d-79.907448!3d32.91603!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88fe6f680d74465f%3A0x71794a3738f3ef15!2sAM+Conservation+Group+Inc!5e0!3m2!1sen!2s!4v1443681715996" width="100%" height="260" frameborder="0" style="border:0" allowfullscreen></iframe>
		<div class="map-content">
				<div class="map-content-wrap">
				<h2>AM Conservation Group, Inc</h2>
				<p>2301, Charleston Regional Parkway<br/>Charleston, SC 29492 USA.</p>
				<span class="rgtarrow"></span>
				</div>
			</div>
		</div>  
		
    </div>

 <!-- Middle Mennu  -->
        <div class="row menu_pos">
        <div class="leftarrow-inner SlideOff"></div>
             <div class="aboutmenu-container">
                 <?php require_once('middle-menu.php'); ?>
             </div>
    </div>
     <!--/  Ends Middle Mennu  -->
         <div class="row">
          <div class="contact-container">
              <div class="col-md-4  col-md-offset-1 cc-left">
<div class="contact-form-container">

<?php
if($is_contact_added == 1) {
	// echo '<p>Contact added successfully.</p>';
	$show_msg = 'block';
	$success_msg = 'Thank you. We will get back to you soon. Please subscribe to our newsletter for latest updates.'; // 'Contact added successfully.';
}

if(!empty($form_errors)) {
	/*
	echo '<p>Some errors found.</p>';
	echo '<ol>';
	foreach($form_errors as $form_errors_item) {
		echo '<li>'.$form_errors_item.'</li>';
	}
	echo '</ol>';
	*/
	$show_msg = 'block';
	$error_msg = 'Errors found. Try, again.';
}
?>

<form name="contUsFrm" id="contUsFrm" method="post" action="" parsley-validate novalidate>
<?php wp_nonce_field('action_contUsFrm', 'name_contUsFrm'); ?>
   <h3 class="contact-get-txt"> Get in touch</h3> 
   <p class="contact-parag-txt">Please call us, or use the form below to make contact:</p>
<script charset="utf-8" src="//js.hsforms.net/forms/current.js"></script>
<script>
  hbspt.forms.create({ 
    portalId: '448562',
    formId: '96ebc406-0d14-4ef5-a804-4137d9e1c34f',
   
  });
</script>

<br/>
</div>  



              </div>
              <div class="col-md-2 divider hidden-xs hidden-sm"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/divider.png"></div>
              <div class="col-md-5 cleardiv">
                <div class="contact-address-container">
<?php
/*
                   <h3 class="contact-get-txt"> AM Conservation Group, Inc.</h3> 
                   <p><strong>2301 Charleston Regional Parkway</br>
                      Charleston, SC 29492 USA</strong>
                   </p>
                   <p><strong>Hours of Operation</strong></br>
                      Monday - Friday, 8:30 a.m. - 5:00 p.m. EST
                   </p>
                   <p>Tel – 843.971.1414, 843.971.1467</br>
                      Fax – 843.971.1472, 843.849.8242
                   </p>
                   <p><strong>Toll Free Order Line/Customer Service/Sales:</strong></br>
                      <?php echo $amc_urls['contact_number']; ?>
                   </p>
                   <p><strong>Email:</strong></br>
                      <a href="mailto:customercare@amconservationgroup.com">customercare@amconservationgroup.com</a>
                   </p>                                                                            
*/
?>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
                </div>
              </div>
          </div>
    </div> 

<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>