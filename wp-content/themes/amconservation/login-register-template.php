<?php 
/* 
Template Name: Login-Register  template 
*/ 
define("CSS", "login.css");

redirectLoggedInUser();

global $wpdb, $amc_urls, $show_msg, $success_msg, $error_msg, $success_msg2, $error_msg2;
$show_msg = 'none';
$show_msg2 = 'none';
$isOldUser = false;

// Handle the user activation
if(isset($_GET['email']) && isset($_GET['act_key']) && ($_GET['email'] != '') && ($_GET['act_key'] != '')) {
	$actEmail = $_GET['email'];
	$actActKey = $_GET['act_key'];

	$actUserData = $wpdb->get_row('SELECT * FROM wp_customers WHERE email = "'.$actEmail.'" AND activate_key="'.$actActKey.'"');
	if($actUserData) {
		$wpdb->update('wp_customers', array(
			'is_active' => 1
		), array(
			'email' => $actEmail,
			'activate_key' => $actActKey
		));

		// Redirect to register page
		wp_safe_redirect(esc_url($amc_urls['register_login'].'?act_show_msg=success')); exit;
	} else {
		// Redirect to register page
		wp_safe_redirect(esc_url($amc_urls['register_login'].'?act_show_msg=error')); exit;
	}
}

// Handle the form submission
$form_errors2 = array();
$login_errors = array();
// $v = wp_check_password('1234567x', '$P$BZSZoCFqYk5UFs9ZQ1Z6Z3WanejfQJ0');

if(isset($_POST['userLgnForm_submit']) && ($_POST['userLgnForm_submit'] == 'SUBMIT') && isset($_POST['name_userLgnForm']) && wp_verify_nonce($_POST['name_userLgnForm'],'action_userLgnForm')) {
	// echo '<pre>'; print_r($_POST); echo '</pre>';

	if(sanitize_text_field($_POST['userLgnForm_email']) == '') {
		$form_errors2['userLgnForm_email'] = 'Please enter email address';
	} else {
		if(!is_email(sanitize_text_field($_POST['userLgnForm_email']))) {
			$form_errors2['userLgnForm_email'] = 'Please enter valid email address';
		}
	}

	if(sanitize_text_field($_POST['userLgnForm_password']) == '') {
		$form_errors2['userLgnForm_password'] = 'Please enter password';
	}

	if(empty($form_errors2)) {
		$loginUserData = $wpdb->get_row('SELECT * FROM wp_customers WHERE email = "'.sanitize_text_field($_POST['userLgnForm_email']).'"');

		if(empty($loginUserData)) {
			$login_errors[] = 'User does not exist.';
		} else {
			// Code to handle the old customers
			if(($loginUserData->is_old == 1) && ($loginUserData->password == '')) {
				$hashKey = wp_generate_password(12, false);
				$wpdb->update('wp_customers', array(
					'olduserkey' => $hashKey
				), array(
					'email' => sanitize_text_field($_POST['userLgnForm_email'])
				));

				// @to-do: sent the new password mail
				/*
				$RstPwdMsg = 'Hi '.$loginUserData->firstname.' '.$loginUserData->lastname.'<br /><br />';
				$RstPwdMsg .= 'Please reset your password by clicking the following link, <br />';
				$RstPwdMsg .= '<a href="'.esc_url($amc_urls['reset_password'].'?email='.$loginUserData->email.'&hashkey='.$hashKey).'">Reset password</a><br /><br />';
				$RstPwdMsg .= get_bloginfo('name');
				*/

				$RstPwdMsg = 'Hi '.ucwords($loginUserData->firstname).' '.ucwords($loginUserData->lastname).','."<br /><br />";
				$RstPwdMsg .= 'Welcome to AMConservation.com with a brand new look and feel and enhanced user experience. We would like to let you know that its essential for you to update your password on our new website to access your account information in the future. Please click on the link below to update your password.'."<br /><br />";
				$RstPwdMsg .= '<a href="'.esc_url($amc_urls['reset_password'].'?email='.$loginUserData->email.'&hashkey='.$hashKey).'">'.esc_url($amc_urls['reset_password'].'?email='.$loginUserData->email.'&hashkey='.$hashKey).'</a>'."<br /><br />";
				$RstPwdMsg .= 'Regards,'."<br />";
				$RstPwdMsg .= $amc_urls['mail_regards']; // 'AMConservation Team';

				// $mailHeaders = 'From: '.get_bloginfo('name').' <'.get_bloginfo('admin_email').'>' . "\r\n";
				$mailHeaders = 'From: '.$amc_urls['mail_from_name'].' <'.get_bloginfo('admin_email').'>' . "\r\n";

				add_filter( 'wp_mail_content_type', 'set_html_content_type' );
				$mailStatus = wp_mail(
					$loginUserData->email, 
					'Update your account password on AMConservation', // get_bloginfo('name').' - Reset password', 
					$RstPwdMsg, 
					$mailHeaders);
				remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

				$show_msg2 = 'none';
				// $success_msg2 = 'Reset password mail has been sent to, '.$loginUserData->email.'.';
				$success_msg2 = 'Welcome Back! We have moved to a brand new website with a better experience and we have sent you an email to your mailbox for you to update your password. In order to access your account, please update your password on the site by clicking on the link in the email.';
				$isOldUser = true;

			} else {
				if($loginUserData->is_active == 0) {
					$login_errors[] = 'User is not active. Please activate.';
				} else {
					if(!wp_check_password(sanitize_text_field($_POST['userLgnForm_password']), $loginUserData->password)) {
						$login_errors[] = 'Incorrect password. Try again.';
					} else {
						// Save the user details in Cookie
						$_SESSION['user_logged_in'] = $loginUserData->id;
						$_SESSION['user_logged_in_data'] = json_encode($loginUserData);
						wp_safe_redirect(get_bloginfo('url')); exit;
					}
				}
			}
		}
	}
}

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}
?>

<!--    content area start      -->

  <div class="row bgfill content">
        <div class=" topspacer">  </div>
           
               <!--      Reg  starts         -->     
<?php if(!is_mobile()) { ?>
            <div class="col-md-5 md-form">
            <div class="login-form-container"> 
                            <div class="row ">  
                           <div class="col-md-12">
                              <div class="row ">



<!--      Form starts         -->

<?php
 if(!empty($form_errors2)) {
	/*
	echo '<p>Some errors found.</p>';
	echo '<ol>';
	foreach($form_errors2 as $form_errors2_item) {
		echo '<li>'.$form_errors2_item.'</li>';
	}
	echo '</ol>';
	*/
	$show_msg2 = 'block';
	$error_msg2 = 'Errors found. Try, again.';
}
if(!empty($login_errors)) {
	/*
	echo '<p>Some errors found.</p>';
	echo '<ol>';
	foreach($login_errors as $login_errors_item) {
		echo '<li>'.$login_errors_item.'</li>';
	}
	echo '</ol>';
	*/
	$show_msg2 = 'block';
	$error_msg2 = $login_errors[0];
}
?>

<form role="form" method="post" action="" name="userLgnForm" parsley-validate novalidate>
<?php wp_nonce_field('action_userLgnForm', 'name_userLgnForm'); ?>

<div class="col-md-12">
	<div class="form-group"> 

<?php
$alertClass = 'alert-warning';
if(!empty($error_msg2)) {
	$alertClass = 'alert-danger';
}

if($show_msg2 != 'none') {
?>

<div class="js_result alert <?php echo $alertClass; ?> fade in">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<?php
if(!empty($success_msg2)) {
	// echo $success_msg2;
} else if(!empty($error_msg2)) {
	echo $error_msg2;
}
?>
</div>

<?php
}
?>

<?php
// Show the activation success/failure message
if(isset($_GET['act_show_msg']) && ($_GET['act_show_msg'] != '')) {
	$actShowMsg = $_GET['act_show_msg'];
	$alertClass = 'alert-warning';
	if($actShowMsg == 'error') {
		$alertClass = 'alert-danger';
	}

?>

<div class="js_result alert <?php echo $alertClass; ?> fade in">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<?php
	if($actShowMsg == 'success') {
		echo 'You can now login with your credentials.';
	} else if($actShowMsg == 'error') {
		echo 'Invalid user. Try, again.';
	}
?>
</div>
<?php
}
?>

<p class="log-parag-txt"> Registered Customers</p>

<?php if(!isset($_GET['olduser'])) { ?>
<?php
/*
<div class="intro_text"> <p>We have moved to a brand new website with a better experience. In order to access your account, please enter your email address to update your password on the site</p></div>
*/
?>
<?php } ?>

<input type="email" name="userLgnForm_email" class="form-control" id="exampleInputEmail1" placeholder="Email Address*" required parsley-trigger="change" parsley-error-message="Please enter a valid email address" /><br>
<input type="password" name="userLgnForm_password" class="form-control" id="exampleInputPassword1" placeholder="Password*" required parsley-trigger="change" parsley-error-message="Please enter your password " />


<div class="row offset-top-1"> 
	<div class="col-md-7 col-xs-6 log-fplink-btn"><a href="<?php echo $amc_urls['forgot_password']; ?>">  Forgot Your Password?</a></div>
	<div class="col-md-5 col-xs-12 text-align1 "> <input type="submit" name="userLgnForm_submit" class="btn fp-submit-btn " value="SUBMIT" /> </div>
</div> 

<!-- 
<div class="row offset-2"><p class="fp-parag-txt">  *Required Fields </p></div>
-->

<?php
/*
<div class="alert js_result" style="display:<?php echo $show_msg2; ?>;">
	<a class="close" data-dismiss="alert">×</a>
<?php if(!empty($success_msg2)) { ?>
	<p style="color:green;"><strong><?php echo $success_msg; ?></strong></p>
<?php } else if(!empty($error_msg2)) { ?>
	<p style="color:red;"><strong><?php echo $error_msg2; ?></strong></p>
<?php } ?>
</div>
*/
?>

	</div>
</div>

</form>

<!--     Form  ends         --> 
                               </div>
                             </div>
                             </div>
                     </div>
                    
                     </div>
                     <!--      Reg  ends        -->        
<?php } else { ?>
<div class="col-md-5 login-sd-section sd-form">
	<div class="login-form-container  logmobile">
	  
		 

<?php
 if(!empty($form_errors2)) {
	/*
	echo '<p>Some errors found.</p>';
	echo '<ol>';
	foreach($form_errors2 as $form_errors2_item) {
		echo '<li>'.$form_errors2_item.'</li>';
	}
	echo '</ol>';
	*/
	$show_msg2 = 'block';
	$error_msg2 = 'Errors found. Try, again.';
}
if(!empty($login_errors)) {
	/*
	echo '<p>Some errors found.</p>';
	echo '<ol>';
	foreach($login_errors as $login_errors_item) {
		echo '<li>'.$login_errors_item.'</li>';
	}
	echo '</ol>';
	*/
	$show_msg2 = 'block';
	$error_msg2 = $login_errors[0];
}
?>

			<!--      Form starts         -->
<form role="form" method="post" action="" name="userLgnForm" parsley-validate novalidate>
<?php wp_nonce_field('action_userLgnForm', 'name_userLgnForm'); ?>
			 
				<div class="form-group">
<?php
$alertClass = 'alert-warning';
if(!empty($error_msg2)) {
	$alertClass = 'alert-danger';
}

if($show_msg2 != 'none') {
?>

<div class="js_result alert <?php echo $alertClass; ?> fade in">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<?php
if(!empty($success_msg2)) {
	// echo $success_msg2;
} else if(!empty($error_msg2)) {
	echo $error_msg2;
}
?>
</div>

<?php
}
?>


<?php
// Show the activation success/failure message
if(isset($_GET['act_show_msg']) && ($_GET['act_show_msg'] != '')) {
	$actShowMsg = $_GET['act_show_msg'];
	$alertClass = 'alert-warning';
	if($actShowMsg == 'error') {
		$alertClass = 'alert-danger';
	}

?>

<div class="js_result alert <?php echo $alertClass; ?> fade in">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<?php
	if($actShowMsg == 'success') {
		echo 'You can now login with your credentials.';
	} else if($actShowMsg == 'error') {
		echo 'Invalid user. Try, again.';
	}
?>
</div>
<?php
}
?>


				  <p class="log-parag-txt"> Registered Customers</p>

<?php if(!isset($_GET['olduser'])) { ?>
<?php
/*
<div class="intro_text"> <p>We have moved to a brand new website with a better experience. In order to access your account, please enter your email address to update your password on the site</p></div>
*/
?>
<?php } ?>

<input type="email" name="userLgnForm_email" class="form-control" id="exampleInputEmail1" placeholder="Email Address*" required parsley-trigger="change" parsley-error-message="Please enter a valid email address" /><br>
<input type="password" name="userLgnForm_password" class="form-control" id="exampleInputPassword1" placeholder="Password*" required parsley-trigger="change" parsley-error-message="Please enter your password " />

				  <div class="row offset-top-1">
					<div class="col-xs-12 text-align1 ">
					  <input type="submit" name="userLgnForm_submit" class="btn fp-submit-btn " value="SUBMIT" />
					</div>
				  </div>
				  <div class=" col-xs-6 log-fplink-btn"> <a href="<?php echo $amc_urls['forgot_password']; ?>"> Forgot Your Password? </a></div>
				  <div class=" col-xs-6 log-fplink-btn"> <a class="register-account" href="#"> Create An Account </a></div>
				</div>
				
			 
</form>
		  </div>
		  
		  <!--     Form  ends         --> 
		  
		
</div>
<?php } ?>
                      <div class="col-md-1 divider md-form">   <img src="<?php echo get_template_directory_uri().'/assets/images/divider.png'; ?>">    </div>
                      
                      
                 <!--      Login  starts         -->     

<?php
// Handle the form submission
$form_errors = array();
// $v = wp_check_password('1234567x', '$P$BZSZoCFqYk5UFs9ZQ1Z6Z3WanejfQJ0');

if(isset($_POST['userRegForm_submit']) && ($_POST['userRegForm_submit'] == 'SUBMIT') && isset($_POST['name_userRegForm']) && wp_verify_nonce($_POST['name_userRegForm'],'action_userRegForm')) {
	// echo '<pre>'; print_r($_POST); echo '</pre>';

	if(sanitize_text_field($_POST['userRegForm_firstname']) == '') {
		$form_errors['userRegForm_firstname'] = 'Please enter first name';
	}
	if(sanitize_text_field($_POST['userRegForm_lastname']) == '') {
		$form_errors['userRegForm_lastname'] = 'Please enter last name';
	}
	/*
	if(sanitize_text_field($_POST['userRegForm_companyname']) == '') {
		$form_errors['userRegForm_companyname'] = 'Please enter company name';
	}
	*/
	if(sanitize_text_field($_POST['userRegForm_email']) == '') {
		$form_errors['userRegForm_email'] = 'Please enter email address';
	} else {
		if(!is_email(sanitize_text_field($_POST['userRegForm_email']))) {
			$form_errors['userRegForm_email'] = 'Please enter valid email address';
		}
	}

	if(sanitize_text_field($_POST['userRegForm_password']) == '') {
		$form_errors['userRegForm_password'] = 'Please enter password';
	}
	if(sanitize_text_field($_POST['userRegForm_password']) != sanitize_text_field($_POST['userRegForm_cpassword'])) {
		$form_errors['userRegForm_password'] = 'Please confirm password';
	}

	// Check for email uniqueness
	if(emailExists(sanitize_text_field($_POST['userRegForm_email']))) {
		$form_errors['userRegForm_email'] = 'Email already exists.';
	}

	if(empty($form_errors)) {
		/*
		$ins_ID = wp_insert_user(array(
			'first_name' => sanitize_text_field($_POST['userRegForm_firstname']),
			'last_name' => sanitize_text_field($_POST['userRegForm_lastname']),
			'user_pass' => sanitize_text_field($_POST['userRegForm_password']),
			'user_login' => sanitize_text_field($_POST['userRegForm_email']),
			'user_email' => sanitize_text_field($_POST['userRegForm_email']),
		));
		*/
		$nlSubscribeData = (sanitize_text_field($_POST['userRegForm_signupNL']) == 'yes') ? 1 : 0;
		$actKey = wp_generate_password(12, false);
		$actLink = esc_url($amc_urls['register_login'].'?email='.sanitize_text_field($_POST['userRegForm_email']).'&act_key='.$actKey);

		$ins_ID = $wpdb->insert( 
			'wp_customers', 
			array( 
				'created' => current_time('mysql'), 
				'modified' => current_time('mysql'),
				'firstname' => sanitize_text_field($_POST['userRegForm_firstname']),
				'lastname' => sanitize_text_field($_POST['userRegForm_lastname']),
				// 'companyname' => sanitize_text_field($_POST['userRegForm_companyname']),
				'email' => sanitize_text_field($_POST['userRegForm_email']),
				'password' => wp_hash_password(sanitize_text_field($_POST['userRegForm_password'])),
				'activate_key' => $actKey,
				'nlsubscribe' => $nlSubscribeData,
				'is_active' => 0
			) 
		);

		if($ins_ID) {
			// @to-do: send activation email link, need to check for unique email
			/*
			$catMsg = 'Hi '.sanitize_text_field($_POST['userRegForm_firstname']).' '.sanitize_text_field($_POST['userRegForm_lastname']).','."\n\n".
			'Please click the below link to activate your account. '."\n\n".
			'<a href="'.esc_url($actLink).'">'.esc_url($actLink).'</a>'."\n\n".
			'Regards'."\n".
			get_bloginfo('name').' Team.';
			*/

			$catMsg = 'Hi!'."<br /><br />";
			$catMsg .= 'You\'re almost there! All you need to do is just confirm your Email Id.'."<br /><br />";
			$catMsg .= 'You have successfully created an account with AM Conservation Group and all you need to do is activate it. It will give you access to your account and you can track all your quotes.'."<br /><br />";
			$catMsg .= 'Please click on the link below to verify your email. '."<br /><br />";
			$catMsg .= '<a href="'.esc_url($actLink).'">Confirm my account now</a>'."<br /><br />";
			$catMsg .= 'Or click on the link below: '."<br /><br />";
			$catMsg .= '<a href="'.esc_url($actLink).'">'.esc_url($actLink).'</a>'."<br /><br />";
			$catMsg .= 'Regards'."<br />";
			$catMsg .= $amc_urls['mail_regards']; // 'AMConservation Team';

			// $mailHeaders = 'From: '.get_bloginfo('name').' <'.get_bloginfo('admin_email').'>' . "\r\n";
			$mailHeaders = 'From: '.$amc_urls['mail_from_name'].' <'.get_bloginfo('admin_email').'>' . "\r\n";

			add_filter( 'wp_mail_content_type', 'set_html_content_type' );
			$mailStatus = wp_mail(
				sanitize_text_field($_POST['userRegForm_email']), 
				'Activate your AM Conservation Group Account.', // get_bloginfo('name').': Activate your account', 
				$catMsg, 
				$mailHeaders);
			remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

			if($nlSubscribeData == 1) {
				// Handle the Newsletter subscription
				NewsletterUsers::instance()->save_user(array(
					'email' => sanitize_text_field($_POST['userRegForm_email']),
					'name' => sanitize_text_field($_POST['userRegForm_firstname']).' '.sanitize_text_field($_POST['userRegForm_lastname']),
					'wp_user_id' => $ins_ID
				));
			}

			// echo '<p>Success created user.</p>';
			$show_msg = 'block';
			$success_msg = 'Your registration was successful and we have sent an activation mail to your inbox. Please click on the link to activate your account on AMConservation.';
			unset($_POST);
		}
	} else {
		/*
		echo '<p>Some errors found.</p>';
		echo '<ol>';
		foreach($form_errors as $form_errors_item) {
			echo '<li>'.$form_errors_item.'</li>';
		}
		echo '</ol>';
		*/

		$show_msg = 'block';
		$error_msg = 'Errors found. Try, again.';
		if(isset($form_errors['userRegForm_email'])) {
			$error_msg = $form_errors['userRegForm_email'];
		}
	}
}
?>

<?php if(!is_mobile()) { ?>
<div class="col-md-5 md-form">
<div class="login-form-container"> 
<div class="row content">  
<form role="form" method="post" action="" name="userRegForm" parsley-validate novalidate>
<?php wp_nonce_field('action_userRegForm', 'name_userRegForm'); ?>
<div class="col-md-12">
<div class="form-group">

<p class="log-parag-txt"> Create an Account</p>

<?php include('successmodal.php'); ?>

<input type="text" name="userRegForm_firstname" value="<?php echo sanitize_text_field($_POST['userRegForm_firstname']) ?>" class="form-control" placeholder="First Name*" required parsley-error-message="Please enter your first name " /> <br>

<input type="text" name="userRegForm_lastname" value="<?php echo sanitize_text_field($_POST['userRegForm_lastname']) ?>" class="form-control" placeholder="Last Name*" required parsley-error-message="Please enter your last name " /> <br>

<?php
/*
<input type="text" name="userRegForm_companyname" value="<?php echo sanitize_text_field($_POST['userRegForm_companyname']) ?>" class="form-control" placeholder="Company Name*" required="" data-errormessage-value-missing="Please enter company name " /> <br>
*/
?>

<input type="email" name="userRegForm_email" value="<?php echo sanitize_text_field($_POST['userRegForm_email']) ?>" class="form-control" id="exampleInputEmail1" placeholder="Email Address*" required parsley-trigger="change" parsley-error-message="Please enter a valid email address" placeholder="Email Address*" /> <br>

<input type="password" name="userRegForm_password" class="form-control" id="passw" placeholder="Password*" required parsley-trigger="change" parsley-error-message="Please enter your password " /><br>

<input type="password" name="userRegForm_cpassword" class="form-control" id="exampleInputPassword1" placeholder="Confirm Password*" parsley-error-message="Please confirm your password " required parsley-trigger="change" parsley-equalto="#passw" /><br>

<div class="checkbox"><label><input type="checkbox" name="userRegForm_signupNL" value="yes">Sign Up for Newsletter</label></div>

</div>

<div class="row offset-top-1"> 
<div class="col-md-8 "><p class="fp-parag-txt-2">  *Mandatory Fields </p> </div>
<div class="col-md-4 text-align1 "><div class=" "> <input type="submit" name="userRegForm_submit" class="btn fp-submit-btn " value="SUBMIT" /></div></div>
</div> 

</div>
</form>

</div>
</div>
</div>

<?php } else { ?>

<div class="col-md-5  reg-sd">
	<div class="login-form-container">
	  <div class="row content">
<form role="form" method="post" action="" name="userRegForm" parsley-validate novalidate>
<?php wp_nonce_field('action_userRegForm', 'name_userRegForm'); ?>
		  <div class="col-md-12">
			<div class="form-group">
			  <p class="log-parag-txt"> Create an Account</p>

<?php include('successmodal.php'); ?>

<input type="text" name="userRegForm_firstname" value="<?php echo sanitize_text_field($_POST['userRegForm_firstname']) ?>" class="form-control" placeholder="First Name*" required parsley-error-message="Please enter your first name " /> <br>

<input type="text" name="userRegForm_lastname" value="<?php echo sanitize_text_field($_POST['userRegForm_lastname']) ?>" class="form-control" placeholder="Last Name*" required parsley-error-message="Please enter your last name " /> <br>


<?php
/*
<input type="text" name="userRegForm_companyname" value="<?php echo sanitize_text_field($_POST['userRegForm_companyname']) ?>" class="form-control" placeholder="Company Name*" required="" data-errormessage-value-missing="Please enter company name " />
<br>
*/
?>

<input type="email" name="userRegForm_email" value="<?php echo sanitize_text_field($_POST['userRegForm_email']) ?>" class="form-control" id="exampleInputEmail1" placeholder="Email Address*" required parsley-trigger="change" parsley-error-message="Please enter a valid email address" placeholder="Email Address*" /> <br>

<input type="password" name="userRegForm_password" class="form-control" id="passw" placeholder="Password*" required parsley-trigger="change" parsley-error-message="Please enter your password " /><br>

<input type="password" name="userRegForm_cpassword" class="form-control" id="exampleInputPassword1" placeholder="Confirm Password*" parsley-error-message="Please confirm your password " required parsley-trigger="change" parsley-equalto="#passw" /><br>

			  <div class="checkbox">
				<label>
				  <input type="checkbox" name="userRegForm_signupNL" value="yes">
				  Sign Up for Newsletter </label>
			  </div>
			</div>
			<div class="row offset-top-1">
			  <div class="">
				<p class="fp-parag-txt-2"> *Mandatory Fields </p>
			  </div>
			  <div class="col-md-4 text-align1 ">
				<div class=" "><input type="submit" name="userRegForm_submit" class="btn fp-submit-btn " value="SUBMIT" /></div>
			  </div>
			</div>

			<div class="log-fplink-btn" style="padding-left:0 !Important"> <a class="register-account2" href="#">Registered Users Click here</a></div>

		  </div>
</form>
	  </div>
	</div>
</div>

<?php } ?>


                     
                     <!--      Login ends            -->
          
                   
                    
                     
            
       
        </div>
  
<?php if($isOldUser) { ?>
<div class="modal fade" id="regModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Welcome Back!</h4>
      </div>
      <div class="modal-body">
        <p><?php echo $success_msg2; ?></p>
		<p class="close_welcome"><a class="fp-submit-btn" href="#" data-dismiss="modal" aria-hidden="true">OK</a></p>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php } ?>
 
<!--
<div class="row bgfill content"><div class="  topspacer"></div></div>

<div class="row bgfill "> <div class="space-above-footer"> </div> </div>
-->
  
  <!--      content area end            -->
  
<script type="text/javascript">
<?php if($isOldUser) { ?>
$('#regModal').modal({
	show: true
});
<?php } ?>

$(window).load(function(e){
	var mq = window.matchMedia( "(max-width: 780px)" );
	if (mq.matches) {
		$( ".register-account").click(function(){
			$( ".reg-sd" ).css("display","block");
			$( ".login-sd-section" ).css("display","none");
			var element2 = $(".fp-mandatory-txt").detach(); $('.fp-parag-txt').after( element2 );
			return false;
		});
		$( ".register-account2").click(function(){
			$( ".reg-sd" ).css("display","none");
			$( ".login-sd-section" ).css("display","block");
			var element2 = $(".fp-mandatory-txt").detach(); $('.fp-parag-txt').after( element2 );
			return false;
		});
	} else {
		$( ".reg-sd" ).css("display","none");
		var element4 = $(".fp-mandatory-txt").detach(); $('.offset-2').prepend( element4 );
	}
	$( ".reg-sd " ).css("display","none");
	callpopupcenter();/* TO MAKE POPUP VERTICALLY ALIGN CENTER*/
});

$(window).resize(function(){
	callpopupcenter();/* TO MAKE POPUP VERTICALLY ALIGN CENTER*/
});

function callpopupcenter(){
    var initModalHeight = $('.modal-dialog').outerHeight(); //give an id to .mobile-dialog
    var userScreenHeight = $(window).outerHeight();
    if (initModalHeight > userScreenHeight) {
        $('.modal-dialog').css('overflow', 'auto'); //set to overflow if no fit
    } else {
        $('.modal-dialog').css('margin-top', 
        (userScreenHeight / 2) - (initModalHeight/2)); //center it if it does fit
    }
}
</script>

<script>
/*
      $('.ie-all [placeholder]').focus(function() {
        var i = $(this);
        if(i.val() == i.attr('placeholder')) {
          i.val('').removeClass('placeholder');
          if(i.hasClass('password')) {
            i.removeClass('password');
            this.type='password';
          }     
        }
      }).blur(function() {
        var i = $(this);  
        if(i.val() == '' || i.val() == i.attr('placeholder')) {
          if(this.type=='password') {
            i.addClass('password');
            this.type='text';
          }
          i.addClass('placeholder').val(i.attr('placeholder'));
        }
      }).blur().parents('form').submit(function() {
       
          $(this).find('[placeholder]').each(function() {
            var i = $(this);
            if(i.val() == i.attr('placeholder'))
              i.val('');
              i.removeClass('placeholder');

          })
        
      });
	  
	   */
	   
	   function getIEVersion() {
        var rv = -1; // Return value assumes failure.
        if (navigator.appName == 'Microsoft Internet Explorer') {
            var ua = navigator.userAgent;
            var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
            if (re.test(ua) != null)
                rv = parseFloat( RegExp.$1 );
        }
        return rv;
    }


    function checkVersion() {
        var ver = getIEVersion();

        if ( ver != -1 ) {
            if (ver <= 9.0) {
            
			$("input[type=password]").wrap('<div class="passwrap"></div>')
			$('.passwrap').append('<span class="place-password"></span>');
			
			$("input[type=password]").each(function() {
			 var plsh = $(this).attr('placeholder');
			 $(this).next('.place-password').html(plsh);
			});
			
			
			$("input[type=password]").removeAttr('placeholder');
			
			$("input[type=password]").on('focus', function() {
				$(this).next('.place-password').hide();
				$(this).next().next('.place-password').hide();
			});
			
			$("input[type=password]").on('blur', function() {
				if ($(this).val() == "") {
					$(this).next('.place-password').show();
					$(this).next().next('.place-password').show();
				}
			});
            }
        }
    }

    checkVersion();
	   
	   
	   
	   
	   
	   /*$(document).ready(function() {
		if ($.browser.msie && parseInt($.browser.version, 10) > 7){
			$("input[type=password]").removeAttr('placeholder');
			$("input[type=password]").on('focus', function() {
				$(this).next('.place-password').hide();
				$(this).next().next('.place-password').hide();
			});
			$("input[type=password]").on('blur', function() {
				$(this).next('.place-password').show();
				$(this).next().next('.place-password').show();
			});
		}
		});*/
		
    </script>

<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>
