<div class="wrap">
	<h2>How did you find us?</h2>

	<p><a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=how_find_us_mng&action=add" title="Add">Add</a></p>

<?php
if(isset($_SESSION['msg'])) {
	if($_SESSION['msg'] == 'deletesuccess') {
		echo '<p style="color:green;">Deleted successfully.</p>';
	}
	if($_SESSION['msg'] == 'activesuccess') {
		echo '<p style="color:green;">Made active successfully.</p>';
	}
	if($_SESSION['msg'] == 'inactivesuccess') {
		echo '<p style="color:green;">Made inactive successfully.</p>';
	}
	unset($_SESSION['msg']);
}
?>

<?php
global $wpdb;
// $wpdb->show_errors();
// $wpdb->print_error();

//Our class extends the HFU_List_Table class, so we need to make sure that it's there
if(!class_exists('HFU_List_Table')){
	require_once( TEMPLATEPATH . '/classes/hfuClass.php' );
}

$action = (isset($_GET['action'])) ? $_GET['action'] : '';

if($action == 'add') {
	// Handle the add screen
	$formErrorAdd = array();
	$formSuccessAdd = array();
	if(isset($_POST['hfuAddFrm_submit']) && $_POST['hfuAddFrm_submit'] == 'Add') {
		$hfuAddFrm_title = sanitize_text_field($_POST['hfuAddFrm_title']);
		$hfuAddFrm_crmcode = sanitize_text_field($_POST['hfuAddFrm_crmcode']);
		$hfuAddFrm_is_active = sanitize_text_field($_POST['hfuAddFrm_is_active']);

		if(empty($hfuAddFrm_title)) {
			$formErrorAdd[] = 'Please enter the name.';
		}

		if(empty($hfuAddFrm_crmcode)) {
			$hfuAddFrm_crmcode[] = 'Please enter the CRM code.';
		}

		if(empty($formErrorAdd)) {
			$wpdb->insert('wp_howfindus', array(
				'created' => current_time('mysql'), 
				'modified' => current_time('mysql'),
				'title' => $hfuAddFrm_title,
				'slug' => sanitize_title($hfuAddFrm_title),
				'crmcode' => $hfuAddFrm_crmcode,
				'is_active' => ((isset($hfuAddFrm_is_active) && $hfuAddFrm_is_active == 'yes') ? 1 : 0)
			));
			$formSuccessAdd[] = 'Added successfully.';
		}
	}
?>
<h3>Add - How did you find us?</h3>
<form name="hfuAddFrm" action="" method="post">
<div>

<?php
if(!empty($formSuccessAdd)) {
	echo '<p style="color:green;">Status follows,</p>';
	echo '<ol>';
	foreach($formSuccessAdd as $formSuccessAddItem) {
		echo '<li>'.$formSuccessAddItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}

if(!empty($formErrorAdd)) {
	echo '<p style="color:red;">The following errors found,</p>';
	echo '<ol>';
	foreach($formErrorAdd as $formErrorAddItem) {
		echo '<li>'.$formErrorAddItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}
?>

<p><label for="hfuAddFrm_title">Name</label><br /> <input type="text" name="hfuAddFrm_title" value="" id="hfuAddFrm_title" /></p>
<p><label for="hfuAddFrm_crmcode">CRM code</label><br /> <input type="text" name="hfuAddFrm_crmcode" value="" id="hfuAddFrm_crmcode" /></p>
<p><label for="hfuAddFrm_is_active">Is Active?</label> <input type="checkbox" name="hfuAddFrm_is_active" value="yes" id="hfuAddFrm_is_active" /></p>
<p><input type="submit" name="hfuAddFrm_submit" value="Add" /></p>
</div>
</form>
<?php

} else if($action == 'edit') {
	// Handle the edit screen
	$hfuId = $_GET['id'];
	$hfuRow = $wpdb->get_row('SELECT * FROM wp_howfindus WHERE id = "'.$hfuId.'"');

	$formErrorEdit = array();
	$formSuccessEdit = array();
	if(isset($_POST['hfuEditFrm_submit']) && $_POST['hfuEditFrm_submit'] == 'Edit') {
		$hfuEditFrm_title = sanitize_text_field($_POST['hfuEditFrm_title']);
		$hfuAddFrm_crmcode = sanitize_text_field($_POST['hfuAddFrm_crmcode']);
		$hfuEditFrm_is_active = sanitize_text_field($_POST['hfuEditFrm_is_active']);

		if(empty($hfuEditFrm_title)) {
			$formErrorEdit[] = 'Please enter the name.';
		}

		if(empty($hfuAddFrm_crmcode)) {
			$hfuAddFrm_crmcode[] = 'Please enter the CRM code.';
		}

		if(empty($formErrorEdit)) {
			$wpdb->update('wp_howfindus', array(
				'modified' => current_time('mysql'),
				'title' => $hfuEditFrm_title,
				'slug' => sanitize_title($hfuEditFrm_title),
				'crmcode' => $hfuAddFrm_crmcode,
				'is_active' => ((isset($hfuEditFrm_is_active) && $hfuEditFrm_is_active == 'yes') ? 1 : 0)
			), array(
				'id' => $hfuId
			));
			$formSuccessEdit[] = 'Edit successfully.';
			$hfuRow = $wpdb->get_row('SELECT * FROM wp_howfindus WHERE id = "'.$hfuId.'"');
		}
	}
?>
<h3>Edit - How did you find us?</h3>
<form name="hfuEditFrm" action="" method="post">
<div>

<?php
if(!empty($formSuccessEdit)) {
	echo '<p style="color:green;">Status follows,</p>';
	echo '<ol>';
	foreach($formSuccessEdit as $formSuccessEditItem) {
		echo '<li>'.$formSuccessEditItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}

if(!empty($formErrorEdit)) {
	echo '<p style="color:red;">The following errors found,</p>';
	echo '<ol>';
	foreach($formErrorEdit as $formErrorEditItem) {
		echo '<li>'.$formErrorEditItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}
?>

<p><label for="hfuEditFrm_title">Name</label><br /> <input type="text" name="hfuEditFrm_title" value="<?php echo $hfuRow->title; ?>" id="hfuEditFrm_title" /></p>
<p><label for="hfuAddFrm_crmcode">CRM code</label><br /> <input type="text" name="hfuAddFrm_crmcode" value="<?php echo $hfuRow->crmcode; ?>" id="hfuAddFrm_crmcode" /></p>
<p><label for="hfuEditFrm_is_active">Is Active?</label> <input type="checkbox" name="hfuEditFrm_is_active" value="yes" id="hfuEditFrm_is_active" <?php echo (($hfuRow->is_active == 1) ? 'checked="checked"' : ''); ?> /></p>
<p><input type="submit" name="hfuEditFrm_submit" value="Edit" /></p>
</div>
</form>
<?php
} else if($action == 'delete') {
	// Handle the delete action
	$hfuId = $_GET['id'];
	$wpdb->delete('wp_howfindus', array(
		'id' => $hfuId
	));

	$_SESSION['msg'] = 'deletesuccess';
	wp_safe_redirect(site_url().'/wp-admin/admin.php?page=how_find_us_mng'); exit;
} else if($action == 'makeactive') {
	// Handle the active action
	$hfuId = $_GET['id'];
	$wpdb->update('wp_howfindus', array(
		'is_active' => 1
	), array(
		'id' => $hfuId
	));

	$_SESSION['msg'] = 'activesuccess';
	wp_safe_redirect(site_url().'/wp-admin/admin.php?page=how_find_us_mng'); exit;
} else if($action == 'makeinactive') {
	// Handle the inactive action
	$hfuId = $_GET['id'];
	$wpdb->update('wp_howfindus', array(
		'is_active' => 0
	), array(
		'id' => $hfuId
	));

	$_SESSION['msg'] = 'inactivesuccess';
	wp_safe_redirect(site_url().'/wp-admin/admin.php?page=how_find_us_mng'); exit;
} else {
	//Prepare Table of elements
	$hfu_list_table = new HFU_List_Table();
	$hfu_list_table->prepare_items();
	//Table of elements
	$hfu_list_table->display();
}
?>

</div>