<?php

define("CSS", "message_us.css");

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}

global $theme_shortname;
?>

<div class="row bgfill ">
	<div class=" topspacer">
		<div class="message-form-container"> 
			<div class="row content">  
				<div class="col-md-4 col-md-offset-4"> 

<div class="post-content not-found"> 
<h2 class="not-found-404">404 - Not Found.</h2>
<span class="not-found-404">Sorry, this page was not found.</span>
</div>

				</div>
			</div>
		</div>
	</div>
</div>

<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>