<?php global $theme_shortname, $theme_options, $amc_urls; 
  $keywords = get_post_meta( $post->ID, '_aioseop_keywords', true );
 $description = get_post_meta( $post->ID, '_aioseop_description', true );

?>
<?php $theme_options = get_option( $theme_shortname . '_options' ); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta name="google-site-verification" content="NZ2Mb4LPjN_Rv2KCnDKPagdyR0VALanyji9JKmMGEGk" />
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="initial-scale=1.0,  user-scalable=yes, user-scalable=1, width=device-width, minimum-scale=1.0, maximum-scale=5.0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="<?php echo $description; ?>" />
  <meta name="keywords" content="<?php echo $keywords; ?>" />
  <meta name="robots" content="INDEX,FOLLOW" />
  <meta name="author" content="AMConservation">  
  <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico">
<title>
<?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;
	wp_title( '|', true, 'right' );
	bloginfo( 'name' );
	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
	echo " | $site_description";
	?></title>
	<?php  //wp_head(); // Removeing this from the WP HEAD to avoid the external CSS links from the page   ?> 
<!-- Bootstrap core CSS -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
  <link href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/css/flexislider.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/css/amc_style.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/css/rwd_styles.css" rel="stylesheet">
  <script type="text/javascript">
  var baseURL = '<?php echo get_template_directory_uri(); ?>';
  </script>
  <!-- Custom styles for this template -->
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <link href="<?php echo get_template_directory_uri(); ?>/assets/css/amc_ie8.css" rel="stylesheet">
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/html5shiv.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/respond.min.js"></script>
  <![endif]-->
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.10.2.min.js" ></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js" ></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/parsley.js"></script>
  
  <style type="text/css">
.welcome-links { margin: 26px 0 0 -21px; }
.username { font-size: 13px; font-weight: bold; }
.userlinks a { color: #131111; font-size: 12px; font-weight: normal; padding: 0 4px; }
html, body{height:auto;}
.wrapper {min-height: auto;height: auto;margin: 0 auto 0;}
  </style>
  <!-- Google Tag Manager --> 
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WQJWJ5" 
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> 
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': 
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], 
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); 
    })(window,document,'script','dataLayer','GTM-WQJWJ5');</script> 
    <!-- End Google Tag Manager -->
    <!-- Start of Async HubSpot Analytics Code -->
  <script type="text/javascript">
    (function(d,s,i,r) {
      if (d.getElementById(i)){return;}
      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/448562.js';
      e.parentNode.insertBefore(n, e);
    })(document,"script","hs-analytics",300000);
  </script>
<!-- End of Async HubSpot Analytics Code -->
  </head> 
  <body <?php // body_class(); ?>>

  <div class="container homepg">
     <div class="row header">
        <div class="col-md-4  col-sm-4 logo"> <a href=" <?php echo home_url(); ?> "><img src="<?php echo get_template_directory_uri().'/assets/images/logo.png';  // echo esc_url( $theme_options['logo'] ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"/> </a></div>

<?php
// Check for login user
if(userIsLoggedIn()) {
	$loggedInUser = getLoggedInUser();
?>
<div class="col-md-8 col-sm-8">
	<div class="welcome-links">
		<span class="username">Hi <?php echo $loggedInUser->firstname; ?>!</span>
		<span class="userlinks"><a href="<?php echo $amc_urls['my_account']; ?>">My Account </a>|</span>
		<span class="userlinks"><a href="<?php echo $amc_urls['quote_basket']; ?>">Quote basket (<?php echo getQuoteCount(); ?>)</a></span> 
	</div>   


	<div class="topnav-right">
		<a href="#" class="menu1" onclick="return false;"><span class="top-icon"></span><span class="toplink1"><?php  echo $amc_urls["contact_number"]; ?> </span></a>
		<a href="<?php echo $amc_urls['message_us_online']; ?>" class="menu2"><span style="background-image: url('<?php echo get_template_directory_uri().'/assets/images/spritesheet.png'; ?>'); background-position: 8.433% 33.273%; background-repeat: no-repeat no-repeat;" class="top-icon2"></span><span style="color: rgb(82, 81, 81);" class="toplink2">  Message Us Online  </span></a>
		<a href="<?php echo $amc_urls['logout']; ?>" class="menu3"><span style="background-image: url('<?php echo get_template_directory_uri().'/assets/images/spritesheet.png'; ?>'); background-position: 18.433% 33.273%; background-repeat: no-repeat no-repeat;" class="top-icon3"></span><span style="color: rgb(82, 81, 81);" class="toplink3"> Log Out  </span></a>
	</div>
</div>
<?php } else { ?>
       
        <div class="col-md-8 col-sm-8">
           <div class="topnav-right">
            <a class="menu1" href="#" onclick="return false;">
              <span class="top-icon"></span>
              <span class="toplink1"><?php echo $amc_urls['contact_number']; ?></span>
            </a>
            <a class="menu2" href="<?php echo $amc_urls['message_us_online']; ?>">
              <span class="top-icon2"></span>
              <span class="toplink2">Message Us Online</span>
            </a>
            <a class="menu3" href="<?php echo $amc_urls['register_login']; ?>">
              <span class="top-icon3"></span>
              <span class="toplink3">Register / Login</span>
            </a>
            <a class="menu7" href="<?php echo $amc_urls['sc_url']; ?>">
              <span class="top-icon7"></span>
              <span class="toplink7">Home Owners</span>
            </a>
           <a class="menu8" data-toggle="modal" data-target=".requestaquote">
              <span class="top-icon8"></span>
              <span class="toplink8">Request A Quote</span>		   
		   </a>				
			<div class="sub_newsletter top-prog"><a href="<?php echo $amc_urls['nl_subscription'];?>">Subscribe <span>To Our Newsletter<span> </a></div>
          </div>
        </div>
<?php } ?>

      </div>
       <div class="row ">
        <div class="mainnav-home">
			<div class="top-bar-wrap">
           <div class="top-prog"><a href="<?php echo $amc_urls['programs_services'];?>">Programs &amp; Services </a></div>
          <ul class="top-bar">  
			<?php 
			if (function_exists('fmg_menu') && fmg_var('category-menu')) { fmg_menu('product_cat'); } else { 
             icore_nav_menu('primary-menu', 'nav sf');} ?>
          
          </ul>
		  <form action="<?php echo site_url(); ?>" method="get" name="headerSearchFrm">
			  <div class="top-search search js_handleSearch">
				<input type="hidden" name="post_type" value="product"  /> 
				<span class="search-arrow"></span>
			  </div>
			  <div class="search-panel">
				<input type="text" name="s" maxlength="100" class="search-form-field js_searchBox" value="<?php echo get_search_query(); ?>" placeholder="TYPE HERE TO SEARCH">
			  </div>
		  </form>
      </div>
      </div>
	  </div>
      
 
  