<!-- Google Analytics Tracking Code -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6700265-1', 'auto');
  ga('send', 'pageview');

</script>


<!-- webtraxs Code -->
<script type="text/javascript">
document.write(unescape("%3Cscript src='" + document.location.protocol + "//www.webtraxs.com/trxscript.php' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
_trxid = "amconservationgroup";
webTraxs();
</script>
<noscript><img src="http://www.webtraxs.com/webtraxs.php?id=amconservationgroup&st=img" alt=""></noscript>

<!-- footer starts-->
<div class="page-footer">
<div class="row">
	<?php global $theme_options, $amc_urls; ?>
	<!-- footer top bar starts-->
	<div class="page-footer-top">
		<div class="row foot-top">
			<!--footer top left starts-->
			<div class="col-md-12 foot-top-left">
				<ul class="foot-social">
					<li><a href="<?php echo $amc_urls['pinterest']; ?>" target="_blank" class="pin-icon"></a></li>
					<li><a href="<?php echo $amc_urls['twitter']; ?>" target="_blank" class="twi-icon"></a></li>
					<li><a href="<?php echo $amc_urls['facebook']; ?>" target="_blank" class="fb-icon"></a></li>
				</ul>
				<ul class="foot-contact">
					<li><span class="f-cont-icon"></span><?php echo $amc_urls['contact_number']; ?></li>
					<li><a href="<?php echo $amc_urls['request_a_catalog']; ?>"><span class="f-quote-icon"></span>Request a Catalog</a></li>
				</ul>
				<!--footer top right starts-->
				<!-- News Letter AMCG -->
					<?php require_once('newsletter-box.php'); ?>
				<!-- / News Letter ends -->
				
			<!--footer top right ends-->
			</div>
			<!--footer top left ends-->
			
		</div>
	</div>
	<!-- footer top bar ends -->
	<!-- footer bottom bar starts-->
	<div class="page-footer-bottom">
		<div class="row foot-bottom">
			<!--footer bottom left starts-->
			<div class="col-md-12 foot-bot-left">
			   

			   <?php  wp_nav_menu( array( 'theme_location' => 'footer-menu',
									   'container_class' => '',
									   'container_id'    => '',
									   'menu_id'         => '' ));    ?>
			   
			   <script type="text/javascript">
				   jQuery('.foot-bot-left ul.menu li:last').addClass('border-none');
				<?php if(userIsLoggedIn()) { ?>
					jQuery('.foot-bot-left ul.menu').prepend('<li class="foonav"><a href="<?php echo $amc_urls['image_gallery']; ?>">Image Gallery</a></li>');
				<?php } ?>
			  </script>
			</div>
			<!--footer bottom left ends-->
			<!--footer bottom right starts-->
			<div class="col-md-12 foot-bot-right">
				<p class="foot-copy">Copyright <?php echo date("Y"); ?> AM Conservation Group</p>
			</div>
			<!--footer bottom right ends-->
		</div>
	</div>
	<!-- footer bottom bar ends-->
	</div>
<!-- footer ends-->
</div>
</div>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.actual.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-flexislider.js"></script>
  <script src="http://jarallax.com/download/jarallax-0.2.js" type="text/javascript"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/placeholder.js"></script>
  
  <script type="text/javascript">

      // Handle the search click
      $('.js_handleSearch').on('click', function() {
      	if($('input[name=s].js_searchBox').val() != undefined && $('input[name=s].js_searchBox').val() != '' && ($('input[name=s].js_searchBox').val() != '<?php echo get_search_query(); ?>')) {
      		$('form[name=headerSearchFrm]').submit();
      	}
      });
	  //top search
		$(document).ready(function() {
			$('.top-search.search').click(function(){
				$(this).find('.search-arrow').fadeToggle("fast");
				$(this).next('.search-panel').fadeToggle("fast");
			});
		});
		
      $(".select").hover(function(){
          var displaced=$(window).scrollTop()-2;
          $(".select ul").css("top",(150-displaced)+"px")
                }, function(){
                }
            ); 
       // For Place Holder
      $('input').placeholder(); 
       $('.mainmenu-icon6').on('hide.bs.popover', function () { 
          $(".mainnav-home").css({'opacity':'0.9'}); 
       });
	   $(".no-touch .top-bar").children('li').last().hover(
		  function () {

			  var ulwidth = $('.top-prog').width() - 30;

			  var submenuWid = 0;
			  $(".no-touch .top-bar li.last > ul  > li").each(function () {
				  submenuWid = submenuWid + $(this).actual( 'width', { absolute : true });
			  });


			  var x = $(this).offset().left - submenuWid + ulwidth;

			  var text = navigator.appVersion;
      var str = navigator.appVersion;
      var n = str.search(/MSIE 8.0;/i);
      if (n == 17) {
         
          $(".no-touch .top-bar li.last ul").animate({
              'padding-left': x + 100
          }, "normal");
      } else {
          $(".no-touch .top-bar li.last ul").animate({
              'padding-left': x
          }, "normal");
      }
		  },
		  function () {
			  $(".no-touch .top-bar li.last ul").css("padding-left", "");
		  }
		);
		/* to make disable hover effect from tab */
		if (!("ontouchstart" in document.documentElement)) {
document.documentElement.className += " no-touch";
}

    </script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/home.js"></script>  
	<script type="text/javascript">
<?php if(is_mobile()) { ?>
$('body').addClass('mobile_dev');

<?php } else { ?>
$('body').addClass('non_mobile_dev');

<?php } ?>
</script>  
</body>
</html>