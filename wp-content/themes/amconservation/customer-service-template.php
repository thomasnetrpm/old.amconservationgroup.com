<?php 
/* 
Template Name: Customer Service Template 
*/ 
define("CSS", "customer_service.css");

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header('shop-amc');
}
?>
 <!-- Title and Breadcrumb  -->
      <div class="row headersession">      
        <div class="col-md-7"><h3 class="headerlabel"><?php the_title(); ?></h3></div>
        <div class="col-md-5 breadcrumbs ">
          <ol class="breadcrumb">
           <?php breadcrumbs_fmg();  ?>
          </ol>
        </div>    
    </div>
     <!--/  Ends Title and Breadcrumb   -->
    <!--    content area start      -->
<div class="row cr">
    
    <!-- Dynamic  contents comes From the  AdminSide  -->   
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
    
    
        

    </div>
    
<script type="text/javascript">
var mq = window.matchMedia( "(max-width: 780px)" );
if (mq.matches) {
	var element ='<div class="col-md-4 back-btn"> <a href="<?php bloginfo('url'); ?>"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/back_btn.png">Back </a></div>'; 
	$('.row.headersession').append(element); 
} else {
	$(".back-btn").detach();
}
</script>
<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>