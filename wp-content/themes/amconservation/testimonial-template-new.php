<?php 
/* 
Template Name: Testimonial template New
*/ 
define("CSS", "testimonials.css");

// Handle the testimonial add
global $wpdb, $amc_urls;
$form_errors = array();
$sucess_msg = array();
$ins_errors = array();

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	// Handle ajax request of testimonials by year
	$tInputYear = date('Y');
	if(isset($_GET['y']) && !empty($_GET['y'])) {
		$tInputYear = $_GET['y'];

		// Code to get the testimonials
		$tResults = $wpdb->get_results('SELECT message AS content, name AS author FROM wp_vm_testimonials WHERE (status = "A" AND isinpool = "yes" AND (YEAR(created) = '.$tInputYear.')) ORDER BY created DESC', ARRAY_A);
		$tCount = $wpdb->num_rows;

		$tResultsStr = "{data:[";

		if(!empty($tResults) && is_array($tResults)) {
			foreach($tResults as $k => $tResultsItem) {
				$tResultsStr .= "{
					content: '".esc_html($tResultsItem['content'])."',
					author: '".$tResultsItem['author']."'
				}";
				if($k != ($tCount - 1)) {
					$tResultsStr .= ',';
				}
			}
		}

		$tResultsStr .= "]}";
		echo $tResultsStr; exit();
	}

	// Handle the testimonial submission
	if(isset($_POST['name_testimonialFrm']) && wp_verify_nonce($_POST['name_testimonialFrm'],'action_testimonialFrm')) {
		if(sanitize_text_field($_POST['testimonialFrm_name']) == '') {
			$form_errors['status'] = 'error';
			$form_errors['msg'] = 'Please enter name';
			echo json_encode($form_errors);
		} else if(sanitize_text_field($_POST['testimonialFrm_email']) == '') {
			$form_errors['status'] = 'error';
			$form_errors['msg'] = 'Please enter email';
			echo json_encode($form_errors);
		} else if(sanitize_text_field($_POST['testimonialFrm_company']) == '') {
			$form_errors['status'] = 'error';
			$form_errors['msg'] = 'Please enter company';
			echo json_encode($form_errors);			
		} else if(sanitize_text_field($_POST['testimonialFrm_subject']) == '') {
			$form_errors['status'] = 'error';
			$form_errors['msg'] = 'Please enter subject';
			echo json_encode($form_errors);
		} else if(sanitize_text_field($_POST['testimonialFrm_testimonial']) == '') {
			$form_errors['status'] = 'error';
			$form_errors['msg'] = 'Please enter testimonial';
			echo json_encode($form_errors);
		}

		if(empty($form_errors)) {
			$ins_ID = $wpdb->insert('wp_vm_testimonials', array(
				'created' => current_time('mysql'),
				'name' => sanitize_text_field($_POST['testimonialFrm_name']),
				'email' => sanitize_text_field($_POST['testimonialFrm_email']),
				'company' => sanitize_text_field($_POST['testimonialFrm_company']),
				'subject' => sanitize_text_field($_POST['testimonialFrm_subject']),
				'message' => sanitize_text_field($_POST['testimonialFrm_testimonial']),
				'status' => 'P'
			));

			if($ins_ID) {
				$sucess_msg['status'] = 'success';
				$sucess_msg['msg'] = 'Thank you for taking time to provide your feedback. We value your input.';
				echo json_encode($sucess_msg);
			} else {
				$ins_errors['status'] = 'error';
				$ins_errors['msg'] = 'Unable to add testimonial. Try, after some time.';
				echo json_encode($ins_errors);
			}
		}
		exit();
	}
}

$testimonialsArray = array();
$tInputYear = date('Y');
if(isset($_GET['y'])) {
	$tInputYear = $_GET['y'];
}

// Code to get the testimonials
$tResults = $wpdb->get_results('SELECT message AS content, name AS author FROM wp_vm_testimonials WHERE (status = "A" AND isinpool = "yes" AND (YEAR(created) = '.$tInputYear.')) ORDER BY created DESC', ARRAY_A);
$tCount = $wpdb->num_rows;

if($tCount == 0) {
	$tInputYear = $tInputYear - 1;
	$tResults = $wpdb->get_results('SELECT message AS content, name AS author FROM wp_vm_testimonials WHERE (status = "A" AND isinpool = "yes" AND (YEAR(created) = '.$tInputYear.')) ORDER BY created DESC', ARRAY_A);
	$tCount = $wpdb->num_rows;
}

$tResultsStr = "{data:[";

if(!empty($tResults) && is_array($tResults)) {
	foreach($tResults as $k => $tResultsItem) {
	
		$testimonialsArray[] = array(
			"content" => esc_html($tResultsItem['content']),
			"author" => $tResultsItem['author']
		);

		// $testimonialsArray[]['content'] = esc_html($tResultsItem['content']);
		// $testimonialsArray[]['content'] = esc_html($tResultsItem['content']);

		$tResultsStr .= "{
			content: '".esc_html($tResultsItem['content'])."',
			author: '".$tResultsItem['author']."'
		}";
		if($k != ($tCount - 1)) {
			$tResultsStr .= ',';
		}
	}
}

$tResultsStr .= "]}";

//echo $tResultsStr;

$tYears = $wpdb->get_results('SELECT YEAR(created) as ylist FROM wp_vm_testimonials WHERE (status = "A" AND isinpool = "yes")  GROUP BY ylist ORDER BY created DESC', ARRAY_A);
$tYearsCount = $wpdb->num_rows;
$tYearStr = '[';
if(!empty($tYears) && is_array($tYears)) {
	foreach($tYears as $k2 => $tYearsItem) {
		$tYearStr .= '"'.$tYearsItem['ylist'].'"';
		if($k2 != ($tYearsCount - 1)) {
			$tYearStr .= ',';
		}
	}
}
$tYearStr .= ']';


if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}
?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/modernizr.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.touchSwipe.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.mousewheel.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/perfect-scrollbar.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/perfect-scrollbar.css">


<div class="row"><div class="col-md-12 aboutbanner"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimonials_banner.jpg"></div></div> 

<!-- Middle Mennu  -->
        <div class="row menu_pos">
        <div class="leftarrow-inner SlideOff"></div>
             <div class="aboutmenu-container">
                 <?php require_once('middle-menu.php'); ?>
             </div>
    </div>
<!--/  Ends Middle Mennu  -->

<div class="row white-bg">
<?php
/*
	<div class="tl-container">
   <div class=" image-container1">
    <div class=" text-align1"> <a class="nav-Prev" width="40" height="40"></a><a class="nav-Next" width="40" height="40"></a>   </div>
  <!-- <div class="col-md-1 col-xs-6 "><a class="nav-Next" width="40" height="40"></a>  </div>   -->
  </div>
<div class="dd-list">
<select id="fat-menu" class="dropdown js_tYearBox" role="menu" aria-labelledby="drop3">
<?php
if(!empty($tYears) && is_array($tYears)) {
	echo '<option value="'.date('Y').'">Year</option>';
	foreach($tYears as $tYearsItem) {
		$selectedStr = '';
		if($tInputYear == $tYearsItem['ylist']) {
			$selectedStr = 'selected="selected"';
		}
		echo '<option value="'.$tYearsItem['ylist'].'" '.$selectedStr.'>'.$tYearsItem['ylist'].'</option>';
	}
}
?>
</select>



</div>

<div class="ts-content">
	 <div id="mcts1"><div><div></div></div></div>       
	<div class="t1-selector"></div>
</div>


<div class=" offset-spacer "><div class="write_testimonials " data-toggle="modal" data-target=".testimonials"> <a> Write your own Testimonial</a></div></div>

  </div>
*/
?>

<?php // if(!is_mobile()) { ?>
		<div class="list_carousel test hidden-xs">

            <ul id="desktop-testimonial">
<?php foreach($testimonialsArray as $testimonialsItem) { ?>
<li>
	<span class="testimonial-arrow"></span>
	<div class="description"><div><?php echo $testimonialsItem['content']; ?><span class="authname"><?php echo $testimonialsItem['author']; ?></span></div></div>
</li>
<?php } ?>
            </ul>
            <div class="clearfix"></div>
            <a class="prev" id="desktop_prev" href="#"><span></span></a>
            <a class="next" id="desktop_next" href="#"><span></span></a>
			
			<div class="select-container">
<select id="fat-menu" class="dropdown js_tYearBox" role="menu" aria-labelledby="drop3">
<?php
if(!empty($tYears) && is_array($tYears)) {
	echo '<option value="'.date('Y').'">Year</option>';
	foreach($tYears as $tYearsItem) {
		$selectedStr = '';
		if($tInputYear == $tYearsItem['ylist']) {
			$selectedStr = 'selected="selected"';
		}
		echo '<option value="'.$tYearsItem['ylist'].'" '.$selectedStr.'>'.$tYearsItem['ylist'].'</option>';
	}
}
?>
</select>
</div>
		</div>

<?php // } else { ?>

		<div class="visible-xs mobile-container">

            <div class="list_carousel">
                <ul id="mobile-testimonial">
<?php foreach($testimonialsArray as $testimonialsItem) { ?>
<li>
	<span class="testimonial-arrow"></span>
	<div class="description"><?php echo $testimonialsItem['content']; ?><span class="authname"><?php echo $testimonialsItem['author']; ?></span></div>
</li>
<?php } ?>
                </ul>
            </div>
			
			<div class="select-container">
<select id="fat-menu" class="dropdown js_tYearBox" role="menu" aria-labelledby="drop3">
<?php
if(!empty($tYears) && is_array($tYears)) {
	echo '<option value="'.date('Y').'">Year</option>';
	foreach($tYears as $tYearsItem) {
		$selectedStr = '';
		if($tInputYear == $tYearsItem['ylist']) {
			$selectedStr = 'selected="selected"';
		}
		echo '<option value="'.$tYearsItem['ylist'].'" '.$selectedStr.'>'.$tYearsItem['ylist'].'</option>';
	}
}
?>
</select>
</div>
        </div>
<?php // } ?>


<div class=" offset-spacer "><div class="write_testimonials " data-toggle="modal" data-target=".testimonials"> <a> Write your own Testimonial</a></div></div>

</div>

<div class="modal fade testimonials content" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content-rac">
  <div class="modal-header-rac">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  </div>
  <div class="modal-body">
    <div class="panel-group" id="accordion">
      <div class="cs-pos-content-heading1"> Submit Your Testimonial </div>

<div class="js_result_tm alert fade in" style="display:none;">
	<button type="button" class="close js_close_tm" aria-hidden="true">×</button>
	<p></p>
</div>
<br />

      <div class="form-group">
        <form name="testimonialFrm" id="testimonialFrm" action="" method="post" parsley-validate novalidate>
<?php wp_nonce_field('action_testimonialFrm', 'name_testimonialFrm'); ?>
          <input type="text" name="testimonialFrm_name" class="form-control" maxlength="40" placeholder="Name*" required parsley-error-message="Please enter your name">
          <br>
          <input type="email" name="testimonialFrm_email" class="form-control" maxlength="40" placeholder="Email*" required parsley-error-message="Please enter your email">
          <br>
          <input type="text" name="testimonialFrm_company" class="form-control" maxlength="40" placeholder="Company*" required parsley-error-message="Please enter your company">
          <br>		  
          <input type="text" name="testimonialFrm_subject" class="form-control" maxlength="45" placeholder="Summary of your Testimonial*" required parsley-error-message="Please enter  subject">
          <br>
         
          <textarea name="testimonialFrm_testimonial" class="form-control" maxlength="500" placeholder="Testimonial*" required parsley-error-message="Please enter testimonial"></textarea>
		   <br>
		   For security purposes, please type in the letters below in the box.
          <br><br>
		 
		   <div id="wrap" align="center">
			<img src="<?php echo get_template_directory_uri(); ?>/captcha/get_captcha.php" alt="" id="captcha" />
            <br clear="all" />
			<input name="code" type="text" id="code">
			</div>
			<img src="<?php echo get_template_directory_uri(); ?>/captcha/refresh.jpg" width="25" alt="" id="refresh" />
            <br clear="all" /><br clear="all" />
			<label>&nbsp;</label>
			<div id="Sends"> </div>
          <div class="row">
            <div class="col-md-6 ">
              <p class="ts-parag-txt-2"> *Mandatory Fields </p>
			  <p class="js_result" style="display:none;"></p>
            </div>
            <div class="col-md-2 col-md-offset-4 ">
              <input type="submit" name="testimonialFrm_submit" value="Submit"  class="btn ts-submit-btn " />
            </div>
          </div>
          <br>
          <br>
          <br>
          <br>
        </form>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
</div></div>

<script type="text/javascript">
$('form[name=testimonialFrm]').on('submit', function() {
	if($('form[name=testimonialFrm]').parsley('isValid')) {
		$.ajax({
			url: '<?php echo $amc_urls['home']; ?>/testimonials/',
			method: 'post',
			data: $('form[name=testimonialFrm]').serialize(),
			success: function(d) {
				var o = $.parseJSON(d);
				/*
				if(o.status == 'error') {
					$('p.js_result').css('color', 'red').text(o.msg).show().fadeOut(2000);
				} else if(o.status == 'success') {
					$('p.js_result').css('color', 'green').text(o.msg).show().fadeOut(2000);
					$('input[type=text], textarea').val('');
				}
				*/

				if(o.status == 'error') {
					$('.js_result_tm').removeClass('alert-warning').addClass('alert-danger').find('p').text(o.msg);
					$('.js_result_tm').show();
				} else if(o.status == 'success') {
					$('.js_result_tm').removeClass('alert-danger').addClass('alert-warning').find('p').text(o.msg);
					$('.js_result_tm').show();
					$('input[type=text], textarea').val('');
					$('img#refresh').click();
				}
			}
		});
	}
	return false;
});

$('button.js_close_tm').on('click', function() {
	$('.js_result_tm').hide();
});

</script>




<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-migrate-1.2.1.js"></script>  

<script type="text/javascript">
var siteURL = '<?php echo $amc_urls['home']; ?>';
var assetURL = '<?php echo get_template_directory_uri(); ?>/assets/';
var WPYearResponse = <?php echo $tYearStr; ?>;
var WPJSONResponse = <?php echo $tResultsStr; ?>;
var testimonialURL = siteURL + '/testimonials/';
</script>


<script type="text/javascript">
// RWD set width for testimonail container start 
$(document).ready(function() {
  
	var url = '<?php echo get_template_directory_uri();?>';
	$('.ts-submit-btn').click(function() {
		$( '#testimonialFrm' ).parsley( 'validate' );
		var codeval = $("#code").val();
		var url = '<?php echo get_template_directory_uri();?>';
        var ajaxStatusVar;
		  
		  
		  $.ajax({
			type: "POST",
			url: url+"/captcha/post.php",
			global: false,
			async: false,
			data: { code: codeval   }
			
			})
			.done(function( data ) {
				if(data.indexOf('fail') > 0) {
					$("#after_submit").html('');
					$("#Sends").after('<label class="error" id="after_submit">Error ! invalid captcha code.</label>');
					ajaxStatusVar = false;
					return false;
			   } else if(data.indexOf('success') > 0){
					$("#after_submit").html('');
					ajaxStatusVar = true;
					return true;
			   }
			});
			if (ajaxStatusVar == false) {return false;}
	});
	
	$('img#refresh').click(function() {  
			
			change_captcha();
	 });
	 
	 function change_captcha()
	 {
		var url = '<?php echo get_template_directory_uri();?>';
	 	document.getElementById('captcha').src=url+"/captcha/get_captcha.php?rnd=" + Math.random();
	 }
	 
});
// RWD set width for testimonail container End

$(window).load(function(){
 //$('#mcts1 .item.evenclass:nth-child(2n+1)').addClass('bgblack');
});

$('select.js_tYearBox').change(function() {
	
	window.location = '<?php echo $amc_urls['testimonial'].'?y='; ?>'+$(this).val();	
     
     

 

	$.ajax({
	url: testimonialURL,	
	method: 'get',
	// dataType: "json",
	data: {y: $(this).val()},
	success: function(d) {
		
		
		//toggleScroll(d);
		
	}
});

	

});


</script>


<script>

$(window).load(function () {
	$("#desktop-testimonial").after('<ul id="desktop-testimonial2" />').next().html($("#desktop-testimonial").html());
	$("#desktop-testimonial li:odd").remove();
	$("#desktop-testimonial2 li:even").remove();
	$("#desktop-testimonial li").addClass('topsec');
	$("#desktop-testimonial2 li").addClass('bottomsec');

	$("#desktop-testimonial").carouFredSel({
		synchronise: "#desktop-testimonial2",
		width: "76%",
		auto: false,
		circular: false,
		infinite: false,
		prev: "#desktop_prev",
		next: "#desktop_next",
		swipe: {
		   
			onTouch: true
		},

		responsive: true,
		
		items: {
			visible: {
				min: 1,
				max: 3
			},
			width: 400
		   

		}
	}).parent().css("margin-left", "10%");
	$("#desktop-testimonial2").carouFredSel({
		synchronise: "#desktop-testimonial",
		auto: false,
		circular: false,
		infinite: false,
		width: "76%",
		responsive: true,
		
		swipe: {
		  
			onTouch: true
		},
		items: {
			visible: {
				min: 1,
				max: 3
			},
			width: 400
		   

		}

	}).parent().css("margin-left", "17%");




	/* MOBILE TESTIMONIAL */


	$("#mobile-testimonial").carouFredSel({
		synchronise: "#desktop-testimonial",
		auto: false,
		circular: false,
		infinite: false,
		width: "100%",
		responsive: true,
		scroll: 1,
		swipe: {
		   
			onTouch: true
		},
		items: {
			visible: {
				min: 1,
				max: 2
			},
			width: 500

		}

	});
	$('#mobile-testimonial li:odd').addClass('oddli');
	$('#mobile-testimonial li:even').addClass('evenli');

	

	$("#desktop-testimonial li.topsec:odd ,#desktop-testimonial2 li.bottomsec:even ").css('background', '#525251');
	$("#desktop-testimonial li.topsec:even ,#desktop-testimonial2 li.bottomsec:odd ").css('background', '#f59c34');




	$("#desktop-testimonial li.topsec:odd span.testimonial-arrow").css('background-image', 'url("<?php echo get_template_directory_uri(); ?>/assets/images/testimonial-arrow-top.png")');

	$("#desktop-testimonial li.topsec:even span.testimonial-arrow").css('background-image', 'url("<?php echo get_template_directory_uri(); ?>/assets/images/testimonial-arrow-yellow-top.png")');

	$("#desktop-testimonial2 li.bottomsec:odd span.testimonial-arrow").css('background-image', 'url("<?php echo get_template_directory_uri(); ?>/assets/images/testimonial-arrow-yellow-bottom.png")');
	$("#desktop-testimonial2 li.bottomsec:even span.testimonial-arrow").css('background-image', 'url("<?php echo get_template_directory_uri(); ?>/assets/images/testimonial-arrow-bottom.png")');

	/*CODE FOR SCROLL FUNCTIONALITY */
	if (!Modernizr.touch) {
	
		scrolldiv();
		
	}
	
})

$(window).resize(function () {
	
	
	if (!Modernizr.touch) {
		//scrolldiv()
		$('.description').perfectScrollbar('update');
	}

});
function scrolldiv() {
	$('.description').perfectScrollbar({
		wheelSpeed: 20,
		wheelPropagation: false,
		suppressScrollX: true
	});
}






</script>



<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>