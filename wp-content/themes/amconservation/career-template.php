<?php 
/* 
Template Name: Career  template 
*/ 
define("CSS", "careers.css");

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}
?>
<div class="row">      
        <div class="col-md-12 aboutbanner"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/Careers.png"></div>  
    </div>

 <!-- Middle Mennu  -->
        <div class="row menu_pos">
        <div class="leftarrow-inner SlideOff"></div>
             <div class="aboutmenu-container">
                 <?php require_once('middle-menu.php'); ?>
             </div>
    </div>
     <!--/  Ends Middle Mennu  -->

    <div class="row careerscontent">
        <div class="col-md-11 col-md-offset-1">
          
<!-- Dynamic  contents comes From the  AdminSide  -->   
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<?php the_content(); ?>
<?php endwhile; endif; ?>

      <?php

  $args = array(
        'post_type'           => 'job_listing',
        'post_status'         => 'publish',
        'ignore_sticky_posts' => 1,
        'posts_per_page'      => $per_page,
        'orderby'             => $orderby,
        'order'               => $order,
      );

      if ( $categories )
        $args['tax_query'] = array(
          array(
            'taxonomy' => 'job_listing_category',
            'field'    => 'slug',
            'terms'    => $categories
          )
        );

      if ( get_option( 'job_manager_hide_filled_positions' ) == 1 )
        $args['meta_query'] = array(
          array(
            'key'     => '_filled',
            'value'   => '1',
            'compare' => '!='
          )
        );

      $jobs = new WP_Query( apply_filters( 'job_manager_output_jobs_args', $args ) );
      ?>   
         <!--content below heading -->
  
         <div class="cs-top-spacer-1"></div>
      </div>
    </div>   

	

<div class="modal fade requestaquote" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content-rac">
            <div class="modal-header-rac">
              
            </div>
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <div class="panel-group" id="accordion">
           <div class="cs-pos-content-heading1"> Available Career Positions </div>
  <?php if ( $jobs->have_posts() ) : 
      
      $i=0;
   ?>    
      <?php while ( $jobs->have_posts() ) : $jobs->the_post(); ?>
      <?php $c_class = ($i==0)?"in":""; ?>

  <div class="panel panel-default">
    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#panel_<?php echo $i;?>">
      <h4 class="panel-title">
        <a >
         <span class="crs-panel-title"> <?php the_title(); ?></span>
        </a>
      </h4>
    </div>
    <div id="panel_<?php echo $i;?>" class="panel-collapse collapse <?php echo $c_class; ?>">
      <div class="panel-body">
      <div class="careers-panel-content">
<?php echo apply_filters( 'the_job_description', get_the_content() ); ?>

</div>
      </div>
    </div>
  </div>
  <?php 
   $i++;
  endwhile; ?>
  <?php  endif;   ?>

</div>
             
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->


<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>