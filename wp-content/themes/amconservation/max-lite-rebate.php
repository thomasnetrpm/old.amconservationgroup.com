<?php 
/* 
Template Name: Maxlite Rebate Page
*/ 
global $amc_urls;


define("CSS", "productlisting.css");

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header('shop-amc');
}
?>
<style type="text/css">
	#programs_services h1 {
    margin: 25px 0 24px;
}
.pns-headertxt {
    color: #3968B1;
    font-family: "open sans";
    font-size: 24px;
    font-weight: bold;
    margin: 40px 0 24px 21px;
}

.#tps_content  {
    color: #000000;
    font-family: "open sans";
    font-size: 14px !important;
    font-weight: normal !important;
    line-height: 18px !important;
    margin: 20px 0 0 !important;
    width: 100% !important;
}
.form-field {
    background-color: #FFFFFF;
    border: 1px solid #CCCCCC;
    border-radius: 0;
    box-shadow: 0 0 7px #CCCCCC inset;
    color: #555555;
    display: block;
    font-size: 13px;
    float: left;
    height: 41px;
    line-height: 1.42857;
    margin: 20px 20px 0 0;
    padding: 0 0 0 11px;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
    vertical-align: middle;
    width: 20%;
}

.rebate_submit {
    color: #fff;
    background-color: #525151;
    border-color: #525151;    
}
.table tbody > tr > td:first-child img.rebeat_img {
  margin: 10px;
  max-width:120px;
}
</style>
 <div class="row headersession">
        <div class="col-md-6"><h3 class="headerlabel">
		<?php the_title(); ?>
		 </h3></div>

<div class="col-md-6 bcs inBread">
          <ol class="breadcrumb">
            <?php breadcrumbs_fmg();  ?>
          </ol>
        </div>
<?php if(is_mobile()) { ?>
	<div class="col-md-4 back-btn back-page-com"> <a onClick="history.go(-1)"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/back_btn.png">Back </a></div>
<?php } ?>
    </div> 
 	<!-- Main Banner Section -->
    <div class="row">
		<div class="col-md-12 aboutbanner">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/commercial-rebates-header.jpg">
		</div>
	</div>
	<!-- Ends Main Banner Section -->
	<!-- Rebate information Section With DropDown-->
	<div class="row pns-content">      
          <div class="col-md-11 col-md-offset-1 pns-container">
            <div id="programs_services">
              <h1 class="pns-headertxt">Commercial Lighting Rebates </h1>
              <div id="tps_content">
                              <!--AM Conservation Group offers an assortment of comprehensive educational programs for utility companies, program managers, and various government agencies. Our programs are 100% turn-key and provide learning and savings through curriculum and easy-to-install conservation kits.
Typical goals of educational programs include:-->
AM Conservation Group offers a number of commercial lighting products for both indoors and outdoors along with detailed utility rebate information. We work closely with utility companies, program managers and government agencies to offer comprehensive programs to assist with their commercial and industrial lighting needs.
<br><br>
Choose your state and utility below to view rebates available for products in your area.
 			 </div> 
            </div>
           <div id="tps_content">
	
<?php
   #Get Api Values 
   $Api_Url = $amc_urls['maxlite_Api_url'];
   # Get The Country Dropdown List #
   $Country_List = file_get_contents($Api_Url.'/utility-rebates/states');
   $Country_List = json_decode($Country_List);
  ?>
  		<form name="rebate_form">
  		<select class="form-field country_list" name="country_list">
		<option value=""> Choose state </option>
  		<?php if($Country_List) {
  			foreach ($Country_List as $key => $value) {
  			?>
  			<option value="<?php echo $value->Id;?>"><?php echo $value->StateName;?></option>
  			<?php 
  				}
  			} ?>
  		</select>
		
		<select id="dropDownList" name="dropDownList" class="form-field">
		<option value=""> Choose state first </option>
        </select> 
  		<input type="button" name="Submit" class="form-field rebate_submit" value="CHECK FOR REBATES"/>
  		</form>
           </div>
    <br><br>   <br><br>
	<table class="table table-prddisplay" data-val="4"></table>
 
      </div> 
	   
      </div>
	 
	<!-- Ends Rebate information Section with Drop Down-->
	
	
<script type="text/javascript">
	 // get utlity information 
	 var Api_Url = "<?php echo $Api_Url.'/utility-rebates/states/'; ?>";
	$("#dropDownList").prop("disabled", true);
    $(".country_list").change(function()
    { 
	  var stval = $(this).val();
	  var apiurl = Api_Url+$(this).val();
	  var xhr = new XMLHttpRequest();
	  if ("withCredentials" in xhr) {
		xhr.open("GET", "http://developer.maxlite.com/api/utility-rebates/states/"+stval, true);
		xhr.send();
	  }	
	  
	  $.support.cors = true;

	  $.getJSON( apiurl, function( data ) {
		$("#dropDownList").prop("disabled", false);
		$('#dropDownList').empty();
		$(".even,.rowone").empty();
		$(".table").html("");
		 $.each(data, function (index, item) {
         $('#dropDownList').append(
              $('<option></option>').val(item.Id).html(item.ProgramName)
          );
		});
	  });
    });
	
	 $("#dropDownList").change(function()
	 {
	    $(".even,.rowone").empty();
		$(".table").html("");
	 });
	
	$(".rebate_submit").click(function()
	{
	  	var sku_val = "<?php echo $_GET['sku']; ?>";
		var utilityID = $('#dropDownList').val();
		var country = $('.country_list').val();
		var itemNo = "";
		var prodName = "";		
		$(".even,.rowone").empty();
		$(".table").html("");
		var get_item_url = "<?php echo $Api_Url.'/utility-rebates/states/'; ?>"+country+"/"+utilityID;
		$.getJSON( get_item_url, function( get_data ) {	
		if(country != '') {
			$("<thead><tr class='even'><th>Image</th><th>Item (SKU)</th><th>Description</th><th>Type</th><th>Rebate Amount</th><th>Per Unit</th><th>Rebate Detail</th><th>Form</th></tr></thead>").appendTo($(".table"));
		}
			$.each(get_data, function (index, item) {			
				 itemNo = item.ItemNo;				 
				 img_url = "<?php echo $Api_Url.'/products/images?itemno='; ?>";	
	 <?php       
		$sha = array();	   
        $args = array( 'post_type' => 'product', 'posts_per_page' => -1, 'product_cat' => 'Energy Efficient Lighting' );		
        $loop = new WP_Query( $args );		
		$meta_values = array();
        while ( $loop->have_posts() ) : $loop->the_post();		
	    $children = $product->get_children();
		
	   foreach($children as $value){
	   $sku_values = get_post_meta( $value, '_sku');
	   $meta_values [] = $sku_values[0];
	   $sha[] = get_permalink();
	   }
		endwhile;

		$unique_meta_values = array_unique($meta_values);
		foreach($unique_meta_values as $metaval) {	
		$post_id = $wpdb->get_results("SELECT post_id FROM $wpdb->postmeta WHERE (meta_key = '_sku' AND meta_value = '". $metaval ."')"); 	   		
		$product_ID = $post_id[0]->post_id;    
        $product_url =	$sha[0].'?variation_id='.$product_ID.'';	
	?>	
		if(itemNo =='<?php echo $metaval; ?>') {	           
				var item_results_url = "<?php echo $Api_Url.'/products?itemno='; ?>"+itemNo;
				
				var product_url = "<?php echo $product_url; ?>";
				
				$.getJSON( item_results_url, function( get_item_data ) {				
				  rebateAmount = item.Amount;
				  newItemAmount = "";
				  if((country =='WY' || country == 'UT' || country == 'ID') && utilityID =='140') {
				   RebateDetail = '<a target="_blank" href="https://www.rockymountainpower.net/content/dam/rocky_mountain_power/doc/About_Us/Rates_and_Regulation/Wyoming/Approved_Tariffs/Rate_Schedules/FinAnswer_Express.pdf" class="pl-btn btn-primary .btn-block">Click here to View Details</a>';
				   DownloadForm = 'http://mylightingrebates.com/rocky-mountain-power-lighting-rebates/';
				  }	
				  else {
					RebateDetail = item.Memo;
					DownloadForm = item.ApplicationURL
				  }
				  
				  if(rebateAmount =='0') {
					newItemAmount = 'Custom Rebate';
				  }
				  else
				  {				  
					  var amount = item.Amount;
					  var str = amount.toString();
					  var decimal=  /^[-+]?[0-9]+\.[0-9]+$/;  
					  if(str.match(decimal)){
						itemAmount = amount.toFixed(2);	
						newItemAmount  = '$' + itemAmount;	
					  } else {
						 newItemAmount  = '$' + amount;
					  }
				  }
				  
				   $('<tr class="odd">').html(
        "<td width='18%'><img src='"+img_url+item.ItemNo+"' alt='"+item.ItemNo+"'  class='rebeat_img' /></td><td width='13%'><a target='_blank' href='"+product_url+"'>" + item.ItemNo + "</a></td><td width='17%'>" + get_item_data.Description + "</td><td width='10%'>" + item.Type + "</td><td width='10%'>" + newItemAmount + "</td><td width='10%'>" + item.PerUnit + "</td><td width='12%'>" + RebateDetail + "</td><td width='10%'><a target='_blank' href="+DownloadForm+" class='pl-btn btn-primary .btn-block'>Download Form</a></td>").appendTo('.table');
		
			  });
			  } 
	<?php
	 } 
	?>	
			  
			  
		   });
		});
		if(country != '') {
			setTimeout(function() {
				if($('.table').find('td').text() == ''){
					   $('<tr class="odd no_found">').html("<td colspan='8' height='100'>No Commercial Lighting Rebates Available</td>").appendTo('.table');
				}
			}, 3000);
		}
	});	
	

</script>
<?php
// Footer Section 
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer('shop');
}
?>