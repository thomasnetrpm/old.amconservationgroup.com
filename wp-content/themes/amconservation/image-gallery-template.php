<?php 
/* 
Template Name: Image gallery template 
*/ 
//echo "<pre>"; print_r($_GET);
redirectUnLoggedUser();
$loggedInUser = getLoggedInUser();
$isSubscribed = isNewsletterSubscribed();
define("CSS", "image-gallery.css");
global $theme_options, $amc_urls; 
if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}

// Checking the selected category for breadcrumb
$bcCatId = $_GET['view'];
    $bcArgs = array(
     	'slug' => $bcCatId // id of the direct parent
	);

$bcCats = get_terms( 'product_cat', $bcArgs );
//echo "<pre>"; print_r($bcCats);
?>
<script type="text/javascript">
$('div.headersession').hide();
</script>

    <div class="row headersession">
        <div class="col-md-7"><h3 class="headerlabel">
                Image Gallery</h3>
        </div>
        <div class="col-md-5 bcs ">
          <ol class="breadcrumb">
            <li><a href="<?php echo $amc_urls['home']; ?>" class="home">Home</a> </li>
            
            <?php if(isset($_GET['view'])) { ?>
                <li><a href="<?php echo $amc_urls['image_gallery']; ?>">Image Gallery</a></li>
                <li><?php echo $bcCats[0]->name; ?></li> 
            <?php }else{ ?>
                <li>Image Gallery</li>
            <?php } ?>
          </ol>
        </div>
    </div>
      <div class="row">
        <div class="col-md-3 "> 
           <div class="bodyrow "> 
            <?php
            // Loads the Categories 
            $catTerms = get_terms('product_cat', array('hide_empty' => 0,'orderby'=> 'none', 'parent'=>0,'hierarchical'  => true));
            //print_r($catTerms);
            foreach($catTerms as $key=> $catTerm) :
            ?>
            <a href="<?php echo $amc_urls['image_gallery'];?>?view=<?php echo $catTerm->slug;?>"><?php echo $catTerm->name;?></a>
           <?php endforeach; ?>
        </div>
        </div>
        <?php 
        $catid 			= empty($_GET['view'])?'energy-efficient-lighting':$_GET['view'];

        $args = array(
     				'slug' => $catid // id of the direct parent
					);

		$cats = get_terms( 'product_cat', $args );
		sort($cats);

        // Check if we are in the main Category or Chiild Category //
        if($cats[0]->parent == 0)
        {

	        	include('woocommerce/taxonomy-product_cat-image-gallery.php');
        }else
        {
        	      include('woocommerce/taxonomy-product-image-gallery.php');
        }

         ?>
       </div>

<script type="text/javascript">
function download(filename, text) {
    var pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    pom.setAttribute('download', filename);
    pom.click();
}

var backbtn = false;

$(".download-btnX").click(function() {
	download( $( this ).parents( ".image-container" ).find("img:first").attr( "src") , 'Hello world!');
});
$(".gallery-2").css("display","none");
$(".galy2").css("display","none");


$("div .products-link").click(function(){

	$( ".gallery-1" ).toggle();
	$( ".gallery-2" ).toggle();

	$(this).empty();

	if(!backbtn)	$(this).append("Back");

	else $(this).append("Load More Products");

	backbtn = !backbtn;

});

var ID;
	 
	  flag = ".gallery-image";
	  
	  
	  
	 $(".ig-closebtn").click(function(){
		
		 $(ID).css("display","block");
		 $(".galy2").css("display","none");
		  $(".loadingmore-row").css("display","block");
	 });
      
	  
$(flag).click(function(){

			 $( this ).closest( ".galy" ).css("display","none");		        
		     ID = "#"+ $( this ).closest( ".galy" ).attr("id");
			 $(".galy2").css("display","block");
			 $(".loadingmore-row").css("display","none");			
			
			
			
			} );

	 $(flag).mouseenter(function(e) {
		 
		
			  $( "<div class='category-mouseover-btn'><p class='view-category'>VIEW IMAGE</p></div>" ).prependTo( $(this).parent().parent());     
			  	           $(this).animate({ opacity: 0.3},200); 	   
		      /* $(this).css("border","1px solid #dcdcdc");*/
			     
			    
		
		
	
})
.mouseleave(function() {
	
$(this).animate({ opacity: 1},200); 


	$(this).parent().parent().find("div.category-mouseover-btn").remove();

		/**/
	/*	$(this).css("border","");*/
});
</script>

<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>