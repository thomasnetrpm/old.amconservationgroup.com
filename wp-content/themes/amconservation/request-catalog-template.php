<?php 
/* 
Template Name: Request A Catalog  template 
*/
ob_start();
error_reporting(0);
define("CSS", "requestcatalog.css");

global $wpdb, $amc_urls, $show_msg, $success_msg, $error_msg;
$show_msg = 'none';

// Check for login user
if(userIsLoggedIn()) {
	$loggedInUser = getLoggedInUser();
	$uFName = $loggedInUser->firstname;
	$uLName = $loggedInUser->lastname;
	$uCompany = $loggedInUser->companyname;
	$uEmail = $loggedInUser->email;
}

// Handle the form submission
$form_errors = array();
$user_errors = array();
$success_msg = array();

if(isset($_POST['reqCatForm_submit']) && ($_POST['reqCatForm_submit'] == 'PLACE REQUEST') && isset($_POST['name_reqCatForm']) && wp_verify_nonce($_POST['name_reqCatForm'],'action_reqCatForm')) {
	if(sanitize_text_field($_POST['reqCatForm_firstname']) == '') {
		$form_errors['reqCatForm_firstname'] = 'Please enter first name';
	}
	if(sanitize_text_field($_POST['reqCatForm_lastname']) == '') {
		$form_errors['reqCatForm_lastname'] = 'Please enter last name';
	}
	if(sanitize_text_field($_POST['reqCatForm_company']) == '') {
		$form_errors['reqCatForm_company'] = 'Please enter company name';
	}

	if(sanitize_text_field($_POST['reqCatForm_email']) == '') {
		$form_errors['reqCatForm_email'] = 'Please enter email address';
	} else {
		if(!is_email(sanitize_text_field($_POST['reqCatForm_email']))) {
			$form_errors['reqCatForm_email'] = 'Please enter valid email address';
		}
	}

	/*if(sanitize_text_field($_POST['reqCatForm_marketSegment']) == '') {
		$form_errors['reqCatForm_marketSegment'] = 'Please select market segment';
	}	*/
	// Removing the empty validation for address, comments
	/*
	if(sanitize_text_field($_POST['reqCatForm_address']) == '') {
		$form_errors['reqCatForm_address'] = 'Please enter address';
	}
	if(sanitize_text_field($_POST['reqCatForm_comments']) == '') {
		$form_errors['reqCatForm_comments'] = 'Please enter comments';
	}
	*/

	if(empty($form_errors)) {
		$emailCat = (sanitize_text_field($_POST['reqCatForm_emailcat']) == 'yes') ? 1 : 0;

		$ins_ID = $wpdb->insert( 
			'wp_requestcatalog', 
			array( 
				'created' => current_time('mysql'), 
				'modified' => current_time('mysql'),
				'firstname' => sanitize_text_field($_POST['reqCatForm_firstname']),
				'lastname' => sanitize_text_field($_POST['reqCatForm_lastname']),
				'company' => sanitize_text_field($_POST['reqCatForm_company']),

				'email' => sanitize_text_field($_POST['reqCatForm_email']),
				'marketsegment' => sanitize_text_field($_POST['reqCatForm_marketSegment']),
				'address' => sanitize_text_field($_POST['reqCatForm_address']),
				'comments' => sanitize_text_field($_POST['reqCatForm_comments']),
				'is_catalog_emailed' => $emailCat
			) 
		);
		
		

		if($ins_ID) {
			$form_id = "request_a_catalog";
			include('hubspot-form-api.php');
			
			$success_msg[] = 'Successfully requested.';

			if($emailCat == 1) {
				// @to-do: sent the catalog
				// $catMsg = 'Hi '.sanitize_text_field($_POST['reqCatForm_firstname']).' '.sanitize_text_field($_POST['reqCatForm_lastname']).'<br /><br />';
				// $catMsg .= 'The catalog you requested. Click to download,<br />';
				// $catMsg .= '<a href="'.$amc_urls['catalog_link'].'">Catalog.pdf</a><br /><br />';
				// $catMsg .= get_bloginfo('name');

				$catMsg .= 'Hi '.ucwords(sanitize_text_field($_POST['reqCatForm_firstname'])).' '.ucwords(sanitize_text_field($_POST['reqCatForm_lastname'])).','."<br /><br />".
				'Thank you for requesting AM Conservation Group\'s catalog. You can click on the link below to download your copy.'."<br /><br />".
				'<a href="'.$amc_urls['catalog_link'].'">AM Conservation Group Catalog</a>'."<br /><br />".
				'Happy Viewing!'."<br /><br />".
				'Regards,'."<br />".
				$amc_urls['mail_regards']; // get_bloginfo('name').' Team.';

				$mailHeaders = 'From: '.$amc_urls['mail_from_name'].' <'.get_bloginfo('admin_email').'>' . "\r\n";

				add_filter( 'wp_mail_content_type', 'set_html_content_type' );
				$mailStatus = wp_mail(sanitize_text_field($_POST['reqCatForm_email']), get_bloginfo('name').': Your request for a Catalog', stripslashes($catMsg), $mailHeaders);
				remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

				if($mailStatus) {
					
				} else {
					// 
				}
			}
		} else {
			$user_errors[] = 'Some error happened. Try, again.';
		}
	}
}

if(!empty($success_msg)) {
	wp_redirect( home_url('/catalog-form-confirmation/') ); exit; 	
  }

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}
?>

<div class="row">      
        <div class="col-md-12 aboutbanner"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/catalog-banner.png"></div>  
    </div>

 <!-- Middle Mennu  -->
        <div class="row menu_pos">
        <div class="leftarrow-inner SlideOff"></div>
             <div class="aboutmenu-container">
                 <?php require_once('middle-menu.php'); ?>
             </div>
    </div>
     <!--/  Ends Middle Mennu  -->

<div class="row">
	<div class="rc-container">
		<div class="col-md-6 col-lg-4 col-md-offset-3 col-lg-offset-4 ">
			<div class="rc-form-container">

<?php
if(!empty($success_msg)) {
	/*
	echo '<p>Success.</p>';
	echo '<ol>';
	foreach($success_msg as $success_msg_item) {
		echo '<li>'.$success_msg_item.'</li>';
	}
	echo '</ol>';
	*/
	$show_msg = 'block';
	$success_msg = 'Thank you for requesting our catalog. We have emailed the catalog to your inbox and you can click on the link to download it. You can also login and view our Image Gallery for a list of products.'; // 'Successfully requested.';
  }

if(!empty($form_errors)) {
	/*
	echo '<p>Some errors found.</p>';
	echo '<ol>';
	foreach($form_errors as $form_errors_item) {
		echo '<li>'.$form_errors_item.'</li>';
	}
	echo '</ol>';
	*/
	$show_msg = 'block';
	$error_msg = 'Errors found. Try, again.';
}

if(!empty($user_errors)) {
	/*
	echo '<p>Some errors found.</p>';
	echo '<ol>';
	foreach($user_errors as $user_errors_item) {
		echo '<li>'.$user_errors_item.'</li>';
	}
	echo '</ol>';
	*/
	$show_msg = 'block';
	$error_msg = 'Errors found. Try, again.';
}
?>

<form role="form" method="post" action="" id="reqCatForm" name="reqCatForm" parsley-validate novalidate>
<?php wp_nonce_field('action_reqCatForm', 'name_reqCatForm'); ?>

<?php include('successmodal.php'); ?>

	<input type="text" name="reqCatForm_firstname" class="rc-form-field" placeholder="First Name*" value="<?php echo $uFName; ?>" required parsley-error-message="Please enter your first name" maxlength="40">
	<input type="text" name="reqCatForm_lastname" class="rc-form-field" placeholder="Last Name*" value="<?php echo $uLName; ?>" required parsley-error-message="Please enter your last name " maxlength="40">
	<input type="text" name="reqCatForm_company" class="rc-form-field" placeholder="Company Name*" value="<?php echo $uCompany; ?>" required parsley-error-message="Please enter your company name " maxlength="45">                   
	<input type="email" name="reqCatForm_email" class="rc-form-field" placeholder="Email Address*" value="<?php echo $uEmail; ?>" required parsley-error-message="Please enter a valid email address" maxlength="45">
    <!--<select name="reqCatForm_marketSegment"  class="rc-form-field form-control-select form-control placeholder" required parsley-error-message="Please select an option" parsley-trigger="change" >	-->
	<div class="ips">
	<select name="reqCatForm_marketSegment"  class="rc-form-field form-control-select form-control placeholder reqCatForm_marketSegment" >		
	<option value="">Market Segment</option>
    <?php echo market_segmentopt(); ?>	
	</select>
	</div>
	<!-- <p class="printcatalog">Please enter your postal address to mail a print catalog</p> -->
	<textarea name="reqCatForm_address" class="rc-form-field2" rows="6" placeholder="Address" maxlength="200"></textarea>
	<textarea name="reqCatForm_comments" class="rc-form-field2" rows="6" placeholder="Comments" maxlength="500" ></textarea>

	<div class="checkbox labeltxt"><label><input name="reqCatForm_emailcat" value="yes" type="checkbox"> Email me the catalog</label></div>
	For security purposes, please type in the letters below in the box.
	<br><br>
		<div id="wrap" align="center">
        <img src="<?php echo get_template_directory_uri(); ?>/captcha/get_captcha.php" alt="" id="captcha" />
        
        <br clear="all" />
        <input name="code" type="text" id="code">
    </div>
    <img src="<?php echo get_template_directory_uri(); ?>/captcha/refresh.jpg" width="25" alt="" id="refresh" />
        
    <br clear="all" /><br clear="all" />
    <label>&nbsp;</label>
	<div id="Sends"> </div>
	<div class="mandatory-txt">*Mandatory Fields</div>
	<input type="submit" name="reqCatForm_submit" class="placerequest-btn btn-primary" value="PLACE REQUEST" />
</form>
			</div> 
		</div>

		<div class="col-md-2 divider"></div>

		<!--<div class="col-md-5">
			<div class="rc-formright-container">
			<h5 class="rc-get-txt"> Downloads</h5> 
			<a href="#">
				<div class="rc-drop-shadow">
					<div class=" col-md-1 col-md-offset-1"><a href="<?php echo $amc_urls['catalog_link']; ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pdf-icon_33.png" /></a></div>  
					<div class=" col-md-offset-2 fileformat"><a href="<?php echo $amc_urls['catalog_link']; ?>">Catalog <span class="filesize">(25.1MB)</span></a></div></div>
			</a>
			</div>
		</div>-->
	</div>
</div>

<script type="text/javascript">
var siteURL = '<?php echo $amc_urls['home']; ?>';
var siteAssetURL = '<?php echo get_template_directory_uri(); ?>/assets/';

$(document).ready(function(){
$('.placerequest-btn').click(function() {
		$( '#reqCatForm' ).parsley( 'validate' );
		var codeval = $("#code").val();
		var url = '<?php echo get_template_directory_uri();?>';
        var ajaxStatusVar;
		  
		  
		  $.ajax({
			type: "POST",
			url: url+"/captcha/post.php",
			global: false,
			async: false,
			data: { code: codeval   }
			
			})
			.done(function( data ) {
				if(data.indexOf('fail') > 0) {
					$("#after_submit").html('');
					$("#Sends").after('<label class="error" id="after_submit">Error ! invalid captcha code.</label>');
					ajaxStatusVar = false;
					return false;
			   } else if(data.indexOf('success') > 0){
					$("#after_submit").html('');
					ajaxStatusVar = true;
					return true;
			   }
			});
			if (ajaxStatusVar == false) {return false;}
	});
	
	$('img#refresh').click(function() {  
			
			change_captcha();
	 });
	 
	 function change_captcha()
	 {
		var url = '<?php echo get_template_directory_uri();?>';
	 	document.getElementById('captcha').src=url+"/captcha/get_captcha.php?rnd=" + Math.random();
	 }
});


</script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/mediaquery.js"></script>




<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>