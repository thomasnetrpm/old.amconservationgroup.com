<?php 
/* 
Template Name: Programs services  template 
*/ 
define("CSS", "programsnservices.css");

/*
Changelog

05.23.14
- Copied Links from the "Read More" under each H3 and made each H3 linked to respective page.

*/
global $amc_urls;

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header('shop-amc');
}
?>
 <!-- Title and Breadcrumb  -->
      <div class="row headersession">      
        <div class="col-md-7"><h3 class="headerlabel"><?php the_title(); ?></h3></div>
        <div class="col-md-5 breadcrumbs ">
          <ol class="breadcrumb">
           <?php breadcrumbs_fmg();  ?>
          </ol>
        </div>    
		<?php if(is_mobile()) { ?>
	<div class="col-md-4 back-btn back-page-com"> <a onClick="history.go(-1)"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/back_btn.png">Back </a></div>
<?php } ?>
    </div>
     <!--/  Ends Title and Breadcrumb   -->
    <!--    content area start      -->
  <div class="row">      
          <div class="col-md-12 aboutbanner"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/request a catalog.png"></div>  
      </div> 
      
      

      <div class="row pns-content">      
          
              <div class="col-md-6 pns_left"><div class="row ">  <div class="col-md-11">
                <h3 class="pns-headertxt"><a href="<?php echo $amc_urls['energy_efficiency_education']; ?>" class="readmore_link">Student Efficiency Education Programs </a></h3>
                <p class="pns-paragtxt">AM Conservation Group offers an assortment of comprehensive educational programs for utility companies, program managers, and various government agencies. </p> 
                <a href="<?php echo $amc_urls['energy_efficiency_education']; ?>" class="readmore_link"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/readmore_pic.png"><span class="readmore-txt">read more</span></a> 

                <h3 class="pns-headertxt"><a href="<?php echo $amc_urls['commercial_and_industrial_fulfillment']; ?>" class="readmore_link">Commercial and Industrial Fulfillment</a></h3>
                <p class="pns-paragtxt">AM Conservation Group directs the fulfillment of products and kits for Commercial and Industrial (C&I) programs directly aimed at saving energy, water and money. </p> 
                <a href="<?php echo $amc_urls['commercial_and_industrial_fulfillment']; ?>" class="readmore_link"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/readmore_pic.png"><span class="readmore-txt">read more</span></a> 
				
				<h3 class="pns-headertxt"><a href="commercial-lighting-rebates" class="readmore_link">Commercial Lighting Rebates</a></h3>
                <p class="pns-paragtxt">AM Conservation Group offers a number of commercial lighting products for both indoors and outdoors along with detailed utility rebate information. We work closely with utility companies, program managers and government agencies to offer comprehensive programs to assist with their commercial and industrial lighting needs. </p> 
                <a href="<?php echo $amc_urls['commercial_lighting_rebates']; ?>" class="readmore_link"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/readmore_pic.png"><span class="readmore-txt">read more</span></a> 

                <h3 class="pns-headertxt"> <a href="<?php echo $amc_urls['efficiency_program_management']; ?>" class="readmore_link">Efficiency Program Management</a></h3>
                <p class="pns-paragtxt">AM Conservation Group successfully manages many of the largest efficiency programs in the country.  </p> 
                <a href="<?php echo $amc_urls['efficiency_program_management']; ?>" class="readmore_link"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/readmore_pic.png"><span class="readmore-txt">read more</span></a> 

                <h3 class="pns-headertxt"><a href="<?php echo $amc_urls['direct_mail_fulfillment']; ?>" class="readmore_link">Direct Mail Fulfillment</a></h3>
                <p class="pns-paragtxt">AM Conservation Group manages direct mail fulfillment campaigns for utility companies, program managers and government agencies. </p> 
                <a href="<?php echo $amc_urls['direct_mail_fulfillment']; ?>" class="readmore_link"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/readmore_pic.png"><span class="readmore-txt">read more</span></a> 

                <h3 class="pns-headertxt"><a href="<?php echo $amc_urls['call_center_services']; ?>" class="readmore_link">Call Center Services</a></h3>
                <p class="pns-paragtxt">AM Conservation Group, Inc. provides a full array of customized inbound help-desk, and outbound promotional, or client-satisfaction related call center services for our corporate clients.</p> 
                <a href="<?php echo $amc_urls['call_center_services']; ?>" class="readmore_link"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/readmore_pic.png"><span class="readmore-txt">read more</span></a>                                                 

</div>
<div class="col-md-1 divider"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pns_divider.png"></div>
</div>
              </div>
              
              <div class="col-md-6 pns_right">
                <h3 class="pns-headertxt"><a href="<?php echo $amc_urls['direct_install_fulfillment']; ?>" class="readmore_link">Direct Install Fulfillment</a></h3>
                <p class="pns-paragtxt">AM Conservation Group helps utility companies, program managers, and government agencies effectively manage direct install programs and offers great depth in experience. </p> 
                <a href="<?php echo $amc_urls['direct_install_fulfillment']; ?>" class="readmore_link"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/readmore_pic.png"><span class="readmore-txt">read more</span></a> 

                <h3 class="pns-headertxt"><a href="<?php echo $amc_urls['weatherization_contractor_services']; ?>" class="readmore_link">Weatherization Contractor Services</a></h3>
                <p class="pns-paragtxt">AM Conservation Group assists utility sponsored contractors source weatherization materials with ease, speed and flexibility.  </p> 
                <a href="<?php echo $amc_urls['weatherization_contractor_services']; ?>" class="readmore_link"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/readmore_pic.png"><span class="readmore-txt">read more</span></a> 

                <h3 class="pns-headertxt"><a href="<?php echo $amc_urls['turnkey_program_solutions']; ?>" class="readmore_link">Turnkey Program Solutions</a></h3>
                <p class="pns-paragtxt">AM Conservation Group has years of proven expertise having managed all types and sizes of programs for utility companies, program managers and government agencies throughout country. </p> 
                <a href="<?php echo $amc_urls['turnkey_program_solutions']; ?>" class="readmore_link"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/readmore_pic.png"><span class="readmore-txt">read more</span></a> 

                <h3 class="pns-headertxt"><a href="<?php echo $amc_urls['database_management']; ?>" class="readmore_link">Database Management</a></h3>
                <p class="pns-paragtxt">AM Conservation Group provides a practical and customized web based information management tool that improves workflow, and speeds the access to customer information. </p> 
                <a href="<?php echo $amc_urls['database_management']; ?>" class="readmore_link"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/readmore_pic.png"><span class="readmore-txt">read more</span></a> 

                <h3 class="pns-headertxt"><a href="<?php echo get_bloginfo('url').'/contact-us/'; ?>">Become a Distributor</a></h3>
                <p class="pns-paragtxt">AM Conservation Group offers a vast selection of innovative energy and water conservation products. If you have a desire to become a distributor promoting superior products, please consult with an account representative by calling <span class="phonetxt"> <?php echo $amc_urls['contact_number']; ?></span> or emailing us a <a href="<?php echo get_bloginfo('url').'/contact-us/'; ?>"><b>“Contact Us”</b></a> form. </p> 

              </div>              
          

      </div> 


<script type="text/javascript">
var siteURL = '<?php echo $amc_urls['home']; ?>';
var siteAssetURL = '<?php echo get_template_directory_uri(); ?>/assets/';
</script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/mediaquery.js"></script>

<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>