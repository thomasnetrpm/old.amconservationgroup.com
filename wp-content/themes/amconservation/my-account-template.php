<?php 
/* 
Template Name: My account template 
*/

if(is_mobile()) {
	include('my-account-mobile-template.php');
	exit();
}

define("CSS", "myaccount.css");

redirectUnLoggedUser();

$loggedInUser = getLoggedInUser();
$isSubscribed = isNewsletterSubscribed();

$form_errors = array();

// Handle the newsletter subscription
global $wpdb, $amc_urls;
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	// Handle the account info update
	if(sanitize_text_field($_POST['frmAcctInfo_fname']) != '') {

		if(!is_email(sanitize_text_field($_POST['frmAcctInfo_email']))) {
			$form_errors['status'] = 'error';
			$form_errors['msg'] = 'Please enter valid email address.';
		}

		$updateArray = array(
			'modified' => current_time('mysql'),
			'firstname' => sanitize_text_field($_POST['frmAcctInfo_fname']),
			'lastname' => sanitize_text_field($_POST['frmAcctInfo_lname']),
			'companyname' => sanitize_text_field($_POST['frmAcctInfo_company']),
			'email' => sanitize_text_field($_POST['frmAcctInfo_email']),
		);

/*
Steps to handle the forgot password,
1) If no new password or confirm password is given, don’t change password
2) if any are given, then the other should match
3) if both are matching, then current passowrd must be given
*/
		if(((sanitize_text_field($_POST['frmAcctInfo_currpwd']) != '') || sanitize_text_field($_POST['frmAcctInfo_newpwd']) != '') || (sanitize_text_field($_POST['frmAcctInfo_cnewpwd']) != '')) {
			// Check the validity of current password
			if(!wp_check_password(sanitize_text_field($_POST['frmAcctInfo_currpwd']), $loggedInUser->password)) {
				$form_errors['status'] = 'error';
				$form_errors['msg'] = 'Your current password is incorrect.';
			} else {
				// Check the validity of new password, confirm password
				if((sanitize_text_field($_POST['frmAcctInfo_newpwd']) == '') || (sanitize_text_field($_POST['frmAcctInfo_cnewpwd']) == '')) {
					$form_errors['status'] = 'error';
					$form_errors['msg'] = 'You seem to have entered your Current Password. Please enter New password to reset password.';
				} else {
					if(sanitize_text_field($_POST['frmAcctInfo_newpwd']) != sanitize_text_field($_POST['frmAcctInfo_cnewpwd'])) {
						$form_errors['status'] = 'error';
						// $form_errors['msg'] = 'New password, confirm password doesn\'t match.';
						$form_errors['msg'] = 'Please confirm your password.';
					} else {
						// Update the new password
						$updateArray['password'] = wp_hash_password(sanitize_text_field($_POST['frmAcctInfo_newpwd']));
					}
				}
			}
		}

		if(empty($form_errors)) {
			$updateId = $wpdb->update('wp_customers', $updateArray, array('id' => $loggedInUser->id));
			if($updateId) {
				$form_errors['status'] = 'success';
				// $form_errors['msg'] = 'Updated successfully.';
				$form_errors['msg'] = 'Your account information has been updated successfully.';
			} else {
				$form_errors['status'] = 'error';
				$form_errors['msg'] = 'Some problem occurred. Try, again.';
			}
		}

		echo json_encode($form_errors);
		exit();
	}

	if(sanitize_text_field($_GET['frmNLSubscribe_chkbox']) == 1) {
		// Need to handle the subscribe
	} else if(sanitize_text_field($_GET['frmNLSubscribe_chkbox']) == 0) {
		$wpdb->delete('wp_newsletter', array('email' => $loggedInUser->email));
	}
	exit();
}

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header('');
}
?>

<script type="text/javascript">
function handleEditClk(elm) {
	$('.myaccount_right > *').hide();
	$('.myaccount_right #myaccount-information').show();
	return false;
}
</script>

<div class="row">
	  <div class="myaccount-container">
			<div class="col-md-4 col-lg-3 col-sm-4">
			  <div class="myaccount_left">
				<div class="orangebox">Account Dashboard</div>
				<div class="greybox">Account Information</div>

			  </div>  
			</div> 
			<div class="col-md-8 col-sm-8">
			  <div class="myaccount_right">
				  <div id="myaccount-dashboard">
					<h3 class="mydashboard_txt">My Dashboard</h3>
<br />
<div class="js_result_newsletter alert fade in" style="display:none;">
	<button type="button" class="close js_close_newsletter" aria-hidden="true">×</button>
	<p></p>
</div>
					 <p class="welcomename">Hi <?php echo $loggedInUser->firstname; ?></p>
					 <p class="welcometxt">From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information. Select a link below to view or edit information.</p>
					 <div class="row">
					 <div class="col-md-6 col-sm-6">
						  <div class="ac_info columnHeight">
							  <h4 class="labelheader">Account Information</h4>
							  <a href="#" class="tooltxt js_acctInfo">Edit</a>
							  <div class="cont-txt"><?php echo $loggedInUser->firstname; ?></div>
							  <div class="cont-txt"><?php echo $loggedInUser->email; ?></div>
						  </div>
						  <div class="quote_basket columnHeight">
							  <h4 class="labelheader">Quote Basket</h4>
							  <a href="<?php echo $amc_urls['quote_basket']; ?>" class="tooltxt2">View</a>
							  <div class="cont-txt"><?php echo getQuoteCount(); ?> Quote(s)</div>
						  </div>
					 </div>
					 <div class="col-md-6 col-sm-6">
						  <div class="newsletter columnHeight">
							  <h4 class="labelheader">Newsletters</h4>
							  <div class="checkbox labeltxt">
<form role="form" action="" method="post" name="frmNLSubscribe">
								<label><input type="checkbox" name="frmNLSubscribe_chkbox" <?php echo ($isSubscribed) ? 'checked="checked"' : ''; ?> class="js_subscribe" value="subscribe">General Subscription </label>
</form>
							  </div>
<!--                                   <div class="cont-txt">General Subscription</div> -->
								<p class="js_subscribeResult" style="display:none; font-size:12px;"></p>
						  </div>
						  <div class="img-gallery columnHeight">
							  <h4 class="labelheader">Image Gallery</h4>
							  <a href="<?php echo $amc_urls['image_gallery']; ?>" class="tooltxt2">View</a>
							  <div class="cont-txt">View and download product images</div>
						  </div>
					 </div>
				 
				  </div>  
</div>


				  <div id="myaccount-information">
					  <div class="col-md-6">
						  <h3 class="mydashboard_txt">Account Information</h3>

<br />
<div class="js_result_myaccount alert fade in" style="display:none;">
	<button type="button" class="close js_close_myaccount" aria-hidden="true">×</button>
	<p></p>
</div>

<form role="form" action="" method="post" name="frmAcctInfo" parsley-validate novalidate>

<input type="text" name="frmAcctInfo_fname" value="<?php echo $loggedInUser->firstname; ?>" class="myaccount-form-field" placeholder="First Name*" required parsley-error-message="Please enter your first name">
<input type="text" name="frmAcctInfo_lname" value="<?php echo $loggedInUser->lastname; ?>" class="myaccount-form-field" placeholder="Last Name*" required parsley-error-message="Please enter your last name">
<input type="email" name="frmAcctInfo_email" value="<?php echo $loggedInUser->email; ?>" class="myaccount-form-field" placeholder="Email Address*" required parsley-error-message="Please enter your email">
<input type="text" name="frmAcctInfo_company" value="<?php echo $loggedInUser->companyname; ?>" class="myaccount-form-field" placeholder="Company Name">
<input type="password" name="frmAcctInfo_currpwd" class="myaccount-form-field" placeholder="Current Password">
<input type="password" name="frmAcctInfo_newpwd" class="myaccount-form-field" placeholder="New Password">
<input type="password" name="frmAcctInfo_cnewpwd" class="myaccount-form-field" placeholder="Confirm New Password">
<input type="submit" name="frmAcctInfo_submit" class="save-btn btn-primary" value="SAVE" />

</form>
						  <div class="mandatory-txt">*Mandatory Fields</div>
						  <p class="js_acctUptResult" style="display:none; font-size:12px;"></p>
					  </div>     

				  </div>


			  </div> 
			</div> 
	  </div>       
</div>
<!-- Modal -->
<div class="modal fade" id="unSubscriptionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Newsletters</h4>
      </div>
      <div class="modal-body">
		  <p>Are you sure you want to unsubscribe?</p>
      </div>
      <div class="modal-footer">
		  <button type="button" class="btn btn-primary" onclick="nlUnSubscription();">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="nlUnSubscriptionCancel();">No</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
$('input[type=checkbox][name=frmNLSubscribe_chkbox].js_subscribe').on('click', function() {
	if($('input[name=frmNLSubscribe_chkbox].js_subscribe:checked').length == 1) {
		// Handling the ajaxified save of subscribe email
		jQuery.ajax({
			url: '<?php echo site_url('/wp-content/plugins/newsletter/do/subscribe.php'); ?>',
			type: "post",
			data: { ne: '<?php echo $loggedInUser->email; ?>' },
			success: function(d) {
				// $('p.js_subscribeResult').html('&#10004; Subscribed successfully').css('color', 'green').show().fadeOut(2000);
				$('.js_result_newsletter').removeClass('alert-danger').addClass('alert-warning').find('p').text('You have successfully subscribed to the newsletter.');
				$('.js_result_newsletter').show();
			}
		});
	} else {
		//$('.js_subscribe').attr('checked','checked');
		
		$('#unSubscriptionModal').modal('show');
	}
});

// Handle the account edit form
$('form[name=frmAcctInfo]').on('submit', function() {
	jQuery.ajax({
		url: '<?php echo $amc_urls['my_account']; ?>',
		type: "post",
		data: $('form[name=frmAcctInfo]').serialize(),
		success: function(d) {
			var o = $.parseJSON(d);
			if(o.status == 'error') {
				// $('p.js_acctUptResult').html('&#10005; '+o.msg).css('color', 'red').show().fadeOut(3500);
				$('.js_result_myaccount').removeClass('alert-warning').addClass('alert-danger').find('p').text(o.msg);
				$('.js_result_myaccount').show();
			} else if(o.status == 'success') {
				// $('p.js_acctUptResult').html('&#10004; '+o.msg).css('color', 'green').show().fadeOut(3500);
				$('.js_result_myaccount').removeClass('alert-danger').addClass('alert-warning').find('p').text(o.msg);
				$('.js_result_myaccount').show();
			}
		}
	});
	return false;
});

$('button.js_close_myaccount').click(function() {
	$('.js_result_myaccount').hide();
});

$('button.js_close_newsletter').click(function() {
	$('.js_result_newsletter').hide();
});

$('.js_acctInfo').on('click', function() {
	$('.myaccount_left div.greybox').trigger('click');
	return false;
});

$(".myaccount_left > *").on( "click", function() {
	$(".myaccount_left > *").attr("class","greybox");
	$( this ).attr("class","orangebox");
	$(".myaccount_right > *").hide();
	$($(".myaccount_right > *")[$( this ).index()]).show();
});
/* to make the parallax box same height */

function setHeight(pxBtmBox) {
    var maxHeight = 0;
    //Get all the element with class = col
    column = $(pxBtmBox);
    column.css('height', 'auto');
    //Loop all the column
    column.each(function () {
        //Store the highest value
        if ($(this).height() > maxHeight) {
            maxHeight = $(this).height();

        }
    });
    //Set the height
    column.height(maxHeight);
}

$(window).load(function () {
   
        setHeight('.columnHeight');

   

});
$(window).resize(function () {
   
        setHeight('.columnHeight');

   

});
// Function to unsubscribe the newsletter
function nlUnSubscription(){
	$('#unSubscriptionModal').modal('hide');
	$.ajax({
		url: '<?php echo $amc_urls['my_account']; ?>',
		data: {frmNLSubscribe_chkbox: $('input[name=frmNLSubscribe_chkbox].js_subscribe:checked').length},
		success: function(d) {
			// $('p.js_subscribeResult').html('&#10004; Unsubscribed successfully').css('color', 'green').show().fadeOut(2000);
			$('.js_result_newsletter').removeClass('alert-danger').addClass('alert-warning').find('p').text('Unsubscribed successfully');
			$('.js_result_newsletter').show();
		}
	});
}
// Function to cancel the newsletter unsubscription
function nlUnSubscriptionCancel(){
	$('input[name=frmNLSubscribe_chkbox].js_subscribe').prop('checked',true);
	$('.js_result_newsletter').hide();
}
</script>

<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>