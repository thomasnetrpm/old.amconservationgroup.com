<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop, $woocommerce,$FilterSet;
$remove_items = array("pa_personalizable","pa_price-per","pa_price-per","pa_zoomable","pa_units-per-case","pa_energy-dollars-saved","pa_energy-star");
$prodDetails = $product;
$prodAttributes = $product->get_attributes();
$prodVariationsIds = $product->get_children();
  $term       = get_queried_object();
  $FilterKey = $term->slug;

$prodVariations = array();
$attribute_list = array();
$attribute_list_value = array();
 // Check if the Left Column in Defined Or not // 
  $LeftColumnValue = $FilterSet[$FilterKey];
  $S_LeftColumnValue = $LeftColumnValue;



foreach($prodVariationsIds as $prodVariationsIdsItem) {

	$prodVariations[] = get_product($prodVariationsIdsItem);
	$variationProdDetails = new WC_Product_Variation($prodVariationsIdsItem);
	$variationProdDetails = $variationProdDetails->variation_data;
	
	 foreach( $variationProdDetails as $key=>$prodAttributesItem)
         {
          $key = str_replace("attribute_","", $key);
           if(!in_array($key, $remove_items) && !empty($prodAttributesItem)) {
                    $attribute_key                  =  $key;
                    $attribute_list_value[$prodVariationsIdsItem][$attribute_key] =  $prodAttributesItem;
                    if($attribute_list_value[$prodVariationsIdsItem][$attribute_key]){
                    	$attribute_list[$attribute_key] =  $prodAttributesItem;
                    }
                } 
         }	
}

//print_r($attribute_list);

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibility
if ( ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] )
	$classes[] = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
	$classes[] = 'last';
?>

<div class="col-md-2">
  
          <?php
		  $prodSideImg = get_the_post_thumbnail($product->post->ID, array(200, 200), array('class'	=> "productcode"));
		  if(empty($prodSideImg)) {
			echo '<img class="productcode" src="'.get_template_directory_uri().'/assets/images/no_image.png" alt="" witdh="140" height="120" />';
		  } else {
			echo $prodSideImg;
		  }
		  ?>
 
         <p class="pl-productname"> <?php the_title(); ?></p>
</div>
 <?php
   // Variation Container Starts From  Here

  if(count($prodVariationsIds) > 0) {
    $i =0; 
    foreach($prodAttributes as $prodAttributesItem) {
    if($prodAttributesItem['is_visible'] == 1)	{
      if($i < 4)
      	 {
           
      	 	$attribute_set[] = $prodAttributesItem['name'];
      	 }
     
     $i++;
       }
    }
    // get The Attributes Set value //
          //$sample = $prodVariations[0]->get_variation_attributes();
         // print_r($sample);
 
   	foreach($prodVariations as $prodVariationsItem) {
		// echo '<pre>'; print_r($prodVariationsItem); echo '</pre>';
       $thumbnail_image[] = $prodVariationsItem->get_image('shop_thumbnail', array('class' => 'small-productcode'));    
       $sku               = $prodVariationsItem->get_sku(); 
       $attributes        = $prodVariationsItem->get_variation_attributes();

      //  print_r($attributes);
   	} 

    ## Creating Product List And  variation array ##
 
foreach($prodVariations as $prodVariationsItem) {

	$thumbnail_image    = $prodVariationsItem->get_image('shop_thumbnail', array('class' => 'small-productcode')); 
	$sku                = $prodVariationsItem->get_sku();
     
   ?>
   

<?php  
    
    }
    //print_r($LeftColumnValue);

// Ends Variation Container  ?>








				 <div class="col-xs-12 col-sm-12">
                    <div class="leftarrow SlideOff"></div>
						<div class="table-responsive">
<table class="table table-bordered rwdtable">
	<thead>
		<tr>
			<th>Product Options</th>

<?php
//print_r($attribute_list_value);
		if(is_array($LeftColumnValue)) {  
			$ctr = 2;
			foreach($LeftColumnValue as $prodAttributesItem) {
				$key = 'pa_'.$prodAttributesItem;
			    //foreach($attribute_list as $key=>$prodAttributesItem) {
				if($ctr < 5) {
					//$filter_name = ($attribute_list_value[$prodVariationsIdsItem][$key])? $woocommerce->attribute_label($key):'N/A';
					$filter_name   = $woocommerce->attribute_label($key);
					// echo '<div class="product-heading'.$ctr.'">'.$filter_name.'</div>';
					echo '<th>'.$filter_name.'</th>';
					++$ctr;
				}
			}
		}else
		{
			$ctr = 2;
			   foreach($attribute_list as $key=>$prodAttributesItem) {
				if($ctr < 5) {
					$filter_name = ($attribute_list_value[$prodVariationsIdsItem][$key])? $woocommerce->attribute_label($key):' ';
					//$filter_name   = $woocommerce->attribute_label($key);
					// echo '<div class="product-heading'.$ctr.'">'.$filter_name.'</div>';
					echo '<th>'.$filter_name.'</th>';
					++$ctr;
				}
			}

		}
?>
			<th></th>
		</tr>
	</thead>


	<tbody>

<?php
$ctr3 = '';
$ctr4 = 1;
?>
<?php foreach($prodVariations as $prodVariationsItem) { 

// Check to show OR do not show the variation
if(!empty($_GET)) {
	$toShow = true;
	$attrFltrs = $_GET;
	$rangeAttrArr = array('filter_lumens', 'filter_rated-watts', 'filter_overall-length', 'filter_r-value','filter_input-wattage','filter_delivered-lumens');

	foreach($attrFltrs as $attrFltrsKey => $attrFltrsItem) {
		$attrX = $attrFltrsKey;

		// Check whether it is range filter
		if(in_array($attrX, $rangeAttrArr)) {
			$attrX = 'pa_'.str_replace('filter_', '', $attrX);
			$attrValueSetX = $attribute_list_value[$prodVariationsItem->variation_id];

			$rangeValArr = array();
			$attrValueX = $attrFltrsItem;
			$attrValueArrX = explode(',', $attrValueX);
			foreach($attrValueArrX as $attrValueArrItemX) {
				$attrValueArrItemArrX = explode('-',$attrValueArrItemX);
				$rangeValArr[] = (int)$attrValueArrItemArrX[0];
				$rangeValArr[] = (int)$attrValueArrItemArrX[1];
			}

			sort($rangeValArr);
			if(!in_array($attrValueSetX[$attrX], range($rangeValArr[0], $rangeValArr[(count($rangeValArr)-1)]))) {
				$toShow = false;
			}
		} else {
			$attrX = 'pa_'.str_replace('filter_', '', $attrX);
			$attrValueSetX = $attribute_list_value[$prodVariationsItem->variation_id];

			$attrFltrTermSlugArr = array();
			$attrValueX = $attrFltrsItem; // 2075,2699
			$attrValueArrX = explode(',', $attrValueX);
			foreach($attrValueArrX as $attrValueArrItemX) {
				$attrFltrTerm = get_term_by('id', $attrValueArrItemX, $attrX);
				$attrFltrTermSlugArr[] = $attrFltrTerm->slug;
			}

			if(!in_array($attrValueSetX[$attrX], $attrFltrTermSlugArr)) {
				$toShow = false;
			}
		}
	}

	if(!$toShow) {
		$ctr4++;
		continue;
	}
}

// Code to show the alt rows
if(in_array($ctr3, array('', 2, 3))) {
	$ctr3_str = $ctr3;
} else {
	if($ctr3%2 == 1) {
		$ctr3_str = 3;
	} else if($ctr3%2 == 0) {
		$ctr3_str = 2;
	}
}
 
 
  ?>
		<tr class="tablerow">
			<td>
<?php
	$prodVarImage = $prodVariationsItem->get_image(array(87, 74), array('class' => 'small-productcode'));
	if(empty($prodVarImage)) {
		echo '<img class="small-productcode" src="'.get_template_directory_uri().'/assets/images/no_image.png" alt="" witdh="87" height="74" />';
		$prodVarCmpImg = '<img src="'.get_template_directory_uri().'/assets/images/no_image.png" alt="" witdh="67" height="65" />';
	} else {
		echo $prodVarImage;
		$prodVarCmpImg = $prodVariationsItem->get_image(array(67, 65));
	}
?>
		<div class="compare-container">
			<div class="checkbox addtocompare"><label><input type="checkbox" value="<?php echo $prodVariationsItem->variation_id.'_'.$ctr4; ?>" class="js_click_compare"><span class="addlabel">compare</span></label></div>
		</div>
			</td>

<!-- compare box product content -->
<div style="display:none;" id="js_click_compare_content_container_<?php echo $prodVariationsItem->variation_id.'_'.$ctr4; ?>">
	<div id="js_click_compare_content_<?php echo $prodVariationsItem->variation_id.'_'.$ctr4; ?>" class="col-md-3 graybg">
		<span class="smallclose"><a href="#" class="js_remove_cp_con_prdt"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/close-small.png"></a></span>
		<div class="product-img"><?php echo $prodVarCmpImg; ?></div>
		<div class="product-codeid"><span class="model">SKU</span><br><span class="codenum"><?php echo $prodVariationsItem->sku; ?></span></div>
		<input type="hidden" name="prodCmpArray[]" value="<?php echo $prodVariationsItem->variation_id; ?>" />
	</div>
</div>
<?php
   
    $attribute_valueSet = $attribute_list_value[$prodVariationsItem->variation_id];
    if(is_array($attribute_valueSet)) {
    	$Balance = 4 - count($attribute_valueSet);
             if($Balance!= 0)
               {
                for($i=count($attribute_valueSet);$i<4;$i++)
                {

                  array_push($attribute_valueSet," ");
                }

               }

if(is_array($LeftColumnValue)) { 
    	$i=2;
    foreach($LeftColumnValue as $key=>$value)	{
    	if($i<5) {
    		$key = 'pa_'.$value;
    		//echo $key ;
     	echo '<td>'.ucfirst($attribute_valueSet[$key]).'<span style="visibility:hidden;">-</span></td>'; // class="product-content'.$i.'"
     	$i++;
     }
     }
  }
  else
  {
    $i=2;
    foreach($attribute_valueSet as $key=>$value)	{
    	if($i<5) {
    		//echo $key ;
     	echo '<td>'.ucfirst($attribute_valueSet[$key]).'<span style="visibility:hidden;">-</span></td>'; // class="product-content'.$i.'"
     	$i++;
     }
     }

  }

 }

?>

<?php
/*
	$ctr2 = 2;
	foreach($attribute_list_value as $variationAttributesItem) {
		if($ctr2 < 5) {

			$variation_attribute[] = $variationAttributesItem;
			++$ctr2;
		}
	}
    
     $variation_attribute[2] ="Yes";
     $variation_attribute[3] =" ";
     $variation_attribute[4] =" Unit ";
     $variation_attribute[5] =" unit";

     for($i=2;$i<5;$i++)
     {

     	echo '<div class="product-content'.$i.'">'.$variation_attribute[$i].'<span style="visibility:hidden;">-</span></div>';
     }

 unset($variation_attribute);
 */
?>
	<td> 
		<a  data-toggle="modal" data-target=".requestaquote"><button type="button" class="pl-btn btn-primary">Request a Quote</button></a>
		<span class="jsPostId" style="display:none;"><?php echo $prodVariationsItem->variation_id; ?></span>

		<a href="<?php the_permalink($prodVariationsItem->variation_id); echo '?variation_id='.$prodVariationsItem->variation_id; ?>"><button type="button" class="pl-btn btn-primary">View Product Details</button></a>
	</td>
</tr>
<?php
	if(empty($ctr3)) {
		$ctr3 = 2;
	} else {
		++$ctr3;
	}
	++$ctr4;
}
?> 
                  </tbody>
</table>
</div>
</div>
				  <?php } else { ?>

<?php
// Include the simple product template file
include('simple-product-mobile.php');
?>

				  <?php } ?>

          <?php
            // global $wpdb;
            // $wpdb->show_errors();
            // $wpdb->print_error();
          ?>
<style type="text/css">
	
	.small-productcode_thumb {
    height: 142px;
    width: 156px;
}
</style>
                  <div style="clear:both; padding:5px 0px 0px 50px;"></div>