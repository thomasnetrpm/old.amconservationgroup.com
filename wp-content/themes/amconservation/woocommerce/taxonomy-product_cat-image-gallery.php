<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $theme_options, $amc_urls; 
 ?>
<div class="col-md-9 ">
 <div class="row bgfill content">
 <div  id= "one" class="gallery-1 galy">
       

          <div>     
           
            
               
        <?php 
        // Get All the Child of this Category 
         $parent_cat_ID = $cats[0]->term_id;
         $args = array(
                       'hierarchical' => 1,
                       'show_option_none' => '',
                       'hide_empty' => 0,
                       'parent' => $parent_cat_ID,
                       'taxonomy' => 'product_cat'
                    );
                  $subcats = get_categories($args);
          // $term = get_term_by( 'slug', $catid, 'product_cat' );
         foreach($subcats as $SubCategory) {
          $CatId = $SubCategory->slug;
        // include the product image from the Category // 
        $args = array( 'post_type' => 'product', 'posts_per_page' => 1, 'product_cat' => $CatId, 'orderby' => 'menu_order', 'order' => 'asc' );
        $loop = new WP_Query( $args );
        ?>
                  <div class="col-md-4 col-lg-3 col-sm-4 col-xs-6">
                    <div class="image-container"style="margin-bottom:30px;">
                      <div class="gallery1 ">
                      <?php  if (has_post_thumbnail( $loop->post->ID )){ ?>
                      <a href="<?php echo $amc_urls['image_gallery']; ?>?view=<?php echo $CatId; ?>">
                        <div><?php  if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog',array('class'  => "gallery-image-category",)); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="100px" height="100px" class="gallery-image" />'; ?><span class="view-category">VIEW PRODUCT</span> </div>
                        </a>
                      <?php  }else{ ?>
                          <div><?php  if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog',array('class'  => "gallery-image-no-category",)); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="100px" height="100px" class="gallery-without-image" />'; ?> </div>
                      <?php  } ?>
                      </div>
                      <div class="desc">
                        <div class="gallery-content-heading"><?php echo $SubCategory->name ?></div>
                        <!--<div class="download-btn">Download Image</div>-->
                      </div>
                    </div>
                  </div>
          <?php } ?>
                </div>
           
           
         
        </div>
      </div>
    </div>
<script type="text/javascript">
  $('.view-category').hide();
	  $('.gallery1').find('img').attr('width','auto');
	  $('.gallery1').find('img').attr('height','auto')
	 $('.gallery1').mouseenter(function(e) {

			     $(this).find('.view-category').show();
			  	           $(this).find('img').animate({ opacity: 0.3},200); 	   

})
.mouseleave(function() {
$(this).find('img').animate({ opacity: 1},200); 
	 $(this).find('.view-category').hide();
});

$(window).load(function(){
rwdImage();
 setHeight('.desc');
})
$(window).resize(function(){
rwdImage();
setHeight('.desc');
})
function rwdImage(){
var currwidth = $('.gallery1').outerWidth();
$('.gallery1').css('height',currwidth + 'px');
$()
}

 function setHeight(pxBtmBox) {
    var maxHeight = 0;
    //Get all the element with class = col
    column = $(pxBtmBox);
    column.css('height', 'auto');
    //Loop all the column
    column.each(function () {
        //Store the highest value
        if ($(this).height() > maxHeight) {
            maxHeight = $(this).height();
           
        }
    });
    //Set the height
    column.height(maxHeight);
}
</script>

 