<?php
global $wpdb, $amc_urls;

$nameReadOnly = '';
$emailReadOnly = '';

// Check for login user
if (userIsLoggedIn()) {
	$loggedInUser = getLoggedInUser();
	$uName = $loggedInUser->firstname . ' ' . $loggedInUser->lastname;
	$uCompany = $loggedInUser->companyname;
	$uEmail = $loggedInUser->email;
	$uPhone = $loggedInUser->userphone;
	$uExt = $loggedInUser->userext;
	$uCountry = $loggedInUser->usercountry;
	$uRegion = $loggedInUser->userregion;
	$uZip = $loggedInUser->userzip;

	$nameReadOnly = 'readonly="readonly"';
	$emailReadOnly = 'readonly="readonly"';
}

$acctRepOpts = '';
$servicesOpts = '';
$prodNameOpts = '';
$prodSkuOpts = '';
$prodQtyOpts = '';

$acctReps = $wpdb->get_results("SELECT acctrepname FROM wp_acctreps WHERE is_active = 1 Order by acctrepname ASC");

foreach ($acctReps as $acctRepsItem) {
	$acctRepOpts .= '<option value="' . $acctRepsItem->acctrepname . '">' . $acctRepsItem->acctrepname . '</option>';
}

$servicesOpts = $wpdb->get_results("SELECT servicename FROM wp_services WHERE is_active = 1 ");
foreach ($servicesOpts as $servicesOptsItem) {
	$servicesOpts .= '<option value="' . $servicesOptsItem->servicename . '">' . $servicesOptsItem->servicename . '</option>';
}

/*
  $prods = $wpdb->get_results("
  SELECT $wpdb->posts.*, $wpdb->postmeta.*
  FROM $wpdb->posts, $wpdb->postmeta
  WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id
  AND $wpdb->postmeta.meta_key = '_sku'
  AND $wpdb->posts.post_status = 'publish'
  AND $wpdb->posts.post_type = 'product'
  ORDER BY $wpdb->posts.post_title ASC
  ");

  foreach($prods as $prodsItem) {
  $prodNameOpts .= '<option value="'.$prodsItem->ID.'">'.htmlentities($prodsItem->post_title).'</option>';
  $prodSkuOpts .= '<option value="'.$prodsItem->ID.'">'.htmlentities($prodsItem->meta_value).'</option>';
  }
 */

$allProds = $wpdb->get_results('SELECT wp_posts.*, wp_postmeta.meta_value FROM wp_posts
INNER JOIN wp_postmeta ON (wp_posts.ID = wp_postmeta.post_id)

WHERE
(wp_posts.`post_type` = "product" OR wp_posts.`post_type` = "product_variation")
AND wp_postmeta.`meta_key` = "_sku"
AND wp_posts.post_status = "publish"

GROUP BY wp_posts.ID
ORDER BY wp_posts.post_title ASC');

$allVarProdIds = array();
$allVarProds = $wpdb->get_results('SELECT wp_posts.ID FROM wp_posts
INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id)

WHERE
(wp_posts.`post_type` = "product")
AND wp_term_relationships.term_taxonomy_id IN (4)
AND wp_posts.post_status = "publish"

GROUP BY wp_posts.ID
ORDER BY wp_posts.post_title ASC');

foreach ($allVarProds as $allVarProdsItem) {
	$allVarProdIds[] = $allVarProdsItem->ID;
}

foreach ($allProds as $allProdsItem) {
	if (!in_array($allProdsItem->ID, $allVarProdIds)) {
		$actualPostTitle = preg_replace('/^Variation\ \#[0-9]{0,}\ of\ /i', '', $allProdsItem->post_title);
		$actualPostTitle .= ' - ' . strtoupper(esc_html($allProdsItem->meta_value));

		$prodNameOpts .= '<option value="' . $allProdsItem->ID . '">' . esc_html($actualPostTitle) . '</option>';
		$prodSkuOpts .= '<option value="' . $allProdsItem->ID . '">' . strtoupper(esc_html($allProdsItem->meta_value)) . '</option>';
	}
}

for ($c = 1; $c <= 10; $c++) {
	$prodQtyOpts .= '<option value="' . $c . '">' . $c . '</option>';
}

// Get the how did you find us options
$hfuOptions = '';
$hfuResults = $wpdb->get_results('SELECT title, crmcode FROM wp_howfindus WHERE is_active = 1');
foreach ($hfuResults as $hfuResultsItem) {
	$hfuOptions .= '<option value="' . $hfuResultsItem->crmcode . '">' . $hfuResultsItem->title . '</option>';
}

// Handle the form submission
?>
<!-- Request a Quote popup page -->
<div class="modal fade requestaquote" id="myModal requestaquote2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content-rac">
			<div class="modal-header-rac">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<script src="<?php echo get_template_directory_uri(); ?>/assets/js/flexcroll.js"></script>
				<form role="form" action="" method="post" id="reqQuoteFrm" name="reqQuoteFrm" parsley-validate novalidate>
					<div class="form-group-rac">
						<h3 class="form-header-txt">Request a Free Quote Today</h3>
						<p class="form-headercaption-txt">Or call <?php echo $amc_urls['contact_number']; ?> to speak to an Account Representative</p>
						<div class="rac-container">
							<div class="col-md-5 rac-left">
								<input type="text" name="reqQuoteFrm_name" class="rac-form-field" placeholder="Name*" value="<?php echo $uName; ?>" required parsley-error-message="Please enter your name " parsley-validated maxlength="40" <?php echo $nameReadOnly; ?> />
								<input type="text" name="reqQuoteFrm_company" class="rac-form-field" placeholder="Company Name*" value="<?php echo $uCompany; ?>" required parsley-error-message="Please enter your company name " parsley-validated maxlength="45"/>
								<input type="email" name="reqQuoteFrm_email" class="rac-form-field" placeholder="Email Address*" value="<?php echo $uEmail; ?>" required parsley-error-message="Please enter a valid email address" maxlength="100" parsley-trigger="change" parsley-validated <?php echo $emailReadOnly; ?> />
								<div class="ips">
					<!--<select name="reqQuoteFrm_marketSegment"  class="form-control-select form-control placeholder" required parsley-error-message="Please select an option" parsley-trigger="change" > -->
				   <select name="reqQuoteFrm_marketSegment"  class="form-control-select form-control reqQuoteFrm_marketSegment placeholder" required parsley-error-message="Please select Market Segment*" parsley-trigger="change">	
					<option value="">Market Segment *</option>
					<?php echo market_segmentopt(); ?>					
					</select>
					</div>
								<div class="row tel-field"> <div class="tel col-md-7"><input type="tel" name="reqQuoteFrm_phone" class="rac-form-field2 number" placeholder="Phone Number*" value="<?php echo $uPhone; ?>" required parsley-error-message="Please enter your correct phone number " parsley-trigger="change" maxlength="15"/></div>
									<div class="ext col-md-5 number"><input type="text" name="reqQuoteFrm_ext" class="rac-form-field3 number " placeholder="Extension" value="<?php echo $uExt; ?>" maxlength="10"></div></div>
								<p class="shipping-txt">Shipping Address * </p>
								<p class="shipping-parag-txt">to help us get you the best shipping rates possible</p>

								<div class="ips">
									<select name="reqQuoteFrm_country" class="form-control-select" required parsley-error-message="Please select your address " parsley-trigger="change">
										<?php require_once('country-list.php'); ?>
									</select>
								</div>

								<?php
								/*
								  <div class="ips js_statebox_us">
								  <select name="reqQuoteFrm_state_us" class="form-control-select">
								  <option value="">Region, State or Province*</option>
								  <?php require_once('us-region-list.php'); ?>
								  </select>
								  </div>

								  <div class="ips js_statebox_ca" style="display:none;">
								  <select name="reqQuoteFrm_state_ca" class="form-control-select">
								  <option value="">Region, State or Province*</option>
								  <?php require_once('canada-region-list.php'); ?>
								  </select>
								  </div>
								 */
								?>

								<div class="ips js_statebox">
									<select id="reqQuoteFrm_state" name="reqQuoteFrm_state" class="form-control-select" required parsley-error-message="Please select your region*" parsley-trigger="change">
										<option value="">Region, State or Province*</option>
										<?php
										if (isset($uCountry)) {
											if ($uCountry == 'US') {
												include('us-region-list.php');
											} else if ($uCountry == 'CA') {
												include('canada-region-list.php');
											} else {
												include('us-region-list.php');
											}
										} else {
											$uCountry = 'US';
											include('us-region-list.php');
										}
										?>
									</select>
								</div>

								<div class="js_us_states_data" style="display:none;"><option value="">Region, State or Province*</option><?php include('us-region-list.php'); ?></div>
								<div class="js_ca_states_data" style="display:none;"><option value="">Region, State or Province*</option><?php include('canada-region-list.php'); ?></div>

								<script type="text/javascript">
									var stateHTML = '';
									stateHTML = '<div class="ips js_statebox">' + $('.js_statebox').html() + '</div>';
									$('select[name=reqQuoteFrm_country]').on('change', function () {
										var _cntryVal = $(this).val();
										if (_cntryVal == 'US') {
											if ($('select[name=reqQuoteFrm_state]').length == 0) {
												$(this).parent().after(stateHTML);
											}
											$('select[name=reqQuoteFrm_state]').empty('').append($('div.js_us_states_data').html());
										} else if (_cntryVal == 'CA') {
											if ($('select[name=reqQuoteFrm_state]').length == 0) {
												$(this).parent().after(stateHTML);
											}
											$('select[name=reqQuoteFrm_state]').empty('').append($('div.js_ca_states_data').html());
										} else {
											$('form[name=reqQuoteFrm]').parsley('removeItem', '#reqQuoteFrm_state');
											$('div.js_statebox').remove();
										}
									});
									$('select[name=reqQuoteFrm_country]').val('<?php echo $uCountry; ?>');
									$('select[name=reqQuoteFrm_state]').val('<?php echo $uRegion; ?>');</script>

								<input type="text" name="reqQuoteFrm_zipcode" maxlength="7" class="rac-form-field5 number" placeholder="Zip/Postal Code*" value="<?php echo $uZip; ?>" required parsley-error-message="Please enter your zip code" parsley-validated onkeydown="return (event.ctrlKey || event.altKey || (47 < event.keyCode && event.keyCode < 58 && event.shiftKey == false) || (95 < event.keyCode && event.keyCode < 106) || (event.keyCode == 8) || (event.keyCode == 9) || (event.keyCode > 34 && event.keyCode < 40) || (event.keyCode == 46))">

								<div class="ips">
									<select name="reqQuoteFrm_howfind" class="form-control-select" required parsley-error-message="Please select an option" parsley-trigger="change" onclick="checkdata(this)">
										<option value="">How did you find us?*</option>

										<?php echo $hfuOptions; ?>
									</select>
								</div>

								<textarea name="reqQuoteFrm_comments" class="rac-form-field4" rows="8" placeholder="Please provide your shipping address to help us provide you with the best possible shipping rates" maxlength="500"></textarea>

								
								<p class="form-contact-method">If you have ordered from us previously, please include the name of the Account Representative you worked with.</p>
								<input type="text" name="reqQuoteFrm_acctreptxt" class="rac-form-field" placeholder="Account Representatives Name">
								<p class="account-txt">Our Account representatives: </p>
								<div class="account-selectbox ips">
									<select name="reqQuoteFrm_acctrep" class="form-control-select">
										<option value="">Select</option>
										<?php echo $acctRepOpts; ?>
									</select>
								</div>

								<script type="text/javascript">
									$('select[name=reqQuoteFrm_acctrep]').on('click', function () {
										$('input[name=reqQuoteFrm_acctreptxt]').val($(this).val());
									});</script>

								<a class="goto-link" href="<?php echo $amc_urls['contact_us']; ?>">Go to General Contact Form</a>

							</div>
							<div class="col-md-2 split hidden-sm hidden-xs"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/rac-divider.png"></div>
							<div class="col-md-5 rac-right">

								<div class="radio-group2">
									<div class="rac-radio1">
										<label><input type="radio" class="check" name="reqQuoteFrm_prodssers" id="optionsRadios11" value="Products" checked="checked">Products</label>

									</div>
									<div class="rac-radio2">
										<label><input type="radio" class="check" name="reqQuoteFrm_prodssers" id="optionsRadios22" value="Services">Services</label>
									</div>
								</div>

								<div class="products" style="display: block;">
									<div class="ips2">
										<select name="tmpPName" class="form-control-select2 js_prodBox" parsley-error-message="Please enter the product name">
											<option value="">Product Name*</option>
											<?php echo $prodNameOpts; ?>
										</select>
									</div>
									<div class="row">
										<div class="col-md-7">
											<div class="ips3">
												<select name="tmpPNo" class="form-control-select3 js_prodNoBox">
													<option value="">Product No</option>
													<?php echo $prodSkuOpts; ?>
												</select>
											</div>
										</div>
										<div class="col-md-5">
											<div class="ips4">
												<input type="text" name="tmpQty" class="form-control-select4 number" placeholder="Qty" autocomplete="off" onkeydown="return (event.ctrlKey || event.altKey || (47 < event.keyCode && event.keyCode < 58 && event.shiftKey == false) || (95 < event.keyCode && event.keyCode < 106) || (event.keyCode == 8) || (event.keyCode == 9) || (event.keyCode > 34 && event.keyCode < 40) || (event.keyCode == 46))">
											</div>
										</div>
									</div>
									<button type="button" class="addproducts-btn btn-primary js_addProd">ADD PRODUCTS</button>
								</div>

								<div class="services" style="display: none;">
									<div class="ips2">
										<select name="tmpService" class="form-control-select2 js_serviceBox" parsley-error-message="Please enter the services you are interested in">
											<option value="">Service Name*</option>
											<?php echo $servicesOpts; ?>
										</select>
									</div>
									<button type="button" class="addproducts-btn btn-primary js_addService">ADD SERVICES</button>
								</div>
								<div id="notice_message1" style="float: right;"></div>
								<div id="notice_message2" style="float: right;"></div>
								<div class="tabpanel">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#home" data-toggle="tab">Products</a></li>
										<li class=""><a href="#profile" data-toggle="tab">Services</a></li>
									</ul>

									<!-- Tab panes -->
									<div class="tab-content">
										<div class="tab-pane active" id="home"><ul class="producttab js_prodBasket"></ul></div>
										<div class="tab-pane" id="profile"><ul class="servicetab js_serviceBasket"></ul></div>
									</div>

								</div>
								<div class="col-md-12 security">
									For security purposes, please
									type in the letters below in the box.
								</div>
								<script type="text/javascript">
									$(".producttab").addClass("flexcroll");
									$(".servicetab").addClass("flexcroll");
									$('.check').on('click', function () {
										var valueAttribute = $(this).attr('value');
										// Clear the  notice Message //
										// $('p.jsQtyError').remove();

										if (valueAttribute == 'Products') {
											$('.nav-tabs li:nth-child(1)').addClass('active');
											$('.nav-tabs li:nth-child(2)').removeClass('active');
											$('.tab-content #home').addClass('active');
											$('.tab-content #profile').removeClass('active');
											$("#notice_message1").show();
											$("#notice_message2").hide();
										} else {
											$('.nav-tabs li:nth-child(2)').addClass('active');
											$('.nav-tabs li:nth-child(1)').removeClass('active');
											$('.tab-content #home').removeClass('active');
											$('.tab-content #profile').addClass('active');
											$("#notice_message1").hide();
											$("#notice_message2").show();
										}
									})

									$('.tabpanel .nav-tabs li').on('click', function () {
										var currIndex = $(this).index();
										if (currIndex == 0) {
											$("#optionsRadios11").click();
										} else {
											$("#optionsRadios22").click();
										}

									})
									$('#requestaquote2').on('hidden', function () {
										$(this).removeData('modal');
									});</script>

								<div id="wrap" class="quotelisting" align="center">
									<img src="<?php echo get_template_directory_uri(); ?>/captcha/get_captcha.php" alt="" id="captcha" />

									<br clear="all" />
									<input name="code" type="text" id="code">
								</div>
								<img src="<?php echo get_template_directory_uri(); ?>/captcha/refresh.jpg" width="25" alt="" id="refresh" />

								<br clear="all" /><br clear="all" />
								<label>&nbsp;</label>
								<div id="Sends"> </div>

								<input type="submit" name="reqQuoteFrm_submit" class="addproducts-btn btn-primary" value="GET QUOTE" />
								<div class="mandatory-txt">*Mandatory Fields</div>
								<div class="alert js_result" style="display:none;">
									<button type="button" class="close" aria-hidden="true">×</button>
									<div></div>
								</div>

							</div>
						</div>
					</div>

				</form>

				<script type="text/javascript">

					/**
					 * HubSpot
					 * @author @_butifarra_
					 * Basic map for this one form.
					 * Can be extended to handle multiple maps if necessary
					 * @returns {Array}
					 */
					function getHubSpotMap() {
						var map = [
							// original form field names
							[
								'reqQuoteFrm_name',
								'reqQuoteFrm_company',
								'reqQuoteFrm_email',
								'reqQuoteFrm_phone',
								'reqQuoteFrm_ext',
								'reqQuoteFrm_country',
								'reqQuoteFrm_state',
								'reqQuoteFrm_zipcode',
								'reqQuoteFrm_howfind',
								'reqQuoteFrm_comments',
								'reqQuoteFrm_prefcont',
								'reqQuoteFrm_acctreptxt',
								'reqQuoteFrm_acctrep',
								'reqQuoteFrm_prodssers',
								'reqQuoteFrm_prodNameList[]',
								'reqQuoteFrm_prodNoList[]',
								'reqQuoteFrm_prodQtyList[]',
								'reqQuoteFrm_serviceList'
							],
							// corresponding HubSpot form field names
							[
								'name',
								'company',
								'email',
								'phone',
								'extension',
								'country',
								'state',
								'zip',
								'how_did_you_find_us_',
								'pleas_provide_your_shipping_address_to_help_us_provide',
								'preferred_contact_method',
								'account_representative_name',
								'our_account_representatives',
								'producs_or_services',
								'product_name',
								'product_number',
								'qty',
								'services'
							]
						];
						return map;
					}

					/**
					 * HubSpot
					 * @author @_butifarra_
					 * Expects jQuery serializedArray form data. Translates the
					 * form fields into HubSpot fields and submits the data to
					 * HubSpot.
					 *
					 * @param {type} formData
					 * @returns {void}
					 */
					function hubSpotFormHandler(formData) {
						var formActionUrl = 'https://forms.hubspot.com/uploads/form/v2/';
						var formPortalId = '448562';
						var formGuid = 'd96297bc-fd68-4530-a96f-c5c8c455c6cd';
						var actionUrl = formActionUrl + formPortalId + '/' + formGuid;

						// map fields and return Ajax ready object
						var mappedData = arrayToObject(hubSpotFormFieldMapper(formData, getHubSpotMap()));

						// add first and last name fields for HubSpot form
						mappedData['firstname'] = '';
						mappedData['lastname'] = '';

						// try to split name into first and last as long as firstname
						var splitName = stringSplitter(mappedData.name || '');
						switch (splitName.length) {
							case 1:
								mappedData['firstname'] = splitName[0];
								mappedData['lastname'] = '';
								break;
							case 2:
								mappedData['firstname'] = splitName[0];
								mappedData['lastname'] = splitName[1];
								break;
							case 3:
								mappedData['firstname'] = splitName[0] + ' ' + splitName[1];
								mappedData['lastname'] = splitName[2];
								break;
							default:
								mappedData['firstname'] = mappedData.name;
								mappedData['lastname'] = mappedData.name;
						}

						hubSpotAjaxFormSubmitter(actionUrl, mappedData);
					}

					function hubSpotAjaxFormSubmitter(actionUrl, data) {
						// setup ajax options
						var options = {
							method: 'post',
							url: actionUrl,
							data: data,
							success: function () {
//								if (console) {
//									console.debug(arguments);
//								}
							},
							error: function () {
//								if (console) {
//									console.error(arguments);
//								}
							}
						};
						// make Ajax call when data exists
						if (data) {
							$.ajax(options);
						}
					}

					/**
					 * HubSpot
					 * @author @_butifarra_
					 * Maps old form field names to new HubSpot form fields.
					 * Expects jQuery serializedArray form data and a map.
					 *
					 * @param {Array} formData
					 * @param {Array} map
					 * @returns {Object|Array}
					 */
					function hubSpotFormFieldMapper(formData, map) {
						// makes sure data passed is an array
						if ($.isArray(formData)) {
							$.each(formData, function (idx, item) {
								// looks for item.name in first map
								var i = jQuery.inArray(item.name, map[0]);
								// when found renames field using second map
								// or oldname on array size mistmatch.
								if (i > -1) {
									item.name = map[1][i] || item.name;
								}
							});
							// returns Ajax-ready object
							return formData;
						}
						// returns false when first argument is not array
						return false;
					}

					/**
					 * Utility
					 * @author @_butifarra_
					 * Splits a string into an array words delimited by passed
					 * parameter or space.
					 *
					 * @param {type} data
					 * @param {type} delimiter
					 * @returns {Array}
					 */
					function stringSplitter(data, delimiter) {
						var array = data.split(delimiter || ' ');
						return array;
					}

					/**
					 * Utility
					 * @author @_butifarra_
					 * Parses an array of objects into an object of name/value pairs
					 * @param {Array} a
					 * @returns {Object} json
					 */
					function arrayToObject(a) {
						var o = {};
						$.each(a, function () {
							// if exists
							if (o[this.name]) {
								// if it is not an array...
								if (!o[this.name].push) {
									// convert its current value to an array
									o[this.name] = [o[this.name]];
								}
								// push new value into it
								o[this.name].push(this.value || '');
							} else {
								o[this.name] = this.value || '';
							}
						});
						return o;
					}

					function checkdata(marked) {
						var marked_value = marked.value;
						if (marked_value != "")
							$(this).parsley('addConstraint', {required: true});
					}

					$(document).ready(function () {

						$('img#refresh').click(function () {
							change_captcha();
						});
						function change_captcha()
						{
							var url = '<?php echo get_template_directory_uri(); ?>';
							document.getElementById('captcha').src = url + "/captcha/get_captcha.php?rnd=" + Math.random();
						}

						$('form[name=reqQuoteFrm]').submit(function () {


							var codeval = $("#code").val();
							var url = '<?php echo get_template_directory_uri(); ?>';
							var ajaxStatusVar;
							$.ajax({
								type: "POST",
								url: url + "/captcha/post.php",
								global: false,
								async: false,
								data: {code: codeval}
							}).done(function (data) {
								if (data.indexOf('fail') > 0) {
									$("#after_submit").html('');
									$("#Sends").after('<label class="error" id="after_submit">Error ! invalid captcha code.</label>');
									ajaxStatusVar = false;
									return false;
								} else if (data.indexOf('success') > 0) {
									$("#after_submit").html('');
									ajaxStatusVar = true;
									return true;
								}
							});
							if (ajaxStatusVar == false) {
								return false;
							}

							// Show message if NO product, service are selected
							if (($('ul.js_prodBasket li').length == 0) && ($('ul.js_serviceBasket li').length == 0)) {
								$('div.js_result').addClass('alert-danger');
								$('div.js_result div').html('Please ADD some products or services.');
								$('div.js_result').show();
								return false;
							}

							$('div.js_result').hide();
							if ($('form[name=reqQuoteFrm]').parsley('isValid')) {
								$('input[type=submit][name=reqQuoteFrm_submit]').attr('disabled', true);
								// HubSpot interceptor
								hubSpotFormHandler($('form[name=reqQuoteFrm]').serializeArray());
								// Default form handling
								var message = "Thank you for requesting a quote. We have sent you an email confirmation";
								$.ajax({
									url: '<?php echo $amc_urls['request_a_quote']; ?>',
									method: 'post',
									data: $('form[name=reqQuoteFrm]').serialize(),
									success: function (d) {
										var res = jQuery.parseJSON(d);
										$('input[type=submit][name=reqQuoteFrm_submit]').attr('disabled', false);
										if (res.status == 'success') {
											$('div.js_result').removeClass('alert-danger').addClass('alert-warning');
											$('ul.js_prodBasket li').remove();
											$('ul.js_serviceBasket li').remove();
											$('input[name=reqQuoteFrm_acctreptxt]').val('');
											$('textarea[name=reqQuoteFrm_comments]').val('');
											$('input[name=tmpQty]').val('');
											$('img#refresh').click();
											// redirect to thank you page
											window.location = '<?php echo home_url('/rfq-form-confirmation/'); ?>';
										} else { // res.status == 'error'
											message = "There was an error processing your request. Please try it again.";
											$('div.js_result').removeClass('alert-warning').addClass('alert-danger');
											$('div.js_result div').html(message);
											$('div.js_result').show();
										}
									}
								});
							}
							return false;
						});
					});
					$('.js_result').find('button.close').on('click', function () {
						$('div.js_result').hide();
					});
					$('select[name=tmpPName]').on('change keypress', function () {
						var _prodId = $(this).val();
						if (_prodId != '' && _prodId > 0) {
							$('select[name=tmpPNo]').val(_prodId);
						}
					});
					$('select[name=tmpPNo]').on('change keypress', function () {
						var _prodId = $(this).val();
						if (_prodId != '' && _prodId > 0) {
							$('select[name=tmpPName]').val(_prodId);
						}
					});
					$('button.js_addProd').on('click', function () {
						if ($('input[name=tmpQty]').val() == '') {
							$('span p.jsQtyError').remove();
							$(this).before('<span><p class="jsQtyError" style="margin: 5px 15px 0px 0px; text-align: right; font-size: 12px; color: red;">Please enter the quantity.</p></span>');
							return false;
						} else {
							if (!$.isNumeric($('input[name=tmpQty]').val()) || ($('input[name=tmpQty]').val() == 0)) {
								$('span p.jsQtyError').remove();
								$(this).before('<span><p class="jsQtyError" style="margin: 5px 15px 0px 0px; text-align: right; font-size: 12px; color: red;">Please enter a valid quantity.</p></span>');
								return false;
							} else {
								$('span p.jsQtyError').remove();
								// check if  its already have any values //
								if (($('ul.js_prodBasket li').length == 0)) { //&& ($('ul.js_serviceBasket li').length == 1)
									$("#notice_message1").html('<p class="jsQtyError" style=" text-align: right; font-size: 12px; color: red;"> Click box above to add additonal products</p>');
								}
								else
								{
									$('#notice_message1 > p.jsQtyError').remove();
								}
							}
						}

						var _oddEven = 'odd';
						if ($('ul.js_prodBasket li').length % 2 == 0) {
							_oddEven = 'even';
						}
						if ($('select[name=tmpPName]').val() != '') {
							//@to-do: handle the repeated addition of same product

							var _prodHtml = '<li class="product-' + _oddEven + '"><span class="bigclose2"><a href="#" class="js_ProdRemove" onclick="$(this).parent().parent().remove(); return false;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/close-big2.png"></a></span><div>'
									+ $('select[name=tmpPName] option:selected').text()
									+ '</div><div>' +
									$('select[name=tmpPNo] option:selected').text()
									+ ' | QTY - ' +
									parseInt($('input[name=tmpQty]').val())
									+ '</div>'

									+ '<input type="hidden" name="reqQuoteFrm_prodNameList[]" value=\'' + $('select[name=tmpPName] option:selected').text() + '\' />'
									+ '<input type="hidden" name="reqQuoteFrm_prodNoList[]" value=\'' + $('select[name=tmpPNo] option:selected').text() + '\' />'
									+ '<input type="hidden" name="reqQuoteFrm_prodQtyList[]" value="' + parseInt($('input[name=tmpQty]').val()) + '" />'

									+ '</li>';
							$(_prodHtml).appendTo('ul.js_prodBasket');
						}
					});
					$('button.js_addService').on('click', function () {
						var _oddEven = 'odd';
						if ($('ul.js_serviceBasket li').length % 2 == 0) {
							_oddEven = 'even';
						}
						if ($('select[name=tmpService]').val() != '') {
							//@to-do: handle the repeated addition of same service

							var _serviceHtml = '<li class="service-' + _oddEven + '">'

									+ '<input type="hidden" name="reqQuoteFrm_serviceList[]" value="' + $('select[name=tmpService] option:selected').text() + '" />'

									+ '<span class="servicelist">'
									+ $('select[name=tmpService]').val()
									+ '</span><span class="bigclose2"><a href="#" class="js_ServiceRemove" onclick="$(this).parent().parent().remove(); return false;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/close-big2.png"></a></span>' +
									+'</li>';
							$(_serviceHtml).appendTo('ul.js_serviceBasket');
							if (($('ul.js_serviceBasket li').length == 1)) { //&& ($('ul.js_serviceBasket li').length == 1)
								$("#notice_message2").html('<p class="jsQtyError" style=" text-align: right; font-size: 12px; color: red;"> Click box above to add additonal services </p>');
							}
							else
							{
								$('#notice_message2 > p.jsQtyError').remove();
							}

						}
					});
					function number(e) {
						var theEvent = e || window.event;
						var key = theEvent.keyCode || theEvent.which;
						if (key != 13 && key != 9 && key != 8 && key != 46) {//allow enter and tab
							key = String.fromCharCode(key);
							var regex = /[0-9]|\./;
							if (!regex.test(key)) {
								theEvent.returnValue = false;
								if (theEvent.preventDefault)
									theEvent.preventDefault();
							}
						}
					}

					$("input").filter(".number,.NUMBER").on({
						"focus": function (e) {

							$(e.target).data('oldValue', $(e.target).val());
						},
						"keypress": function (e) {
							e.target.oldvalue = e.target.value;
							number(e);
						},
					});
					// Pre-select the product name, sku
					$('a[data-target=".requestaquote"], button[data-target=".requestaquote"]').on('click', function () {
						var preSelectProdId = $(this).next('span.jsPostId').text();
						if (preSelectProdId != '') {
							$('select[name=tmpPName]').val(preSelectProdId);
							$('select[name=tmpPNo]').val(preSelectProdId);
						}
						$('div.js_result').hide();
					});
				</script>

			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->