<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if(is_search()) {
	include('archive-product.php');
	exit();
}

  // Here to add a check if am in the second level  Catehory or Not // 
   $term 			= get_queried_object();
   $parent_id 		= empty( $term->parent ) ? 0 : $term->parent;


   if($parent_id > 0) {  woocommerce_get_template( 'taxonomy-second-level.php' );  exit; }

define("CSS", "category_landing.css");

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header('shop-amc');
}
?>
	<?php
		/**
		 * woocommerce_before_main_content hook
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		 
		do_action('woocommerce_before_main_content');
		$metafieldArray = get_option('taxonomy_'. $term->term_id);
		$metafieldoutput = $metafieldArray['cat_product_dec_meta'];
	?>

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
	 <!-- Header Block for the  Category Landing Page -->
      <div class="row">
        <div class="content-top-container month_sale">
        <div > <p class="subheader-content-heading"> <?php woocommerce_page_title(); ?></div>
        <div> <p class="subheader-content"> 
         <?php  echo $term->description; ?>
		</p>
		</div>
		<div class="energy_btn">
		<?php if($metafieldArray['btn_name_category1'] != '') { ?>
		<a href="<?php echo !empty($metafieldArray['cat_product_url1']) ? $metafieldArray['cat_product_url1'] : "#"; ?>" class="compare-btn btn-primary sample_btn" style="text-align:center;padding-top:10px"><?php echo $metafieldArray['btn_name_category1'];?></a>
		<?php } if($metafieldArray['btn_name_category2'] != '') { ?>
		<a href="<?php echo !empty($metafieldArray['cat_product_url2']) ? $metafieldArray['cat_product_url2'] : "#"; ?>" class="compare-btn btn-primary sample_btn" style="text-align:center;padding-top:10px"><?php echo $metafieldArray['btn_name_category2'];?> </a>
		<?php } ?>
		</div>
        </div> 
        </div>
    <!--  /Header Block for the  Category Landing Page -->
		<?php endif; ?>


		<?php if ( have_posts() ) : ?>

			<?php
				/**
				 * woocommerce_before_shop_loop hook
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 * Add something here to shows the navigation on the Top 
				 */
				//do_action( 'woocommerce_before_shop_loop' );
				
			?>
<div class="row bgfill content">
	<div class="  topspacer">
		
			 
			
				<?php woocommerce_product_subcategories(); ?>
			
			
		
	</div>
</div>
			<?php
				/**
				 * woocommerce_after_shop_loop hook
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

	          <h2 align="center">  There are  no products in  this category. </h2>	
	          <p></p>	
      
			<?php //woocommerce_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>
       

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action('woocommerce_after_main_content');
	?>
	<script>
	  //flag = ".gallery-image";
	  $('.view-category').hide();
	  $('.gallery1').find('img').attr('width','');
	  $('.gallery1').find('img').attr('height','')
	 $('.gallery1').mouseenter(function(e) {

			     $(this).find('.view-category').show();
			  	           $(this).find('img').animate({ opacity: 0.3},200); 	   

})
.mouseleave(function() {
$(this).find('img').animate({ opacity: 1},200); 
	 $(this).find('.view-category').hide();
});

$(window).load(function(){
rwdImage();
 setHeight('.desc');
})
$(window).resize(function(){
rwdImage();
setHeight('.desc');
})
function rwdImage(){
var currwidth = $('.gallery1').outerWidth();
$('.gallery1').css('height',currwidth + 'px');
$()
}

 function setHeight(pxBtmBox) {
    var maxHeight = 0;
    //Get all the element with class = col
    column = $(pxBtmBox);
    column.css('height', 'auto');
    //Loop all the column
    column.each(function () {
        //Store the highest value
        if ($(this).height() > maxHeight) {
            maxHeight = $(this).height();
           
        }
    });
    //Set the height
    column.height(maxHeight);
}
  

	  </script>

	<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action('woocommerce_sidebar');
	?>
  

<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>