<?php
/**
 * Related Products
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;

$related = $product->get_related();

if ( sizeof( $related ) == 0 ) return;

$args = apply_filters('woocommerce_related_products_args', array(
	'post_type'				=> 'product',
	'ignore_sticky_posts'	=> 1,
	'no_found_rows' 		=> 1,
	'posts_per_page' 		=> $posts_per_page,
	'orderby' 				=> $orderby,
	'post__in' 				=> $related,
	'post__not_in'			=> array($product->id)
) );

$products = new WP_Query( $args );
$columns = 0;
$woocommerce_loop['columns'] 	= $columns;

if ( $products->have_posts() ) : ?>

	
		<?php  woocommerce_product_loop_start();
        
		 ?>

			<?php while ( $products->have_posts() ) : $products->the_post();
			if($woocommerce_loop['columns'] < 4) {
             if ( has_post_thumbnail() ) {
				$image  = get_the_post_thumbnail( $post->ID, apply_filters('single_product_large_thumbnail_size zoom-image'), array('title' => $image_title,"class"=>"related-gallery-image", 'alt' => get_the_title($post->ID)) );
              
             }
  
              
			 ?>

	      <li class="col-md-3 col-xs-6"> 
			<div class="prd-det-imgwr">
			  <a href="<?php echo the_permalink($post->ID); ?>">  
			  <div class="pd-gallery "><div>
			  <?php echo $image ; ?>
			  </div>
			  </div>
			  </a>
			  <div class="desc">
			  <div class="gallery-content-heading"><?php the_title(); ?></div>
			  </div> 
			 </div>
          </li>
			
	<?php 
  
   $woocommerce_loop['columns'] ++;
   }
	endwhile; // end of the loop. ?>

		<?php woocommerce_product_loop_end(); ?>

	

<?php endif;

wp_reset_postdata();
