<?php // Show the simple product - Product with NO variations ?>

<?php
global $product, $woocommerce_loop, $woocommerce,$post,$FilterSet;
$prodDetails = $product;
$Attr_Keys = array();
$term       = get_queried_object();

			$newPostId = $post->ID;
	        $prodDetails = get_product($newPostId);
            $prodAttributes = $prodDetails->get_attributes();
              $i = 0;
              if(is_array($prodAttributes))  { 
              foreach($prodAttributes as $prodAttributesItem) {
                $attribute_values = false;
                 if(!in_array($prodAttributesItem['name'], $remove_items))
                 {
                        $attribute_key    =  $prodAttributesItem['name'];
                        $attribute_values =  array_shift(woocommerce_get_product_terms($newPostId, $prodAttributesItem['name'], 'names')); 
                       if($attribute_values) {
                       	$Attr_Keys[]  	  = $attribute_key;
                        $attribute_list[$attribute_key] = $attribute_values ;
                        
                      } 
                   $i++;
                 }
                 
              }
              } 
              //print_r($attribute_list);
             // if(count($Attr_Keys) > 0 ) {
             $Balance = 4 - count($Attr_Keys);
             if($Balance!= 0)
               {
                for($i=count($Attr_Keys);$i<4;$i++)
                {

                  array_push($Attr_Keys,"n/a");
                }

               }  
            //   }  
?>

<div class="col-md-9">
	<div class="prd-listing">
		<div class="leftarrow"></div>
<div class="table-responsive">
		<table class="table table-prddisplay" data-val="<?php echo count($S_LeftColumnValue);?>">
                            		<thead>
                              			<tr>                                           
                                   		  <th> Product Options</th>
<?php   
		//print_r($S_LeftColumnValue);
		//echo count($LeftColumnValue)
		if(count($S_LeftColumnValue) == 0){
			echo '<th></th>';
		}
		if(is_array($S_LeftColumnValue)) {  
			$ctr = 2;
			foreach($S_LeftColumnValue as $prodAttributesItem) {
				if($ctr < 6) {
					$filter_name   = $prodAttributesItem["Label"];
					echo '<th>'.$filter_name.'</th>';
					++$ctr;
				}
		}
	}

		
?>
	<th></th>
      </tr>
       </thead>
         <tbody>
             <tr>
               <td>

<?php
$sim_ctr3 = '';
$sim_ctr4 = 1;
?>
	<!--<div class="product-content-container clearfix">--> <!-- product-content-container<?php echo $sim_ctr3; ?> product-content-container<?php echo $sim_ctr4; ?> -->
		<!--<div class="product-content1">-->
		<?php // echo $prodDetails->get_image(array(87, 74), array('class' => 'small-productcode')) ?>
<?php
	$prodDetImage = $prodDetails->get_image(array(87, 74), array('class' => 'small-productcode', 'alt' => $product->get_title()));
	if(empty($prodDetImage)) {
		echo '<img class="small-productcode" src="'.get_template_directory_uri().'/assets/images/no_image.png" alt="'.$product->get_title().'" witdh="87" height="74" />';
		$prodDetCmpImg = '<img src="'.get_template_directory_uri().'/assets/images/no_image.png" alt="'.$product->get_title().'" witdh="67" height="65" />';
	} else {
		echo $prodDetImage;
		$prodDetCmpImg = $prodDetails->get_image(array(67, 65), array('alt' => $product->get_title()));
	}
?>
			<div class="compare-container">
				<div class="checkbox addtocompare"><label><input type="checkbox" value="<?php echo $prodDetails->id.'_'.$sim_ctr4; ?>" class="js_click_compare"><span class="addlabel">compare</span></label></div>
			</div>
		

<!-- compare box product content -->
<div style="display:none;" id="js_click_compare_content_container_<?php echo $prodDetails->id.'_'.$sim_ctr4; ?>">
	<div id="js_click_compare_content_<?php echo $prodDetails->id.'_'.$sim_ctr4; ?>" class="col-md-3 col-lg-2  col-sm-3 col-xs-6 graybg">
	
		<span class="smallclose"><a href="#" class="js_remove_cp_con_prdt"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/close-small.png"></a></span>
		<div class="graybg-compare">
		<div class="product-img"><?php echo $prodDetCmpImg; ?></div>
		<div class="product-codeid"><span class="model">SKU</span><br><span class="codenum"><?php echo strtoupper($prodDetails->sku); ?></span></div>
		<input type="hidden" name="prodCmpArray[]" value="<?php echo $prodDetails->id; ?>" />
	</div>
	</div>
</div>

<?php
	$attrValue = array();
 	foreach($Attr_Keys as $attributesItem) {
		$attrValue[$attributesItem] = $attribute_list[$attributesItem];	
	}
	if(count($S_LeftColumnValue) == 0){
			echo '<td></td>';
		}

   if(is_array($S_LeftColumnValue)) {  
	$sim_ctr2 = 2;
		foreach($S_LeftColumnValue as $key=>$attributesItem) {
			// /print_r($attributesItem);
		if($sim_ctr2 < 6) {
			$key = 'pa_'.$attributesItem["slug"];
			$term = get_term_by('slug',$attribute_list[$key], $key);
			//print_r($attribute_valueSet);
			if($key !="pa_")
			$valueAttr = ($term->name)?$term->name:"n/a";
				echo '<td>'.ucwords($valueAttr).'</td>';
			++$sim_ctr2;
		   }
		}
	}
?>
		<td> 
			<a data-toggle="modal" data-target=".requestaquote"><button type="button" class="pl-btn btn-primary requestquote">Request a Quote</button></a>
			<span class="jsPostId" style="display:none;"><?php echo $product->id; ?></span>

			<a href="<?php the_permalink($product->id); ?>" class="pl-btn btn-primary">View Product Details</a>
		</td>
		</tr>
	</div>
<?php
	if(empty($sim_ctr3)) {
		$sim_ctr3 = 2;
	} else {
		++$sim_ctr3;
	}
	++$sim_ctr4;
?> 
</div>

<script>
/* to make the same height in ipad and mobile */

$(window).load(function(){
	
var country = '<?php echo getClientIPInfo($_SERVER['REMOTE_ADDR']); ?>';
if(country == 'RU' || country == 'RUS') {
	$('.requestquote').css('display', 'none');
}
});
</script>
