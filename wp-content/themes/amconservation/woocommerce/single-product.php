<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

error_reporting(0);

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

define("CSS", "product_details.css");

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header('shop-amc');
}
?>
<!-- Style For The Product Page -->
<style type="text/css">

  #product_main_image {
     width: 100%;
    cursor: -webkit-zoom-in;
    cursor: zoom-in;
    height: auto;
    display: block;
    margin: 0 auto;
    max-width: 400px;


  }

  #modal-image_pop{
   margin:0 auto; 
display:block;   
  } 

  #product_content{
    color: #525151;
    font-size: 13px;
    font-weight: 400;
    text-decoration: none;
    line-height: 1.5em;
    padding: 5px 20px 5px 14%;

  }
  .panel-group{
  text-align:center;
  }

   #product_content h1,h2,h3,h4 {

    color: #66aad9;
    font-size: 18px;
    font-weight: normal;
    text-decoration: none;
    padding: 20px 5px 31px 0%;
  }
  </style>
<?php
global $post, $woocommerce, $product, $amc_urls;
$newPostId = $post->ID;
$attributeFlag= 1;
$xTerms = get_the_terms($newPostId, 'product_cat');
sort($xTerms);

if(isset($xTerms[1])) {
	$xTermsSecond = $xTerms[1];
} else {
	$xTermsSecond = $xTerms[0];
}

// Get variation product details
$variationProdDetails ='';
$getVariationId = (isset($_GET['variation_id'])) ? absint($_GET['variation_id']) : 0;
if($getVariationId > 0) {
	//$variationProdDetails = '';
	$variationProdDetails = new WC_Product_Variation($getVariationId);
	$newPostId = $variationProdDetails->variation_id;
}
 
 $attribute_list = array();
 $TableAttributes = woocommerce_get_product_terms($product->post->ID, 'pa_table-attributes', 'all');
  if(is_array($TableAttributes))
     {
      foreach($TableAttributes as $value) {
      $AttributeShow[$value->slug] = array("Label"=>$value->name,"slug"=>$value->slug);
       }
     }
    // print_r($AttributeShow);
 // get all the attributes values assosciated with the Product // 
 $remove_items = array("pa_personalizable","pa_price-per","pa_price-per","pa_zoomable","pa_energy-star");
 if($getVariationId != 0) 
      {
         $i = 0; 
         $prodAttributes =  $variationProdDetails->variation_data;

         //print_r($prodAttributes);
         foreach( $prodAttributes as $key=>$prodAttributesItem)
         {
          $key = str_replace("attribute_","", $key);
           if(!in_array($key, $remove_items) && !empty($prodAttributesItem)) {
                    $attribute_key                  =  $key;
                    $attribute_list[$attribute_key] =  $prodAttributesItem;
                  }
            $i++;
         }
        $SKU =$variationProdDetails->sku;
      }
      else

      {

            $prodDetails = get_product($newPostId);
            $Ptype =$prodDetails->product_type;
            if( $Ptype == 'variable'){
              $attributeFlag = 0;
            }
           // echo $Ptype;
            // /print_r($prodDetails);
            $prodAttributes = $prodDetails->get_attributes();
              $i = 0;
              if(is_array($prodAttributes))  { 
              foreach($prodAttributes as $prodAttributesItem) {
                
                $attribute_values = false;
                 if(!in_array($prodAttributesItem['name'], $remove_items))
                 {

                        $attribute_key    =  $prodAttributesItem['name'];
                        $attribute_values =  array_shift(woocommerce_get_product_terms($newPostId, $prodAttributesItem['name'], 'names')); 
                        if($attribute_values) {
                        $attribute_list[$attribute_key] = $attribute_values ;
                      }
                   $i++;
                 }
              }
              }  
          $SKU = $prodDetails->get_sku();    
           
      } 
      // Here to Make the Attribute to Sort to Show it Properly in the Listing Page // 
    if(is_array($attribute_list)){
    foreach($attribute_list as $key=>$value)
    {
      $value = str_replace("pa_", "", $key);
      if(is_array($AttributeShow[$value])) {
        $tmp_Array[$value] = $AttributeShow[$value];
    }
    }

  }

  $AttributeShow = $tmp_Array;
 // print_r($attribute_list);
  $term_title = "";
   
/**
* woocommerce_before_main_content hook
*
* @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
* @hooked woocommerce_breadcrumb - 20
*/
// do_action('woocommerce_before_main_content');
?>
<?php 
                            foreach($AttributeShow as $value) {
                                $key = 'pa_'.$value["slug"];
								if($key == 'pa_rated-watts') {
									$term = get_term_by('slug',$attribute_list[$key], $key);
									$term_title = " - ".$term->name." Watts";
								}
								
                             ?>
                            <?php   } ?>
<?php while ( have_posts() ) : the_post();
// For the Product page we have removed all the Other Pages Connected and made it as a Single page //
?>
<!-- Title and Breadcrumbs -->

<div class="row">
	<div class="product-details-top-container">
	<div class="col-md-6 heading-offset"> <p class="pd-content-heading-top"><?php the_title(); echo $term_title ?></p></div>
	<div class="col-md-6 bcs">
<ol class="breadcrumb"><?php breadcrumbs_fmg();  ?></ol>
	</div>
<?php if(is_mobile()) { ?>
	<div class="col-md-4 back-btn back-page-com"> <a onClick="history.go(-1)"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/back_btn.png">Back </a></div>
<?php } ?>
	</div>
</div>


<div class="row content">
      <div class="col-md-6"> 

<div class="center">
<div class="imgBg">
<?php

if ( has_post_thumbnail() ) {
	$image_title 		= esc_attr( get_the_title( get_post_thumbnail_id() ) );
	$image_link  		= wp_get_attachment_url( get_post_thumbnail_id() );
  $image_large    = get_the_post_thumbnail( $newPostId, apply_filters( 'zoom-image', 'full'),array('title' => $image_title,"ID"=>"modal-image_pop", 'alt' => $product->get_title()));
	$image       		= get_the_post_thumbnail( $newPostId, apply_filters( 'single_product_large_thumbnail_size zoom-image', array(200, 200) ), array('title' => $image_title,"ID"=>"product_main_image","data-toggle"=>"modal","data-target"=>".popupimage", 'alt' => $product->get_title()) );
	$attachment_count   = count( $product->get_gallery_attachment_ids() );

	if ( $attachment_count > 0 ) {
		$gallery = '[product-gallery]';
	} else {
		$gallery = '';
	}

	// echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="%s"  rel="prettyPhoto' . $gallery . '">%s</a>', $image_link, $image_title, $image ), $post->ID );
	/*
	echo '<img src="<?php echo get_template_directory_uri(); ?>/assets/images/product01_medium.png" data-target=".popupimage" data-toggle="modal" onclick="showPopup('zoom-image')" id="zoom-image">';
	*/
	echo $image;

} else {
	echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="Placeholder" />', woocommerce_placeholder_img_src() ), $post->ID );
}
?>
</div>
<div class="product_zoom"></div>
<?php
// Show gallery thumbs
$attachment_ids = $product->get_gallery_attachment_ids();
if ( $attachment_ids ) {
?>
<div class="row row-prd-option"> 
<?php
// Display the main product image as the first thumbnail
$mainPostImgId = get_post_thumbnail_id();
$thumbMedImageAry = wp_get_attachment_image_src($mainPostImgId, array(200, 200));
$thumbFullImageAry = wp_get_attachment_image_src($mainPostImgId, 'full');
echo '<div class="col-md-3  col-xs-3 col-sm-3 padding"><div class="img-shadow">';
echo wp_get_attachment_image( $mainPostImgId, apply_filters( 'single_product_small_thumbnail_size', array(109, 76) ), false, array(
	'class' => 'pd-thumb',
	'onclick' => 'swap("'.$thumbMedImageAry[0].'", "'.$thumbFullImageAry[0].'")',
	'alt' => $product->get_title()
));
echo '</div></div>';

$loop = 0;
$columns = apply_filters( 'woocommerce_product_thumbnails_columns', 3 );
foreach ( $attachment_ids as $attachment_id ) {

	$classes = array( 'zoom' ,'pd-thumb');

	if ( $loop == 0 || $loop % $columns == 0 )
	$classes[] = 'first';

	if ( ( $loop + 1 ) % $columns == 0 )
	$classes[] = 'last';

	$image_link = wp_get_attachment_url( $attachment_id );

	if ( ! $image_link )
	continue;

	$thumbMedImageAry = wp_get_attachment_image_src($attachment_id, array(200, 200));
	$thumbFullImageAry = wp_get_attachment_image_src($attachment_id, 'full');

	$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', array(109, 76) ), false, array(
		'class' => 'pd-thumb',
		'onclick' => 'swap("'.$thumbMedImageAry[0].'", "'.$thumbFullImageAry[0].'")',
		'alt' => $product->get_title()
	) );

	$image_class = esc_attr( implode( ' ', $classes ) );
	$image_title = esc_attr( get_the_title( $attachment_id ) );
	echo '<div class="col-md-3  col-xs-3 col-sm-3 padding"><div class="img-shadow">';
	// echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $image, $attachment_id, $post->ID, $image_class );
	echo $image;
	echo '</div></div>';
	$loop++;
?>

<?php } ?>
                  </div>
                  <!--   thumbnail   -->
<?php } ?>
             <!--    thumbnail  -->             
  </div> 
          <div>
          <?php  if(is_array($AttributeShow) && $attributeFlag == 1 ) { ?>
            <div class="product-options">
                <p class="productdetails-content-heading-po"> Product Options</p>             
                <!--table starts here-->
                  <div class="table ">
                    
                        <div class="table-prddisplay-container">
                          <table class="table table-prddisplay">
                          <thead>
                          <tr>
                          <?php  
                           $i=0;
                          foreach($AttributeShow as $key=>$value) { 
                            ?>
                            <th><?php echo $value["Label"] ; ?></th>
                          <?php  } ?>
                          </tr>
                          </thead>
                          <tbody>
                       <tr>
                       <?php 
                            foreach($AttributeShow as $value) {
                                $key = 'pa_'.$value["slug"];
                                $term = get_term_by('slug',$attribute_list[$key], $key);
                             ?>
                              <td><?php echo ucfirst($term->name); ?></td>
                            <?php   } ?>
                          </tr>
                          </tbody>
                          </table>
                        </div>
                      
                  </div>
            </div>
            <?php  } ?>
        <?php
        // Get The Attachments // Spec sheets assosiated with each Product // 
        $PDF = array();
		    $pdf_Get_ID = $post->ID;

        $args = array( 'post_mime_type' => 'application/pdf', 
                       'post_type' => 'attachment', 
                       'numberposts' => -1, 
                       'post_status' => null, 
                       'post_parent' => $pdf_Get_ID 
                       );
        $PDF = get_posts($args);

        if(count($PDF) > 0 ) {
        ?>
       
          <div class="product-details-top-container">
            <p class=" downloads_section"> Downloads </p>
          </div>
       

        <div>
        <div class="row download-padd">
           <?php  foreach($PDF as $_PDF) { ?>
          <div class="col-md-6 col-xs-6 col-sm-6">           
               <a class ="pdf-download" href="<?php echo $_PDF->guid; ?>" target="_blank">
              <img class="spreadsheets-icon" src="<?php echo get_template_directory_uri(); ?>/assets/images/pdf-icon_33.png" border="0">
               <?php echo apply_filters( 'the_title' , $_PDF->post_title ); ?></a>
              </div>
          <?php } ?>
          </div>
        </div>
        <?php } ?>
        <!-- -->
        </div>               
    </div>
 <div class="col-md-6 "> 
       
        <p class="productdetails-content-heading heading-offset2">
       <?php the_title(); echo $term_title ?>
       </p>
       <p class="productdetails-content1">
       <b>Item Number:</b> <?php echo strtoupper ($SKU); ?> <br>
       <?php 
        //$unit_per_case = array_shift(woocommerce_get_product_terms($newPostId,'pa_units-per-case', 'names'));
        $unit_per_case = $attribute_list["pa_units-per-case"];
       // print_r($newPostId);
        if($unit_per_case) {
          $term = get_term_by('slug',$attribute_list["pa_units-per-case"], "pa_units-per-case");

       ?>
       <b> Units Per Case: </b> <?php 
         $Unit = ($term->name)?$term->name:$attribute_list["pa_units-per-case"];
           echo $Unit; ?>
       <?php } ?>
       </p>
        <div class="row quotebuttons ">
        
        <div class="col-md-4 col-xs-12 col-sm-4 heading-offset3">
          <a data-target=".requestaquote" data-toggle="modal"><p class="pad  allcaps centered"> <button class="pl-btn btn-primary .btn-block requestquote" type="button">Request a Quote</button>  </p></a>
		  <span class="jsPostId" style="display:none;"><?php echo $newPostId; ?></span>
         </div>
       <div class="col-md-4 col-xs-12 col-sm-4 margin-left10"> <a href="#" onclick="window.location='<?php echo get_term_link( $xTermsSecond->slug, 'product_cat' ); ?>'; return false;">  <p class="pad align-left2  allcaps centered"><button class="pl-btn btn-primary .btn-block" type="button">Continue Shopping</button> </p> </a> </div>
          <div class="col-md-4 col-xs-12 col-sm-4 margin-left10" style="position:relative;"> 
          <div class="nw_amcpop_btn ">
          <div class="nw_amcpop_img"> <img  src="<?php echo get_template_directory_uri(); ?>/assets/images/share-icon.png" class="share-icon-image"> </div><div class="nw_amcpop_txt"> <span class=" pd-shareicon-link"> &nbsp; <a class="share-icon" href="javascript:void(0);"  >SHARE </a></span>
          </div>
           </div>
           <div class="nw_amc_pop_bx">
                <div class="nw_amc_pop_in">
                    <?php echo show_share_buttons('', true); ?>
                    <div class="nw_amc_pop_social nw_amc_pop_fb"></div>
                    <div class="nw_amc_pop_social nw_amc_pop_twitt"></div>
                    <div class="nw_amc_pop_social nw_amc_pop_pin"></div>
                    <div class="nw_amc_pop_social nw_amc_pop_gplus"></div>
                    <!--<div class="nw_amc_pop_social nw_amc_pop_mail"></div>-->
                </div>
           </div>
             </div>
           
      </div>
       <div><p class="productdetails-content-grey-heading"> <b> Call <?php echo $amc_urls["contact_number"]; ?> </b> <br> to speak to a Sales Executive </p></div>

       <div id="product_content">
            <?php 
			$watersense_attr = $prodAttributes['attribute_pa_water-sense'];
			
			if($watersense_attr == "yes") {
			$watersense_logo = "<div class='watersense_logo'>";
            $watersense_logo .= do_shortcode( '[image_strip="energy" src="waterSense"]' );
            $watersense_logo .= "</div>";
            echo $watersense_logo;			
  			}			
			
             if($post->post_excerpt !="") {			
			 echo apply_filters( 'the_content', $post->post_excerpt );
            }

             ?> 
            <?php  the_content(); ?>
       </div>
	   
	   <?php 
	   
	   if(strpos(get_term_link( $xTermsSecond->slug, 'product_cat' ),"indoor-commercial-lighting") !== false || 
				strpos(get_term_link( $xTermsSecond->slug, 'product_cat' ),"outdoor-commercial-lighting") !== false	
				) { ?>
	  <p> You maybe eligible for a rebate on this item. Click below to see rebates available in your location. </p>
		<div class="row">
			<div class="col-md-4 col-xs-12 col-sm-4 heading-offset3">
			<a href="<?php echo $amc_urls['commercial_lighting_rebates']."/?sku=".$SKU; ?>">
				<p class="pad allcaps centered"> <button class="pl-btn btn-primary .btn-block" type="button">CHECK FOR REBATES</button> </p>
			</a>
		</div>
		</div>
		<?php } ?>
</div></div>


<!-- Related Products -->
<div class="row">
   <div>
	<div class="col-md-12 "> 
   <div>
   
   <p class="productdetails-content-heading3">Related Products</p></div>
   
   </div>
	<div class="col-md-6 "> </div>          
	</div>
	 </div>

	 <div class="row bgfill prd-desc-list">
     
         

         <?php  include("single-product/related_product_detail.php"); ?>

         
        </div>
<!-- / Relate products -->

<?php endwhile; // end of the loop. ?>

<?php require_once('request-quote-modal.php'); ?>
<!--Modal Box Image-->

 <div class="modal fade popupimage content" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content-rac">
      <div class="modal-header-rac">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <div class="panel-group" id="accordion">
          <div class="row">
            <div class="col-md-12"> 
            <?php echo $image_large; ?>
            </div>
          </div>
        </div>
        <!-- /.modal-content --> 
      </div>
      <!-- /.modal-dialog --> 
    </div>
    <!-- /.modal --> 
    
  </div>
</div>

<!-- Modal for share buttons -->
<div class="modal fade" id="shareButtonsBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Share</h4>
			</div>
			<div class="modal-body">
<?php echo show_share_buttons('', true); ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>



<script>

	
var country = '<?php echo getClientIPInfo($_SERVER['REMOTE_ADDR']); ?>';
if(country == 'RU' || country == 'RUS') {
	$('.requestquote').css('display', 'none');
	$('.quotebuttons .heading-offset3').css('display', 'none');
} else {
	$('.requestquote').css('display', 'block');
	$('.quotebuttons .heading-offset3').css('display', 'block');
}

$(".radio-group2 .rac-radio1 :radio").click(function() {
	$("div.rac-container .services").css("display","none");
	$("div.rac-container .products").css("display","block");
});

$(".radio-group2 .rac-radio2 :radio").click(function() {
	$("div.rac-container .products").css("display","none");
	$("div.rac-container .services").css("display","block");
});

  $('#product_content ul li:empty').remove();
  $("#modal-image_pop").removeAttr("height");
  $("#modal-image_pop").removeAttr("width");

function showPopup(imageID){
	document.getElementById("modal-image").src=""+document.getElementById(imageID).src;
}

function swap(newImg, newBigImg){
	document.getElementById("product_main_image").src=""+newImg;
	$('div.popupimage').find('img').attr('src', newBigImg);
}
$('.nw_amcpop_btn').click(function(){
	$('.nw_amc_pop_bx').toggle();
});
$(document).on('mousedown', function (e) {
    if (($(e.target).closest(".nw_amcpop_btn").length === 0) && ($(e.target).closest(".nw_amc_pop_bx").length === 0)) {
        $(".nw_amc_pop_bx").hide();
        }
});

$("#ssba img.ssba").css("display", "none");
$("div.nw_amc_pop_social.nw_amc_pop_fb").appendTo("#ssba a#ssba_facebook_share");
$("div.nw_amc_pop_social.nw_amc_pop_twitt").appendTo("#ssba a#ssba_pinterest_share");
$("div.nw_amc_pop_social.nw_amc_pop_pin").appendTo("#ssba a#ssba_twitter_share");
$("div.nw_amc_pop_social.nw_amc_pop_gplus").appendTo("#ssba a#ssba_google_share");

$('a#ssba_twitter_share').attr("data-text", "Checkout this product");
$(window).load(function(){
 setHeight('.desc');
})
$(window).resize(function(){
setHeight('.desc');
})

 function setHeight(pxBtmBox) {
    var maxHeight = 0;
    //Get all the element with class = col
    column = $(pxBtmBox);
    column.css('height', 'auto');
    //Loop all the column
    column.each(function () {
        //Store the highest value
        if ($(this).height() > maxHeight) {
            maxHeight = $(this).height();
           
        }
    });
    //Set the height
    column.height(maxHeight);
}


</script>

<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>