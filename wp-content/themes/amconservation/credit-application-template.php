<?php 
/* 
Template Name: Credit Application Template 
*/ 
define("CSS", "credit.css");

global $amc_urls;

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header('shop-amc');
}
?>
 <!-- Title and Breadcrumb  -->
      <div class="row headersession">      
        <div class="col-md-7"><h3 class="headerlabel"><?php the_title(); ?></h3></div>
        <div class="col-md-5 breadcrumbs ">
          <ol class="breadcrumb">
           <?php breadcrumbs_fmg();  ?>
          </ol>
        </div>    
    </div>
     <!--/  Ends Title and Breadcrumb   -->
    <!--    content area start      -->
<div class="row creditcontent">
        <div class="col-md-12">
         
        <div class="row"> <div class="col-md-offset-1 cs-top-spacer-1">
              
         </div> </div>

     
     <div class="cs-content-align1">

        <div class="row"> <div class=" col-md-12  ">
        
<!-- Dynamic  contents comes From the  AdminSide  -->   
<?php if (have_posts()) : while (have_posts()) : the_post();?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
         
         
         <div><div class=" cs-top-spacer-1"> <p class="cs-content-heading2 ">  Downloads</p> </div></div>

       <div class="row"> <div class="col-md-2"> <div class="cs-drop-shadow">  
	  
	   <a href="<?php echo $amc_urls['creditapplication_link']; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pdf-icon_33.png" />Credit Application Form</a>
	   
 </div></div></div>
         
         </div> </div>
         

  </div>
         

  

   
   <div class="cs-top-spacer-1"></div>
   

</div>
        </div>    
    

<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>