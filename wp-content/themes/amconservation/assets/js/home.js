var sHt = 0;

function is_touch_device() {
    return !!('ontouchstart' in window);
}

$(".container").ontouchmove = function (event) {
    event.preventDefault();
}

if (is_touch_device() == true) {
    //$(".px-BtmBox button").detach();
    /*$(".px-BtmBox").css({'height':'80%'})
    $(".px-BtmBox").addClass('ptBox-touch');
     $(".px-BtmBox p").css({'height':'70%'})
     $(".px-phoneimgwrap, .px-phoneimg").css({'height':550})*/
    //$(".px-BtmBox").append(' <button class="btn btn-warning">Read More</button>')
    //$(".px-BtmBox-hotspot").css("opacity", "0");
	$('body').addClass('touch');

}

$(window).load(function () {

    $('.flexslider').show();
    $('.flexslider').flexslider({
        animation: "slide",
        controlNav: false,
        prevText: "",
        nextText: "",
        touch: true,
        start: function (slider) {
            $('body').removeClass('loading');
            //alignCarouseltext();
            paraHeight();
        },
        end: function () { }
    });
});

var toolTp = 'You will be redirected to our Simply Conserve shopping site where our products are available for purchase.'
$(".home").attr("data-title", toolTp)
$(".home").tooltip({ html: true, placement: "bottom" });
/*$(document).click(function(){
alert('sdf');
$('.bs-example.twinbtn').remove($('.tooltip').html();
});*/

$(".menu1").hover(function () {
    $(".top-icon").css("background", "url('" + baseURL + "/assets/images/spritesheet.png') no-repeat -3.567% 86.273%");
    $(".toplink1").css("color", "#f4911e");
}, function () {
    $(".top-icon").css("background", "url('" + baseURL + "/assets/images/spritesheet.png') no-repeat -3.567% 33.273%");
    $(".toplink1").css("color", "#525151");
});

$(".menu2").hover(function () {
    $(".top-icon2").css("background", "url('" + baseURL + "/assets/images/spritesheet.png') no-repeat 8.433% 86.273%");
    $(".toplink2").css("color", "#f4911e");
}, function () {
    $(".top-icon2").css("background", "url('" + baseURL + "/assets/images/spritesheet.png') no-repeat 8.433% 33.273%");
    $(".toplink2").css("color", "#525151");
});

$(".menu3").hover(function () {
    $(".top-icon3").css("background", "url(''" + baseURL + "/assets/images/spritesheet.png') no-repeat 18.433% 86.273%");
    $(".toplink3").css("color", "#f4911e");
}, function () {
    $(".top-icon3").css("background", "url('" + baseURL + "/assets/images/spritesheet.png') no-repeat 18.433% 33.273%");
    $(".toplink3").css("color", "#525151");
});

$(".search-btn").hover(function () {
    $(".mainmenu-icon6").css("background", "url('" + baseURL + "/assets/images/spritesheet.png') no-repeat 77.433% 90.273%");
}, function () {
    $(".mainmenu-icon6").css("background", "url('" + baseURL + "/assets/images/spritesheet.png') no-repeat 99.433% 50.273%");
});

$(".phonelink").hover(function () {
    /* $(".bottom-icon1").css("background","url('assets/images/spritesheet.png') no-repeat 42.433% 88.273%");*/
    $(".toplink4").css("color", "#f4911e");
}, function () {
    /*$(".bottom-icon1").css("background","url('assets/images/spritesheet.png') no-repeat 31.433% 35.273%");*/
    $(".toplink4").css("color", "#525151");
});


$(".home").hover(function () {
    $(".arrowicon1").css("background", "url('" + baseURL + "/assets/images/spritesheet.png') no-repeat 86.433% 62.273%");
    $(".btn-default.home").css("background", "#fff");
}, function () {
    $(".arrowicon1").css("background", "url('" + baseURL + "/assets/images/spritesheet.png') no-repeat 86.433% 36.273%");
    $(".btn-default.home").css("background", "#525151");
});

$(".professional").hover(function () {
    $(".arrowicon2").css("background", "url('" + baseURL + "/assets/images/spritesheet.png') no-repeat 86.433% 62.273%");
    $(".professional").css("background", "#fff");
}, function () {
    $(".arrowicon2").css("background", "url('" + baseURL + "/assets/images/spritesheet.png') no-repeat 86.433% 36.273%");
    $(".professional").css("background", "#525151");
});

$(".cataloglink").hover(function () {
    /*  $(".bottom-icon2").css("background","url('assets/images/spritesheet.png') no-repeat 31.38% 87.273%");*/
    $(".toplink5").css("color", "#f4911e");
}, function () {
    /* $(".bottom-icon2").css("background","url('assets/images/spritesheet.png') no-repeat 42.433% 34.273%");*/
    $(".toplink5").css("color", "#525151");
});

$(".newsletter-btn").hover(function () {

    $(".arrowbtn").css("background", "url('" + baseURL + "/assets/images/spritesheet.png') no-repeat 86.433% 92.273%");
}, function () {
    $(".arrowbtn").css("background", "url('" + baseURL + "/assets/images/spritesheet.png') no-repeat 86.433% 35.273%");
});
if (typeof window.chrome == "object") {

    /*$(".submenu-btn").hover(function(){                
                  $(".mainnav-home").css('opacity':'1'); 
                   },function(){                    
                  $(".mainnav-home").css('opacity':'0.9');

              });*/

}




if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
    var viewportmeta = document.querySelector('meta[name="viewport"]');
    if (viewportmeta) {
        viewportmeta.content = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0';
        document.body.addEventListener('gesturestart', function () {
            viewportmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=1.6';
        }, false);
    }
}



function Parallax() {

    var jarallax = new Jarallax();
    jarallax.addAnimation('#px-Program', [{
        progress: '0%',
        top: '1000px'
    }, {
        progress: '100%',
        top: ($(window).width() / 1024 * 58.5) + 'px'
    }])
    jarallax.addAnimation('#px-Testimonial', [{
        progress: '0%',
        top: '1800px'
    }, {
        progress: '100%',
        top: ($(window).width() / 1024 * 58.5) + 'px'
    }])
    jarallax.addAnimation('#px-Talktous', [{
        progress: '0%',
        top: '2300px'
    }, {
        progress: '100%',
        top: ($(window).width() / 1024 * 58.5) + 'px'
    }])
    jarallax.addAnimation('.px-ImageBg', [{
        progress: '0%',
        top: '0px'
    }, {
        progress: '100%',
        top: '-350px'
    }])
}


function parallaxResizeFunction() {

    var currscreenW = $(window).width();
    var currscreenH = $(window).height();


    if (currscreenW > 1200) {
        mWwid = Math.round((($(window).innerWidth()) - 190) / 3);
        mWwid -= 10;
        $('.px-BtmBox').css('width', mWwid + 'px');
        $('#px-Program').css('left', '40px');
        $('#px-Testimonial').css('left', mWwid + 120 + 'px');
        $('#px-Talktous').css('left', (mWwid * 2) + 180 + 'px');
    } else {
        mWwid = Math.round((($(window).innerWidth()) - 90) / 3);
        mWwid -= 10;
        $('.px-BtmBox').css('width', mWwid + 'px');
        $('#px-Program').css('left', '30px');
        $('#px-Testimonial').css('left', mWwid + 60 + 'px');
        $('#px-Talktous').css('left', (mWwid * 2) + 90 + 'px');
    }
    // Parallax();

    if (is_touch_device() == false) Parallax();
    else {
        $('#px-Program').css('top', '20px');
        $('#px-Testimonial').css('top', '20px');
        $('#px-Talktous').css('top', '20px');
    }
}
$(window).resize(function () {
    initParallax();
    pxBtmBox();
    paraHeight();/* call carousal over text alignment */

});

$(window).load(function () {
    initParallax();

    setInterval(function () {
        pxBtmBox();
        $('.contentslider .flex-active-slide').resize();
    }, 2000);
    $(".top-bar li:last-child").addClass('last');

    //setHeight('.px-BtmBox'); /* make the parallax box height same */
	
	$(".menu-container").scrollLeft();
    $(".leftarrow").click(function () {

        $(this).toggleClass("SlideOff");
        $(this).toggleClass("SlideOnn");
        $(this).parent('.product-heading-container').toggleClass("SlideOnn");
        if ($(this).hasClass("SlideOnn") == true) {
            $(".menu-container").animate({
                scrollLeft: 1000
            }, 1000);;
            //alert("yes");
        } else {
            $(".menu-container").animate({
                scrollLeft: 0
            }, 1000);;
            //alert("no");
        }

    });


});

function alignCarouseltext() {
    var refDim = 0;
    /*
     * Dimention calculation for transparent BG
     */
    $(".slides").children("li").find("p").each(function () {
        //$(this).css({'border':'1px solid red'})
        //alert($(this).innerWidth());
        var desWidth = $(this).actual('width');

        var desHeight = $(this).actual('height');

        var parWidth = $(this).parent().actual('width');

        $(this).css('width', parWidth * 0.2); //Assigning width for paragraph

        if (desWidth > desHeight)
            refDim = desWidth + 80;
        else
            refDim = desHeight + 80;
        var trasBG = $(this).parent().find(".text_bg");
        trasBG.css({
            width: refDim,
            height: refDim,
            "-ms-border-radius": refDim,
            "-moz-border-radius": refDim,
            "-webkit-border-radius": refDim,
            "-khtml-border-radius": refDim,
            "-o-border-radius": refDim,
            "border-radius": refDim
        });
    });

    var slideHt = $("ul.slides img").height();
    var slideWt = $("ul.slides img").width();
    var textboxHt = $("ul.slides p").height() / 2;
    var textboxWt = $("ul.slides p").width() / 2;
    $("ul.slides p").css("top", (slideHt / 2 - textboxHt) + "px");
    $("ul.slides p").css("margin-left", (slideWt / 2 - textboxWt) + "px");



    var mLeft = (slideWt - refDim) / 2;
    var pTop = (slideHt - refDim) / 2;

    $("ul.slides .text_bg").css("top", pTop);
    $("ul.slides .text_bg").css("margin-left", mLeft + "px");
}
function initParallax() {
    var int = self.setTimeout(function () {
        //alignCarouseltext();
        SliderHeight();
        startContentslider();
    }, 500);

    function SliderHeight() {
        var slHei = $('.flex-viewport img').height();
        //$('.slider').css('height', slHei + 'px');
        parallaxResizeFunction();
    }
}
function pxBtmBox() {
    $(".px-BtmBox-hotspot").each(function (n) {
        var headTitle = $(this).prev().height() + 40;
        var description = $(this).parent().find("p").height() + 35;

        var hotSoptTopPos = headTitle - 7;
        var btnBtmPos = -(18 + (description / 2));

        var hotSopt = $(this).find('div');
        var button = $(this).find('button.btn');


        hotSopt.css('top', hotSoptTopPos);
        //hotSopt.css('height',description);
        button.css("bottom", btnBtmPos);
    });
}

/*$(".top-bar").children('li').last().hover(
  function () {

      var ulwidth = $('.top-prog').width() - 51;


      var submenuWid = 0;
      $(".top-bar li.last > ul  > li").each(function () {
          submenuWid = submenuWid + $(this).actual(width);
      });


      var x = $(this).offset().left - submenuWid + ulwidth;

      var text = navigator.appVersion;
      var str = navigator.appVersion;
      var n = str.search(/MSIE 8.0;/i);
      if (n == 17) {
        
          $(".top-bar li.last ul").animate({
              'padding-left': x + 90
          }, "normal");
      } else {
          $(".top-bar li.last ul").animate({
              'padding-left': x
          }, "normal");
      }

     
  },
  function () {
      $(".top-bar li.last ul").css("padding-left", "");
  }
);*/
$(window).load(function(){
$(".no-touch .top-bar").children('li').last().hover(
  function () {

      var ulwidth = $('.top-prog').width() - 30;

      var submenuWid = 0;
      $(".no-touch .top-bar li.last > ul  > li").each(function () {
          submenuWid = submenuWid + $(this).actual( 'width', { absolute : true });
      });


      var x = $(this).offset().left - submenuWid + ulwidth;

       var text = navigator.appVersion;
      var str = navigator.appVersion;
      var n = str.search(/MSIE 8.0;/i);
      if (n == 17) {
         
          $(".no-touch .top-bar li.last ul").animate({
              'padding-left': x + 100
          }, "normal");
      } else {
          $(".no-touch .top-bar li.last ul").animate({
              'padding-left': x
          }, "normal");
      }
  },
  function () {
      $(".no-touch .top-bar li.last ul").css("padding-left", "");
  }
);

});


$(".select").hover(
  function () {
      $(".sub-menu1").css("width", "1000px");
  },
  function () {

  }
);

function paraHeight() {
    var screenWidth = $(window).width();
    $('.flexslider ul.slides li').each(function () {

        var liHeight = $(this).height();
        var circleHeight = $(this).find('.text_bg').height();
        var reqcirclePosition = liHeight - circleHeight;
        if (screenWidth > 1300) {
            reqcirclePosition = (reqcirclePosition / 2) + 25;
        } else if ((screenWidth < 1300) && (screenWidth > 767)) {
            reqcirclePosition = (reqcirclePosition / 2) + 13;
        } else {
            reqcirclePosition = (reqcirclePosition / 2) + 10;
        }
        $(this).find('.text_bg').css('margin-top', reqcirclePosition + 'px')
        var pHeight = $(this).find('p').height();
        var reqHeight = circleHeight - pHeight;
        $(this).find('p').css('margin-top', reqHeight / 2 + 'px')
    })
}

/* to make the parallax box same height */

function setHeight(pxBtmBox) {
    var maxHeight = 0;
    //Get all the element with class = col
    column = $(pxBtmBox);
    column.css('height', 'auto');
    //Loop all the column
    column.each(function () {
        //Store the highest value
        if ($(this).height() > maxHeight) {
            maxHeight = $(this).height();

        }
    });
    //Set the height
    column.height(maxHeight);
}

$(window).load(function () {
    setInterval(function () {
        setHeight('.px-BtmBox');

    }, 1000);

});
$(window).resize(function () {
    setInterval(function () {
        setHeight('.px-BtmBox');

    }, 1000);

});








// To test the @id toggling on password inputs in browsers that don’t support changing an input’s @type dynamically (e.g. Firefox 3.6 or IE), uncomment this:
// $.fn.hide = function() { return this; }
// Then uncomment the last rule in the <style> element (in the <head>).
/* $(function() {
  // Invoke the plugin
  $('.js_searchBox').placeholder();
  // That’s it, really.
  // Now display a message if the browser supports placeholder natively
  var html;
  if ($.fn.placeholder.input && $.fn.placeholder.textarea) {
   html = '<strong>Your current browser natively supports <code>placeholder</code> for <code>input</code> and <code>textarea</code> elements.</strong> The plugin won’t run in this case, since it’s not needed. If you want to test the plugin, use an older browser ;)';
  } else if ($.fn.placeholder.input) {
   html = '<strong>Your current browser natively supports <code>placeholder</code> for <code>input</code> elements, but not for <code>textarea</code> elements.</strong> The plugin will only do its thang on the <code>textarea</code>s.';
  }
  if (html) {
   $('<p class="note">' + html + '</p>').insertAfter('form');
  }
 });
 */
//setTimeout(function () {
//alignCarouseltext();
//startContentslider();

// }, 50);

function startContentslider() {
    $('.contentslider').flexslider({
        animation: "slide",
        controlNav: false,
        slideshow: false,
        prevText: "",
        nextText: "",
        touch: true

    });
}

