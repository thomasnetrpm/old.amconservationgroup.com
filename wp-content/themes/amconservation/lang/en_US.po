msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

#. Text in function
#: acceptedtheme/comments.php:22
msgid "% Responses"
msgstr ""

#. Text in function
#: acceptedtheme/comments-popup.php:9
msgid "%1$s - Comments on %2$s"
msgstr ""

#. Text in function
#: acceptedtheme/navigation.php:2
msgid "&laquo; Older Entries"
msgstr ""

#. Text in function
#: acceptedtheme/includes/icore/widgets/widget-recentposts.php:45
#: acceptedtheme/content.php:13
#: acceptedtheme/content.php:31
#: acceptedtheme/single.php:48
#: acceptedtheme/page.php:41
msgid "0 comments"
msgstr ""

#. Text in function
#: acceptedtheme/includes/icore/widgets/widget-recentposts.php:45
#: acceptedtheme/content.php:13
#: acceptedtheme/content.php:31
#: acceptedtheme/single.php:48
#: acceptedtheme/page.php:41
msgid "1 comment"
msgstr ""

#. Text in echo
#: acceptedtheme/comments-popup.php:30
msgid "<abbr title=\"Really Simple Syndication\">RSS</abbr> feed for comments on this post."
msgstr ""

#. Text in echo
#: acceptedtheme/comments-popup.php:80
msgid "<abbr title=\"Universal Resource Locator\">URL</abbr>"
msgstr ""

#. Text in function
#: acceptedtheme/comments.php:88
msgid "Add Your Comment"
msgstr ""

#. Text in function
#: acceptedtheme/includes/icore/theme-customizer.php:1
msgid "Appearance"
msgstr ""

#. Text in echo
#: acceptedtheme/header.php:69
msgid "Call us:"
msgstr ""

#. Text in function
#: acceptedtheme/includes/woocommerce/woocommerce-config.php:1
msgid "Cart:"
msgstr ""

#. Text in echo
#: acceptedtheme/comments-popup.php:103
msgid "Close this window."
msgstr ""

#. Text in function
#: acceptedtheme/comments-popup.php:51
msgid "Comment"
msgstr ""

#. Text in echo
#: acceptedtheme/comments-popup.php:28
#: acceptedtheme/comments.php:22
msgid "Comments"
msgstr ""

#. Text in echo
#: acceptedtheme/comments.php:58
msgid "Comments are closed for this page."
msgstr ""

#. Text in function
#: acceptedtheme/includes/icore/theme-customizer.php:1
msgid "Custom CSS"
msgstr ""

#. Text in function
#: acceptedtheme/includes/icore/theme-customizer.php:1
msgid "Display Homepage Message"
msgstr ""

#. Text in function
#: acceptedtheme/includes/icore/theme-customizer.php:1
msgid "Display Homepage Slider"
msgstr ""

#. Text in function
#: acceptedtheme/includes/icore/theme-customizer.php:1
msgid "Display Search Bar"
msgstr ""

#. Text in echo
#: acceptedtheme/comments-popup.php:75
msgid "E-mail"
msgstr ""

#. Text in function
#: acceptedtheme/comments.php:66
msgid "Email"
msgstr ""

#. Text in echo
#: acceptedtheme/includes/icore/widgets/widget-twitter.php:73
msgid "Follow"
msgstr ""

#. Text in echo
#: acceptedtheme/header.php:78
msgid "Follow:"
msgstr ""

#. Text in function
#: acceptedtheme/comments.php:88
msgid "Leave a Reply to %s"
msgstr ""

#. Text in echo
#: acceptedtheme/comments-popup.php:61
msgid "Leave a comment"
msgstr ""

#. Text in function
#: acceptedtheme/comments-popup.php:62
msgid "Line and paragraph breaks automatic, e-mail address never displayed, <acronym title=\"Hypertext Markup Language\">HTML</acronym> allowed: <code>%s</code>"
msgstr ""

#. Text in function
#: acceptedtheme/comments-popup.php:66
msgid "Logged in as <a href=\"%1$s\">%2$s</a>. <a href=\"%3$s\" title=\"Log out of this account\">Log out &raquo;</a>"
msgstr ""

#. Text in function
#: acceptedtheme/header.php:106
msgid "Menu"
msgstr ""

#. Text in echo
#: acceptedtheme/comments-popup.php:70
#: acceptedtheme/comments.php:66
msgid "Name"
msgstr ""

#. Text in function
#: acceptedtheme/navigation.php:3
msgid "Newer Entries &raquo;"
msgstr ""

#. Text in function
#: acceptedtheme/comments.php:22
msgid "No Responses"
msgstr ""

#. Text in echo
#: acceptedtheme/no-results.php:2
msgid "No Results Found"
msgstr ""

#. Text in echo
#: acceptedtheme/comments-popup.php:57
msgid "No comments yet."
msgstr ""

#. Text in function
#: acceptedtheme/comments.php:22
msgid "One Response"
msgstr ""

#. Text in function
#: acceptedtheme/header.php:10
msgid "Page %s"
msgstr ""

#. Text in function
#: acceptedtheme/comments-popup.php:51
msgid "Pingback"
msgstr ""

#. Text in function
#: acceptedtheme/index.php:2
#: acceptedtheme/single.php:48
#: acceptedtheme/page.php:41
msgid "Posted by "
msgstr ""

#. Text in function
#: acceptedtheme/index.php:2
msgid "Posted in "
msgstr ""

#. Text in function
#: acceptedtheme/index.php:2
msgid "Posted on "
msgstr ""

#. Text in function
#: acceptedtheme/comments-popup.php:113
msgid "Powered by <a href=\"%s\" title=\"Powered by WordPress, state-of-the-art semantic personal publishing platform\"><strong>WordPress</strong></a>"
msgstr ""

#. Text in function
#: acceptedtheme/includes/icore/theme-functions.php:1
msgid "Primary Menu"
msgstr ""

#. Text in function
#: acceptedtheme/includes/woocommerce/woocommerce-config.php:1
msgid "Product added"
msgstr ""

#. Text in echo
#: acceptedtheme/comments-popup.php:93
msgid "Say It!"
msgstr ""

#. Text in function
#: acceptedtheme/index.php:2
#: acceptedtheme/search.php:2
msgid "Search results "
msgstr ""

#. Text in function
#: acceptedtheme/includes/icore/theme-customizer.php:1
msgid "Select color scheme"
msgstr ""

#. Text in function
#: acceptedtheme/includes/icore/theme-options.php:1
msgid "Slideshow Images"
msgstr ""

#. Text in echo
#: acceptedtheme/page-sidebar.php:48
#: acceptedtheme/comments-popup.php:109
#: acceptedtheme/single.php:54
#: acceptedtheme/page.php:48
msgid "Sorry, no posts matched your criteria."
msgstr ""

#. Text in echo
#: acceptedtheme/comments-popup.php:98
msgid "Sorry, the comment form is closed at this time."
msgstr ""

#. Text in function
#: acceptedtheme/comments.php:88
msgid "Submit Comment"
msgstr ""

#. Text in function
#: acceptedtheme/index.php:2
msgid "Tag "
msgstr ""

#. Text in function
#: acceptedtheme/comments-popup.php:33
msgid "The <abbr title=\"Universal Resource Locator\">URL</abbr> to TrackBack this entry is: <em>%s</em>"
msgstr ""

#. Text in echo
#: acceptedtheme/no-results.php:3
msgid "The page you requested could not be found. Try refining your search, or use the navigation above to locate the post."
msgstr ""

#. Text in echo
#: acceptedtheme/comments.php:12
msgid "This post is password protected. Enter the password to view comments."
msgstr ""

#. Text in function
#: acceptedtheme/includes/icore/theme-functions.php:1
msgid "Top Menu"
msgstr ""

#. Text in function
#: acceptedtheme/comments-popup.php:51
msgid "Trackback"
msgstr ""

#. Text in echo
#: acceptedtheme/comments.php:44
msgid "Trackbacks/Pingbacks"
msgstr ""

#. Text in function
#: acceptedtheme/includes/icore/theme-customizer.php:1
msgid "Upload Logo"
msgstr ""

#. Text in function
#: acceptedtheme/includes/icore/theme-options.php:1
msgid "Upload slider Images. Drag and drop to reorganize."
msgstr ""

#. Text in function
#: acceptedtheme/comments.php:66
msgid "Website"
msgstr ""

#. Text in echo
#: acceptedtheme/page-home-widgets.php:22
#: acceptedtheme/page-home-sidebar.php:15
msgid "What's New"
msgstr ""

#. Text in echo
#: acceptedtheme/home.php:35
msgid "What's in Store"
msgstr ""

#. Text in echo
#: acceptedtheme/comments-popup.php:85
msgid "Your Comment"
msgstr ""

#. Text in function
#: acceptedtheme/comments.php:88
msgid "Your email address will not be published. Required fields are marked *"
msgstr ""

#. Text in echo
#: acceptedtheme/content.php:13
#: acceptedtheme/content.php:31
msgid "by "
msgstr ""

#. Text in function
#: acceptedtheme/comments-popup.php:51
msgid "by %1$s &#8212; %2$s @ <a href=\"#comment-%3$s\">%4$s</a>"
msgstr ""

#. Text in function
#: acceptedtheme/includes/icore/widgets/widget-recentposts.php:45
#: acceptedtheme/content.php:13
#: acceptedtheme/content.php:31
#: acceptedtheme/single.php:48
#: acceptedtheme/page.php:41
msgid "comments"
msgstr ""

#. Text in echo
#: acceptedtheme/content.php:13
#: acceptedtheme/content.php:31
#: acceptedtheme/single.php:48
msgid "in "
msgstr ""

#. Text in echo
#: acceptedtheme/content.php:38
msgid "read more"
msgstr ""

