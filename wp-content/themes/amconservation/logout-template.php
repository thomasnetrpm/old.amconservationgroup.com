<?php 
/* 
Template Name: Logout template 
*/ 

// define("CSS", "forget_password.css");

global $wpdb, $amc_urls;

unset($_SESSION['user_logged_in']);
unset($_SESSION['user_logged_in_data']);
wp_safe_redirect($amc_urls['home']);
exit;