<?php
/* 
Template Name: Image download template 
*/ 

// Handle the gallery image download
if(isset($_GET['image_id']) && ((int)$_GET['image_id'] > 0)) {

	$post_thumbnail_id = $_GET['image_id'];
	$imageData = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
	if(!$imageData) {
		wp_safe_redirect(site_url()); exit();
	}

	$imageContent = file_get_contents(ABSPATH.str_replace(site_url().'/', '', $imageData[0]));
	$imageName = basename($imageData[0]);
	$imageMime = wp_check_filetype(ABSPATH.str_replace(site_url().'/', '', $imageData[0]));

	header('Content-Type: "'.$imageMime['type'].'"');
	header('Content-Disposition: attachment; filename="'.$imageName.'"');
	header("Content-Transfer-Encoding: binary");
	header('Expires: 0');
	header('Pragma: no-cache');
	header("Content-Length: ".strlen($imageContent));
	echo $imageContent;
} else {
	wp_safe_redirect(site_url());
}
exit();