

 
 <?php
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
	global $page, $wp_query;
	$post = $wp_query->get_queried_object();
	if($post->post_title == $item->title) {
		$classes[] = "abtmenu-active";
	}
	return $classes;
}

wp_nav_menu(array(
	'theme_location' => 'middle-menu',
	'link_before' => '',
	'link_after' => '',
	'menu_class' => 'menus innermenu',
	'container' => false
));
?>
<script type="text/javascript">
jQuery('ul.menus li[class="abtmenu-active"]').removeClass().addClass('abtmenu-active');
jQuery('ul.menus li:not([class*="abtmenu-active"])').removeClass().addClass('abtmenu');
jQuery('ul.menus li a[title=Blog]').attr('target', '_blank');
</script>