<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

define('CSS', 'product_search_result.css');

$cat_title = __('Search results ','Bonanza');
$cat_desc = get_search_query();
$searchQuery = get_search_query();

// Handle the load more
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
?>
<?php if ( have_posts() ) : ?>

<?php while ( have_posts() ) : the_post(); ?>
<?php woocommerce_get_template_part( 'content', 'product' ); ?>
<?php endwhile; // end of the loop. ?>

<div style="display:none;" class="js_pagiLinks"><?php do_action( 'woocommerce_after_shop_loop' ); ?></div>

<?php endif; ?>
<?php
	exit();
}

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header('search');
}
?>

<style type="text/css">
.compare-container { width: 100%; position: relative; }
img.small-productcode { width: 100%; height: auto; }
</style>

 <div class="row content-session">
 
 <?php
 global $wp_query;
if($wp_query->post_count == 0) {
?>
	<div class="col-md-10 col-md-offset-2 searchcontainer">
	<?php
		echo '<h3 class="ps_txt">Your search yielded no results</h3>';
		echo '<p class="ps_parag">Check if your spelling is correct.<br />Remove quotes around phrases to search for each word individually. <i>bike shed</i> will often show more results than <i>"bike shed"</i>.<br />Consider loosening your query with <i>OR</i>. <i>bike OR shed</i> will often show more results than <i>bike shed</i>.</p>';
	?>
	</div>
<?php
} else {
?>
 
        <div class="col-md-3 space">
          <div class="headerrow">Narrow By</div>
          <div class="bodyrow">
<?php
	if (function_exists('dynamic_sidebar')):
		dynamic_sidebar('AMC Filter');
	endif;
?>
          </div>
        </div>
<div class="col-md-9 ">
<!-- compare box -->
	<!--	<div class="col-md-12 topspace">
<form id="prodCompareBox" method="post" action="<?php echo get_site_url().'/compare-products/' ?>">
          <div class="product-compare-container">
               <span class="bigclose"><a href="#" class="js_close_compareBox"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/close-big.png"></a></span>
               <div class="col-md-3 bluebg">
                    <div class="product-img"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/add-icon.png"></div>
                    <div class="product-codeid"><span class="model">Add<br>Product</span></div>
               </div>
               <div class="col-md-1">
			   <input type="submit" class="compare-btn btn-primary" value="COMPARE">
			   </div>
          </div>
</form>

<script type="text/javascript">
$('.js_close_compareBox').click(function() {
	$('div.topspace').hide();
	return false;
});
</script>

        </div>-->

		<div class="col-md-12 space">
          <div class="headerrow2 js_pagiStr">Items 1-<?php echo get_option('posts_per_page'); ?></div>
		<?php if ( have_posts() ) : ?>
			<?php woocommerce_product_loop_start(); ?>
				<?php //woocommerce_product_subcategories(); ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php woocommerce_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>
			<?php woocommerce_product_loop_end(); ?>
			<div style="display:none;" class="js_pagiLinks">
			<?php
				do_action( 'woocommerce_after_shop_loop' );
			?>
			</div>
		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
			<?php woocommerce_get_template( 'loop/no-products-found.php' ); ?>
		<?php endif; ?>
		 <div class="col-md-12 loadmore js_handleLoadMore" style="cursor:pointer;">
        <div class="loadingmore-row">Load more products</div>
                  </div>
 </div>
</div>
 
 <?php } ?>
 
 </div>

<?php
wp_reset_postdata();
?>

<!-- Page search content -->
<div class="row searchcontent">
	<div class="col-md-10 col-md-offset-2 searchcontainer">
<?php
// Query for page search content
$the_query = new WP_Query(array(
	'post_type' => 'page',
	's' => $searchQuery
));

while ($the_query->have_posts()) {
	$the_query->the_post();
?>
<h3 class="ps_txt"><?php echo get_the_title(); ?></h3>
<p class="ps_parag"><?php echo substr(strip_tags(get_the_content()), 0, 350).' ...'; ?></p>
<a class="readmore" href="<?php echo get_permalink(); ?>">Read More</a>
<?php
}

if($the_query->post_count == 0) {
	echo '<h3 class="ps_txt">Your search yielded no results</h3>';
	echo '<p class="ps_parag">Check if your spelling is correct.<br />Remove quotes around phrases to search for each word individually. <i>bike shed</i> will often show more results than <i>"bike shed"</i>.<br />Consider loosening your query with <i>OR</i>. <i>bike OR shed</i> will often show more results than <i>bike shed</i>.</p>';
}
wp_reset_postdata();
?>
	</div>   
</div>

<?php include('woocommerce/request-quote-modal.php'); ?>

<script type="text/javascript">

 
 $(".onoffswitch").hover(function (){
     	$(this).popover('toggle');
        $(".headerlabel .popover-content").css("width","100%");
     });
	$(".onoffswitch").popover('show');
	window.setTimeout(function(){
		$(".onoffswitch").popover('toggle');
	}, 15000);
var count = 0;
$(".onoffswitch").on('click', function (){
	$(this).popover('hide');
	
	
	
	var text = navigator.appVersion;
	var str = navigator.appVersion;
	var n = str.search(/MSIE 8.0;/i);
	if (n == 17){
	
		if(count%2==0){
		$('.onoffswitch-switch').css({'right':'0'})
			$(".content-session").css("display","none");
			$(".searchcontent").css("display","block");
			$(".horizontal-line-divider").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/news_bg_horizontal.png') repeat-x;");
			
			
		}else{
		$('.onoffswitch-switch').css({'right':'21px'})
			$(".content-session").css("display","block");
			$(".searchcontent").css("display","none");
			$(".horizontal-line-divider").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/news_bg_horizontal2.png') repeat-x;")
			
			
		}
		count++;       
	}else{ 
		if($('.onoffswitch-checkbox').is(':checked')) {
			$(".content-session").css("display","none");
			$(".searchcontent").css("display","block");
			$(".horizontal-line-divider").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/news_bg_horizontal.png') repeat-x;")
		}
		else{
			$(".content-session").css("display","block");
			$(".searchcontent").css("display","none");
			$(".horizontal-line-divider").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/news_bg_horizontal2.png') repeat-x;")
		}
	}
	
});




</script>

<script>
$(".radio-group2 .rac-radio1 :radio").click(function() {
	$("div .services").css("display","none");
	$("div .products").css("display","block");
});

$(".radio-group2 .rac-radio2 :radio").click(function() {
	$("div .products").css("display","none");
	$("div .services").css("display","block");
});
</script>

<script type="text/javascript">
var postLimit = '<?php echo get_option('posts_per_page'); ?>';
$('.js_pagiStr').text('Items 1-' + ($('ul.products p.pl-productname').length));

// Hide the load more if 1st page has less that 'postLimit' products
if($('ul.products p.pl-productname').length < postLimit) {
	$('.js_handleLoadMore').hide();
}

$('.js_handleLoadMore').click(function() {
	$(this).find('div.loadingmore-row').text('Loading ...');
	var nextElmt = $('.js_pagiLinks ul li span.current').parent().next().find('a');

	if($('ul.products .js_pagiLinks').length != 0) {
		nextElmt = $('ul.products .js_pagiLinks:last ul li span.current').parent().next().find('a');
	}

	if(nextElmt.length > 0) {
		var nexturl = nextElmt.attr('href');
		var nextpage = nextElmt.text();
		$.ajax({
			url: nexturl,
			success: function(d) {
				$('.js_handleLoadMore').find('div.loadingmore-row').text('Load more products');

				$('.js_pagiLinks').html($(d).find('.js_pagiLinks').html());
				$(d).appendTo($('ul.products'));
				// $('.js_pagiStr').text('page 1-'+(nextpage*postLimit));
				$('.js_pagiStr').text('Items 1-' + ($('ul.products p.pl-productname').length));

				// Hide the load more for last page
				if($('ul.products').find('div.js_pagiLinks:last').find('ul li:last span').hasClass('current')) {
					$('.js_handleLoadMore').hide();
				}
			}
		});
	}
	return false;
});
</script>




	 <script>
      $('.panel-group').on('shown.bs.collapse', function () {
       $(this).find(".panel-headding").css({'background-image' : "url('<?php echo get_template_directory_uri(); ?>/images/accordion-uparrow.png')", 'background-repeat': 'no-repeat','background-position-x':'93%', 'background-position-y':'50%'})
        });
        $('.panel-group').on('hidden.bs.collapse', function () {
            $(this).find(".panel-headding").css({'background-image' : "url('<?php echo get_template_directory_uri(); ?>/images/accordion-downarrow.png')", 'background-repeat': 'no-repeat','background-position-x':'93%', 'background-position-y':'50%'})

        });

   </script>
	

<script type="text/javascript">
$(document).on( 'click', 'input.js_click_compare', function() {
	$('div.topspace').show();
	var _this = $(this);
	var canAdd = true;

	if($('div.product-compare-container .graybg').length >= 4) {
		return false;
	}

	if($('div.product-compare-container .graybg').length >= 3) {
		hideAddProdBox();
	} else {
		showAddProdBox();
	}

	if($('div.product-compare-container .graybg').length > 3) {
		canAdd = false;
	}

	if(canAdd) {
		if(_this.is(':checked')) {
			$('div.product-compare-container span.bigclose').after($('div#js_click_compare_content_container_'+_this.val()).html());
		} else {
			$('div.product-compare-container').find('div#js_click_compare_content_'+_this.val()).remove();
		}
	} else {
		if(_this.is(':checked')) { } else {
			$('div.product-compare-container').find('div#js_click_compare_content_'+_this.val()).remove();
		}
	}

});

// Remove product item from compare box
$(document).on( 'click', 'a.js_remove_cp_con_prdt', function(e) {
	e.preventDefault();

	if($('div.product-compare-container .graybg').length >= 3) {
		hideAddProdBox();
	} else {
		showAddProdBox();
	}

	// Uncheck the related compare checkbox
	$('input[type=checkbox][value='+$(this).parent().parent().attr('id').replace('js_click_compare_content_', '')+']').attr('checked', false);

	$(this).parent().parent().remove();
});

function showAddProdBox() {
	$('div.product-compare-container .bluebg').show();
}

function hideAddProdBox() {
	$('div.product-compare-container .bluebg').hide();
}

$('.product-content-container:nth-child(2n+1)').addClass('odd');
$('.product-content-container:nth-child(2n+2)').addClass('even');
</script>

<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>