<?php 
/* 
Template Name: Quote basket template 
*/ 

define("CSS", "quotebasket.css");

global $wpdb;

$loggedInUser = getLoggedInUser();

$quotes = $wpdb->get_results('SELECT * FROM wp_requestquote WHERE email = "'.$loggedInUser->email.'"');

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}
?>

<div class="row">
        <div class="col-md-12 quotebasket">      
        <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr class="quoteheader">
              <th>Date of Request</th>
              <th>Contact Details</th>
              <th>Services</th>
              <th>Products</th>
            </tr>
          </thead>
          <tbody>
<?php
if(!empty($quotes)) {
?>
<?php
$altStr = 'even';
$c = 0;
?>
<?php foreach($quotes as $quoteItem) {
if($c%2 == 1) {
	$altStr = 'odd';
}
 ?>
	<tr class="<?php echo $altStr; ?>">
              <td><b><?php echo date('d.m.Y', strtotime($quoteItem->created)); ?></b></td>
              <td><?php echo $quoteItem->username; ?><br>
                  <?php echo $quoteItem->company; ?><br>
                  <?php echo $quoteItem->email; ?><br>
                  <?php echo $quoteItem->phone.' Extn.'.$quoteItem->phoneext; ?>
              </td>
              <td><?php
			  $serviceItems = $quoteItem->serviceitems;
			  $serviceItems = unserialize($serviceItems);
			  echo ((isset($serviceItems) && is_array($serviceItems)) ? implode('<br />', $serviceItems) : '');
			  ?></td>
              <td>
<?php
$prodItems = $quoteItem->productitems;
$prodItems = unserialize($prodItems);
if(isset($prodItems['Products']) && is_array($prodItems['Products'])) {
	foreach($prodItems['Products'] as $prod) {
		echo '<b>Product Name: '.$prod['name'].'</b><br>Product No. '.$prod['sku'].' | QTY - '.$prod['qty'].'<br />';
	}
}
?>
              </td>
            </tr>
<?php } ?>

<?php
} else {
	?>
		<tr class="odd">
			<td colspan="4" style="text-align:center;"><b>Hi <?php echo $loggedInUser->firstname; ?>, Your quote basket is empty. Please fill in our Request a quote form.</b></td>
		</tr>
	<?php
}
?>
          </tbody>
        </table>
      </div>
    </div>  
   </div> 

<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>