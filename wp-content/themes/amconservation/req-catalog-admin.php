<div class="wrap">
	<h2>Request a catalog</h2>

<?php
global $wpdb;
// $wpdb->show_errors();
// $wpdb->print_error();

//Our class extends the MUO_List_Table class, so we need to make sure that it's there
if(!class_exists('reqCatalog_List_Table')){
	require_once( TEMPLATEPATH . '/classes/reqCatalogClass.php' );
}

//Prepare Table of elements
$list_table = new reqCatalog_List_Table();
$list_table->prepare_items();
//Table of elements
$list_table->display();
?>

</div>