<?php 
/* 
Template Name: Newsletter subscription template 
*/ 

define("CSS", "newsletter_subscription.css");

global $wpdb, $amc_urls, $show_msg, $success_msg, $error_msg;
$show_msg = 'none';



if(isset($_GET['msg']) && ($_GET['msg'] != '')) {
	$show_msg = 'block';
	$msg = $_GET['msg'];
	if($msg == 'success') {
		$success_msg = 'Your subscription has been confirmed! Thank you!';
	} else if($msg == 'error') {
		$error_msg = 'Some error happened. Please, try again.';
	} else {
		wp_safe_redirect(esc_url($amc_urls['home'])); exit;
	}
} 

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}
?>

<!--    content area start      -->
<div class="row bgfill ">
    <div class=" topspacer">
		<div class="col-md-12 message-form-container">
            <div class="row content">  
                <div class="col-md-4 col-md-offset-4">
                    <div class="row ">
                              
<div class="form-group">
	<?php include('successmodal.php'); ?>
</div>

						<div class="newsletter newsletter-subscription">
							<div class="news-letter-success"></div>
							<form id="news-letter-subscription" method="post" action="" onsubmit="return newsletter_check(this)" parsley-validate novalidate>


						<!-- first name -->

							<p><input class="form-control parsley-validated"  placeholder="First Name*" required parsley-trigger="change" parsley-error-message="Please enter your first name" type="text" name="nn" maxlength="30"></p>


						<!-- last name -->

							<p><input class="newsletter-lastname form-control parsley-validated" placeholder="Last Name*" required parsley-trigger="change" parsley-error-message="Please enter your last name" type="text" name="ns" maxlength="30"></p>


						<!-- email -->

							<p><input class="newsletter-email form-control parsley-validated" placeholder="Email Address*" required parsley-trigger="change" parsley-error-message="Please enter your email" type="email" name="ne" maxlength="30" required></p>

							<p><input class="newsletter-profile form-control snewsletter-profile-1" placeholder="Company"  type="text" maxlength="30" name="np1"/></p>

							
							
							<!-- <div class="security">
								For security purposes, please type in the letters below in the box.
							</div>
								<br clear="all" />
								<div id="wrap" class="msgwrap" align="center">
								<img src="<?php echo get_template_directory_uri(); ?>/captcha/get_captcha.php" alt="" id="captchas" />

								<br clear="all" />
								<input name="code" type="text" id="msgcode">
								</div>
								<img src="<?php echo get_template_directory_uri(); ?>/captcha/refresh.jpg" width="25" alt="" id="refreshs" />

								<br clear="all" /><br clear="all" />
								<label>&nbsp;</label>
								<div id="Sendsmsg"> </div> -->
							<input class="newsletter-submit btn fp-submit-btn newsub_button" type="submit" value="Subscribe"/>
							</form>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div> 
</div>
         
  
  <!--      content area end            -->
  
<script type="text/javascript">
	var mq = window.matchMedia( "(max-width: 780px)" );

	$('.newsletter-submit').click(function() {
		$( '#news-letter-subscription' ).parsley( 'validate' );
		
		var codeval = $("#msgcode").val();
			var url = '<?php echo get_template_directory_uri();?>';
			/*var ajaxStatusVar;
			  
			  
			  $.ajax({
				type: "POST",
				url: url+"/captcha/post.php",
				global: false,
				async: false,
				data: { code: codeval   }
				
				})
				.done(function( data ) {
					if(data.indexOf('fail') > 0) {
						$("#after_submits").html('');
						$("#Sendsmsg").after('<label class="error" id="after_submits">Error ! invalid captcha code.</label>');
						ajaxStatusVar = false;
						return false;
				   } else if(data.indexOf('success') > 0){
						$("#after_submits").html('');
						ajaxStatusVar = true;
						return true;
				   }
				});
				if (ajaxStatusVar == false) {return false;}*/

	});
	
	/*$('img#refreshs').click(function() {  
		change_captcha();
	});
		 
	function change_captcha()
	{
		var url = '<?php echo get_template_directory_uri();?>';
		document.getElementById('captchas').src=url+"/captcha/get_captcha.php?rnd=" + Math.random();
	}*/

	if (mq.matches) {
		var element = $(".back-btn").detach(); $('.breadcrumbs').append(element); 
		var element2 = $(".fp-mandatory-txt").detach(); $('.fp-parag-txt').after( element2 );
	} else {
		var element3 = $(".back-btn").detach(); $('.offset-top-1').prepend(element3);
		var element4 = $(".fp-mandatory-txt").detach(); $('.offset-2').prepend( element4 );
	}
	
	
	if (typeof newsletter_check !== "function") {
	window.newsletter_check = function (f) {
		
		var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;
		if (!re.test(f.elements["ne"].value)) {
			$("form[name=nl_subscribe_form] .foot-form label").hide();
			$("form[name=nl_subscribe_form] .foot-form span").hide();
			$("form[name=nl_subscribe_form] .foot-form").prepend("<p class='subscription' style='<?php if(is_front_page()) { echo 'margin-top:0px ! important;'; } ?>'>Please enter a valid Email address.</p>");
			$("form[name=nl_subscribe_form] .foot-form").append("<div id='closeButton'><img class='closebtn' src='<?php echo get_template_directory_uri(); ?>/assets/images/close-big2.png'></div>");
			return false;
		}

		// Handling the ajaxified save of subscribe email
		jQuery.ajax({
			url: '<?php echo site_url('/wp-content/plugins/newsletter/do/subscribe.php'); ?>',
			type: "post",
			async: false,
			data: { ne: f.elements["ne"].value, nn: f.elements["nn"].value, ns: f.elements["ns"].value, np1: f.elements["np1"].value  },
			success: function(d) { 
				$(".news-letter-success").addClass("js_result_tm alert fade in alert-warning");
				$(".news-letter-success").html("Thank You. You are now subscribed to our Newsletter");
				//change_captcha();
				//$("#msgcode").val('');
               /* $("form[name=nl_subscribe_form] .foot-form label").hide();
				$("form[name=nl_subscribe_form] .foot-form span").hide();
                                $( "form[name=nl_subscribe_form] .foot-form p.subscription" ).remove();
                                $( "form[name=nl_subscribe_form] .foot-form div#closeButton" ).remove();
                                if (d.indexOf('already subscribed') > -1) {
                                  $("form[name=nl_subscribe_form] .foot-form").prepend("<p class='subscription' style='<?php if(is_front_page()) { echo 'margin-top:0px ! important;'; } ?>'>You have already subscribed to the Newsletter.</p>");
				} else {
                                  $("form[name=nl_subscribe_form] .foot-form").prepend("<p class='subscription' style='<?php if(is_front_page()) { echo 'margin-top:0px ! important;'; } ?>'>Thank You. You are now subscribed to our Newsletter.</p>");
				}
				$("form[name=nl_subscribe_form] .foot-form ").append("<div id='closeButton'><img class='closebtn' src='<?php echo get_template_directory_uri(); ?>/assets/images/close-big2.png'></div>");*/
				return false;
			}
		});
		return false;
	}
}
</script>
  
<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>