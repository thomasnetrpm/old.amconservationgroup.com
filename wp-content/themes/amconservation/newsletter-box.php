<!-- News Letter AMCG -->
<script type="text/javascript">
//<![CDATA[
if (typeof newsletter_check !== "function") {
	window.newsletter_check = function (f) {
		/*
		var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;
		if (!re.test(f.elements["ne"].value)) {
			alert("The email is not correct");
			return false;
		}
		if (f.elements["ny"] && !f.elements["ny"].checked) {
			alert("You must accept the privacy statement");
			return false;
		}
		*/
		// return true;

		var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;
		if (!re.test(f.elements["ne"].value)) {
			$("form[name=nl_subscribe_form] .foot-form label").hide();
			$("form[name=nl_subscribe_form] .foot-form span").hide();
			$("form[name=nl_subscribe_form] .foot-form").prepend("<p class='subscription' style='<?php if(is_front_page()) { echo 'margin-top:0px ! important;'; } ?>'>Please enter a valid Email address.</p>");
			$("form[name=nl_subscribe_form] .foot-form").append("<div id='closeButton'><img class='closebtn' src='<?php echo get_template_directory_uri(); ?>/assets/images/close-big2.png'></div>");
			return false;
		}

		// Handling the ajaxified save of subscribe email
		jQuery.ajax({
			url: '<?php echo site_url('/wp-content/plugins/newsletter/do/subscribe.php'); ?>',
			type: "post",
			async: false,
			data: { ne: f.elements["ne"].value },
			success: function(d) {  
                                $("form[name=nl_subscribe_form] .foot-form label").hide();
				$("form[name=nl_subscribe_form] .foot-form span").hide();
                                $( "form[name=nl_subscribe_form] .foot-form p.subscription" ).remove();
                                $( "form[name=nl_subscribe_form] .foot-form div#closeButton" ).remove();
                                if (d.indexOf('already subscribed') > -1) {
                                  $("form[name=nl_subscribe_form] .foot-form").prepend("<p class='subscription' style='<?php if(is_front_page()) { echo 'margin-top:0px ! important;'; } ?>'>You have already subscribed to the Newsletter.</p>");
				} else {
                                  $("form[name=nl_subscribe_form] .foot-form").prepend("<p class='subscription' style='<?php if(is_front_page()) { echo 'margin-top:0px ! important;'; } ?>'>Thank You. You are now subscribed to our Newsletter.</p>");
				}
				$("form[name=nl_subscribe_form] .foot-form ").append("<div id='closeButton'><img class='closebtn' src='<?php echo get_template_directory_uri(); ?>/assets/images/close-big2.png'></div>");
				f.elements["ne"].value = ''; 
			}
		});
	}
}

$(document).on('click', "div#closeButton", function(){
	$("form[name=nl_subscribe_form] .foot-form .subscription").remove();
	$("form[name=nl_subscribe_form] .foot-form label").show();
	$("form[name=nl_subscribe_form] .foot-form span").show();

	$(".newsletter-btn").hover(function(){
		$(".arrowbtn").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 92.273%");
	},function(){
		$(".arrowbtn").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 35.273%");
	});
	$(this).remove();
});

//]]>
</script>
<!--
<div class="foot-top-right">
<form name="nl_subscribe_form" style="margin: 0px ! important;" method="post" action="<?php echo site_url('/wp-content/plugins/newsletter/do/subscribe.php'); ?>" onsubmit="return newsletter_check(this)">
<?php wp_nonce_field('action_nl_subscribe_form', 'name_nl_subscribe_form'); ?>
	<div class="foot-form" style="margin-bottom:0;margin-top:0;">
		<span class="newlet_left">
			<label class="control-label" for="inputEmail1">Sign Up for Newsletter</label>
		</span>
		<span class="newlet_right">
			<input type="email" placeholder="Please enter an email address" id="inputEmail1" class="form-control-nl" name="ne" required maxlength="45">
			<a href="#" class="newsletter-btn" onclick="newsletter_check(document.nl_subscribe_form); return false;"></a>
		</span>
	</div>
</form>
</div>
-->

<script type="text/javascript">
$('form[name=nl_subscribe_form]').on('submit', function() {
	return false;
});
</script>

<!-- / News Letter  -->

<?php
if(is_front_page()) {
	$marginTop = '0px';
	$subMRight = '25px';
	$subMTop = '-40px';
	$subPad = '15px';
} else {
	$marginTop = '-15px';
	$subMRight = '10px';
	$subMTop = '-53px';
	$subPad = '30px';
}
?>

<style type="text/css">
.subscription{
	
	padding: <?php echo $subPad; ?>;
	font-size:13px;
	color:#fff;
}
.foot-form {
    margin-bottom: 15px;
    margin-top: <?php echo $marginTop; ?>;
}
.closebtn {
margin-top: <?php echo $subMTop; ?>;
margin-right: <?php echo $subMRight; ?>;
float: right;
cursor: pointer;
}

</style>