<?php 
define("CSS", "faq_mobile.css"); 

global $amc_urls;

get_header('mobile'); ?>

<div class="row">      
       <div class="col-md-12 faqbanner">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/faq.png">
        </div>  
    </div>

 <!-- Middle Mennu  -->
        <div class="row menu_pos">
        <div class="leftarrow-inner SlideOff"></div>
             <div class="aboutmenu-container">
                 <?php require_once('middle-menu.php'); ?>
             </div>
    </div>
     <!--/  Ends Middle Mennu  -->
 <div class="row">
	 <div class="faqcontent-container">
		<div class="col-md-10 col-md--offset-2">
		  <p class="faq-caption">Below is a list of the questions most frequently asked by our customers. <br />If you do not find the answer to your question below please contact us at <?php echo $amc_urls['contact_number']; ?>.
		  </p>                                
		</div>
	 </div>
</div>    

<div class="row">
<div class="col-md-12 faq-mob-cont">
<div class="panel-group" id="accordion">

<?php
$ctr = 1;
$args = array(
	'orderby' => 'name',
	'order' => 'ASC',
	'taxonomy'  => 'faq-topic',
	'hide_empty' => 0,
	'hierarchical' => 1
);
$faq_categories = get_categories($args);
foreach($faq_categories as $faq) {
?>

<div class="panel panel-default"><a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $faq->term_id; ?>" class="panelAccordTitle<?php echo ($ctr != 1) ? ' collapsed' : ''; ?>"><span class="panel-heading"><span class="panel-title"><?php echo $faq->name; ?></span></span></a>

<div id="collapse<?php echo $faq->term_id; ?>" class="panel-collapse collapse<?php echo ($ctr == 1) ? ' in' : ''; ?>">
<div class="panel-body">

<?php
$args = array( 'orderby' => 'menu_order', 'order' => 'asc', 'post_type' => 'question', 'posts_per_page' => -1,'faq-topic' => $faq->slug);
$myposts = get_posts( $args );  
foreach( $myposts as $post ) {
	setup_postdata($post);
?>

<p class="question"><strong>Q: <?php the_title(); ?></strong></p>
<p class="ans">A: <?php echo $post->post_content; ?></p>

<?php } ?>

</div>
</div>
</div>
<?php
	++$ctr;
}
?>

</div>
</div>  
</div>  
<script type="text/javascript">
var mq = window.matchMedia( "(max-width: 767px)" );
if (mq.matches) {
	var element ='<div class="col-md-4 back-btn"> <a href="<?php bloginfo('url'); ?>"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/back_btn.png">Back </a></div>'; 
	$('.row.headersession').append(element); 
} else {
	$(".back-btn").detach();
}
</script>
<?php   get_footer('mobile'); ?>