<?php 
/* 
Template Name: Aboutus template 
*/ 
define("CSS", "about.css");

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}
?>
<div class="row">      
        <div class="col-md-12 aboutbanner"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/About-us-new.jpg"></div>  
    </div>

 <!-- Middle Mennu  -->
        <div class="row menu_pos">
        <div class="leftarrow-inner SlideOff"></div>
             <div class="aboutmenu-container">
               <?php require_once('middle-menu.php'); ?>
             </div>
    </div>
     <!--/  Ends Middle Mennu  -->
        <div class="row">
             <div class="aboutcontent-container">
                <div class="col-md-12 ">

                       <!-- Dynamic  contents comes From the  AdminSide  -->   
           <?php if (have_posts()) : while (have_posts()) : the_post();?>
              <?php the_content(); ?>
              <?php endwhile; endif; ?>

                       <!-- Dynamic contents comes From the  AdminSide -->   
              

                
                               
              </div>
            </div>
    </div> 



<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>    