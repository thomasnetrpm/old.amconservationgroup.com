<?php 
/* 
Template Name: Forget password template 
*/ 

define("CSS", "forget_password.css");

redirectLoggedInUser();

global $wpdb, $amc_urls, $show_msg, $success_msg, $error_msg;
$show_msg = 'none';

// Handle the password reset action
$fUId = sanitize_text_field($_GET['uid']);
$fFwdKey = sanitize_text_field($_GET['fwd_key']);
if(
	isset($fUId) 
	&& isset($fFwdKey) 
	&& ((INT)($fUId) > 0) 
	&& ($fFwdKey != '')
) {
	$fwdUserData = $wpdb->get_row('SELECT * FROM wp_customers WHERE id = "'.$fUId.'" and fwd_key = "'.$fFwdKey.'"');
	if(empty($fwdUserData)) {
		redirectUnLoggedUser();
	} else {
		$newPwd = wp_generate_password(12, false);
		$wpdb->update('wp_customers', array(
			'password' => wp_hash_password($newPwd)
		), array(
			'id' => $fUId
		));

		// @to-do: sent the new password mail
		$newPwdMsg = 'Hi '.ucwords($fwdUserData->firstname).' '.ucwords($fwdUserData->lastname).'<br /><br />';
		$newPwdMsg .= 'Your new password, <br />';
		$newPwdMsg .= $newPwd.'<br /><br />';
		$newPwdMsg .= 'Regards<br />';
		$newPwdMsg .= $amc_urls['mail_regards']; // $newPwdMsg .= get_bloginfo('name');

		$mailHeaders = 'From: '.$amc_urls['mail_from_name'].' <'.get_bloginfo('admin_email').'>' . "\r\n";

		add_filter( 'wp_mail_content_type', 'set_html_content_type' );
		$mailStatus = wp_mail($fwdUserData->email, get_bloginfo('name').' - New password', $newPwdMsg, $mailHeaders);
		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

		if($mailStatus) {
			
		} else {
			// 
		}
		wp_safe_redirect($amc_urls['register_login']);
		exit;
	}
}

// Handle the form submission
$form_errors = array();
$user_errors = array();
$success_msg = array();

if(isset($_POST['fgtPwdForm_submit']) && ($_POST['fgtPwdForm_submit'] == 'SUBMIT') && isset($_POST['name_fgtPwdForm']) && wp_verify_nonce($_POST['name_fgtPwdForm'],'action_fgtPwdForm')) {
	// echo '<pre>'; print_r($_POST); echo '</pre>';

	if(sanitize_text_field($_POST['fgtPwdForm_email']) == '') {
		$form_errors['fgtPwdForm_email'] = 'Please enter email address';
	} else {
		if(!is_email(sanitize_text_field($_POST['fgtPwdForm_email']))) {
			$form_errors['fgtPwdForm_email'] = 'Please enter valid email address';
		}
	}

	if(empty($form_errors)) {
		$userData = $wpdb->get_row('SELECT * FROM wp_customers WHERE email = "'.sanitize_text_field($_POST['fgtPwdForm_email']).'"');

		if(empty($userData)) {
			$user_errors[] = 'User does not exist.';
		} else {
			if($userData->is_active == 0) {
				$user_errors[] = 'User is not active. Please activate.';
			} else {
				$fwdKey = wp_generate_password(12, false);
				// $fwdURL = $amc_urls['forgot_password'].'?uid='.$userData->id.'&fwd_key='.$fwdKey;
				$fwdURL = $amc_urls['forgot_reset_password'].'?uid='.$userData->id.'&fwd_key='.$fwdKey;

				$result = $wpdb->update('wp_customers', array(
					'fwd_key' => $fwdKey
				), array(
					'email' => sanitize_text_field($_POST['fgtPwdForm_email'])
				));
				if($result) {
					// @to-do: sent the password reset mail
					/*
					$pwdResetMsg = 'Hi '.$userData->firstname.' '.$userData->lastname.'<br /><br />';
					$pwdResetMsg .= 'Please click the below link to reset your password, <br />';
					$pwdResetMsg .= '<a href="'.$fwdURL.'">'.$fwdURL.'</a><br /><br />';
					$pwdResetMsg .= get_bloginfo('name');
					*/

					$pwdResetMsg = 'Hi '.ucwords($userData->firstname).' '.ucwords($userData->lastname).','."<br /><br />";
					$pwdResetMsg .= 'AM Conservation Group received a request to reset the password to your account. To reset your password, click on the link below or copy and paste the URL into a browser.'."<br /><br />";
					$pwdResetMsg .= '<a href="'.$fwdURL.'">'.$fwdURL.'</a>'."<br /><br />";
					$pwdResetMsg .= 'In case you didn\'t request this change, please ignore this Email.'."<br /><br />";
					$pwdResetMsg .= 'Regards'."<br />";
					$pwdResetMsg .= $amc_urls['mail_regards']; // 'AM Conservation Group Team';

					// $mailHeaders = 'From: '.get_bloginfo('name').' <'.get_bloginfo('admin_email').'>' . "\r\n";
					$mailHeaders = 'From: '.$amc_urls['mail_from_name'].' <'.get_bloginfo('admin_email').'>' . "\r\n";

					add_filter( 'wp_mail_content_type', 'set_html_content_type' );
					$mailStatus = wp_mail(
						$userData->email, 
						'Reset your AM Conservation Group account password.', // get_bloginfo('name').' - Password reset', 
						$pwdResetMsg, 
						$mailHeaders);
					remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

					if($mailStatus) {
						$success_msg[] = 'Password reset mail sent to you.';
					} else {
						$user_errors[] = 'Unable to send password reset mail. Try, again.';
					}
				}

			}
		}
	}
}

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}
?>

<!--    content area start      -->
<div class="row bgfill ">
           <div class=" topspacer">
                    <div class="contact-form-container"> 
					<div class="container">
                            <div class="row content">  
                           <div class="col-md-6 col-lg-4 col-sm-6 col-sm-offset-3 col-md-offset-3 col-lg-offset-4">
                           
                             
                              
<?php
if(!empty($form_errors)) {
	/*
	echo '<p>Some errors found.</p>';
		echo '<ol>';
		foreach($form_errors as $form_errors_item) {
			echo '<li>'.$form_errors_item.'</li>';
		}
		echo '</ol>';
	*/
	$show_msg = 'block';
	$error_msg = 'Please enter a valid email address.'; // 'Errors found. Try, again.';
}
if(!empty($user_errors)) {
	/*
	echo '<p>Some errors found.</p>';
		echo '<ol>';
		foreach($user_errors as $user_errors_item) {
			echo '<li>'.$user_errors_item.'</li>';
		}
		echo '</ol>';
	*/
	$show_msg = 'block';
	$error_msg = 'Please enter a valid email address.'; // 'Errors found. Try, again.';
}
if(!empty($success_msg)) {
	/*
	echo '<p>Success.</p>';
		echo '<ol>';
		foreach($success_msg as $success_msg_item) {
			echo '<li>'.$success_msg_item.'</li>';
		}
		echo '</ol>';
	*/
	$show_msg = 'block';
	// $success_msg = 'Password reset mail sent to you.';
	$success_msg = 'We\'ve sent the password reset instructions to your email address. Please click on the link in the email to reset your password. 
';
}
?>
<form role="form" method="post" action="" name="fgtPwdForm" parsley-validate novalidate>
<?php wp_nonce_field('action_fgtPwdForm', 'name_fgtPwdForm'); ?>

<div class="form-group">

	<p class="fp-parag-txt"> Please enter your email and we'll send you a new password</p>

	<?php include('successmodal.php'); ?>

	
		<input type="email" name="fgtPwdForm_email" class="form-control parsley-validated" id="exampleInputEmail1" placeholder="Email Address*" required parsley-error-message="Please enter a valid email address" parsley-trigger="change" />

		<div class="row offset-top-1"> 
			<div class="col-md-4 col-sm-4 col-xs-4 back-btn forgrtpass"><a onclick="history.back();"><img src="<?php echo get_template_directory_uri().'/assets/images/back-btn-arrow.png'; ?>">  Back </a></div>
			<div class="col-md-8 col-sm-8 col-xs-8 text-align1 " ><div class=" ">
				<input type="submit" name="fgtPwdForm_submit" class="btn fp-submit-btn " value="SUBMIT" />
			</div></div>
		</div> 
<div class="row">
		<div class="col-md-12"><p class="fp-parag-txt">  *Required Fields </p></div></div>
	
</div>
</form>
                                </div>
                              </div>
                             
                             </div>
                    
   
         </div> 
        </div>
		</div>
       
         <div class="row bgfill content">
            
           <div class="  topspacer">
             
         </div> 
         
         </div>
         
         <div class="row bgfill "> <div class="space-above-footer"> </div> </div>
  
  <!--      content area end            -->
  

  
<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>