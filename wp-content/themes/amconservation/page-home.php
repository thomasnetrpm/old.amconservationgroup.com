<?php 
/* 
Template Name: Home
*/ 

 

?>

<?php get_header(); ?>
		
		<div class="row">
        <div class="col-md-12">

	<div class="slider">
<?php
global $theme_options;
// Get Slides
$slides = array();
foreach ($theme_options['slider']['title'] as $k => $v) {
	$slides[] = array(
		'title' => $v,
		'link' => $theme_options['slider']['link'][$k],
		'caption' => $theme_options['slider']['caption'][$k],
		'image' => $theme_options['slider']['image'][$k],
		'button' => $theme_options['slider']['button'][$k]
	);
}
?>
		<div class="flexslider">
			<ul class="slides">
<?php foreach ( $slides as $slide ) { ?>
				<li><img src="<?php echo $slide['image'] ?>" alt="slider image"/></li>
<?php } ?>
			</ul>
		</div>
	</div>

        </div>
      </div>
	  
	  <div class="row">
          <div class="car-menu">
            <div class="menusession">
               <div class="menu-container">
                   <ul class="carmenus">
                    <li class="menu"><a href="#"><div class="caro-menulink">About AMCG</div></a></li>
                    <li class="menu"><a href="#"><div class="caro-menulink">News</div></a></li>
                    <li class="menu"><a href="#"><div class="caro-menulink">Conservation Resources</div></a></li>
                    <li class="menu"><a href="#"><div class="caro-menulink">Case Studies</div></a></li>
                    <li class="menu"><a href="#"><div class="caro-menulink">Testimonials</div></a></li>
                    <li class="menulast"><a href="#"><div class="caro-menulink">Blog</div></a></li>
               </div>
            </div>  
          </div>
      </div>

	  <div class="row">
          <div class="am_conversion">
              <div class="am-container">
              <h2 class="am-header"> AM Conservation Group</h2><span class="dot"></span>
              <p class="yourone-txt">Your one source for energy and water efficiency products, services, and programs of any size 
  with unmatched speed and flexibility. </p>
              <div class="bs-example twinbtn">
                <button type="button" class="btn btn-default home"><span>Home Owner Click Here </span> <span class="arrowicon"></span></button>
                <button type="button" class="btn btn-default professional"><span>Professionals/Businesses Click Here</span><span class="arrowicon"></span></button>              
              </div>
          </div>
      </div>
    </div>

	<div class="row">
      <div class="programs-container">
      </div>
    </div> 

<?php get_footer(); ?>