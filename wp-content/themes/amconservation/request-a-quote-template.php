<?php 
/* 
Template Name: Request a quote  template 
*/

/*
Array
(
    [reqQuoteFrm_name] => Raphel
    [reqQuoteFrm_company] => Raphel Inc.,
    [reqQuoteFrm_email] => raphel@raphelinc.com
    [reqQuoteFrm_phone] => 1.100.101.1234
    [reqQuoteFrm_ext] => 123
    [reqQuoteFrm_country] => US/CA/AF
    [reqQuoteFrm_state_us] => 19
    [reqQuoteFrm_state_ca] => 69
    [reqQuoteFrm_zipcode] => 12112
    [reqQuoteFrm_howfind] => Twitter
    [reqQuoteFrm_comments] => Hi..
    [reqQuoteFrm_prefcont] => Phone
    [reqQuoteFrm_acctreptxt] => Phil DilBetta
    [reqQuoteFrm_acctrep] => Phil DilBetta
    [reqQuoteFrm_prodssers] => Products
    [tmpPName] => 347
    [tmpPNo] => 347
    [tmpQty] => 2
    [tmpService] => Database Management
    [reqQuoteFrm_prodNameList] => Array
        (
            [0] => Candelabra Light Bulb Socket Adapter
            [1] => R-Series Compact Fluorescent Flood Light Bulbs - ENERGY STAR® Approved
        )

    [reqQuoteFrm_prodNoList] => Array
        (
            [0] => z-lt-20797
            [1] => R-SERIES-FLUORESCENT-FLOOD-LIGHTS
        )

    [reqQuoteFrm_prodQtyList] => Array
        (
            [0] => 9
            [1] => 2
        )

    [reqQuoteFrm_serviceList] => Array
        (
            [0] => Efficiency Program Management
            [1] => Direct Install Fulfillment
            [2] => Database Management
        )

)
*/

?>

<?php
global $wpdb;

if(userIsLoggedIn()) {
	$loggedInUser = getLoggedInUser();
}

// Handle the form submission
$form_errors = array();
$user_errors = array();
$success_msg = array();
$result = '';

// Validate the $_POST
if(!empty($_POST)) {
	if(sanitize_text_field($_POST['reqQuoteFrm_name']) == '') {
		$form_errors[] = 'Please enter your name';
	}

	if(sanitize_text_field($_POST['reqQuoteFrm_company']) == '') {
		$form_errors[] = 'Please enter your company';
	}

	if(sanitize_text_field($_POST['reqQuoteFrm_email']) == '') {
		$form_errors[] = 'Please enter your email';
	}
	
	/*if(sanitize_text_field($_POST['reqQuoteFrm_marketSegment']) == '') {
		$form_errors[] = 'Please select market segment';
	}*/
	
	if(sanitize_text_field($_POST['reqQuoteFrm_phone']) == '') {
		$form_errors[] = 'Please enter a valid phone number';
	}

	if(sanitize_text_field($_POST['reqQuoteFrm_zipcode']) == '') {
		$form_errors[] = 'Please enter zip code';
	}

	if((sanitize_text_field($_POST['reqQuoteFrm_howfind']) == '') || (sanitize_text_field($_POST['reqQuoteFrm_howfind']) == 'How did you find us?*')) {
		$form_errors[] = 'Please select how did you found us?';
	}
}

if(empty($form_errors) && !(empty($_POST))) {
	$stateStr = 0;
	$prodItemsStr = '';
	$serviceItemsStr = '';

	/*
	if(sanitize_text_field($_POST['reqQuoteFrm_country']) == 'US') {
		$stateStr = sanitize_text_field($_POST['reqQuoteFrm_state_us']);
	} else if(sanitize_text_field($_POST['reqQuoteFrm_country']) == 'CA') {
		$stateStr = sanitize_text_field($_POST['reqQuoteFrm_state_ca']);
	}
	*/
	$stateStr = sanitize_text_field($_POST['reqQuoteFrm_state']);
	if(empty($stateStr)) {
		$stateStr = 0;
	}

	$c = 0;
	$prodNameList = $_POST['reqQuoteFrm_prodNameList'];
	$prodNoList = $_POST['reqQuoteFrm_prodNoList'];
	$prodQtyList = $_POST['reqQuoteFrm_prodQtyList'];
	$prodMailStr = '';
	if(!empty($prodNameList) && is_array($prodNameList)) {
		foreach($prodNameList as $prodNameListItem) {
			$prodItemsStr['Products'][$c]['name'] = sanitize_text_field($prodNameListItem);
			$prodItemsStr['Products'][$c]['sku'] = sanitize_text_field($prodNoList[$c]);
			$prodItemsStr['Products'][$c]['qty'] = sanitize_text_field($prodQtyList[$c]);
			$prodMailStr .= 'Product: '.$prodItemsStr['Products'][$c]['name'].'<br />'.'Qty: '.$prodItemsStr['Products'][$c]['qty'].'<br /><br />';
			$c++;
		}
		$prodItemsStr = serialize($prodItemsStr);
	}

	$servMailStr = '';
	$serviceItemsAry = $_POST['reqQuoteFrm_serviceList'];
	if(!empty($serviceItemsAry) && is_array($serviceItemsAry)) {
		$serviceItemsStr = serialize($serviceItemsAry);
		$servMailStr .= implode(', ', $serviceItemsAry);
	}

	$commentStr = sanitize_text_field($_POST['reqQuoteFrm_comments']);
	if(empty($commentStr)) {
		$commentStr = 'n/a';
	}

	$requestQuoteAry = array(
		'created' => current_time('mysql'), 
		'modified' => current_time('mysql'),
		'username' => sanitize_text_field($_POST['reqQuoteFrm_name']),
		'company' => sanitize_text_field($_POST['reqQuoteFrm_company']),
		'email' => sanitize_text_field($_POST['reqQuoteFrm_email']),
		'marketsegment' => sanitize_text_field($_POST['reqQuoteFrm_marketSegment']),

		'phone' => sanitize_text_field($_POST['reqQuoteFrm_phone']),
		'phoneext' => sanitize_text_field($_POST['reqQuoteFrm_ext']),
		'country' => sanitize_text_field($_POST['reqQuoteFrm_country']),
		'state' => $stateStr,
		'zipcode' => sanitize_text_field($_POST['reqQuoteFrm_zipcode']),

		'howfind' => sanitize_text_field($_POST['reqQuoteFrm_howfind']),
		'comment' => $commentStr, // sanitize_text_field($_POST['reqQuoteFrm_comments']),
		'prefcont' => sanitize_text_field($_POST['reqQuoteFrm_prefcont']),
		'acctrep' => sanitize_text_field($_POST['reqQuoteFrm_acctreptxt']),

		'productitems' => $prodItemsStr,
		'serviceitems' => $serviceItemsStr
	);

	if(userIsLoggedIn()) {
		$requestQuoteAry['userid'] = $loggedInUser->id;
	}

	$ins_ID = $wpdb->insert(
		'wp_requestquote', 
		$requestQuoteAry
	);

	if($ins_ID) {

// Mail request a quote information to admin
$reqQuoteMsg = 'Name: '.sanitize_text_field($_POST['reqQuoteFrm_name'])."<br />";
$reqQuoteMsg .= 'Company: '.sanitize_text_field($_POST['reqQuoteFrm_company'])."<br />";
$reqQuoteMsg .= 'Email: '.sanitize_text_field($_POST['reqQuoteFrm_email'])."<br />";
$reqQuoteMsg .= 'Market Segment: '.sanitize_text_field($_POST['reqQuoteFrm_marketSegment'])."<br />";
$reqQuoteMsg .= 'Phone: '.sanitize_text_field($_POST['reqQuoteFrm_phone']).' Extn: '.sanitize_text_field($_POST['reqQuoteFrm_ext'])."<br />";
$reqQuoteMsg .= 'Country: '.sanitize_text_field($_POST['reqQuoteFrm_country'])."<br />";
// $reqQuoteMsg .= 'State: '.$stateStr."<br />";
$reqQuoteMsg .= 'Zip code: '.sanitize_text_field($_POST['reqQuoteFrm_zipcode'])."<br />";
$reqQuoteMsg .= 'How did you find us?: '.sanitize_text_field($_POST['reqQuoteFrm_howfind'])."<br />";
$reqQuoteMsg .= 'Comment: '.$commentStr."<br />";
$reqQuoteMsg .= 'Preferred contact: '.sanitize_text_field($_POST['reqQuoteFrm_prefcont'])."<br />";
$reqQuoteMsg .= 'Account representative: '.sanitize_text_field($_POST['reqQuoteFrm_acctreptxt'])."<br />";
$reqQuoteMsg .= $prodMailStr;
$reqQuoteMsg .= 'Services: '.$servMailStr."<br />";


$mailHeaders = 'From: '.$amc_urls['mail_from_name'].' <'.get_bloginfo('admin_email').'>' . "\r\n";

add_filter( 'wp_mail_content_type', 'set_html_content_type' );
$mailStatus = wp_mail(
	get_option('req_a_quote_email_field'), 
	'AM Conservation - Request a quote', 
	stripslashes($reqQuoteMsg), 
	$mailHeaders
);
remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

$confirmation_message = "<p> Thank you for requesting a quote for products from AM Conservation Group, Inc. and submitting all necessary information. An account representative will be in touch with you soon with further details and pricing information for your quote. </p>
<p> By choosing AM Conservation Group for your energy and water conservation needs, you will have access to a dedicated program manager who will work with you during the order and fulfillment process for this order and future orders. In addition, you can also receive assistance in creating a customized program design to achieve your specific goals with a diverse selection of energy and water saving products, competitive pricing, concierge-level customer service and reports to track the results of your order. We strive to provide the highest value for your investment. </p> 
<p> Thank you for your interest in AM Conservation Group products and services. </p> <br />
<p> AM Conservation Group Team </p>";

add_filter( 'wp_mail_content_type', 'set_html_content_type' );
$mailStatus = wp_mail(
	sanitize_text_field($_POST['reqQuoteFrm_email']), 
	'AM Conservation - Request a quote', 
	stripslashes($confirmation_message), 
	$mailHeaders
);
remove_filter( 'wp_mail_content_type', 'set_html_content_type' );



		$success_msg[] = 'Thank you for requesting a quote. We have sent you an email confirmation.'; // 'Successfully requested a quote.';
		$result = array(
			'status' => 'success',
			'message' => 'Thank you for requesting a quote. We have sent you an email confirmation.' // 'Successfully requested a quote.'
		);		

if(userIsLoggedIn()) {
	// Update the extra user data for logged in user
	$wpdb->update('wp_customers', array(
		'companyname' => sanitize_text_field($_POST['reqQuoteFrm_company']),
		'usercountry' => sanitize_text_field($_POST['reqQuoteFrm_country']),
		'userregion' => $stateStr,
		'userzip' => sanitize_text_field($_POST['reqQuoteFrm_zipcode']),
		'userphone' => sanitize_text_field($_POST['reqQuoteFrm_phone']),
		'userext' => sanitize_text_field($_POST['reqQuoteFrm_ext'])
	), array(
		'id' => $loggedInUser->id
	));
}

// Forming the product array for SageCRM
$sCRMProdArr = array();

if(!empty($prodNameList)) {
	$sCRMCtr = 0;
	foreach($prodNameList as $prodNameListItem) {
		$sCRMProdArr[$sCRMCtr]['product_name'] = $prodNameListItem;
		$sCRMProdArr[$sCRMCtr]['product_sku'] = $prodNoList[$sCRMCtr];
		$sCRMProdArr[$sCRMCtr]['product_qty'] = $prodQtyList[$sCRMCtr];
		$sCRMCtr++;
	}
}

//@to-do: SageCRM post
$dataSageCRM = array(
	"account_rep_name" => sanitize_text_field($_POST['reqQuoteFrm_acctreptxt']),
	"comment"   => $commentStr, // sanitize_text_field($_POST['reqQuoteFrm_comments']),
	"company" => strip_tags(sanitize_text_field($_POST['reqQuoteFrm_company'])),
	"email" => sanitize_text_field($_POST['reqQuoteFrm_email']),
	"marketsegment" => sanitize_text_field($_POST['reqQuoteFrm_marketSegment']),
	"extension" => sanitize_text_field($_POST['reqQuoteFrm_ext']),
	"name" => sanitize_text_field($_POST['reqQuoteFrm_name']),
	"preferred_contact" => sanitize_text_field($_POST['reqQuoteFrm_prefcont']),
	"telephone" => sanitize_text_field($_POST['reqQuoteFrm_phone']),
	"country" => sanitize_text_field($_POST['reqQuoteFrm_country']),
	"how_did_you_find_us" => sanitize_text_field($_POST['reqQuoteFrm_howfind']),
	"products" => $sCRMProdArr,
	"region_id" => $stateStr,
	"services" => $_POST['reqQuoteFrm_serviceList'],
	"zip" => sanitize_text_field($_POST['reqQuoteFrm_zipcode'])
);
$jsonSageCRM = json_encode($dataSageCRM);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'http://98.124.78.27/CrmWebServices/AMCGWebsiteToCrmService.svc/AddRequestAQuoteSubmission');
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonSageCRM);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
	'Content-Type: application/json',                                                                                
	'Content-Length: ' . strlen($jsonSageCRM))                                                                       
);
$resultX = curl_exec($ch);
unset($_POST);

	} else {
		$user_errors[] = 'Some error happened. Try, again.';
		$result = array(
			'status' => 'error',
			'message' => 'Some error happened. Try, again.'
		);
	}
} else {
	$result = array(
		'status' => 'error',
		'message' => 'Some error happened. Try, again.'
	);
}

echo json_encode($result);

exit();