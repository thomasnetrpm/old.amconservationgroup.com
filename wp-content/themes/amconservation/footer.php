</div>
<div class="page-sticky-push">

</div>
</div>
<!-- Google Analytics Tracking Code -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6700265-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- webtraxs Code -->
<script type="text/javascript">
document.write(unescape("%3Cscript src='" + document.location.protocol + "//www.webtraxs.com/trxscript.php' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
_trxid = "amconservationgroup";
webTraxs();
</script>
<noscript><img src="http://www.webtraxs.com/webtraxs.php?id=amconservationgroup&st=img" alt=""></noscript>

<!-- Start of Async HubSpot Analytics Code -->
  <script type="text/javascript">
    (function(d,s,i,r) {
      if (d.getElementById(i)){return;}
      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/448562.js';
      e.parentNode.insertBefore(n, e);
    })(document,"script","hs-analytics",300000);
  </script>
<!-- End of Async HubSpot Analytics Code -->
<!-- footer starts-->
<div class="container-foot page-footer">
	<!--footer shadow -->
	<div class="row">
      <div class="col-md-12  horizontal-line-divider"></div>    
    </div> 
	<?php global $theme_options, $amc_urls; ?>
	<!-- footer top bar starts-->
	<div class="row page-footer-top">
		<div class="foot-top">
			<!--footer top left starts-->
			<div class="col-md-12 foot-top-left">
				<ul class="foot-social">
					<li><a href="<?php echo $amc_urls['pinterest']; ?>" target="_blank" class="pin-icon"></a></li>
					<li><a href="<?php echo $amc_urls['twitter']; ?>" target="_blank" class="twi-icon"></a></li>
					<li><a href="<?php echo $amc_urls['facebook']; ?>" target="_blank" class="fb-icon"></a></li>
				</ul>
				<ul class="foot-contact">
					<li><span class="f-cont-icon"></span><?php echo $amc_urls['contact_number']; ?></li>
					<li><a href="<?php echo $amc_urls['request_a_catalog']; ?>"><span class="f-quote-icon"></span>Request a Catalog</a></li>
				</ul>
				
				<!--footer top right starts-->
				<!-- News Letter AMCG -->
					<?php require_once('newsletter-box.php'); ?>
				<!-- / News Letter ends -->
				
				<!--footer top right ends-->
			
			
			</div>
			<!--footer top left ends-->
			
		</div>
	</div>
	<!-- footer top bar ends -->
	<!-- footer bottom bar starts-->
	<div class="row page-footer-bottom">
		<div class="foot-bottom">
			<!--footer bottom left starts-->
			<div class="col-md-12 foot-bot-left">
			   

			   <?php  wp_nav_menu( array( 'theme_location' => 'footer-menu',
									   'container_class' => '',
									   'container_id'    => '',
									   'menu_id'         => '' ));    ?>
			   
			   <script type="text/javascript">
				   jQuery('.foot-bot-left ul.menu li:last').addClass('border-none');
				<?php if(userIsLoggedIn()) { ?>
					jQuery('.foot-bot-left ul.menu').prepend('<li class="foonav"><a href="<?php echo $amc_urls['image_gallery']; ?>">Image Gallery</a></li>');
				<?php } ?>
			  </script>
			</div>
			<!--footer bottom left ends-->
			<!--footer bottom right starts-->
			<div class="col-md-12 foot-bot-right">
				<p class="foot-copy">Copyright <?php echo date("Y"); ?> AM Conservation Group</p>
			</div>
			<!--footer bottom right ends-->
		</div>
	</div>
	<!-- footer bottom bar ends-->
<!-- footer ends-->
</div>	
   <!-- Hidden Div Width In IE 8 -->
   <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.actual.min.js"></script>
   <script src="<?php echo get_template_directory_uri(); ?>/assets/js/placeholder.js"></script>
   <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.columnizer.js"></script>
   
   <!-- / Hidden Div Width In IE 8 -->
   <script type="text/javascript">
       // Site Map Column //
         callcolumnizer();
            function callcolumnizer() {
                screenwidth = $(window).width();

                $('.thin').columnize({ width: 300, buildOnce: false });
            }
       
		// Handle the search click
		$('.js_handleSearch').on('click', function() {
			if($('input[name=s].js_searchBox').val() != undefined && $('input[name=s].js_searchBox').val() != '' && ($('input[name=s].js_searchBox').val() != '<?php echo get_search_query(); ?>')) {
				$('form[name=headerSearchFrm]').submit();
				
			}
		});
		//top search
		function footFindHeight(){
			var footHeight = parseInt($('.page-footer').height());
			//console.log(footHeight);
			$('.page-footer, .page-sticky-push').css({'height':0});
			$('.page-footer, .page-sticky-push').css({'height':footHeight});
			$('.wrapper').css({'margin-bottom':-footHeight});

		}
		$(window).load(function(){
			$('.top-search.search').click(function(){
				$(this).find('.search-arrow').fadeToggle("fast");
				$(this).next('.search-panel').fadeToggle("fast");
			});
			
			$('.page-footer, .page-sticky-push').removeAttr('style');
			//footFindHeight();
			
			
		});
		//sticky footer ---- find height on height on reisize
		$(window).resize(function() {
			
			
			//footFindHeight();
		});
    </script>
   <script type="text/javascript">
		
			
     // $(".breadcrumb > li").hide();
      $(window).load(function(){
			alignProductlistHeight(); 
			$(".top-bar li:last-child").addClass('last');
      resizeTable();
      });
      // For listing page only  //
      function resizeTable() {
       $('.table-prddisplay').each(function () {
                $(this).find('tr:odd').addClass("odd");
                $(this).find('tr:even').addClass("even");
				
				var columnCount = $(this).attr('data-val');
				var totcountWidth = parseInt(65/columnCount);
				
				$(this).find('td').css('width',totcountWidth + '%');
            })
           
            $(".table-prddisplay > tbody > tr > td:first-child, .table-prddisplay > thead > tr > th:first-child").addClass("first");
            $(".table-prddisplay > tbody > tr > td:last-child, .table-prddisplay > thead > tr > th:last-child").addClass("last");
	
      }
     /* $(".menu1").hover(function(){
                $(".top-icon").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat -3.567% 86.273%");
                $(".toplink1").css("color","#f4911e");
                },function(){
                $(".top-icon").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat -3.567% 33.273%");
                $(".toplink1").css("color","#525151");
              });

              $(".menu2").hover(function(){
                $(".top-icon2").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 8.433% 86.273%");
                $(".toplink2").css("color","#f4911e");
                },function(){
                $(".top-icon2").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 8.433% 33.273%");
                $(".toplink2").css("color","#525151");
              });

              $(".menu3").hover(function(){
                $(".top-icon3").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 18.433% 86.273%");
                $(".toplink3").css("color","#f4911e");
                },function(){
                $(".top-icon3").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 18.433% 33.273%");
                $(".toplink3").css("color","#525151");
              });

              $(".search-btn").hover(function(){
                $(".mainmenu-icon6").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 77.433% 90.273%");
                },function(){
                $(".mainmenu-icon6").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 99.433% 50.273%");
              });

              $(".phonelink").hover(function(){
              
                $(".toplink4").css("color","#f4911e");
                },function(){
              
                $(".toplink4").css("color","#525151");
              });
*/

              $(".home").hover(function(){
                $(".arrowicon1").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 62.273%");
                $(".btn-default.home").css("background","#fff");
                },function(){
                $(".arrowicon1").css("background","url('assets/images/spritesheet.png') no-repeat 86.433% 36.273%");
                $(".btn-default.home").css("background","#525151");
              });

              $(".professional").hover(function(){
                $(".arrowicon2").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 62.273%");
                $(".professional").css("background","#fff");
                },function(){
                $(".arrowicon2").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 36.273%");
                $(".professional").css("background","#525151");
              });        

              $(".cataloglink").hover(function(){
              /*  $(".bottom-icon2").css("background","url('assets/images/spritesheet.png') no-repeat 31.38% 87.273%");*/
                $(".toplink5").css("color","#f4911e");
                },function(){
               /* $(".bottom-icon2").css("background","url('assets/images/spritesheet.png') no-repeat 42.433% 34.273%");*/
                $(".toplink5").css("color","#525151");
              });

              /*$(".newsletter-btn").hover(function(){
                $(".arrowbtn").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 92.273%");
                },function(){
                $(".arrowbtn").css("background","url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 35.273%");

              });*/
            

/* _____alignProductlistHeight____ */
$(window).resize(function () {
	alignProductlistHeight()
});
function alignProductlistHeight(){
	$(".product-content2, .product-content3").css("height", $(".product-content1").height()+"px");
}
/* _____alignProductlistHeight____ */ 


$(window).load(function(){
$(".no-touch .top-bar").children('li').last().hover(
  function () {

      var ulwidth = $('.top-prog').width() - 30;

      var submenuWid = 0;
      $(".no-touch .top-bar li.last > ul  > li").each(function () {
          submenuWid = submenuWid + $(this).actual( 'width', { absolute : true });
      });


      var x = $(this).offset().left - submenuWid + ulwidth;

       var text = navigator.appVersion;
      var str = navigator.appVersion;
      var n = str.search(/MSIE 8.0;/i);
      if (n == 17) {
         
          $(".no-touch .top-bar li.last ul").animate({
              'padding-left': x + 100
          }, "normal");
      } else {
          $(".no-touch .top-bar li.last ul").animate({
              'padding-left': x
          }, "normal");
      }
  },
  function () {
      $(".no-touch .top-bar li.last ul").css("padding-left", "");
  }
);

});


$(".no-touch .mainmenu5").hover(
    function(){
        var menuWidth = $(".prog-link").width() + 70 +  $(".energymenu").width() + 18;
        var submenuWid = 0;
        $(".sub-menu5 li").each(function(){
            submenuWid = submenuWid + $(this).actual( 'width' )+6;
        });
		
        var padLeft = menuWidth-submenuWid;
        $(".sub-menu5").animate({
            'padding-left': padLeft
        }, "normal");
    },
    function(){
        $(".sub-menu5").css("padding-left", "");
    }
 );
      $(".select").hover(function(){
          var displaced=$(window).scrollTop()-2;
          $(".select ul").css("top",(150-displaced)+"px")
                }, function(){
                }
            );
      // For Place Holder
      $('input').placeholder();     
	  $('textarea').placeholder();     

	  
$(".collapsableMenu").click(function () {
	$(".staticpages-menu").toggle();
	return false;
});


$(".back-button").click(function () {
	$(".staticpages-menu").toggle();
});

$(".staticpages-menu").on( "mouseenter", function() {
	$(this).show();
}).on("mouseleave", function() {
	$(this).hide();
});
var isIE = document.all && document.addEventListener && !window.atob;
if(isIE){
$('.ips select,.ips2 select,.ips3 select').css({'background':'none','padding':'6px'})
}
/* to make disable hover effect from tab */
		if (!("ontouchstart" in document.documentElement)) {
document.documentElement.className +="no-touch";
}

$(document).click(function () {
	$(".staticpages-menu").hide();
});



 </script>

<script type="text/javascript">
<?php if(is_mobile()) { ?>
$('body').addClass('mobile_dev');
<?php } else { ?>
$('body').addClass('non_mobile_dev');
<?php } ?>
</script>
  </body>
</html>