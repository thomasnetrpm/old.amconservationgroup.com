<?php
global $show_msg, $success_msg, $error_msg;

$alertClass = 'alert-warning';
if(!empty($error_msg)) {
	$alertClass = 'alert-danger';
}

if($show_msg != 'none') {
?>

<div class="js_result alert <?php echo $alertClass; ?> fade in">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<?php
if(!empty($success_msg)) {
	echo $success_msg;
} else if(!empty($error_msg)) {
	echo $error_msg;
}
?>
</div>

<?php
}