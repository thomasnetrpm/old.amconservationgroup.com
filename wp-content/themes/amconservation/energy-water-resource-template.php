<?php 
/* 
Template Name: Energy and Water Template 
*/ 
define("CSS", "energy.css");

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header('shop-amc');
}
?>

 <!-- Title and Breadcrumb  -->
      <div class="row headersession">      
        <div class="col-md-7"><h3 class="headerlabel"><?php the_title(); ?></h3></div>
        <div class="col-md-5 breadcrumbs ">
          <ol class="breadcrumb">
           <?php breadcrumbs_fmg();  ?>
          </ol>
        </div>    
    </div>
     <!--/  Ends Title and Breadcrumb   -->
    <!--    content area start      -->

<div class="row bgfill content">
     
          
         <div class="topspacer">
<?php
$eWAssociates = wpba_get_attachments();
if(!empty($eWAssociates)) {

	foreach($eWAssociates as $eWAssociate) {
		$alt = get_post_meta($eWAssociate->ID, '_wp_attachment_image_alt', true);
		$image_title = $eWAssociate->post_title;
		$eWA_URL = (!empty($eWAssociate->post_excerpt)) ? $eWAssociate->post_excerpt : '#';
?>
<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
	<div class="image-container"><a href="<?php echo $eWA_URL; ?>" target="_blank">
	<img src="<?php echo $eWAssociate->guid; ?>" title="<?php echo $image_title; ?>" alt="<?php echo $alt; ?>"></a></div>
</div>
<?php
	}
}
?>
<?php
/*
<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
	<div class="image-container"><a href="http://www.allianceforwaterefficiency.org/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/energy-associates-1.png"></a></div>
</div>

<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
	<div class="image-container"><a href="http://www.ase.org/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/energy-associates-2.png"></a></div>
</div>

<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
	<div class="image-container"><a href="http://www.publicpower.org/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/energy-associates-3.png"></a></div>
</div>

<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
	<div class="image-container"><a href="http://www.awwa.org/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/energy-associates-4.png"></a></div>
</div>

<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
	<div class="image-container"><a href="http://www.aesp.org/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/energy-associates-5.png"></a></div>
</div>

<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
	<div class="image-container"><a href="#" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/energy-associates-6.png"></a></div>
</div>

<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
	<div class="image-container"><a href="http://www.energystar.gov/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/energy-associates-7.png"></a></div>
</div>

<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
	<div class="image-container"><a href="http://www.esource.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/energy-associates-8.png"></a></div>
</div>


<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
	<div class="image-container"><a href="http://mwalliance.org/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/energy-associates-9.png"></a></div>
</div>

<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
	<div class="image-container"><a href="http://www.nreca.coop/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/energy-associates-10.png"></a></div>
</div>

<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
	<div class="image-container"><a href="http://www.neec.org/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/energy-associates-11.png"></a></div>
</div>

<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
	<div class="image-container"><a href="http://www.nwppa.org/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/energy-associates-12.png"></a></div>
</div>
   

<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
	<div class="image-container"><a href="http://www.seealliance.org/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/energy-associates-13.png"></a></div>
</div>

<div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
	<div class="image-container"><a href="http://www.epa.gov/watersense/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/energy-associates-14.png"></a></div>
</div>
*/
?>

    </div>
           
	
       
       
       
</div>
       
       <div class="row bgfill "> <div class="space-above-footer"> </div> </div>




<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>

<script>
$(window).load(function(){
imageheight();
	
})
$(window).resize(function(){
imageheight();
});

function imageheight(){
$('.image-container').each(function(){
		var currW = $(this).width();
		
		$(this).css({'height':currW + 'px','max-height':'220px'})
		var currH = $(this).height();
		var imgHeight = $(this).find('img').height();
		var reqspace = (currH - imgHeight)/2
		$(this).find('img').css('margin-top',reqspace + 'px')
	});
}
</script>