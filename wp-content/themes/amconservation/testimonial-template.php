<?php 
/* 
Template Name: Testimonial template 
*/ 
define("CSS", "testimonials.css");

// Handle the testimonial add
global $wpdb, $amc_urls;
$form_errors = array();
$sucess_msg = array();
$ins_errors = array();

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	// Handle ajax request of testimonials by year
	$tInputYear = date('Y');
	if(isset($_GET['y']) && !empty($_GET['y'])) {
		$tInputYear = $_GET['y'];

		// Code to get the testimonials
		$tResults = $wpdb->get_results('SELECT message AS content, name AS author FROM wp_vm_testimonials WHERE (status = "A" AND isinpool = "yes" AND (YEAR(created) = '.$tInputYear.')) ORDER BY created DESC', ARRAY_A);
		$tCount = $wpdb->num_rows;

		$tResultsStr = "{data:[";

		if(!empty($tResults) && is_array($tResults)) {
			foreach($tResults as $k => $tResultsItem) {
				$tResultsStr .= "{
					content: '".esc_html($tResultsItem['content'])."',
					author: '".$tResultsItem['author']."'
				}";
				if($k != ($tCount - 1)) {
					$tResultsStr .= ',';
				}
			}
		}

		$tResultsStr .= "]}";
		echo $tResultsStr; exit();
	}

	// Handle the testimonial submission
	if(isset($_POST['name_testimonialFrm']) && wp_verify_nonce($_POST['name_testimonialFrm'],'action_testimonialFrm')) {
		if(sanitize_text_field($_POST['testimonialFrm_name']) == '') {
			$form_errors['status'] = 'error';
			$form_errors['msg'] = 'Please enter name';
			echo json_encode($form_errors);
		} else if(sanitize_text_field($_POST['testimonialFrm_email']) == '') {
			$form_errors['status'] = 'error';
			$form_errors['msg'] = 'Please enter email';
			echo json_encode($form_errors);
		} else if(sanitize_text_field($_POST['testimonialFrm_company']) == '') {
			$form_errors['status'] = 'error';
			$form_errors['msg'] = 'Please enter company';
			echo json_encode($form_errors);			
		} else if(sanitize_text_field($_POST['testimonialFrm_subject']) == '') {
			$form_errors['status'] = 'error';
			$form_errors['msg'] = 'Please enter subject';
			echo json_encode($form_errors);
		} else if(sanitize_text_field($_POST['testimonialFrm_testimonial']) == '') {
			$form_errors['status'] = 'error';
			$form_errors['msg'] = 'Please enter testimonial';
			echo json_encode($form_errors);
		}

		if(empty($form_errors)) {
			$ins_ID = $wpdb->insert('wp_vm_testimonials', array(
				'created' => current_time('mysql'),
				'name' => sanitize_text_field($_POST['testimonialFrm_name']),
				'email' => sanitize_text_field($_POST['testimonialFrm_email']),
				'company' => sanitize_text_field($_POST['testimonialFrm_company']),
				'subject' => sanitize_text_field($_POST['testimonialFrm_subject']),
				'message' => sanitize_text_field($_POST['testimonialFrm_testimonial']),
				'status' => 'P'
			));

			if($ins_ID) {
				$sucess_msg['status'] = 'success';
				$sucess_msg['msg'] = 'Your testimonial has been successfully posted.';
				echo json_encode($sucess_msg);
			} else {
				$ins_errors['status'] = 'error';
				$ins_errors['msg'] = 'Unable to add testimonial. Try, after some time.';
				echo json_encode($ins_errors);
			}
		}
		exit();
	}
}

$tInputYear = date('Y');
if(isset($_GET['y'])) {
	$tInputYear = $_GET['y'];
}

// Code to get the testimonials
$tResults = $wpdb->get_results('SELECT message AS content, name AS author FROM wp_vm_testimonials WHERE (status = "A" AND isinpool = "yes" AND (YEAR(created) = '.$tInputYear.')) ORDER BY created DESC', ARRAY_A);
$tCount = $wpdb->num_rows;

if($tCount == 0) {
	$tInputYear = $tInputYear - 1;
	$tResults = $wpdb->get_results('SELECT message AS content, name AS author FROM wp_vm_testimonials WHERE (status = "A" AND isinpool = "yes" AND (YEAR(created) = '.$tInputYear.')) ORDER BY created DESC', ARRAY_A);
	$tCount = $wpdb->num_rows;
}

$tResultsStr = "{data:[";

if(!empty($tResults) && is_array($tResults)) {
	foreach($tResults as $k => $tResultsItem) {
		$tResultsStr .= "{
			content: '".esc_html($tResultsItem['content'])."',
			author: '".$tResultsItem['author']."'
		}";
		if($k != ($tCount - 1)) {
			$tResultsStr .= ',';
		}
	}
}

$tResultsStr .= "]}";

//echo $tResultsStr;

$tYears = $wpdb->get_results('SELECT YEAR(created) as ylist FROM wp_vm_testimonials WHERE (status = "A" AND isinpool = "yes")  GROUP BY ylist ORDER BY created DESC', ARRAY_A);
$tYearsCount = $wpdb->num_rows;
$tYearStr = '[';
if(!empty($tYears) && is_array($tYears)) {
	foreach($tYears as $k2 => $tYearsItem) {
		$tYearStr .= '"'.$tYearsItem['ylist'].'"';
		if($k2 != ($tYearsCount - 1)) {
			$tYearStr .= ',';
		}
	}
}
$tYearStr .= ']';


if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}
?>

<div class="row"><div class="col-md-12 aboutbanner"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimonials.png"></div></div> 

<!-- Middle Mennu  -->
        <div class="row menu_pos">
        <div class="leftarrow-inner SlideOff"></div>
             <div class="aboutmenu-container">
                 <?php require_once('middle-menu.php'); ?>
             </div>
    </div>
<!--/  Ends Middle Mennu  -->

<div class="row white-bg">

	<div class="tl-container">
   <div class=" image-container1">
    <div class=" text-align1"> <a class="nav-Prev" width="40" height="40"></a><a class="nav-Next" width="40" height="40"></a>   </div>
  <!-- <div class="col-md-1 col-xs-6 "><a class="nav-Next" width="40" height="40"></a>  </div>   -->
  </div>
<div class="dd-list">
<select id="fat-menu" class="dropdown js_tYearBox" role="menu" aria-labelledby="drop3">
<?php
if(!empty($tYears) && is_array($tYears)) {
	echo '<option value="'.date('Y').'">Year</option>';
	foreach($tYears as $tYearsItem) {
		$selectedStr = '';
		if($tInputYear == $tYearsItem['ylist']) {
			$selectedStr = 'selected="selected"';
		}
		echo '<option value="'.$tYearsItem['ylist'].'" '.$selectedStr.'>'.$tYearsItem['ylist'].'</option>';
	}
}
?>
</select>



</div>

<div class="ts-content">
	 <div id="mcts1"><div><div></div></div></div>       
	<div class="t1-selector"></div>
</div>


<div class=" offset-spacer "><div class="write_testimonials " data-toggle="modal" data-target=".testimonials"> <a> Write your own Testimonial</a></div></div>

  </div>
</div>

<div class="modal fade testimonials content" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content-rac">
  <div class="modal-header-rac">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  </div>
  <div class="modal-body">
    <div class="panel-group" id="accordion">
      <div class="cs-pos-content-heading1"> Submit Your Testimonial </div>

<div class="js_result_tm alert fade in" style="display:none;">
	<button type="button" class="close js_close_tm" aria-hidden="true">×</button>
	<p></p>
</div>
<br />

      <div class="form-group">
        <form name="testimonialFrm" action="" method="post" parsley-validate novalidate>
<?php wp_nonce_field('action_testimonialFrm', 'name_testimonialFrm'); ?>
          <input type="text" name="testimonialFrm_name" class="form-control" maxlength="40" placeholder="Name*" required parsley-error-message="Please enter your name">
          <br>
          <input type="text" name="testimonialFrm_email" class="form-control" maxlength="40" placeholder="Email*" required parsley-error-message="Please enter your email">
          <br>
          <input type="text" name="testimonialFrm_company" class="form-control" maxlength="40" placeholder="Company*" required parsley-error-message="Please enter your Company">
          <br>		  
          <input type="text" name="testimonialFrm_subject" class="form-control" maxlength="45" placeholder="Summary of your Testimonial*" required parsley-error-message="Please enter  subject">
          <br>
         
          <textarea name="testimonialFrm_testimonial" class="form-control" maxlength="500" placeholder="Testimonial*" required parsley-error-message="Please enter testimonial"></textarea>
          <br>
          <div class="row">
            <div class="col-md-6 ">
              <p class="ts-parag-txt-2"> *Mandatory Fields </p>
			  <p class="js_result" style="display:none;"></p>
            </div>
            <div class="col-md-2 col-md-offset-4 ">
              <input type="submit" name="testimonialFrm_submit" value="Submit"  class="btn ts-submit-btn " />
            </div>
          </div>
          <br>
          <br>
          <br>
          <br>
        </form>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
</div></div>

<script type="text/javascript">
$('form[name=testimonialFrm]').on('submit', function() {
	if($('form[name=testimonialFrm]').parsley('isValid')) {
		$.ajax({
			url: '<?php echo $amc_urls['home']; ?>/testimonials/',
			method: 'post',
			data: $('form[name=testimonialFrm]').serialize(),
			success: function(d) {
				var o = $.parseJSON(d);
				/*
				if(o.status == 'error') {
					$('p.js_result').css('color', 'red').text(o.msg).show().fadeOut(2000);
				} else if(o.status == 'success') {
					$('p.js_result').css('color', 'green').text(o.msg).show().fadeOut(2000);
					$('input[type=text], textarea').val('');
				}
				*/

				if(o.status == 'error') {
					$('.js_result_tm').removeClass('alert-warning').addClass('alert-danger').find('p').text(o.msg);
					$('.js_result_tm').show();
				} else if(o.status == 'success') {
					$('.js_result_tm').removeClass('alert-danger').addClass('alert-warning').find('p').text(o.msg);
					$('.js_result_tm').show();
					$('input[type=text], textarea').val('');
				}
			}
		});
	}
	return false;
});

$('button.js_close_tm').on('click', function() {
	$('.js_result_tm').hide();
});
</script>




<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-migrate-1.2.1.js"></script>  
<script  src="<?php echo get_template_directory_uri(); ?>/assets/js/flexcroll.js"></script>
<script type="text/javascript">
var siteURL = '<?php echo $amc_urls['home']; ?>';
var assetURL = '<?php echo get_template_directory_uri(); ?>/assets/';
var WPYearResponse = <?php echo $tYearStr; ?>;
var WPJSONResponse = <?php echo $tResultsStr; ?>;
var testimonialURL = siteURL + '/testimonials/';
</script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/matchMedia.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/testimonials.js"></script>

<script type="text/javascript">
// RWD set width for testimonail container start 
$(document).ready(function() {
   if ($(window).width() < 769) {
   var col = $('#mcts1').find('.item').outerWidth();
   var col_count = $('#mcts1').find('.item').length;
   var tot_size = col * col_count;
   $('.item').parent('div').css('width',tot_size);
   //console.log('res' + tot_size);
  
	}
	
	$('#mcts1 .item:even').addClass('evenclass');
	 $('#mcts1 .item:odd').addClass('oddclass');
	 
	 $(".item.evenclass").each(function(i,d){ var item = $(this).find(".popover"); if(i%2==0){ item.css('background','#525251');item.addClass('ar_grey')}else{item.css('background','#f59c34');item.addClass('ar_orange')}}) ;
	 $(".item.oddclass").each(function(i,d){ var item = $(this).find(".popover"); if(i%2!=0){ item.css('background','#525251');item.addClass('ar_grey')}else{item.css('background','#f59c34');item.addClass('ar_orange')}}) ;
	 
	 //$('#mcts1 .item.evenclass:nth-child(2n+1)').addClass('bgblack');
	 
});
// RWD set width for testimonail container End

$(window).load(function(){
 //$('#mcts1 .item.evenclass:nth-child(2n+1)').addClass('bgblack');

});

$('select.js_tYearBox').change(function() {
	
	window.location = '<?php echo $amc_urls['testimonial'].'?y='; ?>'+$(this).val();	
     
     

 

	$.ajax({
	url: testimonialURL,	
	method: 'get',
	// dataType: "json",
	data: {y: $(this).val()},
	success: function(d) {
		
		
		//toggleScroll(d);
		
	}
});

	if(mq.matches && jsonResponse.data.length==1 ) $( "#mcts1 > div" ).css( "overflow-x" , "hidden"  );	
 if(!mq.matches && jsonResponse.data.length<5) $( "#mcts1 > div" ).css( "overflow-x" , "hidden"  );	
 if(!mq.matches && ( $(window).width() > 1660) && jsonResponse.data.length<7) $( "#mcts1 > div" ).css( "overflow-x" , "hidden"  );

});

$(window).bind('orientationchange', function (e) {
    switch (window.orientation) {
        case 0:
            //portrait mode
			location.reload();
            break;
        case 90:
            //turned to the right
            location.reload();
            break;
        case -90:
            //turned to the left
            location.reload();
            break;
    }
});
</script>

<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
?>