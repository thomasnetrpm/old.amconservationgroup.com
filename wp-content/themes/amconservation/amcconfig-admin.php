<div class="wrap">
	<h2>AMC Configuration</h2>

	<p><a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=amcconfig_mng&action=add" title="Add">Add</a></p>

<?php
if(isset($_SESSION['msg'])) {
	if($_SESSION['msg'] == 'deletesuccess') {
		echo '<p style="color:green;">Deleted successfully.</p>';
	}
	if($_SESSION['msg'] == 'activesuccess') {
		echo '<p style="color:green;">Made active successfully.</p>';
	}
	if($_SESSION['msg'] == 'inactivesuccess') {
		echo '<p style="color:green;">Made inactive successfully.</p>';
	}
	unset($_SESSION['msg']);
}
?>

<?php
global $wpdb;
// $wpdb->show_errors();
// $wpdb->print_error();

//Our class extends the Amcconfig_List_Table class, so we need to make sure that it's there
if(!class_exists('Amcconfig_List_Table')){
	require_once( TEMPLATEPATH . '/classes/amcConfigClass.php' );
}

$action = (isset($_GET['action'])) ? $_GET['action'] : '';

if($action == 'add') {
	// Handle the add screen
	$formErrorAdd = array();
	$formSuccessAdd = array();
	if(isset($_POST['amcconfigAddFrm_submit']) && $_POST['amcconfigAddFrm_submit'] == 'Add') {
		$amcconfigAddFrm_confname = sanitize_text_field($_POST['amcconfigAddFrm_confname']);
		$amcconfigAddFrm_confvalue = sanitize_text_field($_POST['amcconfigAddFrm_confvalue']);

		if(empty($amcconfigAddFrm_confname)) {
			$formErrorAdd[] = 'Please enter the name.';
		}
		if(empty($amcconfigAddFrm_confvalue)) {
			$formErrorAdd[] = 'Please enter the value.';
		}

		if(empty($formErrorAdd)) {
			$amcconfigAddFrm_confslug = str_replace('-','_', sanitize_title($amcconfigAddFrm_confname));

			$wpdb->insert('wp_amcconfig', array(
				'created' => current_time('mysql'), 
				'modified' => current_time('mysql'),
				'confname' => $amcconfigAddFrm_confname,
				'confslug' => $amcconfigAddFrm_confslug,
				'confvalue' => $amcconfigAddFrm_confvalue,
				'to_show' => 1
			));
			$formSuccessAdd[] = 'Added successfully.';
		}
	}
?>
<h3>Add - AMC Configuration</h3>
<form name="amcconfigAddFrm" action="" method="post">
<div>

<?php
if(!empty($formSuccessAdd)) {
	echo '<p style="color:green;">Status follows,</p>';
	echo '<ol>';
	foreach($formSuccessAdd as $formSuccessAddItem) {
		echo '<li>'.$formSuccessAddItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}

if(!empty($formErrorAdd)) {
	echo '<p style="color:red;">The following errors found,</p>';
	echo '<ol>';
	foreach($formErrorAdd as $formErrorAddItem) {
		echo '<li>'.$formErrorAddItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}
?>

<p><label for="amcconfigAddFrm_confname">Name</label><br /> <input type="text" name="amcconfigAddFrm_confname" value="" id="amcconfigAddFrm_confname" /></p>
<p><label for="amcconfigAddFrm_confvalue">Value</label><br /> <textarea name="amcconfigAddFrm_confvalue" id="amcconfigAddFrm_confvalue" rows="5" cols="100"></textarea></p>
<p><input type="submit" name="amcconfigAddFrm_submit" value="Add" /></p>
</div>
</form>
<?php

} else if($action == 'edit') {
	// Handle the edit screen
	$amcconfigId = $_GET['id'];
	$amcconfigRow = $wpdb->get_row('SELECT * FROM wp_amcconfig WHERE id = "'.$amcconfigId.'"');

	$formErrorEdit = array();
	$formSuccessEdit = array();
	if(isset($_POST['acctrepEditFrm_submit']) && $_POST['acctrepEditFrm_submit'] == 'Edit') {
		$amcconfigEditFrm_confvalue = sanitize_text_field($_POST['amcconfigEditFrm_confvalue']);

		if(empty($amcconfigEditFrm_confvalue)) {
			$formErrorEdit[] = 'Please enter the value.';
		}

		if(empty($formErrorEdit)) {
			$wpdb->update('wp_amcconfig', array(
				'modified' => current_time('mysql'),
				'confvalue' => $amcconfigEditFrm_confvalue
			), array(
				'id' => $amcconfigId
			));
			$formSuccessEdit[] = 'Edit successfully.';
			$amcconfigRow = $wpdb->get_row('SELECT * FROM wp_amcconfig WHERE id = "'.$amcconfigId.'"');
		}
	}
?>
<h3>Edit - AMC Configuration</h3>
<form name="acctrepEditFrm" action="" method="post">
<div>

<?php
if(!empty($formSuccessEdit)) {
	echo '<p style="color:green;">Status follows,</p>';
	echo '<ol>';
	foreach($formSuccessEdit as $formSuccessEditItem) {
		echo '<li>'.$formSuccessEditItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}

if(!empty($formErrorEdit)) {
	echo '<p style="color:red;">The following errors found,</p>';
	echo '<ol>';
	foreach($formErrorEdit as $formErrorEditItem) {
		echo '<li>'.$formErrorEditItem.'</li>';
	}
	echo '</ol>';
	echo '<hr />';
}
?>

<p><label for="amcconfigEditFrm_confname">Name</label><br /> <?php echo $amcconfigRow->confname; ?></p>
<p><label for="amcconfigEditFrm_confvalue">Value</label><br /> <textarea name="amcconfigEditFrm_confvalue" id="amcconfigEditFrm_confvalue" rows="5" cols="100"><?php echo $amcconfigRow->confvalue; ?></textarea></p>
<p><input type="submit" name="acctrepEditFrm_submit" value="Edit" /></p>
</div>
</form>
<?php
} else if($action == 'delete') {
	// Handle the delete action
	$acctrepId = $_GET['id'];
	$wpdb->delete('wp_amcconfig', array(
		'id' => $acctrepId
	));

	$_SESSION['msg'] = 'deletesuccess';
	wp_safe_redirect(site_url().'/wp-admin/admin.php?page=amcconfig_mng'); exit;
} else if($action == 'makeactive') {
	// Handle the active action
	$acctrepId = $_GET['id'];
	$wpdb->update('wp_amcconfig', array(
		'is_active' => 1
	), array(
		'id' => $acctrepId
	));

	$_SESSION['msg'] = 'activesuccess';
	wp_safe_redirect(site_url().'/wp-admin/admin.php?page=amcconfig_mng'); exit;
} else if($action == 'makeinactive') {
	// Handle the inactive action
	$acctrepId = $_GET['id'];
	$wpdb->update('wp_amcconfig', array(
		'is_active' => 0
	), array(
		'id' => $amcconfigId
	));

	$_SESSION['msg'] = 'inactivesuccess';
	wp_safe_redirect(site_url().'/wp-admin/admin.php?page=amcconfig_mng'); exit;
} else {
	//Prepare Table of elements
	$amcconfig_list_table = new Amcconfig_List_Table();
	$amcconfig_list_table->prepare_items();
	//Table of elements
	$amcconfig_list_table->display();
}
?>

</div>