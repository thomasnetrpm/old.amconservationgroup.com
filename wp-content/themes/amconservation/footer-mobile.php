</div>
</div>
<!-- Google Analytics Tracking Code -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6700265-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
  
<!-- footer starts-->
<div class="container-foot page-footer">
	<div class="footlinetop horizontal-line-divider homfoot"></div> 
	<!--footer shadow -->
	<?php global $theme_options, $amc_urls; ?>
	<!-- footer top bar starts-->
	<div class="row page-footer-top">
		<div class="foot-top">
			<!--footer top left starts-->
			<div class="col-md-6 foot-top-left">
				<ul class="foot-social">
					<li><a href="<?php echo $amc_urls['pinterest']; ?>" target="_blank" class="pin-icon"></a></li>
					<li><a href="<?php echo $amc_urls['twitter']; ?>" target="_blank" class="twi-icon"></a></li>
					<li><a href="<?php echo $amc_urls['facebook']; ?>" target="_blank" class="fb-icon"></a></li>
				</ul>
				<!--horizontal line-->
				<div class="row">
				  <div class="footeline2 horizontal-line-divider"></div>    
				</div> 
				<!--horizontal line ends-->
				<ul class="foot-contact">
					<li><a href="tel:<?php  echo $amc_urls["contact_number"]; ?>"><span class="f-cont-icon"></span><?php echo $amc_urls['contact_number']; ?></a></li>
					<li><a href="<?php echo $amc_urls['request_a_catalog']; ?>"><span class="f-quote-icon"></span>Request a Catalog</a></li>
				</ul>
			</div>
			<!--footer top left ends-->
			
			<!--footer top right starts-->
			<?php require_once('newsletter-box.php'); ?>
				
			<!--footer top right ends-->
		</div>
	</div>
	<!-- footer top bar ends -->
	<!-- footer bottom bar starts-->
	<div class="row page-footer-bottom">
		<div class="foot-bottom">
			<!--footer bottom left starts-->
			<div class="col-md-9 foot-bot-left">
			   <div class="footermenu-container"><a href="#" class="collapsablefooterMenu"> <span class="top-icon4"></span> <span class="toplink4">Menu</span> </a></div>
				<?php
				wp_nav_menu(array(
					'theme_location' => 'footer-menu',
					'after'  => '',
					'container_class' => '',
					'container_id'    => '',
					'menu_class'      => 'footer-sub-menu1',
					'menu_id'         => ''
				));
				?>
				<script type="text/javascript">
				if($('div.headersession').length > 1) {
					 $('div.headersession.head-com').remove();
				}
				
				// jQuery('div.menu-primary_footer_menu-container')
				jQuery('ul.footer-sub-menu1 li a').removeClass().addClass('submenu-btn');
				jQuery('<li> <a class="footer-subBack-btn" href="#">Back</a> </li>').prependTo('ul.footer-sub-menu1');
				<?php if(userIsLoggedIn()) { ?>
				jQuery('ul.footer-sub-menu1 li').first().append('<li class="submenu-btn"><a href="<?php echo $amc_urls['image_gallery']; ?>">Image Gallery</a></li>');
				<?php } ?>
				</script>
			</div>
		
			<!--footer bottom left ends-->
			<!--footer bottom right starts-->
			<div class="col-md-3 foot-bot-right">
				<p class="foot-copy">Copyright <?php echo date("Y"); ?> AM Conservation Group</p>
			</div>
			<!--footer bottom right ends-->
		</div>
	</div>
	<!-- footer bottom bar ends-->
<!-- footer ends-->
</div>	


<script type="text/javascript">
      var signTpl='<input type="text" class="search-form-field" placeholder="TYPE HERE TO SEARCH">'
      $('.mainmenu-icon6').attr("data-content",signTpl)
      $('.mainmenu-icon6').popover({html : true,placement:"left"});
      $('.mainmenu-icon6').on('shown.bs.popover', function () {
          var search_xpos=$(".search").offset().left;
          var pop_wd=$(".popover").offset().left+$(".popover").width();
          var search_coeff=pop_wd-search_xpos;
          //alert("_xpos "+_xpos);
          // console.log(-(search_xpos-27)+" pop_wd "+search_coeff);
          $(".popover").css("left",-(search_xpos)+"px");
          $(".popover-content").css("width",search_xpos+"px");
          $(".arrow").css("left", $(".popover").width()+10+"px");
      })
    </script>
<script type="text/javascript">
function goback() {
    history.go(-1);
}
// Handle the search click
		$('.js_handleSearch').on('click', function() {
			if($('input[name=s].js_searchBox').val() != undefined && $('input[name=s].js_searchBox').val() != '' && ($('input[name=s].js_searchBox').val() != '<?php echo get_search_query(); ?>')) {
				$('form[name=headerSearchFrm]').submit();
			}
		});
		//top search
		$(document).ready(function() {
			$('.top-search.search').click(function(){
				$(this).find('.search-arrow').fadeToggle("fast");
				$(this).next('.search-panel').fadeToggle("fast");
			});
			
		});

/*var collapsablefooterMenu = $(".footer-sub-menu1").width();*/
var flag = false;

var sHt = 0;
$(window).load(function () {
	resizeTable();
    $('#homeslider').flexslider({
        animation: "slide",
        controlNav: true,
        prevText: "",
        nextText: "",
		animationSpeed: 200,
        touch: true,
		after: function(slider){ 
			slider.pause(); 
			slider.play(); 
		},
		pauseOnAction : false
    });
	$('#secondary-slider').flexslider({
        animation: "slide",
        controlNav: true,
        prevText: "",
        nextText: "",
		animationSpeed: 200,
        touch: true,
		after: function(slider){ 
			slider.pause(); 
			slider.play(); 
		},
		pauseOnAction : false
    });
	
	setTimeout(function () {
       		paraHeight();
    }, 500);
	
	
    var toolTp = 'You will be redirected to our Simply Conserve shopping site where our products are available for purchase.'
    $(".home").attr("data-title", toolTp)
    $(".home").tooltip({
        html: true,
        placement: "bottom"
    });

    $(".collapsableMenu").click(function () {
        $(".mainnav-home").toggle();
		return false;
    });
	
    /*$(".energymenu .col-xs-2").click(function () {
        e.stopPropagation();
		$(this).children("ul").removeClass('menu-off');
        $(this).children("ul").addClass('menu-on')
		
		return false;
    });*/
	$('.midmenu-Back-btn').removeAttr('href');
	
	$('.midmenu-Back-btn').click(function()
	{
		$(this).closest("ul").removeClass('menu-on');
		$(this).closest('ul').addClass('menu-off');
		return false;
	});

	
	$(".subBack-btn").click(function (e) {
        //console.log($($(this).parent().parent()).attr("class"))
        // e.stopPropagation();
        $($(this).parent().parent()).css("left", "100%");
		return false;
    });

    $(".collapsablefooterMenu").on('click', function () {
		$(".footer-sub-menu1").removeClass('f-menu-off');
        $(".footer-sub-menu1").addClass('f-menu-on');
		return false;
    });

    $(".footer-subBack-btn").on('click', function (e) {
        // e.stopPropagation();
		$(".footer-sub-menu1").removeClass('f-menu-on');
        $(".footer-sub-menu1").addClass('f-menu-off');
		return false;
    });


    /*$(".menu1").hover(function () {
        $(".top-icon").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat -3.567% 86.273%");
        $(".toplink1").css("color", "#f4911e");
    }, function () {
        $(".top-icon").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat -3.567% 33.273%");
        $(".toplink1").css("color", "#525151");
    });

    $(".menu2").hover(function () {
        $(".top-icon2").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 8.433% 86.273%");
        $(".toplink2").css("color", "#f4911e");
    }, function () {
        $(".top-icon2").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 8.433% 33.273%");
        $(".toplink2").css("color", "#525151");
    });

    $(".menu3").hover(function () {
        $(".top-icon3").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 18.433% 86.273%");
        $(".toplink3").css("color", "#f4911e");
    }, function () {
        $(".top-icon3").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 18.433% 33.273%");
        $(".toplink3").css("color", "#525151");
    });*/

    $(".search-btn").hover(function () {
        $(".mainmenu-icon6").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 77.433% 90.273%");
    }, function () {
        $(".mainmenu-icon6").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 99.433% 50.273%");
    });

    $(".phonelink").hover(function () {
        $(".bottom-icon1").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 42.433% 88.273%");
        $(".toplink4").css("color", "#f4911e");
    }, function () {
        $(".bottom-icon1").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 31.433% 35.273%");
        $(".toplink4").css("color", "#525151");
    });

    $(".home").hover(function () {

        $(".arrowicon1").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 62.273%");
        $(".btn-default.home").css("background", "#fff");
        $(".btn.btn-default.home:before").css("display", "block")
    }, function () {
        $(".arrowicon1").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 36.273%");
        $(".btn-default.home").css("background", "#525151");
        $(".btn.btn-default.home:before").css("display", "none")
    });

    $(".professional").hover(function () {
        $(".arrowicon2").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 62.273%");
        $(".professional").css("background", "#fff");
    }, function () {
        $(".arrowicon2").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 36.273%");
        $(".professional").css("background", "#525151");
    });

    $(".cataloglink").hover(function () {
        $(".bottom-icon2").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 31.38% 87.273%");
        $(".toplink5").css("color", "#f4911e");
    }, function () {
        $(".bottom-icon2").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 42.433% 34.273%");
        $(".toplink5").css("color", "#525151");
    });

    $(".newsletter-btn").hover(function () {
        $(".arrowbtn").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 92.273%");
    }, function () {
        $(".arrowbtn").css("background", "url('<?php echo get_template_directory_uri(); ?>/assets/images/spritesheet.png') no-repeat 86.433% 35.273%");

    });
});




function Parallax() {
    var jarallax = new Jarallax();
    jarallax.addAnimation('#px-Program', [{
        progress: '0%',
        top: '600px'
    }, {
        progress: '100%',
        top: '-350px'
    }])
    jarallax.addAnimation('#px-Testimonial', [{
        progress: '0%',
        top: '1200px'
    }, {
        progress: '100%',
        top: '-350px'
    }])
    jarallax.addAnimation('#px-Talktous', [{
        progress: '0%',
        top: '1800px'
    }, {
        progress: '100%',
        top: '-350px'
    }])
    jarallax.addAnimation('.px-ImageBg', [{
        progress: '0%',
        top: '0px'
    }, {
        progress: '100%',
        top: '-350px'
    }])
}


$(window).resize(function () {
    
	
       		setTimeout(function () {
       		paraHeight();
    }, 500);
    
});

$(document).ready(function () {
    $(".menu-container").scrollLeft();
	
    $(".leftarrow").click(function () {

        $(this).toggleClass("SlideOff");
        $(this).toggleClass("SlideOnn");
        $(this).parent('.product-heading-container').toggleClass("SlideOnn");
        if ($(this).hasClass("SlideOnn") == true) {
            $(".menu-container").animate({
                scrollLeft: 1000
            }, 1000);;
            //alert("yes");
        } else {
            $(".menu-container").animate({
                scrollLeft: 0
            }, 1000);;
            //alert("no");
        }
    });
});


$(document).ready(function () {
  $(".aboutmenu-container").scrollLeft();
    $(".leftarrow-inner").click(function () {
        $(this).toggleClass("SlideOff");
        $(this).toggleClass("SlideOnn");
        $(this).parent('.product-heading-container').toggleClass("SlideOnn");
        if ($(this).hasClass("SlideOnn") == true) {
            $(".aboutmenu-container").animate({
                scrollLeft: 1000
            }, 1000);;
            //alert("yes");
        } else {
            $(".aboutmenu-container").animate({
                scrollLeft: 0
            }, 1000);;
            //alert("no");
        }

    });
});



function paraHeight() {

    var screenWidth = $(window).width();
    $('.flexslider ul.slides li').each(function () {
        
        var liHeight = $(this).height();
        var circleHeight = $(this).find('.text_bg').height();
        var reqcirclePosition = liHeight - circleHeight;
        
            reqcirclePosition = (reqcirclePosition / 2);
        
        $(this).find('.text_bg').css('margin-top', reqcirclePosition + 'px')
        var pHeight = $(this).find('p').height();
        var reqHeight = circleHeight - pHeight;
        $(this).find('p').css('margin-top', reqHeight/2 + 'px')
    })
}

// For listing page only  //
function resizeTable() {
	$('.table-prddisplay').each(function () {
		$(this).find('tr:odd').addClass("odd");
		$(this).find('tr:even').addClass("even");
		var columnCount = $(this).attr('data-val');
		var totcountWidth = parseInt(65/columnCount);
		$(this).find('td').css('width',totcountWidth + '%');
	});

	$(".table-prddisplay > tbody > tr > td:first-child, .table-prddisplay > thead > tr > th:first-child").addClass("first");
	$(".table-prddisplay > tbody > tr > td:last-child, .table-prddisplay > thead > tr > th:last-child").addClass("last");
}


      </script>
</body>
</html>