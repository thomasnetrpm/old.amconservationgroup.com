<?php
/* 
Template Name: Forgot reset password template 
*/

global $wpdb, $amc_urls, $show_msg, $success_msg, $error_msg, $success_msg2, $error_msg2;

$uId = sanitize_text_field($_GET['uid']);
$uFwdKey = sanitize_text_field($_GET['fwd_key']);

if(isset($uId) && !empty($uId) && isset($uFwdKey) && !empty($uFwdKey)) {
	$result = $wpdb->get_row('SELECT * FROM wp_customers WHERE id = "'.$uId.'" AND fwd_key = "'.$uFwdKey.'"');
	if(!empty($result)) {

// Starting of the template
define("CSS", "reset_password.css");
redirectLoggedInUser();

$show_msg = 'none';
$show_msg2 = 'none';

// Handle the form submission
$form_errors2 = array();
$login_errors = array();
// $v = wp_check_password('1234567x', '$P$BZSZoCFqYk5UFs9ZQ1Z6Z3WanejfQJ0');

if(isset($_POST['userFgtRstPwdForm_submit']) && ($_POST['userFgtRstPwdForm_submit'] == 'Submit') && isset($_POST['name_userFgtRstPwdForm']) && wp_verify_nonce($_POST['name_userFgtRstPwdForm'],'action_userFgtRstPwdForm')) {
	// echo '<pre>'; print_r($_POST); echo '</pre>';

	// verify the id/key again
	$uId = sanitize_text_field($_POST['userFgtRstPwdForm_uid']);
	$uFwdKey = sanitize_text_field($_POST['userFgtRstPwdForm_ufwdkey']);
	$result = $wpdb->get_row('SELECT * FROM wp_customers WHERE id = "'.$uId.'" AND fwd_key = "'.$uFwdKey.'"');
	if(empty($result)) {
		$form_errors2['userFgtRstPwdForm_auth'] = 'Invalid request';
	}

	if(sanitize_text_field($_POST['userFgtRstPwdForm_newpassword']) == '') {
		$form_errors2['userFgtRstPwdForm_newpassword'] = 'Please enter password';
	}

	if(sanitize_text_field($_POST['userFgtRstPwdForm_cnewpassword']) == '') {
		$form_errors2['userFgtRstPwdForm_cnewpassword'] = 'Please enter confirm password';
	}

	if(sanitize_text_field($_POST['userFgtRstPwdForm_newpassword']) != sanitize_text_field($_POST['userFgtRstPwdForm_cnewpassword'])) {
		$form_errors2['userFgtRstPwdForm_newpassword'] = 'Your password, confirm password doesn\'t match.';
	}

	if(empty($form_errors2)) {
		// If everything is Ok, then update the password
		$newPwd = sanitize_text_field($_POST['userFgtRstPwdForm_newpassword']);
		$hashNewPwd = wp_hash_password($newPwd);

		$result = $wpdb->update('wp_customers', array(
			'password' => $hashNewPwd,
			'fwd_key' => ''
		), array(
			'id' => $uId,
			'fwd_key' => $uFwdKey
		));

		$show_msg2 = 'block';
		$success_msg2 = 'Your password has been set. Please click <a href="'.$amc_urls['register_login'].'"><strong>here</strong></a> to login.';
	}
}

if(is_mobile()) {
	get_header('mobile');
} else {
	get_header();
}
?>
<div class="row bgfill ">
    <div class=" topspacer">
      <div class="message-form-container">
        <div class="content">
          <div class="col-md-4 col-md-offset-4">
            <div class="form-group">
            
<?php
$alertClass = 'alert-warning';
if(!empty($error_msg2)) {
	$alertClass = 'alert-danger';
}

if($show_msg2 != 'none') {
?>

<div class="js_result alert <?php echo $alertClass; ?> fade in">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<?php
if(!empty($success_msg2)) {
	echo $success_msg2;
} else if(!empty($error_msg2)) {
	echo $error_msg2;
} else if(!empty($form_errors2)) {
	echo implode('<br />', $form_errors2);
}
?>
</div>

<?php
}
?>

<form id="form1" role="form" method="post" action="" name="userFgtRstPwdForm" parsley-validate novalidate>
<?php wp_nonce_field('action_userFgtRstPwdForm', 'name_userFgtRstPwdForm'); ?>

<input type="password" name="userFgtRstPwdForm_newpassword" id="password" class="form-control" placeholder="New Password" required parsley-error-message="Please enter your password"><br>

<input type="password" name ="userFgtRstPwdForm_cnewpassword" id="confirm-pwd" class="form-control" placeholder="Confirm New Password"  required parsley-error-message="Please confirm your password" parsley-equalto="#password"><br>

<input type="hidden" name="userFgtRstPwdForm_uid" value="<?php echo $uId; ?>" />
<input type="hidden" name="userFgtRstPwdForm_ufwdkey" value="<?php echo $uFwdKey; ?>" />

<div class="submit-container"><div class="offset-top-1"><div><input type="submit" value="Submit" class="btn fp-submit-btn " name="userFgtRstPwdForm_submit" /></div></div></div>
</form>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
if(is_mobile()) {
	get_footer('mobile');
} else {
	get_footer();
}
// Ending of the template
	} else {
		wp_safe_redirect(get_bloginfo('url'));
	}
} else {
	wp_safe_redirect(get_bloginfo('url'));
}
exit();